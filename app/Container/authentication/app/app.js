﻿//# sourceURL=app.js

var app =  angular.module("authenticationApp", [
    "ngCookies",
    "ngRoute",
    "ngSanitize",
    "ngCookies"
  ]);

  app.config(["$routeProvider", "$locationProvider", "$httpProvider", "$sceDelegateProvider", "$provide",
    function ($routeProvider, $locationProvider, $httpProvider, $sceDelegateProvider, $provide) {

        //Enable CORS
        $sceDelegateProvider.resourceUrlWhitelist([
                 "self",
                 "**"]);

        // configure html5 to get links working on jsfiddle
        //$locationProvider.html5Mode(true);

        //Enable cross domain calls
        $httpProvider.defaults.useXDomain = true;

        //Remove the header used to identify ajax call  that would prevent CORS from working
        delete $httpProvider.defaults.headers.common["X-Requested-With"];

        app.register =
            {
                factory: $provide.factory,
                service: $provide.service,
                constant: $provide.constant,
                decorator: $provide.decorator
            };

        app.register.service("Session", sessionService);
        app.register.service("LoggingSrvc", loggingService);

        //Overriding $log service
        app.register.decorator("$log", ["$delegate", "LoggingSrvc", function ($delegate, LoggingSrvc) {
            return LoggingSrvc($delegate);
        }]);

        $routeProvider
          .when("/", {
              templateUrl: "authentication/app/views/login.html",
              controller: "AuthenticationCtrl"
          })
          .otherwise({
              redirectTo: "/"
          });
    }]);

