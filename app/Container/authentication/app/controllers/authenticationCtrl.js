﻿//# sourceURL=authenticationCtrl.js
"use strict";

angular
  .module("authenticationApp")
  .controller("AuthenticationCtrl", ["$scope", "$cookies", "$timeout", "AuthenticationSrvc", "Session", function ($scope, $cookies, $timeout, AuthenticationSrvc, Session) {
      $scope.user = { userName: null, password: null };
      $scope.submitted = false;

      $scope.login = function () {
          $scope.submitted = true;
          if ($scope.loginForm.$valid == true) {
              showBusy();
              try{
                  AuthenticationSrvc.login($scope.user).then(function () {
                      if (Session.sessionData != null && Session.sessionData != undefined) {
                          $scope.Session = Session;
                          loadConfiguration();
                      } else {
                          $scope.loginForm.$invalid = true;
                          $scope.user.password = "";
                          hideBusy();
                          showErrorMessage();
                      }
                  });
              } catch (error) {
                  hideBusy();
              }
          }
          else {
              showErrorMessage();
          }
      };

      $scope.key = function($event) {
          if ($event.keyCode == 13) {
              $scope.login();
          }
      }

      function showErrorMessage(msg, title)
      {
          var msg = "Intente de nuevo";
          var title = "Credenciales invalidas";
          toastr.error(msg, title);
      }

      function getPreferedApp(applications) {
          var preferedApp = 0;

          for (var i = 0; i < applications.Applications.length; i++) {
              var isPreferred = applications.Applications[i].ApplicationId == applications.PreferredApplication;
              if (isPreferred) preferedApp = i;
              break;
          }

          return preferedApp;
      }

      function loadConfiguration() {
          AuthenticationSrvc.getConfiguration().then(function (data) {
              $cookies.AppsData = JSON.stringify(data);
              $timeout(function () {
                  var cookieData = getCookieData("AppsData");
                  var preferredApp = getPreferedApp(cookieData);
                  redirectToApp(cookieData.Applications[preferredApp].AppId);
                  hideBusy();
              }, 100);
          });
      };
}]);