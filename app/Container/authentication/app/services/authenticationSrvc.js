﻿//# sourceURL=authenticationSrvc.js
"use strict";

angular.module("authenticationApp")
  .service("AuthenticationSrvc", ["$http", "$q", "Session", "CryptoSrvc", "$location"
      , function AuthenticationSrvc($http, $q, Session, CryptoSrvc, $location) {

      function isValidString(value)
      {
          return value !== null && value !== undefined && value !== "";
      }

      function isValidUser(user)
      {
          return user != null && user !== undefined && isValidString(user.userName) && isValidString(user.password);
      }

      function encrypt(user) {
          var key = CryptoSrvc.enc.Utf8.parse("AMINHAKEYTEM32NYTES1234567891234");
          var iv = CryptoSrvc.enc.Utf8.parse("7061737323313233");
          var encryptedCredentials = CryptoSrvc.AES.encrypt(
              CryptoSrvc.enc.Utf8.parse(user.userName + "|:|" + user.password),
                    "AgmasterPass", key,
                    {
                        keySize: 128,
                        iv: iv,
                        mode: CryptoSrvc.mode.CBC,
                        padding: CryptoSrvc.pad.Pkcs7
                    });
          return encodeURIComponent(encryptedCredentials);
      }

      function getToken(credentials) {
          var url = configuration.serviceBaseAddress + "/Login";
          var deferred = $q.defer();

          $http({
              method: "POST",
              url: url,
              data: JSON.stringify(credentials),
              contentType: "application/json; charset=utf-8",
              dataType: "json",
              async: false
          }).success(function (data, status, headers, cfg) {
              deferred.resolve(data);
          }).error(function (err, status) {
              deferred.reject(status);
          });

          return deferred.promise;
      }

      function login(user) {
          if (isValidUser(user)) {
              var encryptedCredentials = encrypt(user);
              return getToken(encryptedCredentials).then(function (data) {
                  if (data != undefined && data != null) {
                      Session.create(data.UserId, user.userName, data.Role, data.AgencyId, data.Token);
                  }
              });
          }
          return null;
      }

      function getConfiguration() {
          var url = configuration.serviceBaseAddress + "/configuration";
          var deferred = $q.defer();

          $http({
              method: "GET",
              url: url,
              headers: Session.getHeaders()
          }).success(function (data, status, headers, cfg) {
              deferred.resolve(data);
          }).error(function (err, status) {
              deferred.reject(status);
          });

          return deferred.promise;
      }

      function logout() {
          Session.destroy();
      }

      var service = {
          login: login,
          logout: logout,
          getConfiguration: getConfiguration
      };

      return service;
  }]);
