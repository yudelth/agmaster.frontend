﻿/*-------------------------------------------
	Main scripts used by theme
---------------------------------------------*/

/**************************    LOAD JS CONTENT    **********************************/

function bootstrapAngular(app) {
    var selector = "#" + app;
    var element = $(selector);
    angular.bootstrap(element, [app]);
}

function redirectToApp(app) {
    $("#ajax-content").empty();

    var url = $.url().attr('path') + (isValidString(app) ? "?app=" + app : "");
    window.location = url;
}

function getAppPath(app) {
    if (isValidString(app))
        return "/Modules/" + app + '/app/';

    return "/Container/authentication/app/";
}

//
//  Function for load content from url and put in $('.ajax-content') block
//
function loadAjaxContent(appName) {
    showBusy();
    var path = getAppPath(appName);
    var url = path + 'main.html';
    $(".preloader").show();
    $.ajax({
        mimeType: "text/html; charset=utf-8", // ! Need set mimeType only when run from local file
        url: url,
        type: "GET",
        cache: false,
        success: function (data) {
            var content = $("<div>" + data + "</div>");
            $("#ajax-content").html(content);
            $(".preloader").hide();
            var app = getAngularAppName();
            bootstrapAngular(app);
            hideBusy();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            hideBusy();
            alert(errorThrown);
        },
        dataType: "html",
        async: false
    });
}

function replaceScripts(path, content) {
    var elements = content.find('script:not([src^="http"])');
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).attr("src", location.origin + path + $(elements[i]).attr("src"));
    };
}

function replaceStyles(path, content) {
    var elements = content.find('link:not([href^="http"])');
    for (var i = 0; i < elements.length; i++) {
        $(elements[i]).attr("href", location.origin + path + $(elements[i]).attr("href"));
    };
}

/**************************    END LOAD JS CONTENT    **********************************/

/**************************    LOGIN    **********************************/

function loadLoginPage() {
    saveLoginUrl();
    $("#ajax-content").empty();
    loadAjaxContent("");
}

function getPreferedApp(applications) {
    var preferedApp = 0;

    for (var i = 0; i < applications.Applications.length; i++) {
        var isPreferred = applications.Applications[i].ApplicationId === applications.PreferredApplication;
        if (isPreferred) preferedApp = i;
        break;
    }

    return preferedApp;
}

function getCookieData(cookieName) {
    var cookieData = $.cookie(cookieName);
    return cookieData === null || cookieData === undefined ? null : JSON.parse(cookieData);
}

function saveLoginUrl() {
    var url = window.location;
    $.cookie("Login", url);
}

function containerLogout() {
    $.removeCookie("SessionToken");
    $.removeCookie("SessionData");
    $.removeCookie("AppsData");
    var loginUrl = $.cookie("Login");
    $.removeCookie("Login");
    $("#dynamicMenu").html("");
    showLogout(loginUrl);
}

function isValidString(text)
{
    return text !== undefined && text !== null && text !== "";
}

function showLogin()
{
    var sessionData = getCookieData("SessionData");
    $("#userNamePH").html('<i class="fa fa-user"></i>&nbsp;&nbsp;' + sessionData.userName + '&nbsp;&nbsp;<b class="caret"></b>');
    $("#loginMenu").addClass("hidden");
    $("#profileMenu").removeClass("hidden");
}

function showLogout(loginUrl) {
    $("#profileMenu").addClass("hidden");
    $("#loginMenu").removeClass("hidden");
    window.location = loginUrl;
}

/**************************    END LOGIN    **********************************/

$(function () {
    var app = $.url().param("app");

    if (!isValidString(app))
        loadLoginPage();
    else {
        loadDynamicMenu();
        loadAjaxContent(app);
    }
});

function loadDynamicMenu() {
    var applications = getCookieData("AppsData");

    var dynamicMenu = buildDynamicMenu(applications);
    $("#dynamicMenu").html(dynamicMenu.content);
    showLogin();
}

function getMenuItem(application, isPreferred)
{
    return '<li class="dropdown user-dropdown' + (isPreferred ? ' selected-menu' : '') + '">'
           + '<a onclick="loadContent(this, ' + "'" + application.AppId + "')" + '"><i class="' + application.Icon
           +'"></i>&nbsp;&nbsp;' + application.Name + '</a></li>';
}

function buildDynamicMenu(applications)
{
    var content = '<ul class="nav navbar-nav navbar-left navbar-user">';
    var preferedApp = 0;

    for (var i = 0; i < applications.Applications.length; i++) {
        var isPreferred = applications.Applications[i].ApplicationId == applications.PreferredApplication;
        content += getMenuItem(applications.Applications[i], isPreferred);
        if (isPreferred) preferedApp = i;
    }

    content += "</ul>";

    return {content:content, preferedApp:preferedApp};
}

function loadContent(element, path)
{
    $(".selected-menu").removeClass("selected-menu");
    $(element).addClass("selected-menu");
    redirectToApp(path);
}

function showBusy()
{
    $("#loadingAnimation").removeClass("hidden");
}

function hideBusy()
{
    $("#loadingAnimation").addClass("hidden");
}
