//# sourceURL=LoggingService.js

var loggingService = function ($window) {

    function getHeaders() {
        var sessionToken = $.cookie("SessionToken");

        if (sessionToken != undefined && sessionToken != null) {
            return { 'Authorization': "Bearer " + sessionToken };
        }
        return "";
    }

    function getLogMessage(elements) {
        if (typeof elements[0] == "string")
            return elements[0];

        return elements[0].message;
    };

    function remoteLog(elements, type) {
        try {
            var logMessageString = getLogMessage(elements);
            var stackTrace = type === "error" ? printStackTrace({ e: elements[0] }).join() : "";

            var logMessage = {
                Url: $window.location.href,
                Message: logMessageString,
                Type: type,
                StackTrace: stackTrace,
                AppName: getAngularAppName()
            };

            $.ajax({
                type: "POST",
                url: configuration.serviceBaseAddress + "/Logger",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(logMessage),
                headers: getHeaders()
            });
        } catch (loggingError) {
            console.log("Error server-side logging failed");
            console.log(loggingError);
        }
    };

    return function($delegate) {
        return {
            log: function() {
                $delegate.log(arguments);
                remoteLog(arguments, "log");
            },

            info: function() {
                $delegate.info(arguments);
                remoteLog(arguments, 'info');
            },

            error: function () {
                toastr.error("Por favor, intente de nuevo mas tarde", "Error");
                hideBusy();
                $delegate.error(arguments);
                remoteLog(arguments, "error");
            },

            warn: function() {
                $delegate.warn(arguments);
                remoteLog(arguments, "warn");
            }
        };
    };
}