﻿//# sourceURL=sessionService.js

  var sessionService = function ($cookies) {
    this.sessionData = null;

    this.create = function (userId, userName, role, agencyId, token) {
        if (isValidSessionString(token)) {
            this.sessionData = {
                userId: userId,
                userName: userName,
                role: role,
                agencyId: agencyId,
                token: token
            };
            createSessionCookie(this.sessionData);
        }
    };

    this.destroy = function () {
        this.sessionData = null;
        $cookies.SessionToken = undefined;
        $cookies.SessionData = undefined;
    };

    this.join = function () {
        var sessionToken = $cookies.SessionToken;
        var sessionData = JSON.parse($cookies.SessionData);
        this.sessionData = {
            userId: sessionData.userId,
            userName: sessionData.userName,
            role: sessionData.role,
            agencyId: sessionData.agencyId,
            token: sessionToken
        }
    }

    this.getHeaders = function () {
        if (this.sessionData != undefined && this.sessionData != null) {
            return { 'Authorization': 'Bearer ' + this.sessionData.token };
        }
        return '';
    }

    function isValidSessionString(text) {
        return text != undefined && text != null && text != "";
    }

    function createSessionCookie(sessionData) {
        $cookies.SessionToken = sessionData.token;
        var data = JSON.stringify({
            userId: sessionData.userId,
            userName: sessionData.userName,
            role: sessionData.role,
            agencyId: sessionData.agencyId
        });
        $cookies.SessionData = data;
    }

    return this;
  }