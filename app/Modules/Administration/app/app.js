﻿//# sourceURL=app.js

var app = angular.module("administrationApp", [
    "ngCookies",
    "ngRoute",
    "ngSanitize",
    "xeditable"
]);
app.config(["$routeProvider", "$locationProvider", "$httpProvider", "$sceDelegateProvider", "$provide",
    function ($routeProvider, $locationProvider, $httpProvider, $sceDelegateProvider, $provide) {

        //Enable CORS
        $sceDelegateProvider.resourceUrlWhitelist([
                 "self",
                 "**"]);

        // configure html5 to get links working on jsfiddle
        //$locationProvider.html5Mode(true);

        //Enable cross domain calls
        $httpProvider.defaults.useXDomain = true;

        //Remove the header used to identify ajax call  that would prevent CORS from working
        delete $httpProvider.defaults.headers.common["X-Requested-With"];

        app.register =
           {
               factory: $provide.factory,
               service: $provide.service,
               constant: $provide.constant,
               decorator: $provide.decorator
           };

        app.register.service("session", sessionService);
        app.register.service('LoggingSrvc', loggingService);

        //Overriding $log service
        app.register.decorator("$log", ["$delegate", "LoggingSrvc", function ($delegate, LoggingSrvc) {
            return LoggingSrvc($delegate);
        }]);

        $routeProvider
          .when("/password", {
              templateUrl: "../Modules/Administration/app/views/password.html",
              controller: ""
          })
            .when("/users", {
                templateUrl: "../Modules/Administration/app/views/users.html",
                controller: ""
            })
          .otherwise({
              redirectTo: "/password"
          });

        $provide.factory("administrationHttpInterceptor", function ($q) {
            return {

                'request': function (config) {
                    $("#loadingAnimation").removeClass("hidden").fadeIn(400);
                    return config;
                },

                'requestError': function (rejection) {
                    return $q.reject(rejection);
                },

                'response': function (response) {
                    $("#loadingAnimation").addClass("hidden").fadeOut(400);
                    return response;
                },

                'responseError': function (rejection) {
                    $("#loadingAnimation").addClass("hidden").fadeOut(400);
                    if (rejection.status === 440) {
                        toastr.error("Intente de nuevo", "Su sesion ha expirado!");
                        setTimeout(function () { containerLogout(); }, 2000);
                    }
                    return $q.reject(rejection);
                }
            };
        });

        $httpProvider.interceptors.push("administrationHttpInterceptor");
    }]);
app.run(function(editableOptions) {
    editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
});