﻿//# sourceURL=passwordController.js
"use strict";

(function () {
    var passwordController = function ($scope, usersService, sessionService) {

        $scope.init = function () {
            $scope.securityFormSubmitted = false;
            sessionService.join();

			$scope.user = {userName: sessionService.sessionData.userName, password: '', newPassword: '', newPasswordConfirm: ''};
            $scope.message = {message: '', showMessage: false, isMessageError: false};

            $scope.setupAccess();
        };

        $scope.changePassword = function(form){

            $scope.displayMessage("", false);
            form.securityFormSubmitted = true;
  
            if (form.$valid) {  

                if ($scope.user.newPassword != $scope.user.newPasswordConfirm) {
                    $scope.displayMessage("Verifique nueva contrasena", true);
                    return;
                }

            	usersService.changePassword($scope.user).then(function(data){
                    if (data === undefined) {
                        $scope.displayMessage("Ha ocurrido un error, intentelo de nuevo", true);                        
                    } else if (data.Code == 0){
                        $scope.displayMessage("Contrasena actualizada", false);
                        $scope.clearAll(form);
                    } else if (data.Code == 1 && data.Message == "Invalid credentials") {
                        $scope.displayMessage("Informacion invalida", true);
                    } else if (data.Code == 1 && data.Message == "Invalid current password") {
                        $scope.displayMessage("Contrasena incorrecta", true);
                    } else if (data.Code == 1 && data.Message == "Invalid new password") {
                        $scope.displayMessage("Contrasena nueva invalida", true);
                    } else if (data.Code == 1 && data.Message == "Error updating user") {
                        $scope.displayMessage("Error actualizando la information", true);
                    } else {
                        $scope.displayMessage(data.Message, true);
                    }
                });     
            } else {
                $scope.displayMessage("Por favor entre todo los valores", true);
            }
        }
        
        $scope.displayMessage = function(message, isError) {
            if (message == "") {
                $scope.message.message = "";
                $scope.message.showMessage = false;
                $scope.message.class = "";
            } else {
                $scope.message.message = message;
                $scope.message.showMessage = true;
                $scope.message.class = isError ? "bg-danger" : "bg-success";
            }            
        }

        $scope.clearAll = function(form){
			$scope.user = {userName: 'admin', password: '', newPassword: '', newPasswordConfirm: ''};
            form.$setPristine();
            form.$setUntouched();
            form.securityFormSubmitted = false;
        }

        $scope.selectMenu = function (menuName) {
            $scope.selectedMenu = menuName;            
        };

        $scope.setupAccess = function(){
            if (sessionService.sessionData.role !== "Administrador" && sessionService.sessionData.role !== "Avanzado"){
                $("#menuUsers").addClass('hidden');
            } else {
                $("#menuUsers").removeClass('hidden');
            }
        };
    };

    angular.module("administrationApp").controller("PasswordController", ["$scope", "usersService", "session", passwordController]);
}());