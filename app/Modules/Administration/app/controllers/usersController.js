//# sourceURL=usersController.js
"use strict";

(function () {
    var usersController = function ($scope, usersService, sessionService) {

        $scope.roles = [];

        $scope.init = function () {
            sessionService.join();
            $scope.loggedInUser = {userName: sessionService.sessionData.userName, password: '', newPassword: '', newPasswordConfirm: ''};
            $scope.getUsers();
            $scope.inserted = null;
        };

        $scope.loadRoles = function() {
            return $scope.roles.length ? null : usersService.getRoles().then(function(roles) {
                $scope.roles = roles;
            });
        };

        $scope.getRoleName = function(user){
            var selected = $scope.roles.filter(function(role){return role.RoleId === user.Role});
            return (user.Role && selected.length) ? selected[0].RoleName : 'Not set';
        };

        $scope.getUsers = function () {
            $scope.loadRoles().then(function(roles){
                usersService.getUsers().then(function(users){
                    $scope.users = users;
                });
            });
        };

        $scope.getUser = function(userId){
            return $scope.users.filter(function(user){return user.UserId === userId})[0];
        };

        $scope.getMaskedPassword = function(password){
            return password.length > 0 ? '******' : '';
        };

        $scope.checkPassword = function(data, userId) {
            if (data !== undefined && data !== '') {
                return true;
            }
            return "Password cannot be empty";
        };

        $scope.checkRole = function(data, userId){
            if (data !== undefined && data !== 0){
                return true;
            }
            return "You must select a role";
        };

        $scope.saveUser = function(data, userId) {
            var user = userId >= 0 ? $scope.getUser(userId) : $scope.inserted;
            user.UserName = data.UserName;
            user.Role = data.Role;
            user.Active = data.Active;
            if(user.Password !== data.Password){
                user.Password = data.Password;
                user.IsNewPassword = true;
            } else {
                user.IsNewPassword = false;
            }

            if (user.UserId >= 0)
                usersService.updateUser(user);
            else {
                usersService.addUser(user).then(function(data){
                    $scope.users[$scope.users.length - 1].UserId = data;
                });
                $scope.inserted = null;
            }
            return true;
        };

        $scope.addUser = function() {
            if ($scope.inserted === null) {
                $scope.inserted = {
                    UserId: -1,
                    UserName: '',
                    Role: 4,
                    Active: false,
                    Password: ''
                };
                $scope.users.push($scope.inserted);
            }
        };

        $scope.cancelUser = function(rowform, user){
            if (user.UserId === -1) {
                $scope.users.splice($scope.users.length - 1, 1);
                $scope.inserted = null;
            }
            rowform.$cancel();
        };
    };

    angular.module("administrationApp").controller("UsersController", ["$scope", "usersService", "session", usersController]);
}());