//# sourceURL=usersService.js
"use strict";

angular.module("administrationApp")
    .service("usersService", ["$http", "$q", "session", "CryptoSrvc", function usersService($http, $q, sessionService, CryptoSrvc) {

        function isValidString(value)
      {
          return value != null && value != undefined && value != "";
      };

      function isValidUser(user)
      {
          return user != null && user != undefined && isValidString(user.userName) && isValidString(user.password) && isValidString(user.newPassword);
      };

      function encrypt(user) {
          var key = CryptoSrvc.enc.Utf8.parse("AMINHAKEYTEM32NYTES1234567891234");
          var iv = CryptoSrvc.enc.Utf8.parse("7061737323313233");
          var encryptedCredentials = CryptoSrvc.AES.encrypt(
              CryptoSrvc.enc.Utf8.parse(user.userName + "|:|" + user.password + "|:|" + user.newPassword),
                    "AgmasterPass", key,
                    {
                        keySize: 128,
                        iv: iv,
                        mode: CryptoSrvc.mode.CBC,
                        padding: CryptoSrvc.pad.Pkcs7
                    });
          return encodeURIComponent(encryptedCredentials);
      };

      function putResponse(credentials) {
          var url = configuration.serviceBaseAddress + "/Login";
          var deferred = $q.defer();

          $http({
              method: "PUT",
              url: url,
              data: JSON.stringify(credentials),
              headers: sessionService.getHeaders(),
              contentType: "application/json; charset=utf-8",
              dataType: "json"
          }).success(function (data, status, headers, cfg) {
              deferred.resolve(data);
          }).error(function (err, status) {
              deferred.reject(status);
          });

          return deferred.promise;
      };

      function changePassword(user) {
          if (isValidUser(user)) {
              var encryptedCredentials = encrypt(user);
              return putResponse(encryptedCredentials);
          }
          return null;
      };

        function getUsers(){
            var url = configuration.serviceBaseAddress + "/Users";
            var deferred = $q.defer();

            $http({
                method: "GET",
                url: url,
                headers: sessionService.getHeaders(),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).success(function (users, status, headers, cfg) {
                deferred.resolve(users);
            }).error(function (err, status) {
                deferred.reject(status);
            });

            return deferred.promise;
        };

        function getRoles(){
            var url = configuration.serviceBaseAddress + "/Roles";
            var deferred = $q.defer();

            $http({
                method: "GET",
                url: url,
                headers: sessionService.getHeaders(),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).success(function (roles, status, headers, cfg) {
                deferred.resolve(roles);
            }).error(function (err, status) {
                deferred.reject(status);
            });

            return deferred.promise;
        };

        function updateUser(user){
            var url = configuration.serviceBaseAddress + "/Users";
            var deferred = $q.defer();

            $http({
                method: "PUT",
                url: url,
                data: JSON.stringify({NewPassword: user.IsNewPassword, User: user}),
                headers: sessionService.getHeaders(),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).success(function (data, status, headers, cfg) {
                deferred.resolve();
            }).error(function (err, status) {
                deferred.reject(status);
            });

            return deferred.promise;
        };

        function addUser(user){
            var url = configuration.serviceBaseAddress + "/Users";
            var deferred = $q.defer();

            $http({
                method: "POST",
                url: url,
                data: JSON.stringify({NewPassword: user.IsNewPassword, User: user}),
                headers: sessionService.getHeaders(),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).success(function (data, status, headers, cfg) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject(status);
            });

            return deferred.promise;
        };

      var service = {
          changePassword: changePassword,
          getUsers: getUsers,
          getRoles: getRoles,
          updateUser: updateUser,
          addUser: addUser
      };

      return service;
    }
]);