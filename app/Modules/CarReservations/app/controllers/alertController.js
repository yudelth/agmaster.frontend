﻿//# sourceURL=alertController.js
"use strict";

(function () {
    var alertController = function ( $rootScope, $scope, $location, sessionService, alertService, reportGeneratorService, reservationService) {

        var loadAlerts = function(){
            alertService.getAlerts().then(function(data){
                $scope.alerts = data;
                $rootScope.$broadcast("updatingAlerts", {
                    alerts: data
                });
            });
        };

        $scope.init = function () {
            sessionService.join();
            loadAlerts();
        };

        $scope.alertDetails = function(reservationId){
            $location.path("/reservations");
            $location.search("reservationId", reservationId);
        };

        $scope.selectAlertRow = function(){
            $("[id^=row]").removeClass("selectedRow");
            $("#row" + rowId).addClass("selectedRow");
        };

        $scope.showReservationHistory = function(reservationId){
            reportGeneratorService.generateReservationHistory(reservationId);
        };

        $scope.removeAlert = function(reservationId){
            reservationService.updateUrgentNotification(reservationId, false).then(function(){
                loadAlerts();
            })
        };
    };

    angular.module("carReservationsApp").controller("alertController", ["$rootScope", "$scope", "$location", "session", "alertService", "reportGeneratorService", "reservationService", alertController]);
}());