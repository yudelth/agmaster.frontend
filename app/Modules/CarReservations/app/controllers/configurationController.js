//# sourceURL=configurationController.js
"use strict";

(function () {
    var configurationController = function ($scope, $modal, $cookies, lookUpDataService, sessionService, carProviderEnumService, priceGroupsService, retailerService) {
        $scope.PriceGroupToEdit = {};

        function getEmptyPriceObject() {
            var price = {
                PriceId: 0,
                TourOperatorId: null,
                CarCategoryId: null,
                SeasonId: null,
                IntervalId: null,
                PriceValue: 0,
                TourOperatorPriceValue: 0
            };

            return price;
        }

        function getEmptyTourOperatorObject() {
            return {
                TourOperatorId: 0,
                CarProviders: [],
                Active: true,
                IsCuban: false
            };
        };

        function loadPrices() {
            lookUpDataService.getPrices().then(function (data) {
                $scope.prices = data;
                $scope.selectedPrice = getEmptyPriceObject();
            });
        }

        function loadTourOperators() {
            lookUpDataService.getTourOperators().then(function (data) {
                $scope.tourOperators = data;
                var tourOperator = (data !== null && data.length > 0) ? data[0] : getEmptyTourOperatorObject();

                $scope.selectTourOperator(tourOperator);
            });
        };

        function loadActiveTourOperators() {
            lookUpDataService.getActiveTourOperators().then(function (data) {
                $scope.activeTourOperators = data;
            });
        };

        function hasTourOperatorAnyCarProviderAssigned() {
            return (($scope.selectedTourOperator.CarProviders & 1) === 1 ||
            ($scope.selectedTourOperator.CarProviders & 2) === 2 ||
            ($scope.selectedTourOperator.CarProviders & 4) === 4);
        };

        function setupQuotaConfiguration(data) {
            $scope.quotaConfiguration = data;
            if ($scope.quotaConfiguration.TourOperatorQuotas.length > 0) {
                $scope.quotaConfiguration.selectedTourOperatorId = $scope.quotaConfiguration.TourOperatorQuotas[0].Id;
                $scope.quotaConfiguration.selectedTourOperator = $scope.quotaConfiguration.TourOperatorQuotas[0];
            }
        };

        $scope.init = function () {
            sessionService.join();
            $scope.activeTab = "price";
            $scope.configuringNewPrice = false;
            $scope.existingPrice = false;
            $scope.tourOperatorFormSubmitted = false;
            $scope.existingTourOperatorName = false;
            $scope.tourOperatorHasCarProviderAssociated = false;
            $scope.priceMatrix = {};
            $scope.onlinePriceMatrix = {};

            $scope.carProviders = lookUpDataService.getCarProviders();
            lookUpDataService.getSeasons().then(function (data) {
                $scope.seasons = data;
            });
            lookUpDataService.getIntervals().then(function (data) {
                $scope.intervals = data;
            });
            loadPrices();
            loadTourOperators();
            loadActiveTourOperators();
            lookUpDataService.getRetailers().then(function (data) {
                $scope.retailers = data;
            });
            lookUpDataService.getPriceGroups().then(function(data) {
                $scope.priceGroups = data;
            });
            $scope.IsWholeSaler = JSON.parse($cookies.AppsData)["IsWholeSaler"];

            lookUpDataService.getPriceMatrix().then(function (data) {
                $scope.initPriceMatrix(data, true);
            });

            lookUpDataService.getOnlinePriceMatrix().then(function (data) {
                $scope.initOnlinePriceMatrix(data, true);
            });

            lookUpDataService.getQuotaConfiguration().then(function(data) {
                setupQuotaConfiguration(data);
            });

            $(function(){
                $(".wrapper1").scroll(function(){
                    $(".wrapper2")
                        .scrollLeft($(".wrapper1").scrollLeft());
                });
                $(".wrapper2").scroll(function(){
                    $(".wrapper1")
                        .scrollLeft($(".wrapper2").scrollLeft());
                });
            });
        };

        $scope.initPriceMatrix = function (data, selectFirstYourOperator) {
            $scope.priceMatrix.configPrice = data;
            //select last-selected touroperator or first one in the list
            if ($scope.priceMatrix.configPrice.TourOperatorPrices.length > 0) {
                if (selectFirstYourOperator) {
                    $scope.priceMatrix.selectedTourOperatorId = $scope.priceMatrix.configPrice.TourOperatorPrices[0].TourOperatorId;
                    $scope.priceMatrix.selectedTourOperator = $scope.priceMatrix.configPrice.TourOperatorPrices[0];
                } else {
                    $scope.selectTourOperatorPriceMatrix($scope.priceMatrix.selectedTourOperatorId);
                }
            }
        };

        $scope.initOnlinePriceMatrix = function (data) {
            $scope.onlinePriceMatrix.configPrice = data;
            //select last-selected touroperator or first one in the list
            if ($scope.onlinePriceMatrix.configPrice.TourOperatorPrices.length > 0) {
                $scope.onlinePriceMatrix.selectedTourOperatorId = $scope.onlinePriceMatrix.configPrice.TourOperatorPrices[0].TourOperatorId;
                $scope.onlinePriceMatrix.selectedTourOperator = $scope.onlinePriceMatrix.configPrice.TourOperatorPrices[0];
            }
        };

        $scope.seasonFilter= function(interval) {
            return interval.IsMainSeason || !$scope.priceMatrix.configPrice.UseSamePriceAllSeasonIntervals;
        };

        $scope.onlineSeasonFilter= function(interval) {
            return interval.IsMainSeason || !$scope.onlinePriceMatrix.configPrice.UseSamePriceAllSeasonIntervals;
        };

        $scope.cancelPriceMatrix = function () {
            lookUpDataService.getPriceMatrix().then(function (data) {
                $scope.initPriceMatrix(data);
                toastr.info("Cambios cancelados");
            });
        };

        $scope.cancelOnlinePriceMatrix = function () {
            lookUpDataService.getOnlinePriceMatrix().then(function (data) {
                $scope.initOnlinePriceMatrix(data);
                toastr.info("Cambios cancelados");
            });
        };

        $scope.selectTourOperatorPriceMatrix = function (selectedTourOperatorId) {
            for (var i = 0; i < $scope.priceMatrix.configPrice.TourOperatorPrices.length; i++) {
                if ($scope.priceMatrix.configPrice.TourOperatorPrices[i].TourOperatorId === selectedTourOperatorId) {
                    $scope.priceMatrix.selectedTourOperator = $scope.priceMatrix.configPrice.TourOperatorPrices[i];
                    break;
                }
            }
        };

        $scope.savePriceMatrix = function (validForm) {
            lookUpDataService.savePriceMatrix($scope.priceMatrix.configPrice).then(function (data) {
                if (data.Success === true) {
                    toastr.success("Hecho");
                }
                else {
                    toastr.error("Error salvando precios. Favor de contactor Fructosoft");
                }
                $scope.initPriceMatrix(data, false);
            });
        };

        $scope.saveQuotas = function() {
            lookUpDataService.saveQuotas($scope.quotaConfiguration).then(function(data) {
                if (data) {
                    toastr.success("Configuracion salvada");
                } else {
                    toastr.error("Error guardando configuracion. Por favor, notifique Fructosoft");
                }
            });
        }

        $scope.cancelQuotas = function() {
            alert("Canceling quotas");
        }

        $scope.saveOnlinePriceMatrix = function (validForm) {
            lookUpDataService.saveOnlinePriceMatrix($scope.onlinePriceMatrix.configPrice).then(function (data) {
                if (data.Success === true) {
                    toastr.success("Hecho");
                }
                else {
                    toastr.error("Error salvando precios. Favor de contactor Fructosoft");
                }
                $scope.initOnlinePriceMatrix(data, false);
            });
        };

        $scope.selectTourOperatorQuotaConfiguration = function(selectedTourOperatorId) {
            $scope.quotaConfiguration.selectedTourOperator = $scope.quotaConfiguration.TourOperatorQuotas.filter(
                function(t) {
                    return t.Id === selectedTourOperatorId;
                })[0];
        };

        $scope.activateTab = function (tabName) {
            $scope.activeTab = tabName;
        };

        $scope.selectTourOperator = function (tourOperator) {
            $scope.selectedTourOperator = tourOperator;
            $scope.tourOperatorFormSubmitted = false;
        };

        $scope.shouldBeChecked = function (carProvider) {
            if (typeof( $scope.selectedTourOperator ) === "undefined" || $scope.selectedTourOperator === null)
                return false;

            return ($scope.selectedTourOperator.CarProviders & carProvider) === carProvider;
        };

        $scope.toggleSelection = function (carProvider) {
            $scope.selectedTourOperator.CarProviders = $scope.selectedTourOperator.CarProviders ^ carProvider;
        };

        $scope.createNewTourOperator = function () {
            $scope.selectedTourOperator = {
                TourOperatorId: 0,
                ParentTourOperatorId: null,
                Name: "",
                CarProviders: [],
                IsCuban: false,
                Active: true
            };
        };

        var createTourOperator = function (existingTourOperator) {
            if (existingTourOperator === null) {
                lookUpDataService.postTourOperator($scope.selectedTourOperator).then(function (data) {
                    if (data) {
                        toastr.success("Hecho", "Turoperador");
                        loadTourOperators();
                    } else
                        toastr.error("Error creando turoperador. Favor contactar Fructosoft support.");
                });
            } else {
                toastr.info("Turoperador existente", "Turoperador");
                $scope.existingTourOperatorName = true;
            }
        };

        var updateTourOperator = function (existingTourOperator) {
            if (existingTourOperator === null || existingTourOperator.TourOperatorId === $scope.selectedTourOperator.TourOperatorId) {
                lookUpDataService.putTourOperator($scope.selectedTourOperator).then(function (data) {
                    if (data) {
                        toastr.success("Hecho", "Turoperador");
                        loadTourOperators();
                    } else {
                        toastr.error("Error actualizando turoperador. Favor contactar Fructosoft support.");
                    }
                });
            } else {
                toastr.info("Existe un turoperador con ese nombre", "Turoperador");
                $scope.existingTourOperatorName = true;
            }
        };

        $scope.saveTourOperator = function (validForm) {
            $scope.tourOperatorFormSubmitted = true;
            $scope.tourOperatorHasCarProviderAssociated = hasTourOperatorAnyCarProviderAssigned();
            validForm = validForm && $scope.tourOperatorHasCarProviderAssociated;

            if (validForm) {
                if ($scope.selectedTourOperator.TourOperatorId === 0) {
                    lookUpDataService.getTourOperatorByName($scope.selectedTourOperator.Name).then(createTourOperator);
                } else {
                    lookUpDataService.getTourOperatorByName($scope.selectedTourOperator.Name).then(updateTourOperator);
                }
            }
        };

        $scope.getCarCategoriesByTourOperator = function (tourOperatorId) {
            if (typeof (tourOperatorId) !== "undefined" && tourOperatorId !== null)
                lookUpDataService.getCarCategoriesByTourOperator(tourOperatorId).then(function (data) {
                    $scope.carCategories = data;
                });
        };

        $scope.selectPrice = function (price) {
            $scope.configuringNewPrice = false;
            $scope.selectedPrice = price;
            $scope.priceFormSubmitted = false;
            $scope.existingPrice = false;
            $scope.getCarCategoriesByTourOperator(price.TourOperatorId);
        };

        $scope.configureNewPrice = function () {
            $scope.selectedPrice = getEmptyPriceObject();
            $scope.priceFormSubmitted = false;
            $scope.configuringNewPrice = true;
        };

        var createPrice = function (existingPrice) {
            if (existingPrice === null) {
                lookUpDataService.postPrice($scope.selectedPrice).then(function (data) {
                    if (data) {
                        toastr.success("Precio creado", "Precio");
                        loadPrices();
                    } else
                        toastr.error("Error creando precio. Favor contactar Fructosoft support.", "Precio");
                });
            } else {
                $scope.existingPrice = true;
                toastr.info("Existe un precio con esta configuración", "Precio");
            }
        };

        var updatePrice = function (existingPrice) {
            if (existingPrice === null || existingPrice.PriceId === $scope.selectedPrice.PriceId) {
                lookUpDataService.putPrice($scope.selectedPrice).then(function (data) {
                    if (data) {
                        toastr.success("Precio actualizado", "Precio");
                        loadPrices();
                    } else
                        toastr.error("Error actualizando precio. Favor contactar Fructosoft support.", "Precio");
                });
            } else {
                $scope.existingPrice = true;
                toastr.info("Existe un precio con esta configuración", "Precio");
            }
        };

        $scope.savePrice = function (validForm) {
            $scope.priceFormSubmitted = true;

            if (validForm) {
                if ($scope.selectedPrice.PriceId === 0)
                    lookUpDataService.getPriceByConfiguration($scope.selectedPrice).then(createPrice);
                else
                    lookUpDataService.getPriceByConfiguration($scope.selectedPrice).then(updatePrice);
            }
        };

        $scope.deletePrice = function () {
            event.preventDefault();
            lookUpDataService.deletePrice($scope.selectedPrice).then(function (data) {
                if (data) {
                    toastr.success("Precio eliminado", "Precio");
                    loadPrices();
                } else
                    toastr.error("Error eliminando precio. Favor contactar Fructosoft support.", "Precio");
            });
        };

        var priceGroupModal = $modal({
            scope: $scope,
            templateUrl: "/Modules/CarReservations/app/views/modalPriceGroups.html",
            id: "modalPriceLevelsContent",
            show: false
        });

        function CanSaveOrUpdatePriceGroup(priceGroup) {
            return $scope.priceGroups.map(function (item) {
                    return item.Name;
                }).indexOf(priceGroup.Name) < 0;
        };

        $scope.openPriceGroupsModal = function () {
            priceGroupModal.show();
        };

        $scope.CreatePriceGroup = function (priceGroup) {
            if (CanSaveOrUpdatePriceGroup(priceGroup)) {
                priceGroupsService.add(priceGroup).then(function success(data) {
                    $scope.priceGroups.push(data[0]);
                    priceGroupModal.hide();
                    toastr.success("Nivel agregado", "Niveles");
                }, function error(response) {
                    toastr.error("Error eliminando nivel de precio. Favor contactar Fructosoft support.", "Niveles");
                });
            } else {
                toastr.error("Nivel existente.", "Niveles");
            }
        };

        $scope.RemovePriceGroup = function (priceGroup) {
            priceGroupsService.remove(priceGroup).then(function success(data) {
                    var index = $scope.priceGroups.indexOf(priceGroup);
                    $scope.priceGroups.splice(index, 1);
                    toastr.success("Nivel eliminado", "Niveles");
                }
                , function error(response) {
                    toastr.error("Error eliminando nivel de precio. Favor contactar Fructosoft support.", "Niveles");
                }
            )
        };

        $scope.UpdateAgencyPricingGroupAssociation = function (retailer) {
            retailerService.update(retailer).then(function success(data) {
                toastr.success("Asociacion de Nivel Creada", "Retailer");
            }, function error(response) {
                toastr.error("Error asociando nivel con retailer .", "Retailer");
            })
        };

        $scope.CanRemovePriceGroup = function (priceGroup) {
            return $scope.retailers.map(function (item) {
                    return item.PriceGroupId;
                }).indexOf(priceGroup.PriceGroupId) < 0
        };

        $scope.EditPriceGroup = function (priceGroup) {
            $scope.PriceGroupToEdit = angular.copy(priceGroup);
        };

        $scope.getTemplate = function (priceGroup) {
            if (priceGroup.PriceGroupId === $scope.PriceGroupToEdit.PriceGroupId) return "edit";
            else return "display";
        };

        $scope.UpdatePriceGroup = function (idx) {
            $scope.priceGroups[idx] = angular.copy($scope.PriceGroupToEdit);
            priceGroupsService.update($scope.priceGroups[idx]).then(function success(data) {
                    toastr.success("Nivel actualizado", "Niveles");
                    $scope.reset();
                }
                , function error(response) {
                    toastr.error("Error actualizando nivel de precio. Favor contactar Fructosoft support.", "Niveles");
                })
        };

        $scope.reset = function () {
            $scope.PriceGroupToEdit = {};
        };

        $scope.getMonthCssClass = function(month) {
            switch (month) {
                case "Enero":
                case "Abril":
                case "Julio":
                case "Octubre":
                case 1:
                case 4:
                case 7:
                case 10:
                    return "bgSeasonPriceBaja";
                case "Febrero":
                case "Mayo":
                case "Agosto":
                case "Noviembre":
                case 2:
                case 5:
                case 8:
                case 11:
                    return "bgSeasonPriceAlta";
                case "Marzo":
                case "Junio":
                case "Septiembre":
                case "Diciembre":
                case 3:
                case 6:
                case 9:
                case 12:
                    return "bgSeasonPriceExtremaAlta";
            }
        };
    };

    angular.module("carReservationsApp").controller("configurationController", ["$scope", "$modal", "$cookies", "lookUpDataService", "session", "carProviderEnumService", "priceGroupsService", "retailerService", configurationController]);
}());