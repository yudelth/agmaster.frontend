﻿//# sourceURL=customersController.js
"use strict";

(function () {
    var customersController = function ($scope, $location, $modal, customerService, reservationHandler, sessionService) {

        var customersModal;// = $modal({ scope: $scope, template: "modalCustomersContent.html", show: false });

        var loadCustomers = function () {
            customerService.get().then(function (data) {
                $scope.customers = data;
                $scope.selectCustomer($scope.customers[0]);
            });
        };

        $scope.loadCustomerReservations = function () {
            if ($scope.selectedCustomer !== undefined && $scope.selectedCustomer !== null && $scope.selectedCustomer.CustomerId !== undefined) {
                customerService.getCustomerReservations($scope.selectedCustomer.CustomerId).then(function (data) {
                    $scope.selectedReservations = data;
                });
            }
        };

        $scope.init = function () {
            sessionService.join();
            loadCustomers();

            customersModal = $modal({scope: $scope, template: "modalCustomersContent.html", show: false});
            $scope.selectedMatchingCustomer = null;
        };

        $scope.selectCustomer = function (customer) {
            $scope.selectedCustomer = customer;
            $scope.loadCustomerReservations();
        };

        $scope.selectReservation = function (reservation) {
            $location.path("/reservations");
            $location.search("reservationId", reservation.ReservationId);
        };

        $scope.createNewCustomer = function () {
            $scope.selectedCustomer = {
                CustomerId: 0,
                FirstName: "",
                LastName: "",
                Email: "",
                CellPhone: ""
            };
        };

        $scope.insertCustomer = function () {
            customerService.post($scope.selectedCustomer).then(function (result) {
                if (result) {
                    toastr.success("Cliente creado", "Cliente");
                    loadCustomers();
                } else {
                    toastr.error("Error creando cliente", "Cliente");
                }
            });

            customersModal.$promise.then(customersModal.hide);
        };

        var updateCustomer = function () {
            customerService.put($scope.selectedCustomer).then(function (result) {
                if (result) {
                    toastr.success("Cliente actualizado", "Cliente");
                    loadCustomers();
                } else {
                    toastr.error("Error actualizando cliente", "Cliente");
                }
            });

            customersModal.$promise.then(customersModal.hide);
        };

        $scope.saveCustomer = function (validForm) {
            if (validForm) {
                if ($scope.selectedCustomer.CustomerId === 0) {
                    customerService.getCustomerMatchingPhoneNumberPattern($scope.selectedCustomer.CellPhone).then(function (matchingCustomers) {
                        if (typeof matchingCustomers !== "undefined" && matchingCustomers !== null && matchingCustomers.length > 0) {
                            customersModal.$promise.then(function () {
                                $scope.matchingCustomers = matchingCustomers;
                                customersModal.show();
                            });
                        } else
                            $scope.insertCustomer();
                    });
                } else {
                    updateCustomer();
                }
            }
        };

        $scope.selectingCustomer = function (result) {
            if ($scope.selectedCustomer.CustomerId !== 0)
                $scope.selectedCustomer.CellPhone = result.originalObject.CellPhone;
            else
                $scope.selectedCustomer = result.originalObject;
        };

        $scope.chooseMatchingCustomer = function (customer) {
            $scope.selectedMatchingCustomer = customer;
        };

        $scope.isCancelled = function (reservation) {
            if (typeof reservation !== "undefined" && reservation !== null) {
                return reservationHandler.isCancelled(reservation);
            }
        };

        $scope.isConfirmed = function (reservation) {
            return !reservationHandler.isCancelled(reservation) && reservationHandler.isConfirmed(reservation);
        };

        $scope.isNotConfirmed = function (reservation) {
            return !reservationHandler.isCancelled(reservation) && reservationHandler.isNotConfirmed(reservation);
        };
    };

    angular.module("carReservationsApp").controller("customersController", ["$scope", "$location", "$modal", "customerService", "reservationHandler", "session", customersController]);
}());