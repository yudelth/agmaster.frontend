﻿//# sourceURL=dashboardController.js

(function () {
    var dashboardController = function ($scope, $location, reservationStatusEnumService, reservationHandler, dashboardService, sessionService) {

        $scope.init = function () {
            sessionService.join();
            dashboardService.getPartiallyPaidReservations().then(function (data) {
                if (data !== null)
                    $scope.partiallyPaidReservations = data;
            });
        };

        $scope.isPaid = function (reservation) {
            return reservationHandler.isPaid(reservation);
        };

        $scope.isNotPaid = function (reservation) {
            return reservationHandler.isNotPaid(reservation);
        };

        $scope.isPartiallyPaid = function (reservation) {
            return reservationHandler.isPartiallyPaid(reservation);
        };

        $scope.isConfirmed = function (reservation) {
            return reservationHandler.isConfirmed(reservation);
        };

        $scope.isNotConfirmed = function (reservation) {
            return reservationHandler.isNotConfirmed(reservation);
        };

        $scope.reservationDetails = function (reservationId) {
            $location.path("/reservations");
            $location.search("reservationId", reservationId);
        };

        $scope.selectRow = function (rowId) {
            $("[id^=row]").removeClass("selectedRow");
            $("#row" + rowId).addClass("selectedRow");
        };

        $scope.displayAgencyStatusWarning = function () {
            if ($scope.agencyStatus !== undefined && !$scope.agencyStatus.Valid) {
                var message = "";
                if ($scope.agencyStatus.Days > 0) {
                    message = "Basado en sus acuerdos con Fructosoft su licencia expira en " + $scope.agencyStatus.Days + " dias. Por favor contacte a su administrador de Cubagen para efectuar pago.";
                }
                else if ($scope.agencyStatus.Days === 0) {
                    message = "Basado en sus acuerdos con Fructosoft su licencia expira hoy. Por favor contacte a su administrador de Cubagen para efectuar pago.";
                }
                else {
                    message = "Basado en sus acuerdos con Fructosoft su licencia expiro hace " + Math.abs($scope.agencyStatus.Days) + " dias. Por favor contacte a su administrador de Cubagen para efectuar pago.";

                }

                toastr.error(message, "Alerta de pago");

            }
        };

        if ($scope.agencyStatus === undefined) {
            dashboardService.getAgencyStatus().then(function (data) {
                $scope.agencyStatus = data;
                $scope.displayAgencyStatusWarning();
            });
        }

    };

    angular.module("carReservationsApp").controller("dashboardController", ["$scope", "$location", "reservationStatusEnumService", "reservationHandler", "dashboardService", "session", dashboardController]);
}());