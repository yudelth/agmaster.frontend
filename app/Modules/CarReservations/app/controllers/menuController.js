﻿//# sourceURL=menuController.js
"use strict";

(function () {
    var menuController = function($scope, $location, $interval, sessionService, alertService) {
        var timer;

        function updateAlert(){
            sessionService.join();
            alertService.getAlerts().then(function(data){
                $scope.alertTotal = data.length;
            });
        };

        $scope.isActive = function(path) {
            return $location.path() === path;
        }

        $scope.init = function(){
            updateAlert();

            if (angular.isDefined(timer)) {
                $interval.cancel(timer);
            }

            timer = $interval(updateAlert, 60000);
            $scope.userRole = sessionService.sessionData.role.toLowerCase();
        };

        $scope.$on("updatingAlerts", function(event, args){
            $scope.alertTotal = args.alerts.length;
        });
    };
    angular.module("carReservationsApp").controller("menuController", ["$scope", "$location", "$interval", "session", "alertService", menuController]);
}());