﻿angular.module("carReservationsApp").controller("ModalCustomersCtrl", function ($scope) {

    $scope.title = "Cliente(s) coincidente(s)";
    $scope.message = "El cliente con el que estoy trabajando es: ";

    $scope.selectCustomer = function (customer) {
        $scope.chooseCustomer(customer);
    };
});
