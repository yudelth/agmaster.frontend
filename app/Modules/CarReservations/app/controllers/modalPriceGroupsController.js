angular.module("carReservationsApp").controller("ModalPriceGroupsCtrl", function ($scope) {

    $scope.submitButtonClicked = false;
    $scope.priceGroup = {
    	Name:"", 
    	Discount:""
    };
   
    $scope.savePriceGroup = function (validForm) {
        $scope.submitButtonClicked = true;
    	if(validForm){
    	 $scope.CreatePriceGroup($scope.priceGroup);
    	}
    };
});
