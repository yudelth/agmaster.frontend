﻿//# sourceURL=monthlySalesByCarCategoryPieChartController.js

(function () {
    var monthlySalesByCarCategoryPieChartController = function ($scope, dashboardService) {

        function init() {
            dashboardService.getMonthlySalesByCarCategory().then(function (data) {
                $scope.monthlysale = data;

                if (data !== null)
                    $scope.initChart();
            });
        }

        $scope.initChart = function () {
            var chart = new AmCharts.AmPieChart();
            chart.dataProvider = $scope.monthlysale;
            chart.valueField = "Count";
            chart.titleField = "Value";
            chart.depth3D = 10;
            chart.angle = 40;

            chart.write($scope.containerId);
        };

        init();
    };

    angular.module('carReservationsApp').controller("monthlySalesByCarCategoryPieChartController", ['$scope', 'dashboardService', monthlySalesByCarCategoryPieChartController]);
}());