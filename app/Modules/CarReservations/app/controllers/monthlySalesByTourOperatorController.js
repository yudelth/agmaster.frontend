﻿//# sourceURL=monthlySalesByTourOperatorController.js

(function () {
    var monthlySalesByTourOperatorController = function ($scope) {

        $scope.monthlysale = [{
            "tourOperator": "Grupo Viaje Celimar",
            "sales": 56
        }, {
            "tourOperator": "Grupo Viaje Cubatur",
            "sales": 48
        }, {
            "tourOperator": "Viaje Pricipal Riesgo",
            "sales": 24
        }, {
            "tourOperator": "Viaje Principal Cupo",
            "sales": 19
        }, {
            "tourOperator": "San Cristóbal Riesgo",
            "sales": 42
        }, {
            "tourOperator": "San Cristóbal Cupo",
            "sales": 38
        }, {
            "tourOperator": "San Cristóbal 6",
            "sales": 29
        }];

        $scope.initChart = function () {
            var chart = new AmCharts.AmSerialChart();
            chart.dataProvider = $scope.monthlysale;
            chart.categoryField = "tourOperator";
            chart.marginRight = 0;
            chart.marginTop = 0;
            chart.autoMarginOffset = 0;
            //chart.depth3D = 20;
            //chart.angle = 30;

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.labelRotation = 90;
            categoryAxis.dashLength = 5;
            categoryAxis.gridPosition = "start";

            categoryAxis.gridCount = 17;
            categoryAxis.autoGridCount = false;

            // value
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.title = "Reservaciones";
            valueAxis.dashLength = 5;
            chart.addValueAxis(valueAxis);

            // GRAPH            
            var graph = new AmCharts.AmGraph();
            graph.valueField = "sales";
            graph.colorField = "#FF9E01";
            graph.balloonText = "[[category]]: [[value]]";
            graph.type = 'column';
            graph.lineAlpha = 0.2;
            graph.fillAlphas = 0.9;
            chart.addGraph(graph);
            chart.write($scope.containerId);
        };

        $scope.initChart();
    };

    angular.module('carReservationsApp').controller("monthlySalesByTourOperatorController", ['$scope', monthlySalesByTourOperatorController]);
}());