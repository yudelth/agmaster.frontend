﻿//# sourceURL=monthlySalesSerialChartController.js

(function () {
    var monthlySalesSerialChartController = function ($scope, dashboardService) {

        function init() {
            dashboardService.getReservationsByMonth().then(function (data) {
                $scope.monthlysale = data;

                if (data !== null)
                    $scope.initChart();
            });
        }

        $scope.initChart = function () {
            var chart = new AmCharts.AmSerialChart();
            chart.dataProvider = $scope.monthlysale;
            chart.categoryField = "Value";
            chart.marginRight = 0;
            chart.marginTop = 0;
            chart.autoMarginOffset = 0;
            chart.depth3D = 20;
            chart.angle = 30;

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.labelRotation = 90;
            categoryAxis.dashLength = 5;
            categoryAxis.gridPosition = "start";

            categoryAxis.gridCount = 17;
            categoryAxis.autoGridCount = false;

            // value
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.title = "Reservaciones";
            valueAxis.dashLength = 5;
            chart.addValueAxis(valueAxis);

            // GRAPH            
            var graph = new AmCharts.AmGraph();
            graph.valueField = "Count";
            graph.colorField = "#CD0D74";
            graph.balloonText = "[[category]]: [[value]]";
            graph.type = 'column';
            graph.lineAlpha = 0;
            graph.fillAlphas = 1;
            chart.addGraph(graph);
            chart.write($scope.containerId);
        };

        init();
    };

    angular.module('carReservationsApp').controller("monthlySalesSerialChartController", ['$scope', 'dashboardService', monthlySalesSerialChartController]);
}());