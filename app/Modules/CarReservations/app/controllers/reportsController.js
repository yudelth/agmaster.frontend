﻿//# sourceURL=reportsController.js
"use strict";

(function () {
    var reportsController = function ($scope, lookUpDataService, sessionService, reportGeneratorService, sharingService) {

        $scope.init = function () {
            $scope.activeTab = "accounting";
            $scope.selectedMenu = "accounting";
            $scope.todaysDate = new Date();
            sessionService.join();
            $scope.userRole = sessionService.sessionData.role.toLowerCase();
            $scope.quotaReport = {
                selectedTourOperatorId: 0,
                selectedCarCategoryId: 0,
                selectedDate: new Date(), 
                data: [],
                monthlyQuotaAssignments: []
            };
            
            lookUpDataService.getTourOperators().then(function (data) {
                $scope.tourOperators = data;
                $scope.tourOperators.unshift( {
                    TourOperatorId: 0,
                    Name: "-Todos-"
                });
            });

            lookUpDataService.getRetailers().then(function (data) {
                $scope.retailers = data;
                $scope.retailers.unshift( {
                    Identifier: sessionService.sessionData.agencyId,
                    Name: "-Mi Agencia-"
                });
                $scope.retailers.unshift( {
                    Identifier: "all",
                    Name: "-Todas-"
                });
            });

            lookUpDataService.getCarCategories().then(function(data) {
                $scope.carCategories = data;
            });

            $scope.statuses = lookUpDataService.getReservationStatuses("paymentStatus");
            $scope.statuses.unshift({
                StatusId: 0,
                Name: "-Todos-"
            });

            $scope.tourOperatorReport = {
                StartDate: new Date().setDate(new Date(Date.now()).getDate() - 6),
                EndDate: Date.now(),
                TourOperatorId: 0,
                RetailerIdentifier: "all",
                StatusId: 0
            };

            $scope.reservationsReport = {
                StartDate: new Date().setDate(new Date(Date.now()).getDate() - 6),
                EndDate: Date.now()
            };

            $scope.salesByOperatorsReport = {
                StartDate: new Date().setDate(new Date(Date.now()).getDate() - 6),
                EndDate: Date.now()
            };

            $scope.paymentByTypesReport = {
                StartDate: new Date().setDate(new Date(Date.now()).getDate() - 6),
                EndDate: Date.now()
            };

            $scope.salesByCarCategoryReport = {
                StartDate: new Date().setDate(new Date(Date.now()).getDate() - 6),
                EndDate: Date.now()
            };

            $scope.salesByLocationReport = {
                StartDate: new Date().setDate(new Date(Date.now()).getDate() - 6),
                EndDate: Date.now(),
                AllLocations: false
            };

            sharingService.getTourOperatorsWithQuotas(true).then(function(data) {
                $scope.tourOperatorsWithQuota = data;
                $scope.selectedQuoteTourOperator = data[0];
            });

            $scope.quotasReportSubmitted = false;
        };

        $scope.generateTourOperatorReport = function(formValid){
            if (formValid)
                reportGeneratorService.generateSalesReport($scope.tourOperatorReport.StartDate, $scope.tourOperatorReport.EndDate, $scope.tourOperatorReport.TourOperatorId, $scope.tourOperatorReport.StatusId, $scope.tourOperatorReport.RetailerIdentifier);
        };

        $scope.generateTourOperatorExcel = function(formValid){
            if (formValid)
                reportGeneratorService.generateSalesExcel($scope.tourOperatorReport.StartDate, $scope.tourOperatorReport.EndDate, $scope.tourOperatorReport.TourOperatorId, $scope.tourOperatorReport.StatusId, $scope.tourOperatorReport.RetailerIdentifier);
        };

        $scope.generateTourOperatorLimitedReport = function (formValid) {
            if (formValid)
                reportGeneratorService.generateSalesLimitedReport($scope.tourOperatorReport.StartDate, $scope.tourOperatorReport.EndDate, $scope.tourOperatorReport.TourOperatorId, $scope.tourOperatorReport.StatusId);
        };

        $scope.generateReservationsReport = function (formValid) {
            if (formValid)
                reportGeneratorService.generateActiveReservationsReport($scope.reservationsReport.StartDate, $scope.reservationsReport.EndDate);
        };

        $scope.selectMenu = function (menuName) {
            $scope.selectedMenu = menuName;
        };

        $scope.activateTab = function (tabName) {
            $scope.activeTab = tabName;
        };

        $scope.generateSalesByOperatorsReport = function(formValid){
            if (formValid)
                reportGeneratorService.generateSalesByOperatorReport($scope.salesByOperatorsReport.StartDate, $scope.salesByOperatorsReport.EndDate);
        };

        $scope.generateSalesByCarCategoryReport = function (formValid) {
             if (formValid)
                reportGeneratorService.generateSalesByCarCategoryReport($scope.salesByCarCategoryReport.StartDate,
                                                                        $scope.salesByCarCategoryReport.EndDate,
                                                                        $scope.salesByCarCategoryReport.CarCategoryId);
        };

        $scope.generatePaymentByTypesReport = function(formValid){
            if (formValid)
                reportGeneratorService.generatePaymentByTypesReport($scope.paymentByTypesReport.StartDate, $scope.paymentByTypesReport.EndDate);
        };

        $scope.generateSalesByLocationReport = function(formValid){
            if (formValid)
                reportGeneratorService.generateSalesByLocationReport($scope.salesByLocationReport.StartDate, $scope.salesByLocationReport.EndDate, $scope.salesByLocationReport.AllLocations);
        };

        $scope.getCarCategoriesByTourOperator = function(tourOperatorId) {
            lookUpDataService.getCarCategoriesByTourOperator(tourOperatorId).then(function(data) {
                $scope.carCategories = data;
            });
        };

        $scope.generateQuotaReport = function () {
            lookUpDataService.getQuotaReport($scope.quotaReport.selectedDate.getMonth(), $scope.quotaReport.selectedDate.getFullYear()).then(function(data) {
                $scope.quotaReport.monthlyQuotaAssignments = data.MonthlyQuotaAssignments;
                $scope.quotaReport.data = data.TourOperatorQuotas.filter(function(t){
                    return t.TourOperatorId === $scope.selectedTourOperatorId;
                })[0]; 

                $scope.quotaReport.dates = data.Dates.filter(function(date) {
                    return (new Date(date)).getMonth() === $scope.quotaReport.selectedDate.getMonth();
                }).map(function(date) {
                    var newDate = (new Date(date));
                    return newDate.getDate();
                });

                $scope.quotasReportSubmitted = true;
            });
        };

        $scope.getDayCssClass = function (day) {
            var theDate = day;
                       
            if (!Number.isInteger(day)) {
                theDate = new Date(day).getDate();
            }

            var isOdd = theDate % 2 === 1;

            return isOdd ? "bgSeasonPriceBaja" : "bgSeasonPriceExtremaAlta";
        };

        $scope.getMonthlyQuota = function(categoryId, date) {
            var month = new Date(date).getMonth() + 1;
            var quota = $scope.quotaReport.monthlyQuotaAssignments.filter(function(q) {
                return q.TourOperatorId === $scope.quotaReport.selectedTourOperatorId &&
                       q.CarCategoryId === categoryId &&
                       q.Month === month;
            });

            return typeof(quota) !== "undefined" && quota !== null ? (quota.length > 0 ? quota[0].Value : 0) : 0;
        };

        $scope.getDailyQuotaTotal = function(categoryId) {
            return $scope.quotaReport.data.CarCategoryQuotas.filter(function(c) {
                return c.Id === categoryId;
            })[0].QuotasReport.reduce(function(accumulator, currentValue) {
                return accumulator + currentValue.DailyQuota;
            }, 0);
        };
        
        $scope.getDifferenceTotal = function (categoryId) {
            var assignedDailyQuota = $scope.getDailyQuotaTotal(categoryId);
            var assignedMonthlyQuota = $scope.quotaReport.data.CarCategoryQuotas.filter(function (c) { return c.Id === categoryId; })[0].AssignedMonthlyQuota;
            return assignedMonthlyQuota - assignedDailyQuota;
        };
    };

    angular.module("carReservationsApp").controller("reportsController", ["$scope", "lookUpDataService", "session", "reportGeneratorService", "sharingService", reportsController]);
}());