//# sourceURL=reservationController.js

(function () {
    var reservationController = function ($scope, $modal, $location, reservationService, reservationStatusEnumService, paymentEnumService, reservationHandler, lookUpDataService, sessionService, customerService, paymentService, emailService, reportGeneratorService) {

        var onFetchingProvincesComplete = function (data) {
            $scope.provinces = data;
        };

        var onFetchingPickUpLocationsComplete = function (data) {
            $scope.pickUpLocations = data;
        };

        var onFetchingReturnLocationsComplete = function (data) {
            $scope.returnLocations = data;
        };

        var onFetchingCarCategoriesComplete = function (data) {
            $scope.carCategories = data;

            var result = $.grep($scope.carCategories, function(e){ return e.CarCategoryId ===  $scope.selectedReservation.CarCategoryId; });
            if (result.length === 0) {
                $scope.selectedReservation.CarCategoryId = null;
            }
            $scope.retrievePrice();
        };

        var onFetchingTourOperatorsComplete = function (data) {
            $scope.tourOperators = data;

            var result = $.grep($scope.tourOperators, function(e){ return e.TourOperatorId ===  $scope.selectedReservation.TourOperatorId; });
            if (result.length === 0) {
                $scope.selectedReservation.TourOperatorId = null;
            }
            else{
                $scope.setupRetailerMemoEnviroment();
                $scope.setupWholesalerEnviroment();
            }

            $scope.retrievePrice();
        };

        var onFetchingPaymentsComplete = function (data) {
            $scope.reservationPayments = data;
        };

        var onFetchingActiveReservationsComplete = function (data) {
            $scope.activeReservations = data;
            $scope.bookNewReservation();
        };

        var onFetchingInactiveReservationsComplete = function (data) {
            $scope.inactiveReservations = data;
        };

        var loadReservation = function (loadHistory) {
            if (!loadHistory)
                reservationService.loadReservations(loadHistory).then(onFetchingActiveReservationsComplete);
            else
                reservationService.loadReservations(loadHistory).then(onFetchingInactiveReservationsComplete());
        };

        var customersModal = $modal({
            scope: $scope,
            template: "/Modules/CarReservations/app/views/modalCustomersContent.html",
            id: "modalCustomersContent",
            show: false
        });

        var confirmationModal = $modal({
            scope: $scope,
            template: "/Modules/CarReservations/app/views/confirmationModal.html",
            id: "confirmationModal",
            show: false
        });

        var reservationCancellationModal = $modal({
            scope: $scope,
            template: "/Modules/CarReservations/app/views/cancelReservationModal.html",
            id: "CancelReservationModal",
            show: false
        });

        var emailVoucherconfirmationModal = $modal({
            scope: $scope,
            template: "/Modules/CarReservations/app/views/emailVoucherConfirmationModal.html",
            id: "emailVoucherConfirmationModal",
            show: false
        });

        function openCustomersModal(customers) {
            $scope.customers = customers;
            customersModal.$promise.then(function () {
                customersModal.show();
            });
        }

        function updateReservationStatusConfirmationWise() {
            if ($scope.selectedReservation.ConfirmationCode === "" || $scope.selectedReservation.ConfirmationCode === null)
                reservationHandler.unconfirm($scope.selectedReservation);
            else
                reservationHandler.confirm($scope.selectedReservation);
        }

        function formatReservationDateWithTime() {
            var startDate = new Date($scope.selectedReservation.StartDate);
            var endDate = new Date($scope.selectedReservation.EndDate);

            if (String($scope.selectedReservation.StartDate).indexOf("12:00:00.000") < 0) {
                $scope.selectedReservation.StartDate = startDate.getFullYear() + "-" + parseInt(startDate.getMonth() + 1) + "-" + startDate.getDate() + " 12:00:00.000";
            }
            if (String($scope.selectedReservation.EndDate).indexOf("12:00:00.000") < 0) {
                $scope.selectedReservation.EndDate = endDate.getFullYear() + "-" + parseInt(endDate.getMonth() + 1) + "-" + endDate.getDate() + " 12:00:00.000";
            }
        }

        $scope.reservationCancellationModalShow = function(){
            if ($scope.isCancelled($scope.selectedReservation)){
                $scope.cancelOrActivateReservation();
            }
            else {
                reservationCancellationModal.show();
            }
        };

        function addNewReservation(reservation) {
            formatReservationDateWithTime();

            reservation.ReservationStatus = reservationStatusEnumService.status.noPaid;

            if (typeof reservation.FinalPrice === "undefined" || reservation.FinalPrice === 0 || isNaN(reservation.FinalPrice))
                toastr.error("Error adicionando reservación: no tiene precio asignado. ", "Reservaciones");
            else {
                reservationService.addReservation(reservation).then(function (addedReservation) {
                    addedReservation.PickUpProvinceId = reservation.PickUpProvinceId;
                    addedReservation.ReturnProvinceId = reservation.ReturnProvinceId;
                    $scope.selectedReservation = addedReservation;
                    $scope.activeReservations.unshift($scope.selectedReservation);
                    toastr.success("Reservación adicionada", "Reservaciones");
                    $scope.creatingNewReservation = false;
                }, function (error) {
                    toastr.error("Error adicionando la reservación! " + error, "Reservaciones");
                    console.log(error);
                });
            }
        };

        function editReservation(reservation) {
            formatReservationDateWithTime();
            updateReservationStatusConfirmationWise();

            reservationService.saveReservation(reservation).then(function (updatedReservation) {
                updatedReservation.PickUpProvinceId = reservation.PickUpProvinceId;
                updatedReservation.ReturnProvinceId = reservation.ReturnProvinceId;
                loadReservationById(reservation);
                toastr.success("Reservación salvada", "Hecho");
            }, function (error) {
                toastr.error(error, "ERROR", {timeOut: 10000});
            });
        };

        function validateCustomerChangesWithServerList(current, serverList) {
            var count = 0;
            serverList.forEach(function (item) {
                if (item.PhoneNumber === current.PhoneNumber &&
                    item.FirstName === current.FirstName &&
                    item.LastName === current.LastName &&
                    item.EmailAddress === current.EmailAddress) {
                    $scope.selectedReservation.Customer.CustomerId = item.CustomerId;
                    $scope.selectedReservation.CustomerId = item.CustomerId;
                    count++;
                }
            });
            return count <= 0;
        }

        function customerExists(reservation) {
            customerService.getCustomerMatchingPhoneNumberPattern(reservation.Customer.CellPhone)
                .then(function (existingCustomers) {
                    if (existingCustomers !== null && existingCustomers !== undefined && existingCustomers.length !== 0
                        && validateCustomerChangesWithServerList(reservation.Customer, existingCustomers)) {
                        openCustomersModal(existingCustomers);
                    } else {
                        if ($scope.selectedReservation.ReservationId > 0) {
                            editReservation($scope.selectedReservation);
                        } else {
                            addNewReservation($scope.selectedReservation);
                        }
                    }
                });
        }

        $scope.chooseCustomer = function (customer) {
            $scope.selectedReservation.Customer = customer;
            customersModal.$promise.then(customersModal.hide);
            if ($scope.selectedReservation.ReservationId > 0) {
                editReservation($scope.selectedReservation);
            } else {
                addNewReservation($scope.selectedReservation);
            }
        };

        function saveReservation() {
            if ($scope.reservationForm.customerFirstName.$dirty || $scope.reservationForm.customerLastName.$dirty || $scope.reservationForm.customerEmail.$dirty) {
                customerExists($scope.selectedReservation);
            } else {
                if ($scope.selectedReservation.ReservationId > 0) {
                    editReservation($scope.selectedReservation);
                } else {
                    reservationHandler.getReadyToCreate($scope.selectedReservation);
                    addNewReservation($scope.selectedReservation);
                }
            }
            $scope.reservationForm.$dirty = false;
            $scope.paymentForm.$dirty = false;
            $scope.reservationForm.$setPristine();
            $scope.paymentForm.$setPristine();
        };

        $scope.customerSelected = false;
        $scope.creatingNewReservation = false;
        $scope.submitReservationButtonClicked = false;
        $scope.reservationSubmitted = false;
        $scope.creatingNewPayment = false;
        $scope.addPaymentClicked = false;
        $scope.minDate = new Date();
        $scope.activeReservations = [];
        $scope.activeReservationTab = "activeReservationTab";
        $scope.disableAllForms = true;
        $scope.directEmailTo = {emailAddress : "", reservationId: ""};

        $scope.init = function () {
            sessionService.join();
            var reservationId = $location.search().reservationId;

            if (typeof reservationId === "undefined" || reservationId === null)
                loadReservation(false);
            else {
                reservationService.loadReservationById(reservationId).then(function (data) {
                    $scope.activeReservations = [data];
                    $scope.selectReservation(data);
                });
            }

            $scope.carProviders = lookUpDataService.getCarProviders();
            lookUpDataService.getProvinces().then(onFetchingProvincesComplete);
            $scope.matchingCustomers = {};
            $scope.cancelOrActivateButtonPressed = false;
            lookUpDataService.getRetailers().then(function(data){
                data.unshift({
                    Identifier: sessionService.sessionData.agencyId,
                    Name: sessionService.sessionData.agencyId
                });
                $scope.retailers = data;
            });
        };

        $scope.getPickUpPlaces = function (provinceId) {
            lookUpDataService.pickUpLocationsByProvince(provinceId).then(onFetchingPickUpLocationsComplete);
        };

        $scope.getReturnPlaces = function(provinceId){
            lookUpDataService.pickUpLocationsByProvince(provinceId).then(onFetchingReturnLocationsComplete);
        };

        $scope.carProviderSelectionChanges = function (carProviderId) {
            lookUpDataService.carCategoriesByCarProvider(carProviderId).then(onFetchingCarCategoriesComplete);
            lookUpDataService.activeTourOperatorUsedToMakeReservationByCarProvider(carProviderId).then(onFetchingTourOperatorsComplete);
        };

        $scope.getTourOperatorEmail = function(tourOperatorId) {
            if ($scope.tourOperators !== undefined) {
                for (var i = 0; i < $scope.tourOperators.length; i++) {
                    if ($scope.tourOperators[i].TourOperatorId == tourOperatorId) {
                        $scope.directEmailTo.emailAddress = $scope.tourOperators[i].EmailAddress;
                        break;
                    }
                }
            }
        };

        function loadReservationById(reservation) {
            if (typeof reservation === "undefined" || reservation === null)
                return;

            reservationService.loadReservationById(reservation.ReservationId).then(function (data) {
                $scope.selectedReservation = data;
                $scope.selectedReservation.$original = angular.copy($scope.selectedReservation);
                $scope.activeReservations[$scope.activeReservations.indexOf(reservation)] = data;
                $scope.selectedReservation.PickUpProvinceId = data.PickUpLocation.ProvinceId;
                $scope.creatingNewReservation = false;
                $scope.carProviderSelectionChanges($scope.selectedReservation.CarProvider);
                $scope.getPickUpPlaces($scope.selectedReservation.PickUpProvinceId);

                if (typeof data.ReturnLocation !== "undefined" && data.ReturnLocation !== null ) {
                    $scope.selectedReservation.ReturnProvinceId = data.ReturnLocation.ProvinceId;
                    $scope.getReturnPlaces($scope.selectedReservation.ReturnProvinceId);
                }
                if($scope.selectedReservation.TourOperator !== null){
                    $scope.getTourOperatorEmail($scope.selectedReservation.TourOperator.TourOperatorId);
                };
            });
        }

        $scope.selectReservation = function (reservation) {
            if ($scope.reservationForm.$dirty && typeof $scope.reservationForm.ReservationId !== "undefined" && $scope.reservationForm.ReservationId !== 0) {
                confirmationModal.show();
            }
            else {
                loadReservationById(reservation);
            }
        };

        $scope.isCancelled = function (reservation) {
            if (typeof reservation !== "undefined" && reservation !== null) {
                return reservationHandler.isCancelled(reservation);
            }
        };

        $scope.isPaid = function (reservation) {
            return !reservationHandler.isCancelled(reservation) && reservationHandler.isPaid(reservation);
        };

        $scope.isNotPaid = function (reservation) {
            return !reservationHandler.isCancelled(reservation) && reservationHandler.isNotPaid(reservation);
        };

        $scope.isPartiallyPaid = function (reservation) {
            return !reservationHandler.isCancelled(reservation) && reservationHandler.isPartiallyPaid(reservation);
        };

        $scope.isConfirmed = function (reservation) {
            return !reservationHandler.isCancelled(reservation) && reservationHandler.isConfirmed(reservation);
        };

        $scope.isNotConfirmed = function (reservation) {
            return !reservationHandler.isCancelled(reservation) && reservationHandler.isNotConfirmed(reservation);
        };

        $scope.wasPaymentMadeByCard = function (payment) {
            return payment.PaymentMethod === paymentEnumService.paymentMethod.card;
        };

        $scope.wasPaymentMadeByCheck = function (payment) {
            return payment.PaymentMethod === paymentEnumService.paymentMethod.check;
        };

        $scope.wasPaymentMadeByCash = function (payment) {
            return payment.PaymentMethod === paymentEnumService.paymentMethod.cash;
        };

        $scope.wasPaymentMadeByDeposit = function (payment) {
            return payment.PaymentMethod === paymentEnumService.paymentMethod.directDeposit;
        };

        $scope.wasPaymentRegular = function (payment) {
            return payment.PaymentType === paymentEnumService.paymentType.regular;
        };

        $scope.wasPaymentTip = function (payment) {
            return payment.PaymentType === paymentEnumService.paymentType.tip;
        };

        $scope.bookNewReservation = function () {
            $scope.creatingNewReservation = true;
            $scope.customerSelected = false;

            $scope.selectedReservation = new Reservation();
            $scope.selectedReservation.Fee = 0;
            $scope.selectedReservation.AgencyOwnerId = sessionService.sessionData.agencyId;

            $scope.getPickUpPlaces($scope.selectedReservation.PickUpProvinceId);
            $scope.getReturnPlaces($scope.selectedReservation.ReturnProvinceId);
        };

        $scope.findCustomer = function () {
            customerService.getCustomerMatchingPhoneNumberPattern($scope.selectedReservation.Customer.CellPhone)
                .then(function (existingCustomers) {
                    if (existingCustomers !== null && existingCustomers !== undefined && existingCustomers.length !== 0) {
                        openCustomersModal(existingCustomers);
                    }
                    $scope.customerSelected = true;
                });
        };

        $scope.cancelReservationChanges = function () {
            $scope.creatingNewReservation = false;
            $scope.submitReservationButtonClicked = false;
            $scope.reservationForm.$setPristine();

            if ($scope.creatingNewReservation) {
                $scope.selectedReservation = angular.copy($scope.selectedReservation.$original);
            } else
                $scope.selectedReservation = null;
        };

        $scope.submitReservation = function () {
            $scope.submitReservationButtonClicked = true;

            if ($scope.reservationForm.$valid && (!$scope.paymentForm.$dirty || $scope.paymentForm.$valid)) {
                $scope.submitReservationButtonClicked = false;
                saveReservation();
            }
        };

        $scope.getReservationPayments = function (reservationId) {
            lookUpDataService.paymentsByReservation(reservationId).then(onFetchingPaymentsComplete);
        };

        $scope.selectPayment = function (payment) {
            $scope.selectedPayment = payment;
        };

        $scope.createPayment = function (reservation) {
            $scope.creatingNewPayment = true;
            $scope.selectedPayment = new Payment(reservation);
            $scope.selectedPayment.PaymentType = 1;
        };

        $scope.cancelPayment = function () {
            $scope.creatingNewPayment = false;
            $scope.paymentForm.$setPristine();
            $scope.selectedPayment = null;
            $scope.addPaymentClicked = false;
        };

        $scope.addNewPayment = function () {
            if ($scope.selectedReservation.RemainingPaymentsAmount < $scope.selectedPayment.PaymentAmount && parseInt($scope.selectedPayment.PaymentType) === paymentEnumService.paymentType.regular) {
                toastr.error("No se puede procesar el pago porque está pagando mas de lo que debe", "Procesando pago");
                return;
            }

            if (($scope.selectedReservation.RemainingPaymentsAmount > 0) && (parseInt($scope.selectedPayment.PaymentType) === paymentEnumService.paymentType.tip)) {
                toastr.error("No se procesará propina hasta que la reserva esté completamente pagada", "Procesando pago");
                return;
            }

            if (!($scope.selectedPayment.PaymentMethod > 0)) {
                toastr.error("Seleccione forma de pago", "Procesando pago");
                return;
            }

            if (!($scope.selectedPayment.PaymentType > 0)) {
                toastr.error("Seleccione tipo de pago", "Procesando pago");
                return;
            }

            $scope.addPaymentClicked = true;
            if ($scope.paymentForm.$valid) {
                paymentService.add($scope.selectedPayment).then(function (payment) {
                        $scope.selectedPayment.PaymentId = payment.PaymentId;
                        $scope.selectedReservation.ModifiedOn = payment.PaymentDate;
                        reservationHandler.addPaymentToReservation($scope.selectedReservation, $scope.selectedPayment);
                        var paymentTotal = $scope.selectedReservation.Payments !== null ? $scope.selectedReservation.Payments.length : 0;
                        $scope.selectedReservation.Payments[paymentTotal] = $scope.selectedPayment;
                        $scope.creatingNewPayment = false;
                        $scope.addPaymentClicked = false;
                        toastr.success("Hecho", "Pago");
                    }, function () {
                        toastr.error("Hubo un error adicionando el pago. Por favor, contacte Fructosoft", "Pagos");
                    }
                );
            }
        };

        $scope.deletePayment = function (payment) {
            paymentService.remove(payment).then(function (value) {
                reservationHandler.removePaymentFromReservation($scope.selectedReservation, payment);
                var index = $scope.selectedReservation.Payments.indexOf(payment);
                $scope.selectedReservation.Payments.splice(index, 1);
                $scope.selectedPayment = null;
                $scope.selectedReservation.ModifiedOn = value;
                toastr.success("Hecho", "Pagos");
            });
        };

        function canRetrievePrice() {
            if (typeof $scope.selectedReservation.TourOperatorId !== "number" ||
                typeof $scope.selectedReservation.CarCategoryId !== "number" ||
                typeof $scope.selectedReservation.StartDate === "undefined" ||
                typeof $scope.selectedReservation.EndDate === "undefined" ||
                $scope.selectedReservation.EndDate === null) {
                return false;
            }

            var oneDay = 24 * 60 * 60 * 1000;
            var dayDiff = Math.round(($scope.selectedReservation.EndDate - $scope.selectedReservation.StartDate) / oneDay) + 1;

            if (dayDiff >= 30) {
                toastr.error("No puede crear reservación de mas de 29 días", "Error");
                return false;
            }

            return true;
        }

        $scope.clearPrices = function() {
            $scope.selectedReservation.AgencyBasePrice = 0;
            $scope.selectedReservation.TourOperatorBasePrice = 0;
            $scope.selectedReservation.TourOperatorTotalPrice = 0;
            $scope.selectedReservation.RemainingPaymentsAmount = 0;
            $scope.selectedReservation.AgencyTotalPrice = 0;
        };

        $scope.retrievePrice = function () {
            if (canRetrievePrice()) {
                if ($scope.selectedReservation.StartDate >= $scope.selectedReservation.EndDate) {
                    toastr.error("Intervalo de fecha inválido", "Fechas de la reservación");
                } else {
                    reservationHandler.assignTotalDaysToReservation($scope.selectedReservation);
                    lookUpDataService.getPrice($scope.selectedReservation.TourOperatorId,
                        $scope.selectedReservation.CarCategoryId,
                        $scope.selectedReservation.StartDate,
                        $scope.selectedReservation.EndDate).then(function (price) {
                            if (price.PriceId === 0) {
                                $scope.clearPrices();

                                toastr.error("Precio no configurado! ", "Precios");
                            } else {
                                $scope.selectedReservation.TourOperatorBasePrice = price.TourOperatorBasePrice;
                                $scope.selectedReservation.TourOperatorTotalPrice = price.TourOperatorTotalPrice;
                                $scope.selectedReservation.AgencyBasePrice = price.AgencyBasePrice;
                                $scope.selectedReservation.AgencyTotalPrice = price.AgencyTotalPrice;
                                var remainingPaymentAmount = $scope.selectedReservation.AgencyTotalPrice - $scope.selectedReservation.TotalPaid;

                                $scope.selectedReservation.RemainingPaymentsAmount = remainingPaymentAmount < 0 ? 0 : remainingPaymentAmount;
                            }
                        }
                    );

                    if ($scope.reservationForm.$dirty) {
                        $scope.verifyTourOperator();
                    }
                }
            }
            else {
                $scope.clearPrices();
            }
        };

        $scope.verifyTourOperator = function () {
            if (typeof $scope.tourOperators === "undefined" || $scope.tourOperators === null) {
                return;
            }

            var tourOperator = $scope.firstOrDefault($scope.tourOperators.filter(function(t){
                return t.TourOperatorId === $scope.selectedReservation.TourOperatorId}));
            
            if(tourOperator !== null && tourOperator.HasQuota) {
                lookUpDataService.getQuotaStatus(
                    $scope.selectedReservation.StartDate, 
                    $scope.selectedReservation.EndDate,
                    $scope.selectedReservation.CarCategoryId, 
                    $scope.selectedReservation.TourOperatorId)
                    .then(function(quotaStatus){
                        if (quotaStatus === 10 || quotaStatus === 20) {
                            toastr.warning("Advertencia de Cupo ", "Por favor, tenga en cuenta que ha agotado el cupo para este " + (quotaStatus === 10 ? "dia" : "mes")  + ".");
                        }
                    });
            }
        };

        $scope.firstOrDefault = function(elements) {
            if (elements !== null && elements !== undefined && elements.length > 0) {
                return elements[0];
            }
            return null;
        };

        /*@END PAYMENTS */

        $scope.getReservationFinalPrice = function (reservation) {
            if (typeof reservation === "undefined" || reservation === null)
                return 0;

            return reservationHandler.getReservationFinalPrice(reservation);
        };

        $scope.cancelOrActivateReservation = function () {
            $scope.cancelOrActivateButtonPressed = true;
            if (!reservationHandler.isCancelled($scope.selectedReservation)) {
                reservationHandler.cancelReservation($scope.selectedReservation);
                $scope.disableAllForms = true;

            } else {
                reservationHandler.activateReservation($scope.selectedReservation);
                $scope.disableAllForms = false;
            }
            saveReservation();
        };

        $scope.confirmReservation = function () {
            reservationHandler.confirm($scope.selectedReservation);
            $scope.reservationConfirmed = true;
        };

        $scope.activateTab = function (tabName) {
            $scope.activeReservationTab = tabName;
            $scope.selectedReservation = null;
            switch (tabName) {
                case "activeReservationTab":
                    reservationService.loadReservations(false).then(onFetchingActiveReservationsComplete);
                    break;
                case "inactiveReservationTab":
                    reservationService.loadReservations(true).then(onFetchingInactiveReservationsComplete);
                    break;
                case "externalReservationTab":
                    toastr.error("Not Implemented Yes!" + error, "Reservaciones Externas");
                    break;
            }
        };

        $scope.FetchCustomers = function (pattern) {
            customerService.getCustomerMatchingPhoneNumberPattern(pattern).then(function (data) {
                $scope.matchingCustomers = data;
            });
        };

        $scope.selectingCustomer = function (result) {
            $scope.selectedReservation.Customer = result.originalObject;
            $scope.customerSelected = true;
        };

        $scope.prepareForNewCustomer = function (customer) {
            customer.CustomerId = 0;
        };

        $scope.RefreshReservationsList = function () {
            $scope.activateTab("activeReservationTab");
        };

        $scope.emailVoucher = function () {
            emailService.emailVoucher($scope.selectedReservation.ReservationId).then(function () {
                toastr.info("Voucher", "El voucher fue enviado");
            });
        };

        $scope.printVoucher = function () {
            reportGeneratorService.generateVoucher($scope.selectedReservation.ReservationId);
        };

        $scope.DontSaveCurrentReservationChanges = function () {
            //if (typeof $scope.selectedReservation.$original !== "undefined" && $scope.selectedReservation.$original !== null) {
            $scope.selectedReservation = angular.copy($scope.selectedReservation.$original, $scope.selectedReservation);
            $scope.reservationForm.$setPristine();
            //}

            confirmationModal.hide();
        };

        $scope.SaveCurrentReservationChanges = function () {
            delete $scope.selectedReservation.$original;
            confirmationModal.hide();
            saveReservation();
        };

        $scope.okCancelReservationModal = function () {
            reservationCancellationModal.hide();
            $scope.cancelOrActivateReservation();
        };

        $scope.cancelCancelReservationModal = function () {
            reservationCancellationModal.hide();
        };

        $scope.canToggleMayPrintVoucher = function () {
            return typeof ($scope.selectedReservation) !== "undefined" &&
                   $scope.selectedReservation != null &&
                   sessionService.sessionData.agencyId !== $scope.selectedReservation.AgencyOwnerId &&
                   !$scope.creatingNewReservation &&
                    $scope.selectedReservation.MayPrintVoucher;
        };

        $scope.toggleMayPrintVoucher = function () {
            reservationService.allowPrintVoucher($scope.selectedReservation.ReservationId, !$scope.selectedReservation.AllowRetailerToPrintVoucher).then(function(){
                toastr.info("Acción satisfactoriamente realizada", "Reserva");
            }), function error (response) {
                toastr.error("Ha ocurrido un error. Por favor, contactar Fructosoft", "Reserva");
            };
            $scope.selectedReservation.AllowRetailerToPrintVoucher = !$scope.selectedReservation.AllowRetailerToPrintVoucher;

        };

        $scope.submitVoucher = function () {
            if ($scope.reservationForm.customerEmail.$dirty ||
                $scope.reservationForm.customerFirstName.$dirty ||
                $scope.reservationForm.customerLastName.$dirty) {
                toastr.error("Requerido salvar los cambios hechos antes de enviar el voucher", "Enviar voucher");
                return;
            }

            if (typeof $scope.selectedReservation.Customer.Email === "undefined" || $scope.selectedReservation.Customer.Email === null || $scope.selectedReservation.Customer.Email === "") {
                toastr.error("Customer no tiene email definido", "Enviar Voucher");
                return;
            }

            if ($scope.isNotPaid($scope.selectedReservation) || $scope.isNotConfirmed($scope.selectedReservation)) {
                emailVoucherconfirmationModal.show();
            } else {
                $scope.emailVoucher();
            }
        };

        $scope.showReservationHistory = function () {
            reportGeneratorService.generateReservationHistory($scope.selectedReservation.ReservationId);
        };

        $scope.$watch(function () {
            $("#reservationList").css("height", $("#reservationDetails").height() - 217);
            $("#paymentList").css("height", $("#reservationPanelDetail").height() - 365);
        });

        $scope.getPaymentMethod = function (payment) {
            var result = "";
            var paymentMethodId = parseInt(payment.PaymentMethod);

            switch (paymentMethodId) {
                case paymentEnumService.paymentMethod.card:
                    result = "Tarjeta";
                    break;
                case paymentEnumService.paymentMethod.cash:
                    result = "Efectivo";
                    break;
                case paymentEnumService.paymentMethod.check:
                    result = "Cheque";
                    break;
                case paymentEnumService.paymentMethod.directDeposit:
                    result = "Depósito";
                    break;
            }

            return result;
        };

        $scope.getPaymentType = function (payment) {
            var result = "";
            var paymentTypeId = parseInt(payment.PaymentType);

            switch (paymentTypeId) {
                case paymentEnumService.paymentType.regular:
                    result = "Regular";
                    break;
                case paymentEnumService.paymentType.tip:
                    result = "Propina";
                    break;
            }

            return result;
        };

        $scope.getReservationStatusPaymentWise = function (reservation) {
            if (reservationHandler.isCancelled(reservation))
                return "cancelada";

            var result = "";

            if (reservationHandler.isPaid(reservation))
                result = "pagada";
            else if (reservationHandler.isPartiallyPaid(reservation))
                result = "parcialmente pagada";
            else
                result = "no pagada";

            return result;
        };

        $scope.getReservationStatusConfirmationWise = function (reservation) {
            if (reservationHandler.isCancelled(reservation))
                return "cancelada";

            return reservationHandler.isConfirmed(reservation) ? "confirmada" : "no confirmada";
        };

        $scope.search = function (reservation) {
            if (typeof $scope.searchText === "undefined" || $scope.searchText === null || $scope.searchText === "")
                return true;

            var query = $scope.searchText.toLowerCase();
            var fullName = reservation.Customer.FirstName.toLowerCase() + " " + reservation.Customer.LastName.toLowerCase();

            return fullName.indexOf(query) !== -1;
        };

        $scope.sendEmailDirectly = function(){
           $scope.directEmailTo.reservationId = $scope.selectedReservation.ReservationId;
           emailService.emailVoucherDirectly($scope.directEmailTo).then(function () {
                toastr.info("Reserva", "La Informacion de Reserva fue enviada a:  " + $scope.directEmailTo.emailAddress );
                $scope.directEmailTo.emailAddress = "";
            }, function error (response) {
                toastr.error("Error mandando informacion de reserva a " + $scope.directEmailTo.emailAddress + ".", "Reserva");
                $scope.directEmailTo.emailAddress = "";
            });
        };

        $scope.multipleEmailsPattern = (function() {
            var regexp = /^[\W]*([\w+\-.%]+@[\w\-.]+\.[A-Za-z]{2,4}[\W]*;{1}[\W]*)*([\w+\-.%]+@[\w\-.]+\.[A-Za-z]{2,4})[\W]*$/;
            return {
                test: function(value) {
                   return regexp.test(value);
                }
            };
        })();

        $scope.setupRetailerMemoEnviroment = function(){
            var retailer = $scope.retailers.filter(function(retailer){
                return retailer.Identifier === $scope.selectedReservation.AgencyOwnerId;
            }).map(function(retailer){
                return retailer;
            })[0];

            var tourOperator = $scope.tourOperators.filter(function(tourOperator){
                return tourOperator.TourOperatorId === $scope.selectedReservation.TourOperatorId;
            }).map(function(tourOperator){
                return tourOperator;
            })[0];

            $scope.selectedRetailerName = retailer.Name;
            $scope.showRetailerMemo = !tourOperator.isCuban && $scope.selectedReservation.IsForeignReservation;
        };

        $scope.setupWholesalerEnviroment = function(){
            var wholesaler = $scope.tourOperators.filter(function(tourOperator){
                return tourOperator.TourOperatorId === $scope.selectedReservation.TourOperatorId;
            }).map(function(tourOperator){
                return tourOperator;
            })[0];

            $scope.showWholesalerMemo = !wholesaler.IsCuban;
            $scope.wholesalerName = wholesaler.Name;
        };
    };

    angular.module("carReservationsApp").controller("reservationController", ["$scope", "$modal", "$location", "reservationService", "reservationStatusEnumService", "paymentEnumService", "reservationHandler", "lookUpDataService", "session", "customerService", "paymentService", "emailService", "reportGeneratorService", reservationController]);
}());





