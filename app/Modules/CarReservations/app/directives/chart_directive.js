﻿//# sourceURL=chart_directive.js
(function () {
    var carReservationApp = angular.module('carReservationsApp');

    carReservationApp.directive('chart', function () {
        return {
            restrict: 'E',
            replace: true,
            template: "<div class='col-lg-12' style='height:400px'></div>",
            controller: "dashboardController",
            scope: {
                divname: "="
            }
        }
    });
}());

