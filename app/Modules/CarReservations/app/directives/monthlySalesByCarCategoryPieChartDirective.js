﻿//# sourceURL=monthlySalesByCarCategoryPieChartDirective.js
(function () {
    var carReservationApp = angular.module('carReservationsApp');

    carReservationApp.directive('pieChartMonthlySales', function () {
        return {
            restrict: 'E',
            replace: true,
            template: "<div class='col-lg-12' style='height:300px'></div>",
            controller: "monthlySalesByCarCategoryPieChartController",
            scope: {
                containerId: '=',
                algo: '@'
            }
        }
    });
}());
