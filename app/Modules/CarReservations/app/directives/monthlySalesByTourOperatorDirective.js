﻿//# sourceURL=monthlySalesByTourOperatorDirective.js
(function () {
    var carReservationApp = angular.module('carReservationsApp');

    carReservationApp.directive('monthlySalesByTourOperator', function () {
        return {
            restrict: 'E',
            replace: true,
            template: "<div class='col-lg-12' style='height:300px'></div>",
            controller: "monthlySalesByTourOperatorController",
            scope: {
                containerId: '='
            }
        }
    });
}());
