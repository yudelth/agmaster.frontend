﻿//# sourceURL=serialChartDirective.js
(function () {
    var carReservationApp = angular.module('carReservationsApp');

    carReservationApp.directive('serialChartMonthlySales', function () {
        return {
            restrict: 'E',
            replace: true,
            template: "<div class='col-lg-12' style='height:300px'></div>",
            controller: "monthlySalesSerialChartController",
            scope: {
                containerId: '=',
                algo: '@'
            }
        }
    });
}());
