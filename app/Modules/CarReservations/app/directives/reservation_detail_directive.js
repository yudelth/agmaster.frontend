﻿//# sourceURL=reservation_detail_directive.js


(function () {
    var carReservationApp = angular.module('carReservationsApp');

    carReservationApp.directive('reservation_detail', function() {
        var directive = {};

        directive.restrict = 'E';
        directive.template = '<div> This is a reservation {{4*5}}</div>';

        return directive;
    });
}());