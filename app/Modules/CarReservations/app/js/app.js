﻿//# sourceURL=app.js

var app = angular.module("carReservationsApp", [
    "ngCookies",
    "ngRoute",
    "ngSanitize",
    "mgcrea.ngStrap",
    "angucomplete-alt",
    "angularUtils.directives.dirPagination"
]);
app.config(["$routeProvider", "$locationProvider", "$httpProvider", "$sceDelegateProvider", "$provide",
    function ($routeProvider, $locationProvider, $httpProvider, $sceDelegateProvider, $provide) {

        //Enable CORS
        $sceDelegateProvider.resourceUrlWhitelist([
                 "self",
                 "**"]);

        // configure html5 to get links working on jsfiddle
        //$locationProvider.html5Mode(true);

        //Enable cross domain calls
        $httpProvider.defaults.useXDomain = true;

        //Remove the header used to identify ajax call  that would prevent CORS from working
        delete $httpProvider.defaults.headers.common["X-Requested-With"];

        app.register =
           {
               factory: $provide.factory,
               service: $provide.service,
               constant: $provide.constant,
               decorator: $provide.decorator
           };

        app.register.service("session", sessionService);
        app.register.service("LoggingSrvc", loggingService);

        //Overriding $log service
        app.register.decorator("$log", ["$delegate", "LoggingSrvc", function ($delegate, LoggingSrvc) {
            return LoggingSrvc($delegate);
        }]);

        $routeProvider
          .when("/reservations", {
              templateUrl: "../Modules/CarReservations/app/views/reservations.html",
              controller: ""
          })
          .when("/dashboard", {
              templateUrl: "../Modules/CarReservations/app/views/dashboard.html",
              controller: ""
          })
          .when("/configuration", {
              templateUrl: "../Modules/CarReservations/app/views/configuration.html",
              controller: ""
          })
          .when("/customers", {
              templateUrl: "../Modules/CarReservations/app/views/customers.html"
          })
          .when("/reservations/:reservation_id", {
              templateUrl: "../Modules/CarReservations/app/views/reservations.html",
              controller: ""
          })
            .when("/reports", {
                templateUrl: "../Modules/CarReservations/app/views/reports.html",
                controller: ""
            })
          .when("/alert", {
              templateUrl: "../Modules/CarReservations/app/views/alerts.html",
              controller: ""
            })             
          .otherwise({
              redirectTo: "/dashboard"
          });

        $provide.factory("carReservationHttpInterceptor", ["$q", function ($q) {
            return {
                'request': function (config) {
                    if (!(config !== undefined && config.url !== undefined
                        && (config.url.indexOf('/api/Alerts/') > -1 || ( config.url.indexOf('api/Customers?pattern=') > -1 )))) {
                        $("#loadingAnimation").addClass("waiting-to-show");
                        setTimeout(function() {
                             if($("#loadingAnimation").hasClass("waiting-to-show")){
                                 $("#loadingAnimation").removeClass("waiting-to-show hidden").fadeIn(400);
                             }
                        }, 400);
                    }
                    return config;
                },

                'requestError': function (rejection) {
                    return $q.reject(rejection);
                },

                'response': function (response) {
                    $("#loadingAnimation").removeClass("waiting-to-show");
                    $("#loadingAnimation").addClass("hidden").fadeOut(400);
                    return response;
                },

                'responseError': function (rejection) {
                     $("#loadingAnimation").removeClass("waiting-to-show");
                     $("#loadingAnimation").addClass("hidden").fadeOut(400);

                    if (rejection.status === 440) {
                        toastr.error("Intente de nuevo", "Su sesion ha expirado!");
                        setTimeout(function () { containerLogout(); }, 2000);
                    }
                    return $q.reject(rejection);
                }
            };
        }]);

        $httpProvider.interceptors.push("carReservationHttpInterceptor");
    }]);