﻿//# sourceURL=car_reservation_charts.js
$(document).ready(function () {
    AmCharts.ready(function () {

        var chartData = [{
            "month": "Enero",
            "sales": 4252
        }, {
            "month": "Febrero",
            "sales": 1882
        }, {
            "month": "Marzo",
            "sales": 1809
        }, {
            "month": "Abril",
            "sales": 1322
        }, {
            "month": "Mayo",
            "sales": 1122
        }, {
            "month": "Junio",
            "sales": 1114
        }, {
            "month": "Julio",
            "sales": 984
        }, {
            "month": "Agosto",
            "sales": 711
        }, {
            "month": "Septiembre",
            "sales": 665
        }, {
            "month": "Octubre",
            "sales": 580
        }, {
            "month": "Noviembre",
            "sales": 443
        }, {
            "month": "Diciembre",
            "sales": 441
        }];

        var chart = new AmCharts.AmSerialChart();
        chart.dataProvider = chartData;
        chart.categoryField = "month";
        var graph = new AmCharts.AmGraph();
        graph.valueField = "sales";
        graph.type = "column";
        chart.addGraph(graph);
        chart.addGraph('salesByMonth');
    });
});
