﻿//# sourceURL=customer.js

function Customer(firstName, lastName, cellPhone, emailAddress) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.cellPhone = cellPhone;
    this.emailAddress = emailAddress;
}