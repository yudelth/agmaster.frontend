﻿//# sourceURL=reservation.js

var Reservation = function() {
    this.ReservationId = 0;
    this.Customer = new Customer("Nombre", "Apellido", "Teléfono", "Dirección electrónica");
    this.StartDate = null;
    this.PickupTime = null;
    this.EndDate = null;
    this.TotalDays = 0;
    this.FlightNumber = null;
    this.ModifiedBy = "";
    this.CarProviderId = null;
    this.TourOperatorId = null;
    this.CarCategoryId = null;
    this.Memo = "";
    this.PickUpProvinceId = 3;
    this.PickUpLocationId = null;
    this.ReturnProvinceId = 3;
    this.ReturnLocationId = null;
    this.AgencyBasePrice = 0;
    this.AgencyTotalPrice = 0;
    this.TourOperatorBasePrice = 0;
    this.TourOperatorTotalPrice = 0;
    this.Discount = 0;
    this.TotalPaid = 0;
    this.RemainingPaymentsAmount = 0;
    this.Payments = [];
};



