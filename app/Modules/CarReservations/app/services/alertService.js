/**
 * Created by ytchicamboud on 11/16/2015.
 */
//# sourceURL=alertService.js
"use strict";
angular.module("carReservationsApp")
    .service("alertService", ["$http", "$q", "session", function alertService($http, $q, sessionService) {

        function executeRequest(method, entity) {
            var deferred = $q.defer();
            var url = configuration.serviceBaseAddress + "/Alerts/";

            $http({
                method: method,
                url: url,
                headers: sessionService.getHeaders(),
                data: entity === null ? null : entity,
                contentType: "application/json; charset=utf-8"
            }).success(function (data, status, headers, cfg) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject(status);
            });

            return deferred.promise;
        };

        function getAlerts(){
            return executeRequest("GET", null);
        };

        var service = {
            getAlerts: getAlerts
        };

        return service;
    }
    ]);
