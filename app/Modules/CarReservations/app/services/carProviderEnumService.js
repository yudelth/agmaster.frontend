﻿//# sourceURL=carProviderEnumService.js

(function () {

    var carProviderEnumService = function () {

        return {
            types: {
                cubacar: 1,
                havanaAutos: 2,
                rex: 4,
                properties : {
                    1: { value: 1, name: "Cubacar" },
                    2: { value: 2, name: "Havana Autos" },
                    4: { value: 4, name: "Rex" }
                }
            }
        };
    };

    var module = angular.module("carReservationsApp");
    module.factory("carProviderEnumService", carProviderEnumService);
}());