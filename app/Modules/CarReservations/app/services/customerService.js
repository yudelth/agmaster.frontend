﻿//# sourceURL=customerService.js
"use strict";

angular.module("carReservationsApp")
    .service("customerService", ["$http", "$q", "session", function customerService($http, $q, sessionService) {

        function executeRequest(url, method, entity) {
            var deferred = $q.defer();
            url = configuration.serviceBaseAddress + "/" + url;

            $http({
                method: method,
                url: url,
                headers: sessionService.getHeaders(),
                data: entity === null ? null : entity,
                contentType: "application/json; charset=utf-8"
            }).success(function (data, status, headers, cfg) {
                data = data === "" ? true : data;
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject(false);
            });

            return deferred.promise;
        };

        function get() {
            return executeRequest("Customers", "GET", null);
        };

        function getCustomerMatchingPhoneNumberPattern(pattern, customerId) {
            var url = "Customers?pattern=" + pattern;
            url = url + (typeof customerId !== "undefined" ? "&customerId=" + customerId : "");

            return executeRequest(url, "GET", null);
        };

        function getCustomerReservations(customerId) {
            var url = "Customers/Reservations/" + customerId;

            return executeRequest(url, "GET", null);
        };

        function post(customer) {
            return executeRequest("Customers", "POST", customer);
        };

        function put(customer) {
            return executeRequest("Customers", "PUT", customer);
        };

        var service = {
            get: get,
            getCustomerMatchingPhoneNumberPattern: getCustomerMatchingPhoneNumberPattern,
            getCustomerReservations: getCustomerReservations,
            post: post,
            put: put
        };

        return service;
    }
]);