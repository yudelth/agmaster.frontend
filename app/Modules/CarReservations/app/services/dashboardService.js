﻿//# sourceURL=dashboardService.js
"use strict";

angular.module("carReservationsApp")
    .service("dashboardService", ["$http", "$q", "session", function dashboardService($http, $q, sessionService) {

        function getReservationsByMonth() {
            return getResult(configuration.serviceBaseAddress + "/" + "Dashboard/Reservations/Month");
        };
        
        function getPartiallyPaidReservations() {
            return getResult(configuration.serviceBaseAddress + "/" + "Dashboard/Reservations/PartiallyPaid");
        };

        function getMonthlySalesByCarCategory() {
            return getResult(configuration.serviceBaseAddress + "/" + "Dashboard/Reservations/CarCategory");
        };


        function getAgencyStatus() {
            return getResult(configuration.serviceBaseAddress + "/" + "Dashboard/AgencyStatus")
        }

        function getResult(url) {
            var deferred = $q.defer();

            $http({
                method: "GET",
                url: url,
                headers: sessionService.getHeaders(),
                contentType: "application/json; charset=utf-8",
            }).success(function (data, status, headers, cfg) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject(status);
            });

            return deferred.promise;
        }

        var service = {
            getReservationsByMonth: getReservationsByMonth,
            getPartiallyPaidReservations: getPartiallyPaidReservations,
            getMonthlySalesByCarCategory: getMonthlySalesByCarCategory,
            getAgencyStatus: getAgencyStatus
        };

        return service;
    }
    ]);