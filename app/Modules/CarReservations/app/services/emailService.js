/**
 * Created by ytchicamboud on 9/4/2015.
 */
//# sourceURL = emailService.js
"use strict";

angular.module("carReservationsApp")
    .service("emailService", ["$http", "$q", "session", function emailService($http, $q, sessionService) {

        function executeRequest(url, method, entity) {
            var deferred = $q.defer();
            var url = configuration.serviceBaseAddress + "/Emails/" + url;

            $http({
                method: method,
                url: url,
                headers: sessionService.getHeaders(),
                data: entity === null ? null : entity,
                contentType: "application/json; charset=utf-8"
            }).success(function (data, status, headers, cfg) {
                deferred.resolve(true);
            }).error(function (err, status) {
                deferred.reject(false);
            });

            return deferred.promise;
        };

        function emailVoucher(reservationId){
            return executeRequest(reservationId, "POST", null);
        };

        function emailVoucherDirectly (directEmailTo) {
            return executeRequest("", "POST", directEmailTo);
        }

        var service = {
            emailVoucher: emailVoucher,
            emailVoucherDirectly: emailVoucherDirectly
        };

        return service;
    }
    ]);

