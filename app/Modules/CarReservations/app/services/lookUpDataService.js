﻿//# sourceURL=lookUpDataService.js
"use strict";

angular.module("carReservationsApp")
    .service("lookUpDataService", ["$http", "$q", "session", "carProviderEnumService", "reservationStatusEnumService", function lookUpDataService($http, $q, sessionService, carProviderEnumService, reservationStatusEnumService) {

        var carProviders = [];

        function executeRequest(url, method, entity) {
            var deferred = $q.defer();
            url = configuration.serviceBaseAddress + "/" + url;

            $http({
                method: method,
                url: url,
                headers: sessionService.getHeaders(),
                data: entity === null ? null : entity,
                contentType: "application/json; charset=utf-8"
            }).success(function (data, status, headers, cfg) {
                data = data === "" ? true : data;
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject(false);
            });

            return deferred.promise;
        };

        function getCarProviders() {
            if (carProviders.length === 0) {
                var providers = carProviderEnumService.types;

                for (var index = 1; index <= 4; index++) {
                    if (providers.properties[index] !== undefined) {
                        carProviders.push({
                            CarProviderId: providers.properties[index].value,
                            Name: providers.properties[index].name
                        });
                    }
                }
            }

            return carProviders;
        }

        function getReservationStatuses(statusType){
            var statuses = reservationStatusEnumService.status;
            var result = new Array();

            if (statusType === "paymentStatus") {
                for (var index = 1; index <= 8; index++){
                    if (statuses.properties[index] !== undefined){
                        var value = statuses.properties[index].value;

                        if (value === 1 || value === 2 || value === 4 )
                            result.push({
                                StatusId: value,
                                Name: statuses.properties[index].name
                            });
                    }
                }
            }

            return result;
        };

        function getProvinces() {
            return executeRequest("Provinces/", "GET", null);
        };

        function pickUpLocationsByProvince(provinceId) {
            return executeRequest("PickUpLocations/Province/" + provinceId, "GET", null);
        };

        function carCategoriesByCarProvider(carProviderId) {
            return executeRequest("CarCategories/CarProvider/" + carProviderId, "GET", null);
        };

        function tourOperatorByCarProvider(carProviderId) {
            return executeRequest("TourOperators/CarProvider/" + carProviderId, "GET", null);
        };

        function getPrice(tourOperatorId, carCategoryId, startDate, endDate) {
            var startDateDateTime = new Date(startDate);
            var endDateDateTime = new Date(endDate);

            startDate = startDateDateTime.getFullYear() + "-" + parseInt(startDateDateTime.getMonth() + 1 ) + "-" + startDateDateTime.getDate();
            endDate = endDateDateTime.getFullYear() + "-" + parseInt( endDateDateTime.getMonth() + 1 ) + "-" + endDateDateTime.getDate();

            var url = "Prices";
            url += "/TourOperator/" + tourOperatorId;
            url += "/CarCategory/" + carCategoryId;
            url += "/StartDate/" + startDate;
            url += "/EndDate/" + endDate;

            return executeRequest(url, "GET", null);
        };

        function getPriceMatrix() {
            var url = "Prices";
            url += "/GetPriceMatrix";

            return executeRequest(url, "GET", null);
        };

        function getOnlinePriceMatrix() {
            var url = "Prices";
            url += "/GetOnlinePriceMatrix";

            return executeRequest(url, "GET", null);
        };

        function savePriceMatrix(priceMatrix) {
            var url = "Prices";
            url += "/SavePriceMatrix";

            return executeRequest(url, "POST", priceMatrix);
        };

        function saveOnlinePriceMatrix(priceMatrix) {
            var url = "Prices";
            url += "/SaveOnlinePriceMatrix";

            return executeRequest(url, "POST", priceMatrix);
        };

        function paymentsByReservation(reservationId) {
            return executeRequest("/Payments/" + reservationId, "GET", null);
        };

        function loadReservations() {
            return executeRequest("Reservations/", "GET", null);
        };

        function getTourOperators() {
            return executeRequest("TourOperators/", "GET", null);
        };

        function getSeasons() {
            return executeRequest("Seasons/", "GET", null);
        };

        function getIntervals() {
            return executeRequest("Intervals/", "GET", null);
        };

        function getPrices() {
            return executeRequest("Prices/", "GET", null);
        };

        function getCarCategories() {
            return executeRequest("CarCategories/", "GET", null);
        };

        function getCarCategoriesByTourOperator(tourOperatorId) {
            return executeRequest("CarCategories/TourOperator/" + tourOperatorId, "GET", null);
        };

        function postPrice(price) {
            return executeRequest("Prices", "POST", price);
        };

        function putPrice(price) {
            return executeRequest("Prices", "PUT", price);
        };

        function deletePrice(price) {
            return executeRequest("Prices/" + price.PriceId, "DELETE", null);
        };

        function postTourOperator(tourOperator) {
            return executeRequest("TourOperators", "POST", tourOperator);
        };

        function putTourOperator(tourOperator) {
            return executeRequest("TourOperators", "PUT", tourOperator);
        };

        function getTourOperatorByName(tourOperatorName) {
            return executeRequest("TourOperators?name=" + tourOperatorName, "GET", null);
        };

        function getActiveTourOperators() {
            return executeRequest("TourOperators?active=true", "GET", null);
        };

        function getPriceByConfiguration(price) {
            var queryString = "tourOperatorId=" + price.TourOperatorId;
            queryString += "&carCategoryId=" + price.CarCategoryId;
            queryString += "&seasonId=" + price.SeasonId;
            queryString += "&intervalId=" + price.IntervalId;

            return executeRequest("Prices?" + queryString, "GET", null);
        };

        function getTourOperatorById(tourOperatorId){
            return executeRequest("TourOperators/" + tourOperatorId, "GET", null);
        };

        function activeTourOperatorUsedToMakeReservationByCarProvider(carProviderId) {
            var url = "TourOperators?carProviderId=" + carProviderId;
            url += "&active=true";
            url += "&tobook=true";

            return executeRequest(url, "GET", null);
        };

        function getRetailers(){
            return executeRequest("Retailers", "GET", null);
        };

        function getPriceGroups(){
            return executeRequest("PriceGroups", "GET", null);
        };

        function getQuotaConfiguration() {
            var url = "Quotas";

            return executeRequest(url, "GET", null);
        };

        function saveQuotas(quotas) {
            var url = "Quotas/";
            return executeRequest(url, "PUT", quotas);
        };

        function getQuotaStatus(startDateTime, endDateTime, carCategoryId, tourOperatorId) {
            var startDate = new Date(startDateTime);
            var endDate = new Date(endDateTime);
            startDate = startDate.getFullYear() + "-" + parseInt(startDate.getMonth() + 1) + "-" + startDate.getDate();
            endDate = endDate.getFullYear() + "-" + parseInt(endDate.getMonth() + 1) + "-" + endDate.getDate();

            var url = "Quotas/Status";
            url += "/StartDate/" + startDate;
            url += "/EndDate/" + endDate;
            url += "/CarCategory/" + carCategoryId;
            url += "/TourOperator/" + tourOperatorId;

            return executeRequest(url, "GET", null);
        };

        function getTourOperatorsWithQuotas(hasQuota) {
            return executeRequest("TourOperators/HasQuota/" + hasQuota, "GET", null);
        }

        function getTotalDaysInMonth(year, month) {
            var deferred = $q.defer();
            var result = new Date(year, month, 0).getDate();
            deferred.resolve(result);

            return deferred.promise;
        }

        function getQuotaReport(month, year) {
            var url = "Quotas/QuotaReport";
            var actualMonth = month + 1;
            url += "/month/" + actualMonth + "/year/" + year;
          
            return executeRequest(url, "GET", null);
        }

        var service = {
            getCarProviders: getCarProviders,
            getProvinces: getProvinces,
            pickUpLocationsByProvince: pickUpLocationsByProvince,
            carCategoriesByCarProvider: carCategoriesByCarProvider,
            tourOperatorByCarProvider: tourOperatorByCarProvider,
            getPrice: getPrice,
            paymentsByReservation: paymentsByReservation,
            loadReservations: loadReservations,
            getTourOperators: getTourOperators,
            putTourOperator: putTourOperator,
            getTourOperatorByName: getTourOperatorByName,
            getTourOperatorById: getTourOperatorById,
            getActiveTourOperators: getActiveTourOperators,
            getSeasons: getSeasons,
            getIntervals: getIntervals,
            getPrices: getPrices,
            getCarCategories: getCarCategories,
            getCarCategoriesByTourOperator: getCarCategoriesByTourOperator,
            postPrice: postPrice,
            putPrice: putPrice,
            deletePrice: deletePrice,
            postTourOperator: postTourOperator,
            getPriceByConfiguration: getPriceByConfiguration,
            getReservationStatuses: getReservationStatuses,
            activeTourOperatorUsedToMakeReservationByCarProvider: activeTourOperatorUsedToMakeReservationByCarProvider,
            getRetailers: getRetailers,
            getPriceGroups: getPriceGroups,
            getPriceMatrix: getPriceMatrix,
            getOnlinePriceMatrix: getOnlinePriceMatrix,
            savePriceMatrix: savePriceMatrix,
            saveOnlinePriceMatrix: saveOnlinePriceMatrix,
            getQuotaConfiguration: getQuotaConfiguration,
            saveQuotas: saveQuotas,
            getQuotaStatus: getQuotaStatus,
            getTourOperatorsWithQuotas: getTourOperatorsWithQuotas,
            getTotalDaysInMonth: getTotalDaysInMonth,
            getQuotaReport: getQuotaReport
        };

        return service;
    }
]);