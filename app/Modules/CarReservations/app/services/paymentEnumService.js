﻿//# sourceURL=paymentEnumService.js

(function () {

    var paymentEnumService = function () {

        return {
            paymentType: {
                regular: 1,
                tip: 2
            },
            paymentMethod: {
                card: 1,
                check: 2,
                cash: 3,
                directDeposit: 4
            }
        };
    };

    var module = angular.module("carReservationsApp");
    module.factory("paymentEnumService", paymentEnumService);
}());