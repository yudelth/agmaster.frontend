/**
 * Created by ytchicamboud on 9/4/2015.
 */
//# sourceURL = paymentService.js
"use strict";

angular.module("carReservationsApp")
    .service("paymentService", ["$http", "$q", "session", function paymentService($http, $q, sessionService) {

        function executeRequest(url, method, entity) {
            var deferred = $q.defer();
            var url = configuration.serviceBaseAddress + "/Payments/" + url;

            $http({
                method: method,
                url: url,
                headers: sessionService.getHeaders(),
                data: entity === null ? null : entity,
                contentType: "application/json; charset=utf-8"
            }).success(function (data, status, headers, cfg) {
                deferred.resolve(data);
            }).error(function (err, status) {
                deferred.reject(status);
            });

            return deferred.promise;
        };

        function add(payment) {
            return executeRequest("", "POST", payment);
        };

        function remove(payment) {
            return executeRequest(payment.PaymentId, "DELETE", null);
        };

        var service = {
            add: add,
            remove: remove
        };

        return service;
    }
    ]);

