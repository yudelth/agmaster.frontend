"use strict";

angular.module("carReservationsApp")
.service("priceGroupsService", ["$http", "$q", "session", function priceGroupsService($http, $q, sessionService) {

    function executeRequest(url, method, entity) {
        var deferred = $q.defer();
        var url = configuration.serviceBaseAddress + "/PriceGroups/" + url;

        $http({
            method: method,
            url: url,
            headers: sessionService.getHeaders(),
            data: entity === null ? null : entity,
            contentType: "application/json; charset=utf-8"
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(status);
        });

        return deferred.promise;
    };

    function add(priceGroup) {
        return executeRequest("", "POST", priceGroup);
    };

    function remove(priceGroup) {
        return executeRequest(priceGroup.PriceGroupId, "DELETE", null);
    };

    function update(priceGroup) {
        return executeRequest("", "PUT", priceGroup);
    };

    var service = {
        add: add,
        remove: remove,
        update: update
    };

    return service;
}
]);
