/**
 * Created by ytchicamboud on 7/26/2015.
 */
//# sourceURL=reportGeneratorService.js
"use strict";

angular.module("carReservationsApp")
    .service("reportGeneratorService", ["$http", "$q", "session", function reportGeneratorService($http, $q, sessionService) {

        function formatDate(date) {
            var result = new Date(date);
            return result.getFullYear() + "-" + parseInt(result.getMonth() + 1) + "-" + result.getDate();
        }

        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, {type: contentType});
            return blob;
        }

        function showPdf(data) {
            var file = new Blob([data], { type: "application/pdf" });
            var fileUrl = URL.createObjectURL(file);
            window.open(fileUrl);
        };

        function generateReport(url) {
            $http({
                method: "GET",
                url: url,
                headers: sessionService.getHeaders(),
                contentType: "application/json; charset=utf-8",
                responseType: "arraybuffer"
            }).success(function (data, status, headers, cfg) {
                showPdf(data);
            }).error(function (err, status) {
                if (status === 401) {
                    toastr.error("Usted no esta autorizado a realizar esta accion.", "Reserva");
                }
                else {
                    toastr.error("Ha ocurrido un error. Por favor, contactar Fructosoft", "Reserva");
                }
            });
        };

        function generateSalesReport(from, to, tourOperatorId, statusId, retailerIdentifier) {
            var url = "Reports/Accounting";
            url += "?from=" + formatDate(from);
            url += "&to=" + formatDate(to);
            url += "&tourOperatorId=" + tourOperatorId;
            url += "&statusId=" + statusId;
            url += "&retailerIdentifier=" + retailerIdentifier;
            url = configuration.serviceBaseAddress + "/" + url;

            generateReport(url);
        }

        function generateSalesExcel(from, to, tourOperatorId, statusId, retailerIdentifier) {
            var url = "Reports/AccountingExcel";
            url += "?from=" + formatDate(from);
            url += "&to=" + formatDate(to);
            url += "&tourOperatorId=" + tourOperatorId;
            url += "&statusId=" + statusId;
            url += "&retailerIdentifier=" + retailerIdentifier;
            url = configuration.serviceBaseAddress + "/" + url;

            $http({
                method: "GET",
                url: url,
                headers: sessionService.getHeaders(),
                contentType: "application/json; charset=utf-8"
            }).success(function (data, status, headers, cfg) {

                var blob = b64toBlob(data, "text/csv;base64;");
                var fileName = "ReporteContable_" + formatDate(from) + ".csv";

                if (navigator.msSaveBlob) { // IE 10+
                    navigator.msSaveBlob(blob, fileName)
                } else {
                    var link = document.createElement("a");
                    if (link.download !== undefined) { // feature detection
                        // Browsers that support HTML5 download attribute
                        var url = URL.createObjectURL(blob);
                        link.setAttribute("href", url);
                        link.setAttribute("download", fileName);
                        link.style = "visibility:hidden";
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                    }
                }

            }).error(function (err, status) {

            });
        }

        function generateSalesLimitedReport(from, to, tourOperatorId, statusId) {
            var url = "Reports/LimitedAccounting";
            url += "?from=" + formatDate(from);
            url += "&to=" + formatDate(to);
            url += "&tourOperatorId=" + tourOperatorId;
            url += "&statusId=" + statusId;
            url = configuration.serviceBaseAddress + "/" + url;

            generateReport(url);
        }

        function generateActiveReservationsReport(from, to) {
            var url = "Reports/Reservations";
            url += "?from=" + formatDate(from);
            url += "&to=" + formatDate(to);
            url = configuration.serviceBaseAddress + "/" + url;

            generateReport(url);
        }

        function generateVoucher(reservationId) {
            var url = configuration.serviceBaseAddress + "/Reports/Voucher/" + reservationId;
            generateReport(url);
        }

        function generateSalesByOperatorReport(from, to) {
            var url = "Reports/Agents";
            url += "?from=" + formatDate(from);
            url += "&to=" + formatDate(to);
            url = configuration.serviceBaseAddress + "/" + url;

            generateReport(url);
        }

        function generateSalesByLocationReport(from, to, allLocations) {
            var url = "Reports/Locations";
            url += "?from=" + formatDate(from);
            url += "&to=" + formatDate(to);
            url += "&allLocations=" + allLocations;
            url = configuration.serviceBaseAddress + "/" + url;

            generateReport(url);
        }

        function generatePaymentByTypesReport(from, to) {

            var url = "Reports/Payments";
            url += "?from=" + formatDate(from);
            url += "&to=" + formatDate(to);
            url = configuration.serviceBaseAddress + "/" + url;

            generateReport(url);
        }

        function generateReservationHistory(reservationId) {
            var url = configuration.serviceBaseAddress + "/Reports/Reservations/History/" + reservationId;

            generateReport(url);
        }

        function generateSalesByCarCategoryReport(from, to, carCategoryId) {
            var url = "Reports/Reservations/CarCategory";
            url += "?from=" + formatDate(from);
            url += "&to=" + formatDate(to);
            url += "&carCategoryId=" + carCategoryId;
            url = configuration.serviceBaseAddress + "/" + url;

            generateReport(url);
        }

        var service = {
            generateSalesReport: generateSalesReport,
            generateSalesExcel: generateSalesExcel,
            generateActiveReservationsReport: generateActiveReservationsReport,
            generateVoucher: generateVoucher,
            generateSalesByOperatorReport: generateSalesByOperatorReport,
            generatePaymentByTypesReport: generatePaymentByTypesReport,
            generateReservationHistory: generateReservationHistory,
            generateSalesLimitedReport: generateSalesLimitedReport,
            generateSalesByCarCategoryReport: generateSalesByCarCategoryReport,
            generateSalesByLocationReport: generateSalesByLocationReport
        };

        return service;
    }
    ]);