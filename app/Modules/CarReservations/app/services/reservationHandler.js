﻿//# sourceURL=reservationHandler.js

(function () {
    "use strict";

    var reservationHandler = function (reservationStatusEnumService, paymentEnumService) {

        function updateReservationStatusPaymentWise(reservation) {
            reservation.ReservationStatus = reservation.ReservationStatus & (~reservationStatusEnumService.status.noPaid);
            reservation.ReservationStatus = reservation.ReservationStatus & (~reservationStatusEnumService.status.partiallyPaid);
            reservation.ReservationStatus = reservation.ReservationStatus & (~reservationStatusEnumService.status.paid);

            if (reservation.TotalPaid === 0)
                reservation.ReservationStatus |= reservationStatusEnumService.status.noPaid;
            else if (reservation.TotalPaid < reservation.FinalPrice)
                reservation.ReservationStatus |= reservationStatusEnumService.status.partiallyPaid;
            else
                reservation.ReservationStatus |= reservationStatusEnumService.status.paid;
        }

        var addPaymentToReservation = function (reservation, payment) {
            if (parseInt(payment.PaymentType) !== paymentEnumService.paymentType.tip) {
                reservation.RemainingPaymentsAmount = getReservationFinalPrice(reservation) - reservation.TotalPaid - payment.PaymentAmount;
                reservation.TotalPaid += payment.PaymentAmount;

                updateReservationStatusPaymentWise(reservation);
            }
        };

        var removePaymentFromReservation = function (reservation, payment) {

            if (parseInt(payment.PaymentType) === paymentEnumService.paymentType.regular) {
                reservation.RemainingPaymentsAmount = getReservationFinalPrice(reservation) - (reservation.TotalPaid - payment.PaymentAmount);
                reservation.TotalPaid -= payment.PaymentAmount;

                updateReservationStatusPaymentWise(reservation);
            }
        };

        var getReservationFinalPrice = function (reservation) {
            //calculating new final price and adjusting what's left to pay
            reservation.FinalPrice = reservation.AgencyTotalPrice + (reservation.TotalDays * reservation.Discount) + (reservation.Fee * 1);
            reservation.RemainingPaymentsAmount = reservation.FinalPrice - reservation.TotalPaid;

            if (reservation.FinalPrice !== reservation.FinalPrice) {
                return 0;
            }

            setPaymentStatus(reservation);

            return reservation.FinalPrice;
        };

        var assignTotalDaysToReservation = function (reservation) {
            var millisecondsPerDay = 1000 * 60 * 60 * 24;
            var startDate = new Date(reservation.StartDate.toString());
            var endDate = new Date(reservation.EndDate.toString());

            startDate = new Date(startDate.getFullYear(), parseInt(startDate.getMonth()), startDate.getDate());
            endDate = new Date(endDate.getFullYear(), parseInt(endDate.getMonth()), endDate.getDate());
            var millisBetween = endDate.getTime() - startDate.getTime();

            reservation.TotalDays = Math.round(millisBetween / millisecondsPerDay);
        };

        var setPaymentStatus = function (reservation) {
            //clear flags
            if ((reservation.ReservationStatus & reservationStatusEnumService.status.paid) === reservationStatusEnumService.status.paid) {
                reservation.ReservationStatus ^= reservationStatusEnumService.status.paid;
            }
            if ((reservation.ReservationStatus & reservationStatusEnumService.status.partiallyPaid) === reservationStatusEnumService.status.partiallyPaid) {
                reservation.ReservationStatus ^= reservationStatusEnumService.status.partiallyPaid;
            }
            if ((reservation.ReservationStatus & reservationStatusEnumService.status.noPaid) === reservationStatusEnumService.status.noPaid) {
                reservation.ReservationStatus ^= reservationStatusEnumService.status.noPaid;
            }

            //set new payment flags
            if (reservation.TotalPaid == 0) {
                reservation.ReservationStatus |= reservationStatusEnumService.status.noPaid
            }
            else if (reservation.TotalPaid >= reservation.FinalPrice) {
                reservation.ReservationStatus |= reservationStatusEnumService.status.paid
            }
            else {
                reservation.ReservationStatus |= reservationStatusEnumService.status.partiallyPaid
            }
        };

        var isPaid = function (reservation) {
            return reservation !== undefined
                && reservation != null
                && (reservation.ReservationStatus & reservationStatusEnumService.status.paid) === reservationStatusEnumService.status.paid;
        };

        var isNotPaid = function (reservation) {
            return (reservation.ReservationStatus & reservationStatusEnumService.status.noPaid) === reservationStatusEnumService.status.noPaid;
        };

        var isPartiallyPaid = function (reservation) {
            return (reservation.ReservationStatus & reservationStatusEnumService.status.partiallyPaid) === reservationStatusEnumService.status.partiallyPaid;
        };

        var isConfirmed = function (reservation) {
            return (reservation.ReservationStatus & reservationStatusEnumService.status.confirmed) === reservationStatusEnumService.status.confirmed;
        };

        var isNotConfirmed = function (reservation) {
            return (reservation.ReservationStatus & reservationStatusEnumService.status.noConfirmed) === reservationStatusEnumService.status.noConfirmed;
        };

        var isCancelled = function (reservation) {
            return reservation !== undefined
                && reservation != null
                && (reservation.ReservationStatus & reservationStatusEnumService.status.cancelled) === reservationStatusEnumService.status.cancelled;
        };

        var confirm = function (reservation) {
            if ((reservation.ReservationStatus & reservationStatusEnumService.status.noConfirmed) === reservationStatusEnumService.status.noConfirmed)
                reservation.ReservationStatus ^= reservationStatusEnumService.status.noConfirmed;

            reservation.ReservationStatus |= reservationStatusEnumService.status.confirmed;
        };

        var unconfirm = function (reservation) {
            if ((reservation.ReservationStatus & reservationStatusEnumService.status.confirmed) === reservationStatusEnumService.status.confirmed)
                reservation.ReservationStatus ^= reservationStatusEnumService.status.confirmed;

            reservation.ReservationStatus |= reservationStatusEnumService.status.noConfirmed;
        };

        var cancelReservation = function (reservation) {
            if ((reservation.ReservationStatus & reservationStatusEnumService.status.cancelled) !== reservationStatusEnumService.status.cancelled)
                reservation.ReservationStatus ^= reservationStatusEnumService.status.cancelled;
        };

        var activateReservation = function (reservation) {
            if ((reservation.ReservationStatus & reservationStatusEnumService.status.cancelled) === reservationStatusEnumService.status.cancelled)
                reservation.ReservationStatus ^= reservationStatusEnumService.status.cancelled;
        };

        var toggleActiveCancelledStatus = function (reservation) {
            reservation.ReservationStatus ^= reservationStatusEnumService.status.cancelled;
        };

        var getReadyToCreate = function (reservation) {
            reservation.RemainingPaymentsAmount = getReservationFinalPrice(reservation);
            assignTotalDaysToReservation(reservation);
            reservation.TotalPaid = 0;
        };

        return {
            addPaymentToReservation: addPaymentToReservation,
            removePaymentFromReservation: removePaymentFromReservation,
            getReservationFinalPrice: getReservationFinalPrice,
            assignTotalDaysToReservation: assignTotalDaysToReservation,
            isPaid: isPaid,
            isNotPaid: isNotPaid,
            isPartiallyPaid: isPartiallyPaid,
            isConfirmed: isConfirmed,
            isNotConfirmed: isNotConfirmed,
            isCancelled: isCancelled,
            confirm: confirm,
            unconfirm: unconfirm,
            toggleActiveCancelledStatus: toggleActiveCancelledStatus,
            getReadyToCreate: getReadyToCreate,
            cancelReservation: cancelReservation,
            activateReservation: activateReservation,
            setPaymentStatus: setPaymentStatus
        };
    };

    var module = angular.module("carReservationsApp");
    module.factory("reservationHandler", ['reservationStatusEnumService', "paymentEnumService", reservationHandler]);
}());
