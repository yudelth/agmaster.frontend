﻿//# sourceURL=reservationService.js
"use strict";
angular.module("carReservationsApp")
    .service("reservationService", ["$http", "$q", "session", function reservationService($http, $q, sessionService) {

        function executeRequest(url, method, entity) {
            var deferred = $q.defer();
            url = configuration.serviceBaseAddress + "/Reservations/" + url;

            $http({
                method: method,
                url: url,
                headers: sessionService.getHeaders(),
                data: entity === null ? null : entity,
                contentType: "application/json; charset=utf-8"
            }).success(function (data, status, headers, cfg) {
                deferred.resolve(data);
            }).error(function (err, status) {
                var errorMessage = "";
                if (status === 409)
                    errorMessage = "La reservación fue modificada por otro agente o proceso. Actualízela e intente salvarla nuevamente";
                else errorMessage = err;

                deferred.reject(errorMessage);
            });

            return deferred.promise;
        };

        function loadReservations(history) {
            return executeRequest("List/history=" + history, "GET", null);
        };

        function saveReservation(reservation) {
            return executeRequest("", "PUT", reservation);
        };

        function addReservation(reservation) {
            return executeRequest("", "POST", reservation);
        };

        function loadReservationById(reservationId) {
            return executeRequest("" + reservationId, "GET", null);
        };

        function updateUrgentNotification(reservationId, value) {
            return executeRequest("" + reservationId + "/UrgentNotification/" + value, "PUT", null);
        };

        function allowPrintVoucher(reservationId, value){
            return executeRequest("" + reservationId + "/PrintVoucher/" + value, "PUT", null);
        }

        var service = {
            loadReservations: loadReservations,
            saveReservation: saveReservation,
            addReservation: addReservation,
            loadReservationById: loadReservationById,
            updateUrgentNotification: updateUrgentNotification,
            allowPrintVoucher: allowPrintVoucher
        };

        return service;
    }
    ]);
