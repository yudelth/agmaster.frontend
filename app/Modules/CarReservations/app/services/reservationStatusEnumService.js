﻿//# sourceURL=reservationStatusEnumService.js

(function () {

    var reservationStatusEnumService = function () {

        return {            
            status: {
                unknown: 0,
                noPaid: 1,
                partiallyPaid: 2,
                paid: 4,
                confirmed: 8,
                noConfirmed: 16,
                cancelled: 32,
                properties : {
                    0: { value: 0, name: "unknown"},
                    1: { value: 1, name: "No Pagada"},
                    2: { value: 2, name: "Parcialmente Pagada"},
                    4: { value: 4, name: "Pagada"},
                    8: { value: 8, name: "Confirmada"},
                    16: { value: 16, name: "No Confirmada"},
                    32: { value: 32, name: "Cancelada"}
                }
            }
        };
    };

    var module = angular.module("carReservationsApp");
    module.factory("reservationStatusEnumService", reservationStatusEnumService);
}());