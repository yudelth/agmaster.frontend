"use strict";

angular.module("carReservationsApp")
.service("retailerService", ["$http", "$q", "session", function retailerService($http, $q, sessionService) {

    function executeRequest(url, method, entity) {
        var deferred = $q.defer();
        var url = configuration.serviceBaseAddress + "/Retailers/" + url;

        $http({
            method: method,
            url: url,
            headers: sessionService.getHeaders(),
            data: entity === null ? null : entity,
            contentType: "application/json; charset=utf-8"
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(status);
        });

        return deferred.promise;
    };

    function update(retailer) {
        return executeRequest("", "PUT", retailer);
    };

    var service = {
        update: update
        
    };

    return service;
}
]);
