//# sourceURL=sharingService.js
"use strict";
angular.module("carReservationsApp")
    .service("sharingService",
        [
            "$q", "lookUpDataService", function sharingService($q, lookUpDataService) {

                var tourOperatorsWithQuota = [];

                function getTourOperatorsWithQuotas() {
                    var deferred = $q.defer();

                    if (tourOperatorsWithQuota !== null && tourOperatorsWithQuota.length > 0) {
                        deferred.resolve(tourOperatorsWithQuota);
                    } else {
                        lookUpDataService.getTourOperatorsWithQuotas(true).then(function(data) {
                            tourOperatorsWithQuota = data;
                            deferred.resolve(data);
                        });
                    }

                    return deferred.promise;
                }

                var service = {
                    getTourOperatorsWithQuotas: getTourOperatorsWithQuotas
                };

                return service;
            }
        ]);
