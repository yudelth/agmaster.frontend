var gulp = require('gulp');
var useref = require('gulp-useref');
//var useref = require('node-useref');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var clean = require('gulp-clean');
var replace = require('gulp-replace');
var gutil = require('gulp-util');
var ftp = require( 'vinyl-ftp' );

/**
 * Demo
 */
 var user = 'agmasterDemo\\fructosoft';
 var password = 'Fruct0s0ft01';
 var host = 'waws-prod-blu-033.ftp.azurewebsites.windows.net';
 var localFiles = ['dist/**/*'];
 var remoteFolder = '/site/wwwroot/';
 var authenticationBasePath = 'Container/authentication/app/';
 var carReservationsModuleBasePath = 'Modules/CarReservations/app/';
 var administrationModuleBasePath = 'Modules/Administration/app/';
 var apiPath = 'agmasterservicedemo.azurewebsites.net';

/**
 * Production
 */
//var user = 'agmasternew\\fructosoft';
//var password = 'Fruct0s0ft01';
//var host = 'waws-prod-blu-059.ftp.azurewebsites.windows.net';
//var localFiles = ['dist/**/*'];
//var remoteFolder = '/site/wwwroot/';
//var authenticationBasePath = 'Container/authentication/app/';
//var carReservationsModuleBasePath = 'Modules/CarReservations/app/';
//var administrationModuleBasePath = 'Modules/Administration/app/';
//var apiPath = 'agmasterservice.azurewebsites.net';

/**Variable**/
var devApiPath =  'localhost:49269';

gulp.task('cleanDist', function (done) {
    return gulp.src('dist', {read: false, allowEmpty: true})
        .pipe(clean({force:true}));
    done();
});

gulp.task('copyImages', function() {
    return gulp.src([authenticationBasePath + 'css/images/**.{png,jpg,jpeg,gif}'])
        .pipe(gulp.dest("dist/" + authenticationBasePath + '/css/images'));
});

gulp.task('copyWebConfig', function() {
    return gulp.src(["web.config"])
        .pipe(gulp.dest("dist/"));
});

gulp.task('copyAuthenticationViews', function() {
    return gulp.src([authenticationBasePath + 'views/**/**.**'])
        .pipe(gulp.dest('dist/Container/authentication/app/views'));
});

gulp.task('doAuthenticationMinify', gulp.parallel('copyAuthenticationViews','copyImages', function(){
    return gulp.src([authenticationBasePath + 'main.html'])
        .pipe(useref({
            transformPath: function (filePath) {
                return filePath.replace('authentication/app/', '');
            }
        }))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(gulpif('*.js', uglify({mangle: false})))
        .pipe(replace('js/', 'authentication/app/js/'))
        .pipe(replace('css/', 'authentication/app/css/'))
        .pipe(gulp.dest('dist/' + authenticationBasePath));
}));

gulp.task('copyContainerFonts', function() {
    gulp.src(['Container/plugins/font-awesome/fonts/**/**.**'])
        .pipe(gulp.dest('dist/Container/fonts'));
    return gulp.src(['Container/plugins/bootstrap/fonts/**/**.**'])
        .pipe(gulp.dest('dist/Container/fonts'));
});

gulp.task('doContainerMinify', gulp.parallel('copyContainerFonts', 'doAuthenticationMinify', function(done) {
    gulp.src(['Container/index.html'])
        .pipe(useref())
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(gulpif('*.js', uglify({mangle: false})))
        .pipe(replace(devApiPath, apiPath))
        .pipe(gulp.dest('dist/Container'));
    done();
}));

gulp.task('copyCarReservationsModuleImages', function() {
    return gulp.src([carReservationsModuleBasePath + 'images/**.{png,jpg,jpeg,gif}'])
        .pipe(gulp.dest("dist/" + carReservationsModuleBasePath + '/images'));
});

gulp.task('copyCarReservationsModuleViews', function() {
    return gulp.src([carReservationsModuleBasePath + 'views/**/**.**'])
        .pipe(gulp.dest('dist/' + carReservationsModuleBasePath + 'views'));
});

gulp.task('doCarReservationsModuleMinify', gulp.parallel('copyCarReservationsModuleViews', 'copyCarReservationsModuleImages', function(){
    return gulp.src([carReservationsModuleBasePath + 'main.html'])
        .pipe(useref({
            transformPath: function (filePath) {
                return filePath.replace('../Modules/CarReservations/app/', '');
            }
        }))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(gulpif('*.js', uglify({mangle: false})))
        .pipe(replace('js/', '../' + carReservationsModuleBasePath + 'js/'))
        .pipe(replace('css/', '../' + carReservationsModuleBasePath + 'css/'))
        .pipe(gulp.dest('dist/' + carReservationsModuleBasePath));
}));

gulp.task('copyAdministrationModuleViews', function() {
    return gulp.src([administrationModuleBasePath + 'views/**/**.**'])
        .pipe(gulp.dest('dist/' + administrationModuleBasePath + 'views'));
});

gulp.task('doAdministrationModuleMinify', gulp.parallel('copyAdministrationModuleViews', function(){
    return gulp.src([administrationModuleBasePath + 'main.html'])
        .pipe(useref({
            transformPath: function (filePath) {
                return filePath.replace('../Modules/Administration/app/', '').replace('../Modules/Administration/', '../');
            }
        }))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(gulpif('*.js', uglify({mangle: false})))
        .pipe(replace('js/', '../' + administrationModuleBasePath + 'js/'))
        .pipe(replace('css/', '../' + administrationModuleBasePath + 'css/'))
        .pipe(gulp.dest('dist/' + administrationModuleBasePath));
}));

gulp.task('doModulesMinify', gulp.series('doAdministrationModuleMinify','doCarReservationsModuleMinify'));

gulp.task('doAgmaster', gulp.series('cleanDist', 'copyWebConfig', 'doContainerMinify','doModulesMinify'));

function getFtpConnection() {
    return ftp.create({
        host: host,
        user: user,
        password: password,
        log: gutil.log
    });
};

gulp.task('deployProd', gulp.series('doAgmaster', function(done) {
    var conn = getFtpConnection();
    conn.rmdir( remoteFolder + 'Container',  function(){
        conn.rmdir( remoteFolder + 'Modules',  function(){
            gulp.src(localFiles, { base: 'dist', buffer: false })
                .pipe( conn.newer( remoteFolder ) ) // only upload newer files
                .pipe( conn.dest( remoteFolder ) );
            done();
        });
    });
}));