﻿describe("Login", function () {
    var username = element(by.id("userName"));
    var password = element(by.id("password"));
    var loginButton = element(by.id("btnLogin"));

    beforeEach(function(){
        browser.get("/Container/");
    });

    it("should take user to dashboard page if given right credentials", function () {
        username.sendKeys("admin");
        password.sendKeys("1Shot7Kills");
        var loginUrl = browser.getCurrentUrl();
        loginButton.click();
        expect(browser.getCurrentUrl()).toContain("/dashboard");
    });

    it("should stays at login page when login a user if given wrong credentials (wrong password)", function () {
        username.sendKeys("admin");
        password.sendKeys("pasword");
        var loginPageUrl = browser.getCurrentUrl();
        loginButton.click();
        var redirectedPageUrl = browser.getCurrentUrl();

        expect(loginPageUrl).toEqual(redirectedPageUrl);
    });

    it("should stays at login page when login a user if given wrong credentials (wrong user)", function () {
        username.sendKeys("admin1");
        password.sendKeys("1Shot7Kills");
        var loginPageUrl = browser.getCurrentUrl();
        loginButton.click();
        var redirectedPageUrl = browser.getCurrentUrl();

        expect(loginPageUrl).toEqual(redirectedPageUrl);
    });

    it("should give error message when login a user if given wrong credentials (wrong user)", function () {
        username.sendKeys("wrong_user");
        password.sendKeys("1Shot7Kills");
        loginButton.click();

        var errors = element.all(by.css(".has-error"));
        expect(errors.count()).toBe(2);
    });

    it("should give error message when login a user if given wrong credentials (wrong password)", function () {
        username.sendKeys("admin");
        password.sendKeys("wrong pass");
        loginButton.click();

        var errors = element.all(by.css(".has-error"));
        expect(errors.count()).toBe(2);
    });
});
