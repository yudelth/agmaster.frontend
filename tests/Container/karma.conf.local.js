// Karma configuration
// http://karma-runner.github.io/0.12/config/configuration-file.html
// Generated on 2014-06-24 using
// generator-karma 0.8.2

module.exports = function(config) {
  config.set({
    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // base path, that will be used to resolve files and exclude
    basePath: "",

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ["jasmine"],

    // list of files / patterns to load in the browser
    files: [
      "../../app/Container/js/json3.js",
      "../../app/Container/js/angular/angular.min.js",
      "../../app/Container/js/angular/angular-mocks.js",
      "../../app/Container/js/angular/angular-cookies.min.js",
      "../../app/Container/js/angular/angular-sanitize.min.js",
      "../../app/Container/js/angular/angular-animate.min.js",
      "../../app/Container/js/angular/angular-route.min.js",
      "../../app/Container/authentication/app/js/*.js",
      "../../app/Container/js/sessionService.js",
      "../../app/Container/js/loggingService.js",
      "../../app/Container/js/config.js",
      "../../app/Container/authentication/app/app.js",
      "../../app/Container/authentication/app/services/**/*.js",
      "../../app/Container/authentication/app/controllers/**/*.js",
      "specs/**/*.js",
    ],

    // list of files / patterns to exclude
    exclude: [],

   // web server port
    port: 8080,

    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: [
      "PhantomJS"
    ],

    // Which plugins to enable
    plugins: [
      "karma-chrome-launcher",
      "karma-phantomjs-launcher",
      "karma-jasmine"
    ],

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false,

    colors: true,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,

    // Uncomment the following lines if you are using grunt's server to run the tests
    // proxies: {
    //   '/': 'http://localhost:9000/'
    // },
    // URL root prevent conflicts with the site root
    // urlRoot: '_karma_'
  });
};
