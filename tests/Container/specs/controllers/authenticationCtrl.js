﻿'use strict';

describe('Controller: AuthenticationCtrl', function () {
    var scope, authenticationCtrl, authenticationSrvc, session, deferred, $timeout;

    beforeEach(module('authenticationApp', function ($provide) {
        session = {
            create: function (userId, userName, role, token) {
                if (userId == 1) {
                    this.sessionData = { userName: userName, userId: userId, role: role, token: token };
                } else
                    this.sessionData = undefined;
            },
            destroy: function () {
                this.sessionData = null;
            }
        };

        authenticationSrvc = {
            login: function () {
                var userId = 0;
                if (scope.user.userName == 'test') {
                    userId = 1;
                }
                session.create(userId, scope.user.userName, 'admin', '123token');
                return deferred.promise;
            },
            logout: function () {
                session.destroy();
            },
            getConfiguration: function () {
                return deferred.promise;
            }
        };

        showBusy = function () { };
        hideBusy = function () { };
        toastr = {
            error: function () { }
        };

        $provide.value('Session', session);
        $provide.value('AuthenticationSrvc', authenticationSrvc);
    }));

    beforeEach(inject(function ($rootScope, $controller, $q, _$timeout_) {
        deferred = $q.defer();
        $timeout = _$timeout_;

        scope = $rootScope.$new();
        authenticationCtrl = $controller('AuthenticationCtrl', {
            $scope: scope
        });

        //deferred.resolve();
    }));

    it('should not be submitted by default', function () {
        expect(scope.submitted).toBe(false);
    });

    it('should be submitted on login', function () {
        var user = { userName: 'test', password: 'pass' };
        scope.user = user;
        scope.loginForm = { $invalid: false, $valid: true };
        containerLogin = function () { };
        scope.login();
        deferred.resolve();
        $timeout.flush();
        scope.$apply();
        expect(scope.submitted).toBe(true);
    });

    it('should create a session with valid token on login', function () {
        var user = { userName: 'test', password: 'pass' };
        scope.user = user;
        scope.loginForm = { $invalid: false, $valid: true };
        containerLogin = function () { };
        scope.login();
        deferred.resolve();
        $timeout.flush();
		scope.$apply();
		expect(scope.Session.sessionData.token).toBe('123token');
    });

    //it('should create a session with the right userName on login', function () {
    //    var user = { userName: 'test', password: 'pass' };
    //    scope.user = user;
    //    scope.loginForm = { $invalid: false, $valid: true };
    //    containerLogin = function () {};
    //    scope.login();
    //    deferred.resolve();
    //    scope.$apply();
    //    $timeout.flush();
		//expect(scope.Session.sessionData.userName).toBe('test');
    //});

    it('should invalidate the form if credentials are not valid on login', function () {
        var user = { userName: 'myName', password: 'pass' };
        scope.user = user;
        scope.loginForm = { $invalid: false, $valid: true };
        session.sessionData = null;
        scope.login();
        deferred.resolve();
        $timeout.flush();
        scope.$apply();
        expect(scope.loginForm.$invalid).toBeTruthy();
    });

    //it('should load the configuration', function () {
    //    var user = { userName: 'test', password: 'pass' };
    //    scope.user = user;
    //    scope.loginForm = { $invalid: false, $valid: true };
    //    var count = 0;
    //    containerLogin = function (){
    //        count += 1;
    //    };
    //    scope.login();
    //    deferred.resolve();
    //    scope.$apply();
    //    $timeout.flush();
    //    expect(count).toBe(1);
    //});
});

//To help with reference to function on container.js
var containerLogin;
var showBusy;
var hideBusy;
var toastr;
