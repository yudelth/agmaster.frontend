﻿'use strict';

describe('Service: AuthenticationSrvc', function () {

    var authenticationSrvc, SessionMockSrvc, rootScope, httpBackend, deferred, CryptSrvcMock;

    // load the controller's module
    beforeEach(function () {
        module('authenticationApp', function ($provide) {
            SessionMockSrvc = {
                token: null,
                create: function (userId, userName, role, agencyId, token) {
                    this.token = token;
                },
                destroy: function () {
                    this.token = null;
                },
                getHeaders: function() {
                    return { 'Authorization': 'Bearer ' + '12344' };
                }
            };
            $provide.value('Session', SessionMockSrvc);

            CryptSrvcMock = {
                enc: {
                    Utf8: {
                        parse: function (username, password) {
                            return 'testcrypted';
                        }
                    }
                }
            };
            $provide.value('CryptSrvc', CryptSrvcMock);
        });

        inject(function (_AuthenticationSrvc_, _$q_, _$rootScope_, _$httpBackend_) {
            deferred = _$q_.defer();
            authenticationSrvc = _AuthenticationSrvc_;
            rootScope = _$rootScope_;
            httpBackend = _$httpBackend_;

            var data = { UserId: 1, UserName: 'testUser', Role: 'Administrator', AgencyId: '123ASS1', Token: 'test123' };
            httpBackend.whenPOST('http://localhost:49269/api/Login').respond(data);

            httpBackend.whenGET('http://localhost:49269/api/configuration').respond('Applications');

            deferred.resolve(data);
        });
    });

    it('should exist', function () {
        expect(!!authenticationSrvc).toBe(true);
    });

    it('should not have session on creation', function () {
        expect(SessionMockSrvc.token).toBe(null);
    });

    it('should not create session if invalid userName', function () {
        var user = { userName: '', password: 'password' };
        authenticationSrvc.login(user);
        expect(SessionMockSrvc.token).toBe(null);
    });

    it('should not create session if invalid password', function () {
        var user = { userName: 'user', password: null };
        authenticationSrvc.login(user);
        expect(SessionMockSrvc.token).toBe(null);
    });

    it('should create token on login for valid credentials', function () {
        var user = { userName: 'user', password: 'password' };
        authenticationSrvc.login(user);
        httpBackend.flush();
        expect(SessionMockSrvc.token).toBe('test123');
    });

    it('should destroy the session on logout', function() {
        var user = { userName: 'user', password: 'password' };
        authenticationSrvc.login(user);
        httpBackend.flush();
        expect(SessionMockSrvc.token).toBe('test123');
        authenticationSrvc.logout();
        expect(SessionMockSrvc.token).toBe(null);
    });

    it('should return the configuration', function () {
        authenticationSrvc.getConfiguration().then(function(result) {
            expect(result).toBe('Applications');
        });
        httpBackend.flush();
    });
});