﻿'use strict';

describe('Service: CryptoSrvc', function () {

    // load the service's module
    beforeEach(module('authenticationApp'));

    // instantiate service
    var crypto;
    beforeEach(inject(function (_CryptoSrvc_) {
        crypto = _CryptoSrvc_;
    }));

    it('should exist', function () {
        expect(!!crypto).toBe(true);
    });

    it('should encrypt the given text', function() {
        var result = crypto.enc.Utf8.parse('testUserName');
        expect(result).toBeDefined();
        expect(result.toString()).toBe('74657374557365724e616d65');
    });
});