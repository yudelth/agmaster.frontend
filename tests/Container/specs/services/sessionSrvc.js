﻿'use strict';

describe('Service: SessionSrvc', function () {

    var cookies;

    // load the service's module
    beforeEach(module('authenticationApp'));

    // instantiate service
    var session;
    beforeEach(inject(function ($cookies, _Session_) {
        cookies = $cookies;
        session = _Session_;
    }));

    it('should exist', function () {
        expect(!!session).toBe(true);
    });

    it('should do nothing for invalid token', function () {
        session.create(1, 'testUser', 'administrator', '23SDSD233', '');

        expect(session.sessionData).toBeNull();
    });

    it('should set a userId, a userName a role, a agencyId, and a token on create', function () {
        session.create(1, 'testUser', 'administrator', '123ASAS223', 'testToken123');

        expect(session.sessionData.userId).toBe(1);
        expect(session.sessionData.userName).toBe('testUser');
        expect(session.sessionData.role).toBe('administrator');
        expect(session.sessionData.agencyId).toBe('123ASAS223');
        expect(session.sessionData.token).toBe('testToken123');
    });

    it('should create two cookies on create', function () {
        expect(cookies.SessionToken).toBe(undefined);
        expect(cookies.SessionData).toBe(undefined);
        session.create(1, 'testUser', 'administrator', '123ASAS223', 'testToken123');

        expect(cookies.SessionToken).toBe('testToken123');
        expect(cookies.SessionData).toBeDefined();
    });

    it('should delete the cookies on destroy', function () {
        session.create(1, 'testUser', 'administrator', 'testToken123');
        session.destroy();

        expect(cookies.SessionToken).toBe(undefined);
        expect(cookies.SessionData).toBe(undefined);

    });

    it('should delete the session data on destroy', function () {
        cookies.SessionToken = 'testToken123';
        cookies.SessionData = {
            userId: 1,
            userName: 'testUser',
            role: 'administrator',
            agencyId: '123ASAS223'
        };
        session.destroy();

        expect(session.sessionData).toBe(null);
    });

    it('should get the token and session data from the cookies', function () {
        cookies.SessionToken = 'testToken123';
        cookies.SessionData = {
            userId: 1,
            userName: 'testUser',
            role: 'administrator',
            agencyId: '123ASAS223'
        };

        spyOn(JSON, "parse").and.returnValue(cookies.SessionData);

        session.join();
        expect(session.sessionData.userId).toBe(1);
        expect(session.sessionData.userName).toBe('testUser');
        expect(session.sessionData.role).toBe('administrator');
        expect(session.sessionData.agencyId).toBe('123ASAS223');
        expect(session.sessionData.token).toBe('testToken123');
    });

    it('should return the headers given the session token', function () {
        session.create(1, 'testUser', 'administrator', '123ASAS223', 'testToken123');
        var result = session.getHeaders();
        expect(result.Authorization).toBe('Bearer testToken123');
    });

});