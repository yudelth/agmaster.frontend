exports.config = {
    framework: "jasmine",
    seleniumAddress: "http://localhost:4444/wd/hub",
    specs: ["../../app/Container/authentication/tests/e2e/login.js"],
    baseUrl: "http://localhost:8048/Container/#/",
    rootElement: "div.bg-whitesmoke",
    capabilities: {
        "browserName": "chrome"
    },
}

