"use strict";

describe("Users Controller", function() {
    var sessionServ;
    var scope;
    var usersService;
    var usersController;
    var getUserDeferred;
    var getRolesDeferred;
    var saveUsersDeferred;

    beforeEach(module("administrationApp", function ($provide) {
        sessionServ = {
            join: jasmine.createSpy("join"),
            getHeaders: jasmine.createSpy(),
            sessionData: {userName: "MyUser"}
        };
        //
        usersService = {
            getRoles: jasmine.createSpy("getRoles"),
            getUsers: jasmine.createSpy("getUsers"),
            addUser: jasmine.createSpy("addUser"),
            updateUser: jasmine.createSpy("updateUser")
        };

        $provide.value("usersService", usersService);
        $provide.value("session", sessionServ);
    }));

    beforeEach(inject(function ($injector) {
        getUserDeferred = $injector.get("$q").defer();
        getRolesDeferred = $injector.get("$q").defer();
        saveUsersDeferred = $injector.get("$q").defer();
        scope = $injector.get("$rootScope");

        var controller = $injector.get("$controller");
        usersController = controller("UsersController", { "$scope": scope });
    }));

    it("should join sessionService", function () {
        //arrange
        scope.getUsers = jasmine.createSpy("getUsers");

        //act
        scope.init();

        //assert
        expect(sessionServ.join).toHaveBeenCalled();
    });

    it("should set the loggedInUser information", function () {
        //arrange
        scope.getUsers = jasmine.createSpy("getUsers");
        scope.loggedInUser = undefined;

        //act
        scope.init();

        //assert
        expect(scope.loggedInUser.userName).toBe('MyUser');
        expect(scope.loggedInUser.password).toBe('');
        expect(scope.loggedInUser.newPassword).toBe('');
        expect(scope.loggedInUser.newPasswordConfirm).toBe('');
    });

    it("should load users on init", function () {
        //arrange
        scope.getUsers = jasmine.createSpy();
        scope.getUsers.and.returnValue(getUserDeferred.promise);

        //act
        scope.init();

        //assert
        expect(scope.getUsers).toHaveBeenCalled();
    });

    it("should set inserted to null", function () {
        //arrange
        scope.getUsers = jasmine.createSpy("getUsers");
        scope.inserted = undefined;

        //act
        scope.init();

        //assert
        expect(scope.inserted).toBeNull();
    });

    it("should return null on loading the roles if the roles were already loaded", function () {
        //arrange
        scope.roles = ["Administrator"];

        //act
        var result = scope.loadRoles();

        //assert
        expect(result).toBeNull();
    });

    it("should load the roles from the service and assign them to the scope", function () {
        //arrange
        var roles = ["Administrator"];
        usersService.getRoles.and.returnValue(getRolesDeferred.promise);
        scope.roles = [];

        //act
        scope.loadRoles();
        getRolesDeferred.resolve(roles);
        scope.$apply();

        //assert
        expect(scope.roles).toBe(roles);
    });

    it("should return the role name for an existing role", function () {
        //arrange
        scope.roles = [{RoleId:0, RoleName:"Administrator"},{RoleId:1, RoleName:"Advanced"}]
        var user = {Role:1};

        //act
        var result = scope.getRoleName(user);

        //assert
        expect(result).toBe("Advanced");
    });

    it("should return Not Set for a non existing role", function () {
        //arrange
        scope.roles = [{id:0, text:"Administrator"},{id:1, text:"Advanced"}];
        var user = {Role:2};

        //act
        var result = scope.getRoleName(user);

        //assert
        expect(result).toBe("Not set");
    });

    it("should load the roles first", function () {
        //arrange
        var roles =[{id:0, text:"Administrator"},{id:1, text:"Advanced"}];
        scope.loadRoles = jasmine.createSpy("getRoles");
        scope.loadRoles.and.returnValue(getRolesDeferred.promise);
        var users = ["user1"];
        usersService.getUsers.and.returnValue(getUserDeferred.promise);

        //act
        scope.getUsers();
        getRolesDeferred.resolve(roles);
        getUserDeferred.resolve(users);
        scope.$apply();

        //assert
        expect(scope.loadRoles).toHaveBeenCalled();
    });

    it("should bring the users from the service", function () {
        //arrange
        var roles =[{id:0, text:"Administrator"},{id:1, text:"Advanced"}];
        usersService.getRoles.and.returnValue(getRolesDeferred.promise);
        var users = ["user1"];
        usersService.getUsers.and.returnValue(getUserDeferred.promise);

        //act
        scope.getUsers();
        getRolesDeferred.resolve(roles);
        getUserDeferred.resolve(users);
        scope.$apply();

        //assert
        expect(usersService.getUsers).toHaveBeenCalled();
        expect(scope.users).toBe(users);
    });

    it("should get the user for the given userId", function () {
        //arrange
        scope.users = [{UserId:1}, {UserId:2}, {UserId:3}];

        //act
        var result = scope.getUser(2);

        //assert
        expect(result).toBe(scope.users[1]);
    });

    it("should get the maskedPassword for existing passwords", function () {
        //arrange
        //act
        var result = scope.getMaskedPassword("123");

        //assert
        expect(result).toBe('******');
    });

    it("should get empty string for non existing passwords", function () {
        //arrange
        //act
        var result = scope.getMaskedPassword("");

        //assert
        expect(result).toBe('');
    });

    it("should return true for a valid password", function () {
        //arrange
        var password = 'mypass';

        //act
        var result = scope.checkPassword(password, 1);

        //assert
        expect(result).toBeTruthy();
    });

    it("should return message for invalid password", function () {
        //arrange
        var password = undefined;

        //act
        var result = scope.checkPassword(password, 1);

        //assert
        expect(result).toBe('Password cannot be empty');
    });

    it("should return true for a valid role", function () {
        //arrange
        var role = 'admin';

        //act
        var result = scope.checkRole(role, 1);

        //assert
        expect(result).toBeTruthy();
    });

    it("should return message for invalid role", function () {
        //arrange
        var role = undefined;

        //act
        var result = scope.checkRole(role, 1);

        //assert
        expect(result).toBe('You must select a role');
    });

    it("should update the user if it already exists", function () {
        //arrange
        var userId = 1;
        var user = {UserId: userId};
        scope.getUser = jasmine.createSpy("getUsers");
        scope.getUser.and.returnValue(user);
        var data = {UserName: 'myName', Role:'myRole', Active:true, Password:'myPassword'};

        //act
        var result = scope.saveUser(data, userId);
        getUserDeferred.resolve(user);
        scope.$apply();

        //assert
        expect(usersService.updateUser).toHaveBeenCalledWith(user);
        expect(user.UserName).toBe(data.UserName);
        expect(user.Role).toBe(data.Role);
        expect(user.Active).toBe(data.Active);
        expect(user.Password).toBe(data.Password);
    });

    it("should mark the password as new for new passwords for existing users", function () {
        //arrange
        var userId = 1;
        var user = {UserId: userId};
        scope.getUser = jasmine.createSpy("getUsers");
        scope.getUser.and.returnValue(user);
        var data = {UserName: 'myName', Role:'myRole', Active:true, Password:'myPassword'};

        //act
        var result = scope.saveUser(data, userId);
        getUserDeferred.resolve(user);
        scope.$apply();

        //assert
        expect(usersService.updateUser).toHaveBeenCalledWith(user);
        expect(user.UserName).toBe(data.UserName);
        expect(user.Role).toBe(data.Role);
        expect(user.Active).toBe(data.Active);
        expect(user.Password).toBe(data.Password);
        expect(user.IsNewPassword).toBeTruthy();
    });

    it("should not mark the password as new for old passwords for existing users", function () {
        //arrange
        var userId = 1;
        var user = {UserId: userId, Password:'myPassword'};
        scope.getUser = jasmine.createSpy("getUsers");
        scope.getUser.and.returnValue(user);
        var data = {UserName: 'myName', Role:'myRole', Active:true, Password:'myPassword'};

        //act
        var result = scope.saveUser(data, userId);
        getUserDeferred.resolve(user);
        scope.$apply();

        //assert
        expect(usersService.updateUser).toHaveBeenCalledWith(user);
        expect(user.UserName).toBe(data.UserName);
        expect(user.Role).toBe(data.Role);
        expect(user.Active).toBe(data.Active);
        expect(user.Password).toBe(data.Password);
        expect(user.IsNewPassword).toBeFalsy();
    });

    it("should add the user if new user", function () {
        //arrange
        var userId = -1;
        scope.inserted = {UserId: userId, UserName: '', Role: '', Active:false, Password:''};
        scope.users = [scope.inserted];
        var data = {UserName: 'myName', Role:'myRole', Active:true, Password:'myPassword'};
        usersService.addUser.and.returnValue(saveUsersDeferred.promise);

        //act
        var result = scope.saveUser(data, userId);
        saveUsersDeferred.resolve(1);
        scope.$apply();

        //assert
        expect(usersService.addUser).toHaveBeenCalled();
        expect(scope.users[scope.users.length - 1].UserId).toBe(1);
    });

    it("should clean the list if inserting", function () {
        //arrange
        var user = {UserId: -1, UserName: '', Role: '', Active:false, Password:''};
        scope.users = [scope.inserted];
        scope.inserted = user;
        var rowform = {$cancel:jasmine.createSpy('$cancel')};

        //act
        scope.cancelUser(rowform, user);

        //assert
        expect(scope.inserted).toBeNull();
        expect(rowform.$cancel).toHaveBeenCalled();
    });
});
