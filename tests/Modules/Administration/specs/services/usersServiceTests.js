"use strict";

describe("Users Service", function () {
    var usersService;
    var httpBackend;
    var sessionMockService;
    var users = [];
    var roles = [];

    beforeEach(module("administrationApp", function ($provide) {
        sessionMockService = {
            getHeaders: function () {
                return {'Authorization': "Bearer " + "Some Bearer"};
            }
        };
        $provide.value("Session", sessionMockService);
    }));

    beforeEach(inject(function ($injector) {
        usersService = $injector.get("usersService");
        httpBackend = $injector.get("$httpBackend");
        users = [{UserId: 1, Password: "password"}];
        roles = [{RoleId: 1, RoleName: "admin"}];

        httpBackend.when("GET", configuration.serviceBaseAddress + "/Users").respond(users);
        httpBackend.when("PUT", configuration.serviceBaseAddress + "/Users").respond(1);
        httpBackend.when("POST", configuration.serviceBaseAddress + "/Users").respond(undefined);

        httpBackend.when("GET", configuration.serviceBaseAddress + "/Roles").respond(roles);
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it("should create Session", function () {
        expect(sessionMockService).toBeDefined();
        expect(sessionMockService).not.toBeNull();
    });

    it("should bring the users from the service", function(){
        //arrange
        httpBackend.expect("GET", configuration.serviceBaseAddress + "/Users");

        //act
        var result = usersService.getUsers();
        httpBackend.flush();

        //assert
        result.then(function(data){
            expect(data.length).toBe(users.length);
            expect(data[0].UserId).toBe(users[0].UserId);
            expect(data[0].Password).toBe(users[0].Password);
        });
    });

    it("should bring the roles from the service", function(){
        //arrange
        httpBackend.expect("GET", configuration.serviceBaseAddress + "/Roles");

        //act
        var result = usersService.getRoles();
        httpBackend.flush();

        //assert
        result.then(function(data){
            expect(data.length).toBe(roles.length);
            expect(data[0].RoleId).toBe(roles[0].RoleId);
            expect(data[0].RoleName).toBe(roles[0].RoleName);
        });
    });

    it("should update the user in the backend", function(){
        //arrange
        //assert
        httpBackend.expect("PUT", configuration.serviceBaseAddress + "/Users");

        //act
        usersService.updateUser({UserId:1});
        httpBackend.flush();
    });

    it("should add the user in the backend", function(){
        //arrange
        //assert
        httpBackend.expect("POST", configuration.serviceBaseAddress + "/Users");

        //act
        usersService.addUser({UserId:-1});
        httpBackend.flush();
    });
});

var $ = function(){
    return {
        removeClass: function(){return {fadeIn: function(){}}},
        addClass: function(){return {fadeOut: function(){}}}
    };
};