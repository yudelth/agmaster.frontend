exports.config = {
    framework: "jasmine",
    seleniumAddress: "http://localhost:4444/wd/hub",
    specs: ["specs/e2e/*.js"],
    baseUrl: "http://agmasterdemo.azurewebsites.net/#/",
    rootElement: "div.bg-whitesmoke",
    capabilities: {
        "browserName": "chrome"
    }
}

