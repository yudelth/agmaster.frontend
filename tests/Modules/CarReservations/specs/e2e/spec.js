describe("Wholesaler-Create reservation", function () {
    var cellPhoneNumber = element(by.id("id"));
    var customerEmail = element(by.model("selectedReservation.Customer.Email"));
    var customerFirstName = element(by.id("customerFirstName"));
    var customerLastName = element(by.id("customerLastName"));
    var carProvider = element(by.model("selectedReservation.CarProvider"));
    var turOperator = element(by.model("selectedReservation.TourOperatorId"));
    var carCategory = element(by.model("selectedReservation.CarCategoryId"));
    var flightNumber = element(by.model("selectedReservation.FlightNumber"));
    var startDate = element(by.model("selectedReservation.StartDate"));
    var pickUpTime = element(by.model("selectedReservation.PickupTime"));
    var endDate = element(by.model("selectedReservation.EndDate"));
    var pickUpLocation = element(by.model("selectedReservation.PickUpLocationId"));
    var saveReservationButton = element(by.id("btnSaveReservation"));

    beforeEach(function () {
        browser.get("/Container/");
        element(by.id("userName")).sendKeys("retailer");
        element(by.id("password")).sendKeys("1Shot7Kills");
        element(by.id("btnLogin")).click();
        element(by.id("tabReservation")).click();
        browser.pause();
    });

    it("should create reservation", function () {
        //cellPhoneNumberTextbox.sendKeys("7864515454");
        customerEmail.sendKeys("asdf@asdf.com");
        customerFirstName.sendKeys("First Name");
        customerLastName.sendKeys("Last Name");
        carProvider.$("[value='1']").click();
        turOperator.$("[value='2'").click();
        carCategory.$("[value='2']").click();
        flightNumber.sendKeys("9985");
        startDate.sendKeys("12/05/2016");
        
        pickUpTime.sendKeys("10:00 AM");
        endDate.sendKeys("21/05/2016");
        pickUpLocation.$("[value='2']").click();
        saveReservationButton.click();
        browser.pause();
        expect(true).toBe(true);
    });
});
