﻿"use strict";

describe("Alert Controller", function() {
    var sessionServ;
    var alertServ;
    var reportGeneratorServ;
    var reservationServ;
    var location;
    var scope;
    var alertController;

    var getAlertsDeferred;
    var updateUrgentNotificationDeferred;

    beforeEach(module("carReservationsApp", function ($provide) {
        sessionServ = {
            join: jasmine.createSpy("join"),
            getHeaders: jasmine.createSpy()
        };

        reportGeneratorServ = {
            generateReservationHistory: jasmine.createSpy()
        };

        location = {
            path : jasmine.createSpy(),
            search: jasmine.createSpy()
        };

        alertServ = jasmine.createSpyObj("alertServ", ["getAlerts"]);
        reservationServ = jasmine.createSpyObj("reservationServ", ["updateUrgentNotification"]);

        $provide.value("alertService", alertServ);
        $provide.value("session", sessionServ);
        $provide.value("reportGeneratorService", reportGeneratorServ);
        $provide.value("reservationService", reservationServ);
    }));

    beforeEach(inject(function ($injector) {
        getAlertsDeferred = $injector.get("$q").defer();
        updateUrgentNotificationDeferred = $injector.get("$q").defer();
        scope = $injector.get("$rootScope");

        var controller = $injector.get("$controller");
        alertController = controller("alertController", { "$scope": scope, "$location": location });
    }));

    it("should join sessionService", function () {
        //arrange
        alertServ.getAlerts.and.returnValue(getAlertsDeferred.promise);

        //act
        scope.init();

        //assert
        expect(sessionServ.join).toHaveBeenCalled();
    });

    it("should load alerts", function () {
        //arrange
        alertServ.getAlerts.and.returnValue(getAlertsDeferred.promise);

        //assert
        scope.alerts = [];

        //act
        scope.init();
        getAlertsDeferred.resolve([{},{}]);
        scope.$apply();

        //assert
        expect(scope.alerts.length).toBe(2);
    });

    it("should send info to reservation controller", function(){
        //act
        scope.alertDetails(1);

        //assert
        expect(location.path).toHaveBeenCalledWith("/reservations");
        expect(location.search).toHaveBeenCalledWith("reservationId", 1);
    })

    it("should call reportGeneratorServ.generateReservationHistory", function(){
        //arrange
        var reservationId = 4;

       //act
        scope.showReservationHistory(reservationId);

       //assert
        expect(reportGeneratorServ.generateReservationHistory).toHaveBeenCalledWith(reservationId);
    });

    it("should call reservationServ.updateUrgentNotification", function(){
        //arrange
        var reservationId = 10;
        reservationServ.updateUrgentNotification.and.returnValue(updateUrgentNotificationDeferred.promise);

        //act
        scope.removeAlert(reservationId);

        //assert
        expect(reservationServ.updateUrgentNotification).toHaveBeenCalledWith(reservationId, false);
    });

    it("should call loadAlerts after successfully calling updateUrgentNotification", function() {
        //arrange
        var reservationId = 10;
        alertServ.getAlerts.and.returnValue(getAlertsDeferred.promise);
        reservationServ.updateUrgentNotification.and.returnValue(updateUrgentNotificationDeferred.promise);

        //act
        scope.removeAlert(reservationId);
        updateUrgentNotificationDeferred.resolve();
        scope.$apply();

        //assert
        expect(alertServ.getAlerts).toHaveBeenCalled();
    });
});
