﻿"use strict";

describe("Configuration Controller", function () {
    var scope;
    var lookUpDataServ;
    var sessionServ;
    var configurationController;
    var postTourOperatorDeferred;
    var putTourOperatorDeferred;
    var getTourOperatorDeferred;
    var getActiveTourOperatorDeferred;
    var carProvidersDeferred;
    var postPriceDeferred;
    var putPriceDeferred;
    var getTourOperatorByNameDeferred;
    var getPriceByConfigurationDeferred;
    var carProviderEnumServ;
    var getSeasonsDeferred;
    var getIntervalDeferred;
    var getPricesDeferred;
    var getCarCategoriesByTourOperatorDeferred;
    var deletePriceDeferred;
    var getRetailersDeferred;
    var getPriceGroupDeferred;
    var cookiesMock;
    var httpBackendMock;
    var priceGroupServ;
    var retailerServ;
    var modalMock;
    var getPriceMatrixDeferred;

    beforeEach(module("carReservationsApp", function ($provide) {
        lookUpDataServ = jasmine.createSpyObj("lookUpDataServ", [
            "getTourOperators",
            "getCarProviders",
            "getSeasons",
            "getIntervals",
            "getPrices",
            "getCarCategoriesByTourOperator",
            "postPrice",
            "putPrice",
            "deletePrice",
            "postTourOperator",
            "putTourOperator",
            "getTourOperatorByName",
            "getPriceByConfiguration",
            "getActiveTourOperators",
            "getRetailers",
            "getPriceGroups",
            "getPriceMatrix"
        ]);

        sessionServ = {
            join: jasmine.createSpy("join")
        };

        cookiesMock = {
          AppsData: "{}"
        };

        priceGroupServ = {};
        modalMock = jasmine.createSpy();

        $provide.value("lookUpDataService", lookUpDataServ);
        $provide.value("session", sessionServ);
        $provide.value("$cookies", cookiesMock);
        $provide.value("priceGroupsService", priceGroupServ);
        $provide.value("retailerService", retailerServ);
        $provide.value("$modal", modalMock);
    }));

    beforeEach(inject(function ($injector) {
        scope = $injector.get("$rootScope");
        getIntervalDeferred = $injector.get("$q").defer();
        postTourOperatorDeferred = $injector.get("$q").defer();
        getTourOperatorDeferred = $injector.get("$q").defer();
        carProvidersDeferred = $injector.get("$q").defer();
        putTourOperatorDeferred = $injector.get("$q").defer();
        postPriceDeferred = $injector.get("$q").defer();
        getTourOperatorByNameDeferred = $injector.get("$q").defer();
        getPriceByConfigurationDeferred = $injector.get("$q").defer();
        putPriceDeferred = $injector.get("$q").defer();
        getSeasonsDeferred = $injector.get("$q").defer();
        getPricesDeferred = $injector.get("$q").defer();
        getCarCategoriesByTourOperatorDeferred = $injector.get("$q").defer();
        deletePriceDeferred = $injector.get("$q").defer();
        getActiveTourOperatorDeferred = $injector.get("$q").defer();
        getRetailersDeferred = $injector.get("$q").defer();
        getPriceGroupDeferred = $injector.get("$q").defer();
        getPriceMatrixDeferred = $injector.get("$q").defer();

        var controller = $injector.get("$controller");
        configurationController = controller("configurationController", {"$scope": scope});
        carProviderEnumServ = $injector.get("carProviderEnumService");
        httpBackendMock = $injector.get("$httpBackend");
        httpBackendMock.when("GET", "/Modules/CarReservations/app/views/modalPriceGroups.html").respond({});
    }));

    describe("init method", function () {
        function arrangeInit(){
            lookUpDataServ.getSeasons.and.returnValue(getSeasonsDeferred.promise);
            lookUpDataServ.getIntervals.and.returnValue(getIntervalDeferred.promise);
            lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);
            lookUpDataServ.getTourOperators.and.returnValue(getTourOperatorDeferred.promise);
            lookUpDataServ.getActiveTourOperators.and.returnValue(getActiveTourOperatorDeferred.promise);
            lookUpDataServ.getRetailers.and.returnValue(getRetailersDeferred.promise);
            lookUpDataServ.getPriceGroups.and.returnValue(getPriceGroupDeferred.promise);
            lookUpDataServ.getPriceMatrix.and.returnValue(getPriceMatrixDeferred.promise);
        };

        it("should assign tourOperators", function() {
            //arrange
            arrangeInit();

            //act
            scope.init();
            getTourOperatorDeferred.resolve({});
            getActiveTourOperatorDeferred.resolve({});
            getSeasonsDeferred.resolve({});
            getIntervalDeferred.resolve({});
            getPricesDeferred.resolve({});
            getRetailersDeferred.resolve({});
            getPriceGroupDeferred.resolve({});
            scope.$apply();

            //assert
            expect(scope.tourOperators).toBeDefined();
            expect(scope.tourOperators).not.toBeNull();
        });

        it("should assign activeTourOperators", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();
            getTourOperatorDeferred.resolve({});
            getActiveTourOperatorDeferred.resolve({});
            getSeasonsDeferred.resolve({});
            getIntervalDeferred.resolve({});
            getPricesDeferred.resolve({});
            scope.$apply();

            //assert
            expect(scope.activeTourOperators).toBeDefined();
            expect(scope.activeTourOperators).not.toBeNull();
        });

        it("should call sessionService.join when loading", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();

            //assert
            expect(sessionServ.join).toHaveBeenCalled();
        });

        it("should assign carProviders", function () {
            //arrange
            arrangeInit();
            lookUpDataServ.getCarProviders.and.returnValue([]);

            //act
            scope.init();

            //assert
            expect(scope.carProviders).toBeDefined();
        });

        it("should assign selectedTourOperator", function () {
            //arrange
            var tourOperators = [
                {TourOperatorId: 1, Name: "Name", ParentTourOperatorId: 1},
                {TourOperatorId: 2, Name: "Name", ParentTourOperatorId: 1}
            ];

            arrangeInit();

            //act
            scope.init();
            getTourOperatorDeferred.resolve(tourOperators);
            scope.$apply();

            //assert
            expect(scope.selectedTourOperator).toBeDefined();
            expect(scope.selectedTourOperator).not.toBeNull();
        });

        it("should assign selectedTourOperator even when there is no touroperator in db", function () {
            //arrange
            var tourOperators = null;
            arrangeInit();

            //act
            scope.init();
            getTourOperatorDeferred.resolve(tourOperators);
            scope.$apply();

            //assert
            expect(scope.selectedTourOperator).toBeDefined();
            expect(scope.selectedTourOperator).not.toBeNull();
        });

        it("should assign selectedTourOperator with right value even when there is no touroperator in db (null)", function () {
            //arrange
            var tourOperators = null;
            arrangeInit();

            //act
            scope.init();
            getTourOperatorDeferred.resolve(tourOperators);
            scope.$apply();

            //assert
            expect(scope.selectedTourOperator.CarProviders).toBeDefined();
        });

        it("should assign selectedTourOperator with right value even when there is no touroperator in db (empty array)", function () {
            //arrange
            var tourOperators = [];
            arrangeInit();

            //act
            scope.init();
            getTourOperatorDeferred.resolve(tourOperators);
            scope.$apply();

            //assert
            expect(scope.selectedTourOperator.CarProviders).toBeDefined();
            expect(scope.selectedTourOperator.TourOperatorId).toBe(0);
            expect(scope.selectedTourOperator.Active).toBe(true);
            expect(scope.selectedTourOperator.IsCuban).toBe(false);
        });

        it("has to defined tourOperatorHasCarProviderAssociated", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();

            //assert
            expect(scope.tourOperatorHasCarProviderAssociated).toBeDefined();
        });

        it("has to initially set tourOperatorHasCarProviderAssociated to false", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();

            //assert
            expect(scope.tourOperatorHasCarProviderAssociated).toBe(false);
        });

        it("should call & assign retailer", function () {
            //arrange
            arrangeInit();
            var result = [];

            //act
            scope.init();
            getRetailersDeferred.resolve(result);
            scope.$apply();

            //assert
            expect(lookUpDataServ.getRetailers).toHaveBeenCalled();
            expect(scope.retailers).not.toBeUndefined();
        });

        it("should call & assign priceGroup", function () {
            //arrange
            arrangeInit();
            var result = [];

            //act
            scope.init();
            getPriceGroupDeferred.resolve(result);
            scope.$apply();

            //assert
            expect(lookUpDataServ.getPriceGroups).toHaveBeenCalled();
            expect(scope.priceGroups).not.toBeUndefined();
        });

        it("should assign scope.agencies", function () {
            //arrange
            arrangeInit();
            var retailers = [{}, {}];

            //act
            scope.init();
            getRetailersDeferred.resolve(retailers);
            scope.$apply();

            //assert
            expect(scope.retailers).toBe(retailers);
        });

        it("should active tourOperator tab when loading", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();

            //assert
            expect(scope.activeTab).toBe("price");
        });

        it("should assign seasons", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();
            getSeasonsDeferred.resolve({});
            scope.$apply();

            //assert
            expect(scope.seasons).toBeDefined();
        });

        it("should assign intervals", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();
            getIntervalDeferred.resolve({});
            scope.$apply();

            //assert
            expect(scope.intervals).toBeDefined();
        });

        it("should load prices", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();
            getPricesDeferred.resolve({});
            scope.$apply();

            //assert
            expect(scope.prices).toBeDefined();
        });

        it("should assign selectedPrice to empty price even if there are prices", function () {
            //arrange
            var firstPrice = {TourOperatorId: 1, CarCategoryId: 1, SeasonId: 1, IntervalId: 1};
            var secondPrice = {TourOperatorId: 2, CarCategoryId: 2, SeasonId: 2, IntervalId: 2};

            var prices = [firstPrice, secondPrice];
            arrangeInit();

            //act
            scope.init();
            getPricesDeferred.resolve(prices);
            scope.$apply();

            //assert
            expect(scope.selectedPrice.TourOperatorId).toBeNull();
            expect(scope.selectedPrice.CarCategoryId).toBeNull();
            expect(scope.selectedPrice.SeasonId).toBeNull();
            expect(scope.selectedPrice.IntervalId).toBeNull();
            expect(scope.selectedPrice.PriceValue).toBe(0);
            expect(scope.selectedPrice.TourOperatorPriceValue).toBe(0);
        });

        it("should set selectedPrice to be an empty price if there is no price", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();
            getPricesDeferred.resolve({});
            scope.$apply();

            //assert
            expect(scope.selectedPrice.TourOperatorId).toBeNull();
            expect(scope.selectedPrice.CarCategoryId).toBeNull();
            expect(scope.selectedPrice.SeasonId).toBeNull();
            expect(scope.selectedPrice.IntervalId).toBeNull();
            expect(scope.selectedPrice.PriceValue).toBe(0);
            expect(scope.selectedPrice.TourOperatorPriceValue).toBe(0);
        });

        it("should set configuringNewPrice to false", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();

            //assert
            expect(scope.configuringNewPrice).toBe(false);
        });

        it("should set tourOperatorFormSubmitted to false", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();

            //assert
            expect(scope.tourOperatorFormSubmitted).toBe(false);
        });
    });

    it("should set activeTab value when calling activateTab method", function () {
        scope.activateTab("aTabId");

        //assert
        expect(scope.activeTab).toBe("aTabId");
    });

    it("should have an instance of lookUpDataService", function () {
        //assert
        expect(lookUpDataServ).toBeDefined();
    });

    it("should have an instance of session service", function () {
        //assert
        expect(sessionService).toBeDefined();
    });

    it("should set selectedTourOperator when calling selectTourOperator method", function () {
        //arrange
        var tourOperator = {
            TourOperatorId: 1,
            Name: "Name",
            ParentTourOperatorId: 2
        };

        //act
        scope.selectTourOperator(tourOperator);

        //assert
        expect(scope.selectedTourOperator).toBe(tourOperator);
    });

    it("should return true when calling shouldBeChecked method and carProvider is associated with tourOperator", function () {
        //arrange
        var carProvider = carProviderEnumServ.types.cubacar;
        scope.selectedTourOperator = {
            CarProviders: carProviderEnumServ.types.cubacar | carProviderEnumServ.types.havanaAutos
        };

        //act
        var response = scope.shouldBeChecked(carProvider);

        //assert
        expect(response).toBe(true);
    });

    it("should return false when calling shouldBeChecked method and carProvider is not associated with tourOperator", function () {
        //arrange
        var carProvider = carProviderEnumServ.types.rex;
        scope.selectedTourOperator = {
            CarProviders: carProviderEnumServ.types.cubacar | carProviderEnumServ.types.havanaAutos
        };

        //act
        var response = scope.shouldBeChecked(carProvider);

        //assert
        expect(response).toBe(false);
    });

    it("should return false when calling shouldBeChecked method and selectedTourOperator is undefined", function () {
        //arrange
        var carProvider = carProviderEnumServ.types.rex;
        scope.selectedTourOperator = undefined;

        //act
        var response = scope.shouldBeChecked(carProvider);

        //assert
        expect(response).toBe(false);
    });

    it("should return false when calling shouldBeChecked method and selectedTourOperator is null", function () {
        //arrange
        var carProvider = carProviderEnumServ.types.rex;
        scope.selectedTourOperator = null;

        //act
        var response = scope.shouldBeChecked(carProvider);

        //assert
        expect(response).toBe(false);
    });

    it("should remove carProvider from selectedTourOperator.CarProviders when calling toggleSelection and carProvider was previously selected", function () {
        //arrange
        var carProvider = carProviderEnumServ.types.cubacar;
        scope.selectedTourOperator = {
            CarProviders: carProviderEnumServ.types.cubacar | carProviderEnumServ.types.havanaAutos
        };

        //act
        scope.toggleSelection(carProvider);
        var result = (scope.selectedTourOperator.CarProviders & carProvider) !== carProvider;

        //assert
        expect(result).toBe(true);
    });

    it("should add carProvider to scope.selectTourOperator.CarProviders when calling toggleSelection and carProvider was not previously selected", function () {
        //arrange
        var carProvider = carProviderEnumServ.types.rex;
        scope.selectedTourOperator = {
            CarProviders: carProviderEnumServ.types.cubacar | carProviderEnumServ.types.havanaAutos
        };

        //act
        scope.toggleSelection(carProvider);

        var result = (scope.selectedTourOperator.CarProviders & carProvider) === carProvider;

        //assert
        expect(result).toBe(true);
    });

    it("should create an empty tourOperator with Id equals to 0 when calling createNewTourOperator method", function () {
        //act
        scope.createNewTourOperator();

        //assert
        expect(scope.selectedTourOperator.TourOperatorId).toBe(0);
    });

    it("should create an empty tourOperator with empty name when calling createNewTourOperator method", function () {
        //act
        scope.createNewTourOperator();

        //assert
        expect(scope.selectedTourOperator.Name).toBe("");
    });

    it("should create an empty tourOperator with ParentTourOperatorId set to null when calling createNewTourOperator method", function () {
        //act
        scope.createNewTourOperator();

        //assert
        expect(scope.selectedTourOperator.ParentTourOperatorId).toBeNull();
    });

    it("should create an empty tourOperator with empty array of carProviders when calling createNewTourOperator method", function () {
        //act
        scope.createNewTourOperator();

        //assert
        expect(scope.selectedTourOperator.CarProviders.length).toBe(0);
    });

    it("should create an empty tourOperator with IsCuban set to false when calling createNewTourOperator method", function () {
        //act
        scope.createNewTourOperator();

        //assert
        expect(scope.selectedTourOperator.IsCuban).toBe(false);
    });

    it("should create an empty tourOperator with IsCuban set to false when calling createNewTourOperator method", function () {
        //act
        scope.createNewTourOperator();

        //assert
        expect(scope.selectedTourOperator.Active).toBe(true);
    });

    it("should set a value to selectedPrice when calling method selectPrice", function () {
        //arrange
        scope.selectedPrice = {};
        var price = {
            PriceId: 1,
            TourOperatorId: 1,
            TourOperatorName: "",
            CarCategoryId: 1,
            CarCategoryName: "",
            IntervalId: 1,
            IntervalName: "",
            SeasonId: 1,
            SeasonName: "",
            Value: 200
        };
        lookUpDataServ.getCarCategoriesByTourOperator.and.returnValue(getCarCategoriesByTourOperatorDeferred.promise);

        //act
        scope.selectPrice(price);

        //assert
        expect(scope.selectedPrice).toBe(price);
    });

    it("should set to false priceFormSubmitted when selecting a price", function () {
        //arrange
        scope.priceFormSubmitted = true;
        lookUpDataServ.getCarCategoriesByTourOperator.and.returnValue(getCarCategoriesByTourOperatorDeferred.promise);

        //act
        scope.selectPrice({});

        //assert
        expect(scope.priceFormSubmitted).toBe(false);
    });

    it("should set to false existingPrice when selecting a price", function () {
        //arrange
        scope.existingPrice = true;
        lookUpDataServ.getCarCategoriesByTourOperator.and.returnValue(getCarCategoriesByTourOperatorDeferred.promise);

        //act
        scope.selectPrice({});

        //assert
        expect(scope.existingPrice).toBe(false);
    });

    it("should not load car categories associated with tour operator when calling method getCarCategoriesByTourOperator and null parameter", function () {
        //act
        scope.getCarCategoriesByTourOperator(null);

        //assert
        expect(scope.carCategories).not.toBeDefined();
    });

    it("should not load car categories associated with tour operator when calling method getCarCategoriesByTourOperator and undefined parameter", function () {
        //act
        scope.getCarCategoriesByTourOperator(undefined);

        //assert
        expect(scope.carCategories).not.toBeDefined();
    });

    it("should load car categories associated with tour operator when calling method getCarCategoriesByTourOperator", function () {
        //arrange
        lookUpDataServ.getCarCategoriesByTourOperator.and.returnValue(getCarCategoriesByTourOperatorDeferred.promise);

        //act
        scope.getCarCategoriesByTourOperator(2);
        getCarCategoriesByTourOperatorDeferred.resolve({});
        scope.$apply();

        //assert
        expect(scope.carCategories).toBeDefined();
    });

    it("should load right amount of car categories associated with tour operator when calling method getCarCategoriesByTourOperator", function () {
        //arrange
        var carProviders = [
            {"CarCategoryId": 1, "CarProviderId": 1, "Name": "Económico (TM)", "CarProvider": null},
            {"CarCategoryId": 2, "CarProviderId": 2, "Name": "Económico (TA)", "CarProvider": null},
            {"CarCategoryId": 3, "CarProviderId": 1, "Name": "Medio (TM)", "CarProvider": null}
        ];
        lookUpDataServ.getCarCategoriesByTourOperator.and.returnValue(getCarCategoriesByTourOperatorDeferred.promise);

        //act
        scope.getCarCategoriesByTourOperator(2);
        getCarCategoriesByTourOperatorDeferred.resolve(carProviders);
        scope.$apply();

        //assert
        expect(scope.carCategories.length).toBe(3);
    });

    it("should load right car categories associated with tour operator when calling method getCarCategoriesByTourOperator", function () {
        //arrange
        var carCategories = [
            {"CarCategoryId": 1, "CarProviderId": 1, "Name": "Económico (TM)", "CarProvider": null},
            {"CarCategoryId": 2, "CarProviderId": 2, "Name": "Económico (TA)", "CarProvider": null},
            {"CarCategoryId": 3, "CarProviderId": 1, "Name": "Medio (TM)", "CarProvider": null}
        ];
        lookUpDataServ.getCarCategoriesByTourOperator.and.returnValue(getCarCategoriesByTourOperatorDeferred.promise);

        //act
        scope.getCarCategoriesByTourOperator(2);
        getCarCategoriesByTourOperatorDeferred.resolve(carCategories);
        scope.$apply();

        //assert
        expect(scope.carCategories).toContain(jasmine.objectContaining({CarCategoryId: 1}));
        expect(scope.carCategories).toContain(jasmine.objectContaining({CarCategoryId: 2}));
        expect(scope.carCategories).toContain(jasmine.objectContaining({CarCategoryId: 3}));
        expect(scope.carCategories).toContain(jasmine.objectContaining({Name: "Económico (TM)"}));
        expect(scope.carCategories).toContain(jasmine.objectContaining({Name: "Económico (TA)"}));
        expect(scope.carCategories).toContain(jasmine.objectContaining({Name: "Medio (TM)"}));
    });

    it("should load carCategories when calling selectPrice", function () {
        //arrange
        var price = {
            TourOperatorId: 1
        };

        //arrange
        var carCategories = [
            {"CarCategoryId": 1, "CarProviderId": 1, "Name": "Económico (TM)", "CarProvider": null},
            {"CarCategoryId": 2, "CarProviderId": 2, "Name": "Económico (TA)", "CarProvider": null},
            {"CarCategoryId": 3, "CarProviderId": 1, "Name": "Medio (TM)", "CarProvider": null}
        ];

        scope.carCategories = null;
        lookUpDataServ.getCarCategoriesByTourOperator.and.returnValue(getCarCategoriesByTourOperatorDeferred.promise);

        //act
        scope.selectPrice(price);
        getCarCategoriesByTourOperatorDeferred.resolve(carCategories);
        scope.$apply();

        //assert
        expect(scope.carCategories).toBeDefined();
        expect(scope.carCategories).not.toBeNull();
    });

    it("should assing new instance of price to selectedPrice when calling method configureNewPrice", function () {
        //arrange
        var price = {
            TourOperatorId: 1,
            CarCategoryId: 1,
            SeasonId: 3,
            IntervalId: 4
        };
        scope.selectedPrice = price;

        //act
        scope.configureNewPrice();

        //assert
        expect(price).not.toBe(scope.selectedPrice);
    });

    it("should clear selectedPrice when calling method configureNewPrice", function () {
        //arrange
        scope.selectedPrice = {
            TourOperatorId: 1,
            CarCategoryId: 1,
            SeasonId: 3,
            IntervalId: 4
        };

        //act
        scope.configureNewPrice();

        //assert
        expect(scope.selectedPrice.TourOperatorId).toBeNull();
        expect(scope.selectedPrice.CarCategoryId).toBeNull();
        expect(scope.selectedPrice.SeasonId).toBeNull();
        expect(scope.selectedPrice.IntervalId).toBeNull();
        expect(scope.selectedPrice.PriceId).toBe(0);
    });

    it("should set priceFormSubmitted to false when calling method configureNewPrice", function () {
        //arrange
        scope.priceFormSubmitted = true;
        scope.selectedPrice = {PriceId: 1};

        //act
        scope.configureNewPrice();

        //assert
        expect(scope.priceFormSubmitted).toBe(false);
    });

    it("should set priceFormSubmitted to true when calling method savePrice and form is valid", function () {
        //arrange
        scope.priceFormSubmitted = false;
        scope.selectedPrice = {PriceId: 1};
        lookUpDataServ.getPriceByConfiguration.and.returnValue(getPriceByConfigurationDeferred.promise);

        //act
        scope.savePrice(true);

        //assert
        expect(scope.priceFormSubmitted).toBe(true);
    });

    it("should set priceFormSubmitted to true when calling method savePrice and form is invalid", function () {
        //arrange
        scope.priceFormSubmitted = false;

        //act
        scope.savePrice(false);

        //assert
        expect(scope.priceFormSubmitted).toBe(true);
    });

    it("should not create a new price if form is invalid", function () {
        //arrange
        scope.selectedPrice = {PriceId: 0};

        //act
        scope.savePrice(false);

        //assert
        expect(lookUpDataServ.postPrice).not.toHaveBeenCalled();
    });

    it("should create a new price when calling method savePrice with new price configuration", function () {
        //arrange
        scope.selectedPrice = {PriceId: 0};
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        lookUpDataServ.getPriceByConfiguration.and.returnValue(getPriceByConfigurationDeferred.promise);
        lookUpDataServ.postPrice.and.returnValue(postPriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);

        //act
        scope.savePrice(true);
        getPriceByConfigurationDeferred.resolve(null);
        postPriceDeferred.resolve(true);
        scope.$apply();

        //assert
        expect(lookUpDataServ.postPrice).toHaveBeenCalled();
    });

    it("should NOT create a new price when calling method savePrice with existing price configuration", function () {
        //arrange
        scope.selectedPrice = {PriceId: 0};
        toastr = jasmine.createSpyObj("toastr", ["info"]);
        lookUpDataServ.getPriceByConfiguration.and.returnValue(getPriceByConfigurationDeferred.promise);
        lookUpDataServ.postPrice.and.returnValue(postPriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);

        //act
        scope.savePrice(true);
        getPriceByConfigurationDeferred.resolve({});
        postPriceDeferred.resolve(true);
        scope.$apply();

        //assert
        expect(lookUpDataServ.postPrice).not.toHaveBeenCalled();
    });

    it("should load prices after successfully creating one", function () {
        //arrange
        scope.selectedPrice = {PriceId: 0};
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        lookUpDataServ.getPriceByConfiguration.and.returnValue(getPriceByConfigurationDeferred.promise);
        lookUpDataServ.postPrice.and.returnValue(postPriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);

        //act
        scope.savePrice(true);
        getPriceByConfigurationDeferred.resolve(null);
        postPriceDeferred.resolve(true);
        scope.$apply();

        //assert
        expect(lookUpDataServ.getPrices).toHaveBeenCalled();
    });

    it("should notify client when successfully creating a price", function () {
        //arrange
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        scope.selectedPrice = {PriceId: 0};
        lookUpDataServ.getPriceByConfiguration.and.returnValue(getPriceByConfigurationDeferred.promise);
        lookUpDataServ.postPrice.and.returnValue(postPriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);

        //act
        scope.savePrice(true);
        postPriceDeferred.resolve(true);
        getPriceByConfigurationDeferred.resolve(null);
        scope.$apply();

        //assert
        expect(toastr.success).toHaveBeenCalled();
    });

    it("should notify client when creating a price fails", function () {
        //arrange
        toastr = jasmine.createSpyObj("toastr", ["error"]);
        scope.selectedPrice = {PriceId: 0};
        lookUpDataServ.getPriceByConfiguration.and.returnValue(getPriceByConfigurationDeferred.promise);
        lookUpDataServ.postPrice.and.returnValue(postPriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);

        //act
        scope.savePrice(true);
        getPriceByConfigurationDeferred.resolve(null);
        postPriceDeferred.resolve(false);
        scope.$apply();

        //assert
        expect(toastr.error).toHaveBeenCalled();
    });

    it("should set existingPrice to true when creating a new price with existing configuration", function () {
        //arrange
        scope.selectedPrice = {PriceId: 0};
        toastr = jasmine.createSpyObj("toastr", ["info"]);
        lookUpDataServ.getPriceByConfiguration.and.returnValue(getPriceByConfigurationDeferred.promise);
        lookUpDataServ.postPrice.and.returnValue(postPriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);

        //act
        scope.savePrice(true);
        getPriceByConfigurationDeferred.resolve({PriceId: 1});
        postPriceDeferred.resolve(false);
        scope.$apply();

        //assert
        expect(scope.existingPrice).toBe(true);
    });

    it("should notify clients when creating a new price with existing configuration", function () {
        //arrange
        toastr = jasmine.createSpyObj("toastr", ["info"]);
        scope.selectedPrice = {PriceId: 0};
        lookUpDataServ.getPriceByConfiguration.and.returnValue(getPriceByConfigurationDeferred.promise);
        lookUpDataServ.postPrice.and.returnValue(postPriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);

        //act
        scope.savePrice(true);
        getPriceByConfigurationDeferred.resolve({PriceId: 1});
        postPriceDeferred.resolve(false);
        scope.$apply();

        //assert
        expect(toastr.info).toHaveBeenCalled();
    });

    it("should update selected price when calling method savePrice on existing price and form is valid", function () {
        //arrange
        scope.selectedPrice = {PriceId: 1};
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        lookUpDataServ.getPriceByConfiguration.and.returnValue(getPriceByConfigurationDeferred.promise);
        lookUpDataServ.postPrice.and.returnValue(postPriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);
        lookUpDataServ.putPrice.and.returnValue(putPriceDeferred.promise);

        //act
        scope.savePrice(true);
        getPriceByConfigurationDeferred.resolve(null);
        putPriceDeferred.resolve(true);
        scope.$apply();

        //assert
        expect(lookUpDataServ.putPrice).toHaveBeenCalled();
    });

    it("should update selected price when calling method savePrice and there is no price with the same configuration", function () {
        //arrange
        scope.selectedPrice = {PriceId: 1};
        lookUpDataServ.getPriceByConfiguration.and.returnValue(getPriceByConfigurationDeferred.promise);
        lookUpDataServ.postPrice.and.returnValue(postPriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);
        lookUpDataServ.putPrice.and.returnValue(putPriceDeferred.promise);

        //act
        scope.savePrice(true);
        getPriceByConfigurationDeferred.resolve(null);
        putPriceDeferred.resolve(true);
        scope.$apply();

        //assert
        expect(lookUpDataServ.putPrice).toHaveBeenCalled();
    });

    it("should update selected price when calling method savePrice on existing price and the existing price is itself", function () {
        //arrange
        scope.selectedPrice = {PriceId: 1};
        lookUpDataServ.getPriceByConfiguration.and.returnValue(getPriceByConfigurationDeferred.promise);
        lookUpDataServ.postPrice.and.returnValue(postPriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);
        lookUpDataServ.putPrice.and.returnValue(putPriceDeferred.promise);

        //act
        scope.savePrice(true);
        getPriceByConfigurationDeferred.resolve({PriceId: 1});
        putPriceDeferred.resolve(true);
        scope.$apply();

        //assert
        expect(lookUpDataServ.putPrice).toHaveBeenCalled();
    });

    it("should NOT update selected price when calling method savePrice on existing price and form is invalid", function () {
        //arrange
        scope.selectedPrice = {PriceId: 1};

        //act
        scope.savePrice(false);

        //assert
        expect(lookUpDataServ.putPrice).not.toHaveBeenCalled();
    });

    it("should NOT update selected price when calling method savePrice on existing price and there is a price with the same configuration", function () {
        //arrange
        scope.selectedPrice = {PriceId: 1};
        toastr = jasmine.createSpyObj("toastr", ["info"]);
        lookUpDataServ.getPriceByConfiguration.and.returnValue(getPriceByConfigurationDeferred.promise);
        lookUpDataServ.postPrice.and.returnValue(postPriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);
        lookUpDataServ.putPrice.and.returnValue(putPriceDeferred.promise);

        //act
        scope.savePrice(true);
        getPriceByConfigurationDeferred.resolve({PriceId: 2});
        putPriceDeferred.resolve(true);
        scope.$apply();

        //assert
        expect(lookUpDataServ.putPrice).not.toHaveBeenCalled();
    });

    it("should notify client when successfully updating a price", function () {
        //arrange
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        scope.selectedPrice = {PriceId: 1};
        lookUpDataServ.getPriceByConfiguration.and.returnValue(getPriceByConfigurationDeferred.promise);
        lookUpDataServ.postPrice.and.returnValue(postPriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);
        lookUpDataServ.putPrice.and.returnValue(putPriceDeferred.promise);

        //act
        scope.savePrice(true);
        getPriceByConfigurationDeferred.resolve(null);
        putPriceDeferred.resolve(true);
        scope.$apply();

        //assert
        expect(toastr.success).toHaveBeenCalled();
    });

    it("should notify client when updating a price fails", function () {
        //arrange
        toastr = jasmine.createSpyObj("toastr", ["error"]);
        scope.selectedPrice = {PriceId: 1};
        lookUpDataServ.getPriceByConfiguration.and.returnValue(getPriceByConfigurationDeferred.promise);
        lookUpDataServ.postPrice.and.returnValue(postPriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);
        lookUpDataServ.putPrice.and.returnValue(putPriceDeferred.promise);

        //act
        scope.savePrice(true);
        getPriceByConfigurationDeferred.resolve(null);
        putPriceDeferred.resolve(false);
        scope.$apply();

        //assert
        expect(toastr.error).toHaveBeenCalled();
    });

    it("should load prices after successfully updating a price", function () {
        //arrange
        scope.selectedPrice = {PriceId: 1};
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        lookUpDataServ.getPriceByConfiguration.and.returnValue(getPriceByConfigurationDeferred.promise);
        lookUpDataServ.postPrice.and.returnValue(postPriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);
        lookUpDataServ.putPrice.and.returnValue(putPriceDeferred.promise);

        //act
        scope.savePrice(true);
        getPriceByConfigurationDeferred.resolve(null);
        putPriceDeferred.resolve(true);
        scope.$apply();

        //assert
        expect(lookUpDataServ.getPrices).toHaveBeenCalled();
    });

    it("should set existingPrice to true when updating a price with existing configuration", function () {
        //arrange
        scope.selectedPrice = {PriceId: 1};
        toastr = jasmine.createSpyObj("toastr", ["info"]);
        lookUpDataServ.getPriceByConfiguration.and.returnValue(getPriceByConfigurationDeferred.promise);
        lookUpDataServ.postPrice.and.returnValue(postPriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);
        lookUpDataServ.putPrice.and.returnValue(putPriceDeferred.promise);

        //act
        scope.savePrice(true);
        getPriceByConfigurationDeferred.resolve({PriceId: 2});
        scope.$apply();

        //assert
        expect(scope.existingPrice).toBe(true);
    });

    it("should notify clients when updating a new price with existing configuration", function () {
        //arrange
        toastr = jasmine.createSpyObj("toastr", ["info"]);
        scope.selectedPrice = {PriceId: 1};
        lookUpDataServ.getPriceByConfiguration.and.returnValue(getPriceByConfigurationDeferred.promise);
        lookUpDataServ.postPrice.and.returnValue(postPriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);
        lookUpDataServ.putPrice.and.returnValue(putPriceDeferred.promise);

        //act
        scope.savePrice(true);
        getPriceByConfigurationDeferred.resolve({PriceId: 2});
        scope.$apply();

        //assert
        expect(toastr.info).toHaveBeenCalled();
    });

    it("should set configuringNewPrice to true when calling method configureNewPrice", function () {
        //act
        scope.configureNewPrice();

        //assert
        expect(scope.configuringNewPrice).toBe(true);
    });

    it("should set configuringNewPrice to false when calling method selectPrice", function () {
        //arrange
        lookUpDataServ.getCarCategoriesByTourOperator.and.returnValue(getCarCategoriesByTourOperatorDeferred.promise);

        //act
        scope.selectPrice({});

        //assert
        expect(scope.configuringNewPrice).toBe(false);
    });

    it("should prevent form submission when calling method deletePrice", function () {
        //arrange
        event = jasmine.createSpyObj("event", ["preventDefault"]);
        lookUpDataServ.deletePrice.and.returnValue(deletePriceDeferred.promise);

        //act
        scope.deletePrice();

        //assert
        expect(event.preventDefault).toHaveBeenCalled();
    });

    it("should delete selected price when calling method deletePrice", function () {
        //arrange
        lookUpDataServ.deletePrice.and.returnValue(deletePriceDeferred.promise);

        //act
        scope.deletePrice();

        //assert
        expect(lookUpDataServ.deletePrice).toHaveBeenCalled();
    });

    it("should notify client when successfully deleting a price", function () {
        //arrange
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        scope.selectedPrice = {};
        lookUpDataServ.deletePrice.and.returnValue(deletePriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);

        //act
        scope.deletePrice();
        deletePriceDeferred.resolve(true);
        scope.$apply();

        //assert
        expect(toastr.success).toHaveBeenCalled();
    });

    it("should notify client when deleting a price fails", function () {
        //arrange
        toastr = jasmine.createSpyObj("toastr", ["error"]);
        scope.selectedPrice = {};
        lookUpDataServ.deletePrice.and.returnValue(deletePriceDeferred.promise);

        //act
        scope.deletePrice();
        deletePriceDeferred.resolve(false);
        scope.$apply();

        //assert
        expect(toastr.error).toHaveBeenCalled();
    });

    it("should load prices after successfully deleting a price", function () {
        //arrange
        scope.selectedPrice = {};
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        lookUpDataServ.deletePrice.and.returnValue(deletePriceDeferred.promise);
        lookUpDataServ.getPrices.and.returnValue(getPricesDeferred.promise);

        //act
        scope.deletePrice();
        deletePriceDeferred.resolve(true);
        scope.$apply();

        //assert
        expect(lookUpDataServ.getPrices).toHaveBeenCalled();
    });

    it("should set tourOperatorFormSubmitted to true when calling saveTourOperator method", function () {
        //arrange
        scope.tourOperatorFormSubmitted = false;
        scope.selectedTourOperator = {
            TourOperatorId: 0,
            CarProviders: carProviderEnumServ.types.cubacar
        };

        //act
        scope.saveTourOperator();

        //assert
        expect(scope.tourOperatorFormSubmitted).toBe(true);
    });

    it("should set tourOperatorFormSubmitted to false when calling method selectTourOperator", function () {
        //arrange
        scope.tourOperatorFormSubmitted = true;

        //act
        scope.selectTourOperator({});

        //assert
        expect(scope.tourOperatorFormSubmitted).toBe(false);
    });

    it("should create a new tourOperator when calling method saveTourOperator and the form is valid, new tourOperator has some associated car providers and tourOperator does not exist yet", function () {
        //arrange
        scope.selectedTourOperator = {
            TourOperatorId: 0,
            CarProviders: carProviderEnumServ.types.cubacar
        };
        lookUpDataServ.getTourOperatorByName.and.returnValue(getTourOperatorByNameDeferred.promise);
        lookUpDataServ.postTourOperator.and.returnValue(postTourOperatorDeferred.promise);

        //act
        scope.saveTourOperator(true);
        getTourOperatorByNameDeferred.resolve(null);
        scope.$apply();

        //assert
        expect(lookUpDataServ.postTourOperator).toHaveBeenCalled();
    });

    it("should not create a new tourOperator when calling method saveTourOperator and the form is invalid", function () {
        //arrange
        scope.selectedTourOperator = {
            TourOperatorId: 0,
            CarProviders: [
                {
                    CarProvider: {CarProviderId: 1}
                }]
        };

        //act
        scope.saveTourOperator(false);

        //assert
        expect(lookUpDataServ.postTourOperator).not.toHaveBeenCalled();
    });

    it("should not create a new tourOperator when calling method saveTourOperator and there is no car provider associated", function () {
        //arrange
        scope.selectedTourOperator = {
            TourOperatorId: 0
        };
        lookUpDataServ.getTourOperatorByName.and.returnValue(getTourOperatorByNameDeferred.promise);

        //act
        scope.saveTourOperator(true);
        getTourOperatorByNameDeferred.resolve(null);
        scope.$apply();

        //assert
        expect(lookUpDataServ.postTourOperator).not.toHaveBeenCalled();
    });

    it("should not create a new tourOperator when calling method saveTourOperator when the name already exists", function () {
        //arrange
        scope.selectedTourOperator = {
            TourOperatorId: 0,
            CarProviders: carProviderEnumServ.types.cubacar
        };
        toastr = jasmine.createSpyObj("toastr", ["info"]);
        lookUpDataServ.getTourOperatorByName.and.returnValue(getTourOperatorByNameDeferred.promise);
        lookUpDataServ.postTourOperator.and.returnValue(postTourOperatorDeferred.promise);

        //act
        scope.saveTourOperator(true);
        getTourOperatorByNameDeferred.resolve({});
        postTourOperatorDeferred.resolve(true);
        scope.$apply();

        //assert
        expect(lookUpDataServ.postTourOperator).not.toHaveBeenCalled();
    });

    it("should set existingTourOperatorName to true when calling method saveTourOperator when the name already exists", function () {
        //arrange
        scope.selectedTourOperator = {
            TourOperatorId: 0,
            CarProviders: carProviderEnumServ.types.cubacar
        };
        lookUpDataServ.getTourOperatorByName.and.returnValue(getTourOperatorByNameDeferred.promise);
        lookUpDataServ.postTourOperator.and.returnValue(postTourOperatorDeferred.promise);

        //act
        scope.saveTourOperator(true);
        getTourOperatorByNameDeferred.resolve({});
        scope.$apply();

        //assert
        expect(scope.existingTourOperatorName).toBe(true);
    });

    it("should notify client about existing touroperator when calling method saveTourOperator and the name already exists", function () {
        //arrange
        scope.selectedTourOperator = {
            TourOperatorId: 0,
            CarProviders: carProviderEnumServ.types.cubacar
        };

        toastr = jasmine.createSpyObj("toastr", ["info"]);
        lookUpDataServ.getTourOperatorByName.and.returnValue(getTourOperatorByNameDeferred.promise);

        //act
        scope.saveTourOperator(true);
        getTourOperatorByNameDeferred.resolve({});
        scope.$apply();

        //assert
        expect(toastr.info).toHaveBeenCalled();
    });

    it("should load tourOperators after successfully creating one", function () {
        //arrange
        scope.selectedTourOperator = {
            TourOperatorId: 0,
            CarProviders: carProviderEnumServ.types.cubacar
        };
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        lookUpDataServ.getTourOperatorByName.and.returnValue(getTourOperatorByNameDeferred.promise);
        lookUpDataServ.postTourOperator.and.returnValue(postTourOperatorDeferred.promise);
        lookUpDataServ.getTourOperators.and.returnValue(getTourOperatorDeferred.promise);

        //act
        scope.saveTourOperator(true);
        getTourOperatorByNameDeferred.resolve(null);
        postTourOperatorDeferred.resolve(true);
        getTourOperatorDeferred.resolve([{}]);
        scope.$apply();

        //assert
        expect(lookUpDataServ.getTourOperators).toHaveBeenCalled();
    });

    it("should notify client when successfully creating a tourOperator", function () {
        //arrange
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        scope.selectedTourOperator = {
            TourOperatorId: 0,
            CarProviders: carProviderEnumServ.types.cubacar
        };
        lookUpDataServ.getTourOperatorByName.and.returnValue(getTourOperatorByNameDeferred.promise);
        lookUpDataServ.postTourOperator.and.returnValue(postTourOperatorDeferred.promise);
        lookUpDataServ.getTourOperators.and.returnValue(getTourOperatorDeferred.promise);

        //act
        scope.saveTourOperator(true);
        getTourOperatorByNameDeferred.resolve(null);
        postTourOperatorDeferred.resolve(true);
        getTourOperatorDeferred.resolve([{}]);
        scope.$apply();

        //assert
        expect(toastr.success).toHaveBeenCalled();
    });

    it("should notify client when creating a tourOperator fails", function () {
        //arrange
        toastr = jasmine.createSpyObj("toastr", ["error"]);
        scope.selectedTourOperator = {
            TourOperatorId: 0,
            CarProviders: carProviderEnumServ.types.cubacar
        };
        lookUpDataServ.getTourOperatorByName.and.returnValue(getTourOperatorByNameDeferred.promise);
        lookUpDataServ.postTourOperator.and.returnValue(postTourOperatorDeferred.promise);
        lookUpDataServ.getTourOperators.and.returnValue(getTourOperatorDeferred.promise);

        //act
        scope.saveTourOperator(true);
        getTourOperatorByNameDeferred.resolve(null);
        postTourOperatorDeferred.resolve(false);
        scope.$apply();

        //assert
        expect(toastr.error).toHaveBeenCalled();
    });

    it("should update touroperator when calling method saveTourOperator tourOperatorForm is valid, there are associated car providers, and the name is not duplicated", function () {
        //arrange
        scope.selectedTourOperator = {
            TourOperatorId: 1,
            CarProviders: carProviderEnumServ.types.cubacar
        };
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        lookUpDataServ.getTourOperatorByName.and.returnValue(getTourOperatorByNameDeferred.promise);
        lookUpDataServ.putTourOperator.and.returnValue(putTourOperatorDeferred.promise);
        lookUpDataServ.getTourOperators.and.returnValue(getTourOperatorDeferred.promise);

        //act
        scope.saveTourOperator(true);
        getTourOperatorByNameDeferred.resolve(null);
        putTourOperatorDeferred.resolve(true);
        scope.$apply();

        //assert
        expect(lookUpDataServ.putTourOperator).toHaveBeenCalled();
    });

    it("should update touroperator when calling method saveTourOperator tourOperatorForm is valid, there are associated car providers, and the name is not duplicated (second case)", function () {
        //arrange
        scope.selectedTourOperator = {
            TourOperatorId: 1,
            CarProviders: carProviderEnumServ.types.cubacar
        };
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        lookUpDataServ.getTourOperatorByName.and.returnValue(getTourOperatorByNameDeferred.promise);
        lookUpDataServ.putTourOperator.and.returnValue(putTourOperatorDeferred.promise);
        lookUpDataServ.getTourOperators.and.returnValue(getTourOperatorDeferred.promise);

        //act
        scope.saveTourOperator(true);
        getTourOperatorByNameDeferred.resolve({TourOperatorId: 1});
        putTourOperatorDeferred.resolve(true);
        scope.$apply();

        //assert
        expect(lookUpDataServ.putTourOperator).toHaveBeenCalled();
    });

    it("should not update selectedTourOperator when calling method saveTourOperator and the form is invalid", function () {
        //arrange
        scope.selectedTourOperator = {
            TourOperatorId: 1,
            CarProviders: carProviderEnumServ.types.cubacar
        };

        //act
        scope.saveTourOperator(false);

        //assert
        expect(lookUpDataServ.putTourOperator).not.toHaveBeenCalled();
    });

    it("should not update selectedTourOperator when calling method saveTourOperator and there are no associated car providers", function () {
        //arrange
        scope.selectedTourOperator = {
            TourOperatorId: 1
        };

        //act
        scope.saveTourOperator(true);
        scope.$apply();

        //assert
        expect(lookUpDataServ.putTourOperator).not.toHaveBeenCalled();
    });

    it("should not update selectedTourOperator when calling method saveTourOperator and the name already exists", function () {
        //arrange
        scope.selectedTourOperator = {
            TourOperatorId: 1,
            CarProviders: carProviderEnumServ.types.cubacar
        };
        toastr = jasmine.createSpyObj("toastr", ["info"]);
        lookUpDataServ.getTourOperatorByName.and.returnValue(getTourOperatorByNameDeferred.promise);

        //act
        scope.saveTourOperator(true);
        getTourOperatorByNameDeferred.resolve({TourOperatorId: 2});
        scope.$apply();

        //assert
        expect(lookUpDataServ.putTourOperator).not.toHaveBeenCalled();
    });

    it("should notify client when calling updateTourOperator and the name already exists", function () {
        //arrange
        toastr = jasmine.createSpyObj("toastr", ["info"]);
        scope.selectedTourOperator = {
            TourOperatorId: 1,
            CarProviders: carProviderEnumServ.types.cubacar
        };
        lookUpDataServ.getTourOperatorByName.and.returnValue(getTourOperatorByNameDeferred.promise);

        //act
        scope.saveTourOperator(true);
        getTourOperatorByNameDeferred.resolve({TourOperatorId: 2});
        postTourOperatorDeferred.resolve(true);
        getTourOperatorDeferred.resolve([{}]);
        scope.$apply();

        //assert
        expect(toastr.info).toHaveBeenCalled();
    });

    it("should set existingTourOperatorName to true when calling updateTourOperator and the name already exists", function () {
        //arrange
        toastr = jasmine.createSpyObj("toastr", ["info"]);
        scope.selectedTourOperator = {
            TourOperatorId: 1,
            CarProviders: carProviderEnumServ.types.cubacar
        };
        lookUpDataServ.getTourOperatorByName.and.returnValue(getTourOperatorByNameDeferred.promise);

        //act
        scope.saveTourOperator(true);
        getTourOperatorByNameDeferred.resolve({TourOperatorId: 2});
        postTourOperatorDeferred.resolve(true);
        getTourOperatorDeferred.resolve([{}]);
        scope.$apply();

        //assert
        expect(scope.existingTourOperatorName).toBe(true);
    });

    it("should load tourOperators after successfully updating one", function () {
        //arrange
        scope.selectedTourOperator = {
            TourOperatorId: 1,
            CarProviders: carProviderEnumServ.types.cubacar
        };
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        lookUpDataServ.getTourOperatorByName.and.returnValue(getTourOperatorByNameDeferred.promise);
        lookUpDataServ.putTourOperator.and.returnValue(putTourOperatorDeferred.promise);
        lookUpDataServ.getTourOperators.and.returnValue(getTourOperatorDeferred.promise);

        //act
        scope.saveTourOperator(true);
        getTourOperatorByNameDeferred.resolve(null);
        putTourOperatorDeferred.resolve(true);
        getTourOperatorDeferred.resolve([{}]);
        scope.$apply();

        //assert
        expect(lookUpDataServ.getTourOperators).toHaveBeenCalled();
    });

    it("should notify client when successfully creating a tourOperator", function () {
        //arrange
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        scope.selectedTourOperator = {
            TourOperatorId: 1,
            CarProviders: carProviderEnumServ.types.cubacar
        };
        lookUpDataServ.getTourOperatorByName.and.returnValue(getTourOperatorByNameDeferred.promise);
        lookUpDataServ.putTourOperator.and.returnValue(putTourOperatorDeferred.promise);
        lookUpDataServ.getTourOperators.and.returnValue(getTourOperatorDeferred.promise);

        //act
        scope.saveTourOperator(true);
        getTourOperatorByNameDeferred.resolve(null);
        putTourOperatorDeferred.resolve(true);
        getTourOperatorDeferred.resolve([{}]);
        scope.$apply();

        //assert
        expect(toastr.success).toHaveBeenCalled();
    });

    it("should notify client when updating a tourOperator fails", function () {
        //arrange
        toastr = jasmine.createSpyObj("toastr", ["error"]);
        scope.selectedTourOperator = {
            TourOperatorId: 1,
            CarProviders: carProviderEnumServ.types.cubacar
        };
        lookUpDataServ.getTourOperatorByName.and.returnValue(getTourOperatorByNameDeferred.promise);
        lookUpDataServ.putTourOperator.and.returnValue(putTourOperatorDeferred.promise);

        //act
        scope.saveTourOperator(true);
        getTourOperatorByNameDeferred.resolve(null);
        putTourOperatorDeferred.resolve(false);
        scope.$apply();

        //assert
        expect(toastr.error).toHaveBeenCalled();
    });

    it("should define carProviderEnumService", function () {
        //assert
        expect(carProviderEnumServ).toBeDefined();
    });
});

var toastr;
var event;