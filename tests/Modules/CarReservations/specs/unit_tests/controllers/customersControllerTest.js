﻿"use strict";

describe("Customers Controller", function () {
    var scope;
    var customersController;
    var customerServ;
    var sessionServ;
    var getCustomersDeferred;
    var getCustomerMatchingPhoneNumberPatternDeferred;
    var postCustomerDeferred;
    var putCustomerDeferred;
    var modalMock;
    var customersModalMock;
    var modalMockPromiseDeferred;
    var promisePromiseDeferred;

    beforeEach(module("carReservationsApp", function ($provide) {
        customerServ = jasmine.createSpyObj("customerServ", [
           "get",
            "getCustomerMatchingPhoneNumberPattern",
            "post",
            "put"
        ]);

        sessionServ = {
            join: jasmine.createSpy("join")
        };

        customersModalMock = jasmine.createSpyObj("customersModalMock", ["show", "hide", "$promise"]);
        modalMock = function () {
            return customersModalMock;
        };

        $provide.value("customerService", customerServ);
        $provide.value("session", sessionServ);
        $provide.value("$modal", modalMock);
    }));

    beforeEach(inject(function ($injector) {
        scope = $injector.get("$rootScope");
        getCustomersDeferred = $injector.get("$q").defer();
        getCustomerMatchingPhoneNumberPatternDeferred = $injector.get("$q").defer();
        postCustomerDeferred = $injector.get("$q").defer();
        putCustomerDeferred = $injector.get("$q").defer();
        modalMockPromiseDeferred = $injector.get("$q").defer();
        promisePromiseDeferred = $injector.get("$q").defer();

        var controller = $injector.get("$controller");
        customersController = controller("customersController", { "$scope": scope });
    }));

    it("should load customer when loading", function () {
        //arrange
        customerServ.get.and.returnValue(getCustomersDeferred.promise);

        //act
        scope.init();
        getCustomersDeferred.resolve([{}, {}]);
        scope.$apply();

        //assert
        expect(scope.customers).toBeDefined();
    });

    it("should set selectedCustomer when loading", function () {
        //arrange
        customerServ.get.and.returnValue(getCustomersDeferred.promise);

        //act
        scope.init();
        getCustomersDeferred.resolve([{}, {}]);
        scope.$apply();

        //assert
        expect(scope.selectedCustomer).toBeDefined();
    });

    it("should join app's session when loading", function () {
        //arrange
        customerServ.get.and.returnValue(getCustomersDeferred.promise);

        //act
        scope.init();

        //assert
        expect(sessionServ.join).toHaveBeenCalled();
    });


    it("should set selectedCustomer when calling selectCustomer method", function () {
        //act
        scope.selectCustomer({});

        //assert
        expect(scope.selectedCustomer).not.toBeNull();
    });

    it("should assign an empty customer to selectedCustomer when calling createNewCustomer", function () {
        //arrange
        scope.selectedCustomer = {};

        //act
        scope.createNewCustomer();

        //assert
        expect(scope.selectedCustomer.CustomerId).toBe(0);
        expect(scope.selectedCustomer.FirstName).toBe("");
        expect(scope.selectedCustomer.LastName).toBe("");
        expect(scope.selectedCustomer.Email).toBe("");
        expect(scope.selectedCustomer.CellPhone).toBe("");
    });

    it("should assign the first customer to selectedCustomer when loading", function () {
        //arrange
        customerServ.get.and.returnValue(getCustomersDeferred.promise);

        //act
        scope.init();

        //assert
        expect(scope.selectedCustomer).not.toBeNull();
    });

    it("should set selectedMatchingCustomer to null when loading", function () {
        //arrange
        customerServ.get.and.returnValue(getCustomersDeferred.promise);

        //act
        scope.init();

        //assert
        expect(scope.selectedMatchingCustomer).toBeNull();
    });

    it("should show existing customer dialog when inserting new customer and its phone number matches other existing ones", function () {
        //arrange
        scope.selectedCustomer = {
            CustomerId: 0,
            CellPhone: 7887878787
        };
        customerServ.get.and.returnValue(getCustomersDeferred.promise);
        customerServ.getCustomerMatchingPhoneNumberPattern.and.returnValue(getCustomerMatchingPhoneNumberPatternDeferred.promise);

        //act
        scope.init();
        customersModalMock.$promise = modalMockPromiseDeferred.promise;

        //act
        scope.saveCustomer(true);
        getCustomerMatchingPhoneNumberPatternDeferred.resolve([
            { CustomerId: 1 },
            { CustomerId: 2 }]);
        modalMockPromiseDeferred.resolve(null);
        scope.$apply();

        //assert
        expect(customersModalMock.show).toHaveBeenCalled();
    });

    it("should assign matchingCustomers when inserting customer and there are other customers matching the phone number", function () {
        //arrange
        scope.matchingCustomers = null;
        scope.selectedCustomer = {
            CustomerId: 0,
            CellPhone: 7897897987
        };
        customerServ.get.and.returnValue(getCustomersDeferred.promise);
        customerServ.getCustomerMatchingPhoneNumberPattern.and.returnValue(getCustomerMatchingPhoneNumberPatternDeferred.promise);

        //act
        scope.init();
        customersModalMock.$promise = modalMockPromiseDeferred.promise;

        //act
        scope.saveCustomer(true);
        getCustomerMatchingPhoneNumberPatternDeferred.resolve([
            { CustomerId: 1 },
            { CustomerId: 2 }]);
        modalMockPromiseDeferred.resolve(null);
        scope.$apply();

        //assert
        expect(scope.matchingCustomers).not.toBeNull();
    });

    it("should insert customer when inserting a customer and its phone number does not match other existing ones (null)", function () {
        //arrange
        scope.selectedCustomer = {
            CustomerId: 0,
            CellPhone: 7887878787
        };
        customerServ.get.and.returnValue(getCustomersDeferred.promise);
        customerServ.getCustomerMatchingPhoneNumberPattern.and.returnValue(getCustomerMatchingPhoneNumberPatternDeferred.promise);
        customerServ.post.and.returnValue(postCustomerDeferred.promise);

        //act
        scope.init();
        customersModalMock.$promise = modalMockPromiseDeferred.promise;

        //act
        scope.saveCustomer(true);
        getCustomerMatchingPhoneNumberPatternDeferred.resolve(null);
        scope.$apply();

        //assert
        expect(customerServ.post).toHaveBeenCalled();
    });

    it("should do nothing when calling method saveCustomer and the form is invalid", function () {
        //act
        scope.saveCustomer(false);

        //assert
        expect(customerServ.getCustomerMatchingPhoneNumberPattern).not.toHaveBeenCalled();
    });

    it("should check for duplicate customer based on cellPhone when inserting new customer", function () {
        //arrange
        scope.selectedCustomer = {
            CustomerId: 0,
            CellPhone: 7887878787
        };
        customerServ.getCustomerMatchingPhoneNumberPattern.and.returnValue(getCustomerMatchingPhoneNumberPatternDeferred.promise);

        //act
        scope.saveCustomer(true);

        //assert
        expect(customerServ.getCustomerMatchingPhoneNumberPattern).toHaveBeenCalled();
    });

    it("should update customer when calling saveCustomer method on existing customer", function () {
        //arrange
        scope.selectedCustomer = {
            CustomerId: 1,
            CellPhone: 7897897987
        };

        customerServ.get.and.returnValue(getCustomersDeferred.promise);
        customersModalMock.$promise = modalMockPromiseDeferred.promise;
        customerServ.put.and.returnValue(putCustomerDeferred.promise);
        scope.init();

        //act
        scope.saveCustomer(true);

        //assert
        expect(customerServ.put).toHaveBeenCalled();
    });

    it("should notify user when customer is successfully created", function() {
        //arrange
        scope.selectedCustomer = {
            CustomerId: 0,
            CellPhone: 7887878787
        };
        customerServ.get.and.returnValue(getCustomersDeferred.promise);
        customersModalMock.$promise = modalMockPromiseDeferred.promise;
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        customerServ.getCustomerMatchingPhoneNumberPattern.and.returnValue(getCustomerMatchingPhoneNumberPatternDeferred.promise);
        customerServ.post.and.returnValue(postCustomerDeferred.promise);
        scope.init();


        //act
        scope.saveCustomer(true);
        getCustomerMatchingPhoneNumberPatternDeferred.resolve([]);
        postCustomerDeferred.resolve(true);
        scope.$apply();

        //assert
        expect(toastr.success).toHaveBeenCalled();
    });

    it("should notify user when there is an error creating customer", function () {
        //arrange
        scope.selectedCustomer = {
            CustomerId: 0,
            CellPhone: 7887878787
        };
        customersModalMock.$promise = modalMockPromiseDeferred.promise;
        toastr = jasmine.createSpyObj("toastr", ["error"]);
        customerServ.get.and.returnValue(getCustomersDeferred.promise);
        customerServ.getCustomerMatchingPhoneNumberPattern.and.returnValue(getCustomerMatchingPhoneNumberPatternDeferred.promise);
        customerServ.post.and.returnValue(postCustomerDeferred.promise);
        scope.init();

        //act
        scope.saveCustomer(true);
        getCustomerMatchingPhoneNumberPatternDeferred.resolve([]);
        postCustomerDeferred.resolve(false);
        scope.$apply();

        //assert
        expect(toastr.error).toHaveBeenCalled();
    });

    it("should notify user when customer is successfully updated", function () {
        //arrange
        scope.selectedCustomer = {
            CustomerId: 1,
            CellPhone: 7887878787
        };
        customersModalMock.$promise = modalMockPromiseDeferred.promise;
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        customerServ.get.and.returnValue(getCustomersDeferred.promise);
        customerServ.put.and.returnValue(putCustomerDeferred.promise);
        scope.init();

        //act
        scope.saveCustomer(true);
        getCustomerMatchingPhoneNumberPatternDeferred.resolve([]);
        putCustomerDeferred.resolve(true);
        scope.$apply();

        //assert
        expect(toastr.success).toHaveBeenCalled();
    });

    it("should notify user when there is an error updating customer", function () {
        //arrange
        scope.selectedCustomer = {
            CustomerId: 1,
            CellPhone: 7887878787
        };

        customerServ.get.and.returnValue(getCustomersDeferred.promise);
        customerServ.put.and.returnValue(putCustomerDeferred.promise);
        customersModalMock.$promise = modalMockPromiseDeferred.promise;
        toastr = jasmine.createSpyObj("toastr", ["error"]);
        scope.init();

        //act
        scope.saveCustomer(true);
        getCustomerMatchingPhoneNumberPatternDeferred.resolve([]);
        putCustomerDeferred.resolve(false);
        scope.$apply();

        //assert
        expect(toastr.error).toHaveBeenCalled();
    });

    it("should close modal after successfully inserting customer", function () {
        //arrange
        scope.selectedCustomer = {
            CustomerId: 0,
            CellPhone: 7887878787
        };
        customerServ.get.and.returnValue(getCustomersDeferred.promise);
        customerServ.getCustomerMatchingPhoneNumberPattern.and.returnValue(getCustomerMatchingPhoneNumberPatternDeferred.promise);
        customerServ.post.and.returnValue(postCustomerDeferred.promise);
        customersModalMock.$promise = modalMockPromiseDeferred.promise;
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        scope.init();

        //act
        scope.saveCustomer(true);
        getCustomerMatchingPhoneNumberPatternDeferred.resolve([]);
        postCustomerDeferred.resolve(true);
        modalMockPromiseDeferred.resolve(null);
        scope.$apply();

        //assert
        expect(customersModalMock.hide).toHaveBeenCalled();
    });

    it("should close modal after unsuccessfully inserting customer", function () {
        //arrange
        scope.selectedCustomer = {
            CustomerId: 0,
            CellPhone: 7887878787
        };

        customersModalMock.$promise = modalMockPromiseDeferred.promise;
        toastr = jasmine.createSpyObj("toastr", ["error"]);
        customerServ.get.and.returnValue(getCustomersDeferred.promise);
        customerServ.getCustomerMatchingPhoneNumberPattern.and.returnValue(getCustomerMatchingPhoneNumberPatternDeferred.promise);
        customerServ.post.and.returnValue(postCustomerDeferred.promise);
        scope.init();

        //act
        scope.saveCustomer(true);
        getCustomerMatchingPhoneNumberPatternDeferred.resolve([]);
        postCustomerDeferred.resolve(false);
        modalMockPromiseDeferred.resolve(null);
        scope.$apply();

        //assert
        expect(customersModalMock.hide).toHaveBeenCalled();
    });

    it("should close modal after successfully updating customer", function () {
        //arrange
        scope.selectedCustomer = {
            CustomerId: 1,
            CellPhone: 7887878787
        };
        customerServ.get.and.returnValue(getCustomersDeferred.promise);
        customerServ.put.and.returnValue(putCustomerDeferred.promise);
        customersModalMock.$promise = modalMockPromiseDeferred.promise;
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        scope.init();

        //act
        scope.saveCustomer(true);
        getCustomerMatchingPhoneNumberPatternDeferred.resolve([]);
        putCustomerDeferred.resolve(true);
        modalMockPromiseDeferred.resolve(null);
        scope.$apply();

        //assert
        expect(customersModalMock.hide).toHaveBeenCalled();
    });

    it("should close modal after unsuccessfully updating customer", function () {
        //arrange
        scope.selectedCustomer = {
            CustomerId: 1,
            CellPhone: 7887878787
        };
        customersModalMock.$promise = modalMockPromiseDeferred.promise;
        toastr = jasmine.createSpyObj("toastr", ["error"]);
        customerServ.get.and.returnValue(getCustomersDeferred.promise);
        customerServ.put.and.returnValue(putCustomerDeferred.promise);
        scope.init();

        //act 
        scope.saveCustomer(true);
        getCustomerMatchingPhoneNumberPatternDeferred.resolve([]);
        putCustomerDeferred.resolve(false);
        modalMockPromiseDeferred.resolve(null);
        scope.$apply();

        //assert
        expect(customersModalMock.hide).toHaveBeenCalled();
    });

    it("should load customer when successfully creating one", function() {
        //arrange
        scope.selectedCustomer = {
            CustomerId: 0,
            CellPhone: 7887878787
        };

        customerServ.get.and.returnValue(getCustomersDeferred.promise);
        customerServ.post.and.returnValue(postCustomerDeferred.promise);
        customersModalMock.$promise = modalMockPromiseDeferred.promise;
        customerServ.getCustomerMatchingPhoneNumberPattern.and.returnValue(getCustomerMatchingPhoneNumberPatternDeferred.promise);
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        scope.init();

        //act 
        scope.saveCustomer(true);
        getCustomerMatchingPhoneNumberPatternDeferred.resolve([]);
        postCustomerDeferred.resolve(true);
        modalMockPromiseDeferred.resolve(null);
        scope.$apply();

        //assert
        expect(customerServ.get.calls.count()).toBe(2);
    });

    it("should load customer when successfully updating one", function () {
        //arrange
        scope.selectedCustomer = {
            CustomerId: 1,
            CellPhone: 7887878787
        };
        customerServ.get.and.returnValue(getCustomersDeferred.promise);
        customerServ.put.and.returnValue(putCustomerDeferred.promise);
        customersModalMock.$promise = modalMockPromiseDeferred.promise;
        toastr = jasmine.createSpyObj("toastr", ["success"]);
        scope.init();

        //act
        scope.saveCustomer(true);
        getCustomerMatchingPhoneNumberPatternDeferred.resolve([]);
        putCustomerDeferred.resolve(true);
        scope.$apply();

        //assert
        expect(customerServ.get.calls.count()).toBe(2);
    });
});

var toastr;
