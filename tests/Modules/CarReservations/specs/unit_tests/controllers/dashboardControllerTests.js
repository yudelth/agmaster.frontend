﻿//"use strict";

describe("Dashboard Controller", function () {
    var scope;
    var deferred;
    var dashboardController;
    var dashboardService;
    var reservationHandler;
    var resStatusEnumServ;
    var sessionServ;
    var location;

    beforeEach(module("carReservationsApp", function ($provide) {

        reservationHandler = {
            getReservationFinalPrice: function () {
            },
            addPaymentToReservation: function () {
            },
            removePaymentFromReservation: function () {
            },
            assignTotalDaysToReservation: function () {
            },
            isPaid: function (reservation) {
            },
            isNotPaid: function (reservation) {
            },
            isPartiallyPaid: function (reservation) {
            },
            isConfirmed: function (reservation) {
            },
            isNotConfirmed: function (reservation) {
            },
            isCancelled: function (reservation) {
            }
        };

        dashboardService = {
            getPartiallyPaidReservations: function () {
                return deferred.promise;
            }
        };

        sessionServ = {
            join: jasmine.createSpy("join")
        };

        location = {
            path: function(){},
            search: function(){}
        };

        $provide.value("dashboardService", dashboardService);
        $provide.value("reservationHandler", reservationHandler);
        $provide.value("session", sessionServ);
    }));

    beforeEach(inject(function ($injector) {
        scope = $injector.get("$rootScope");
        deferred = $injector.get("$q").defer();

        resStatusEnumServ = $injector.get("reservationStatusEnumService");

        var controller = $injector.get("$controller");
        dashboardController = controller("dashboardController", { "$scope": scope, $location: location });
    }));

    //it("should load the list of partially paid reservations", function () {
    
    //    var data = [
    //            { Count: 1, Value: 1 },
    //            { Count: 2, Value: 2 },
    //            { Count: 1, Value: 3 },
    //            { Count: 4, Value: 4 },
    //            { Count: 5, Value: 5 },
    //            { Count: 2, Value: 6 }
    //    ];
    
    //    //action
    //    scope.init();
    //    deferred.resolve(data);
    //    scope.$apply();
    
    //    //assert
    //    expect(scope.partiallyPaidReservations).not.toBeNull();
    //    expect(scope.partiallyPaidReservations.length).toEqual(6);
    //});

//    it("should call reservation handler to find out if reservation is paid", function () {
//        //arrange
//        var reservation = { ReservationStatus: resStatusEnumServ.status.paid };
//        spyOn(reservationHandler, "isPaid");

//        //act
//        var result = scope.isPaid(reservation);

//        //assert
//        expect(reservationHandler.isPaid).toHaveBeenCalled();
//    });

//    it("should call reservation handler to find out if reservation is partially paid", function () {
//        //arrange
//        var reservation = { ReservationStatus: resStatusEnumServ.status.partiallyPaid };
//        spyOn(reservationHandler, "isPartiallyPaid");

//        //act
//        var result = scope.isPartiallyPaid(reservation);

//        //assert
//        expect(reservationHandler.isPartiallyPaid).toHaveBeenCalled();
//    });

//    it("should call reservation handler to find out if reservation is not paid", function () {
//        //arrange
//        var reservation = { ReservationStatus: resStatusEnumServ.status.noPaid };
//        spyOn(reservationHandler, "isNotPaid");

//        //act
//        var result = scope.isNotPaid(reservation);

//        //assert
//        expect(reservationHandler.isNotPaid).toHaveBeenCalled();
//    });

//    it("should call reservation handler to find out if reservation is confirmed", function () {
//        //arrange
//        var reservation = { ReservationStatus: resStatusEnumServ.status.confirmed };
//        spyOn(reservationHandler, "isConfirmed");

//        //act
//        var result = scope.isConfirmed(reservation);

//        //assert
//        expect(reservationHandler.isConfirmed).toHaveBeenCalled();
//    });

//    it("should call reservation handler to find out if reservation is not confirmed", function () {
//        //arrange
//        var reservation = { ReservationStatus: resStatusEnumServ.status.noConfirmed };
//        spyOn(reservationHandler, "isNotConfirmed");

//        //act
//        var result = scope.isNotConfirmed(reservation);

//        //assert
//        expect(reservationHandler.isNotConfirmed).toHaveBeenCalled();
//    });

//    it("should redirect to reservation details", function () {
//        //Arrange
//         spyOn(location, "path");
//         spyOn(location, "search");

//        //act
//        scope.reservationDetails(1);

//        //assert
//        expect(location.path).toHaveBeenCalledWith("/reservations");
//        expect(location.search).toHaveBeenCalledWith('reservationId', 1);
//    });

//    it("should clear all rows except desired one", function () {
//        //arrange
//        var removed = 0;
//        var added = 0;

//        $ = function (selector) {
//            return {
//                removeClass: function () { removed++; return { fadeIn: function () { }, fadeOut: function () { } };},
//                addClass: function () { added++; return { fadeIn: function () { }, fadeOut: function () { } }; }
//            };
//        };

//        //act
//        var result = scope.selectRow(1);

//        //assert
//        expect(removed).toEqual(1);
//        expect(added).toEqual(1);
//    });

//    it("should have an instance of session service", function () {
//        //assert
//        expect(sessionService).toBeDefined();
//    });

//    it("should call sessionService.join when loading", function () {
//        //act
//        scope.init();

//        //assert
//        expect(sessionServ.join).toHaveBeenCalled();
//    });

//    it("should assign scope.partiallyPaidReservations", function(){
//        //arrange
//        scope.partiallyPaidReservations = null;

//        //act
//        scope.init();
//        deferred.resolve({});
//        scope.$apply();

//       //assert
//        expect(scope.partiallyPaidReservations).not.toBeNull();
//    });

//    it("should not assign scope.partiallyPaidReservations when there is no paid reservation", function(){
//        //act
//        scope.init();
//        deferred.resolve(null);
//        scope.$apply();

//        //assert
//        expect(scope.partiallyPaidReservations).toBeUndefined();
//    });
});

var $;
