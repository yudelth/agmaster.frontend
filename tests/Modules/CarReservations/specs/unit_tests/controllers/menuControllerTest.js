/**
 * Created by ytchicamboud on 11/20/2015.
 */
"use strict";

describe("Menu Controller", function() {
    var scope;
    var sessionServ;
    var alertServ;
    var menuController;
    var location;
    var interval;

    var getAlertsDeferred;

    beforeEach(module("carReservationsApp", function ($provide) {
        sessionServ = jasmine.createSpyObj( "sessionServ", [ "join"]);
        sessionServ.sessionData = {
            role: "userRole"
        };

        location = {
            path : jasmine.createSpy()
        };

        alertServ = jasmine.createSpyObj("alertServ", ["getAlerts"]);
        interval = jasmine.createSpy();

        $provide.value("alertService", alertServ);
        $provide.value("session", sessionServ);
    }));

    beforeEach(inject(function ($injector) {
        getAlertsDeferred = $injector.get("$q").defer();
        scope = $injector.get("$rootScope");

        var controller = $injector.get("$controller");
        menuController = controller("menuController", { "$scope": scope, "$location": location, "$interval": interval });
    }));

    it("should join sessionService", function () {
        //arrange
        alertServ.getAlerts.and.returnValue(getAlertsDeferred.promise);

        //act
        scope.init();

        //assert
        expect(interval).toHaveBeenCalled();
    });

    it("should assign role to scope.userRole", function(){
        //arrange
        scope.userRole = "";
        alertServ.getAlerts.and.returnValue(getAlertsDeferred.promise);

       //act
        scope.init();

        //assert
        expect(scope.userRole).toBe("userrole");
    });
});

