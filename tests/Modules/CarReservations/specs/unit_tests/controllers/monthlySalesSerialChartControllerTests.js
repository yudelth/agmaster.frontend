// /**
//  * Created by ytchicamboud on 8/5/2015.
//  */
// "use strict";
//
// describe("Monthly Sales Serial Chart Controller", function () {
//     var scope;
//     var dashboardServ;
//     var getReservationsByMonthDeferred;
//     var controller;
//
//     beforeEach(module("carReservationsApp", function ($provide) {
//
//         dashboardServ = jasmine.createSpyObj("dashboardService", ["getReservationsByMonth"]);
//         // dashboardService = {
//         //     getReservationsByMonth: jasmine.createSpy().andCallFake(function() {
//         //         return getReservationsByMonthDeferred.promise;
//         //     })
//         // };
//
//         categoryAxisMock = {};
//         valueAxisMock = {};
//         graphMock = {};
//
//         chartMock = {
//             categoryAxis: categoryAxisMock,
//             addValueAxis: jasmine.createSpy(),
//             addGraph: jasmine.createSpy(),
//             write: jasmine.createSpy()
//         };
//
//         AmCharts = jasmine.createSpyObj("AmCharts", ["AmSerialChart", "ValueAxis", "AmGraph" ]);
//
//         // AmCharts = {
//         //     AmSerialChart: jasmine.createSpy().andCallFake(function(){
//         //         return chartMock;
//         //     }),
//         //     ValueAxis: jasmine.createSpy().andCallFake(function(){
//         //         return valueAxisMock;
//         //     }),
//         //     AmGraph: jasmine.createSpy().andCallFake(function(){
//         //         return graphMock;
//         //     })
//         // };
//
//         $provide.value("dashboardService", dashboardServ);
//     }));
//
//     beforeEach(inject(function ($injector) {
//         scope = $injector.get("$rootScope");
//         getReservationsByMonthDeferred = $injector.get("$q").defer();
//
//         var controllerMaster = $injector.get("$controller");
//         controller = controllerMaster("monthlySalesSerialChartController", { "$scope": scope });
//     }));
//
//     it("should assign scope.monthlySale when there is data", function(){
//         //arrange
//         scope.monthlysale = null;
//
//        //act
//         init();
//         getReservationsByMonthDeferred.resolve({});
//         scope.$apply();
//
//         //assert
//         expect( scope.monthlysale).not.toBeNull();
//     });
//
//     it("should not assign scope.monthlySale when there is no data", function(){
//         //arrange
//         scope.monthlysale = {};
//
//         //act
//         init();
//         getReservationsByMonthDeferred.resolve(null);
//         scope.$apply();
//
//         //assert
//         expect( scope.monthlysale).toBeNull();
//     });
//
//     it("should not call initChart when there is no data", function(){
//         //arrange
//         scope.initChart = jasmine.createSpy("initChart").andCallFake(function(){
//
//         });
//
//         //act
//         init();
//         getReservationsByMonthDeferred.resolve(null);
//         scope.$apply();
//
//         //assert
//         expect( scope.initChart).not.toHaveBeenCalled();
//     });
// });
//
// var init = jasmine.createSpy("init");
// var AmCharts;
// var chartMock;
// var categoryAxisMock;
// var valueAxisMock;
// var graphMock;