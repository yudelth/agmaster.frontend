﻿"use strict";

describe("Reports Controller", function () {
    var scope;
    var reportsController;
    var lookUpDataServ;
    var sessionServ;
    var reportGeneratorServ;
    var getTourOperatorDeferred;
    var generateSalesReportDeferred;
    var generateActiveReservationsReportDeferred;
    var generateSalesByOperatorReportDeferred;
    var generateSalesLimitedReportDeferred;
    var getRetailersDeferred;
    var getCarCategoriesDeferred;

    beforeEach(module("carReservationsApp", function ($provide) {
        lookUpDataServ = jasmine.createSpyObj("lookUpDataServ", ["getTourOperators", "getReservationStatuses", "getRetailers", "getCarCategories"]);
        var getHeadersMock = jasmine.createSpyObj("getHeadersMock", ["getHeaders"]);

        sessionServ = {
            join: jasmine.createSpy("join"),
            getHeaders: getHeadersMock,
            sessionData: {
                userId: 0,
                role: "role"
            }
        };

        reportGeneratorServ = jasmine.createSpyObj("reportGeneratorServ",
            ["generateSalesReport", "generateSalesLimitedReport",
                "generateActiveReservationsReport", "generateSalesByOperatorReport", "generateSalesByCarCategoryReport"]);

        $provide.value("lookUpDataService", lookUpDataServ);
        $provide.value("session", sessionServ);
        $provide.value("reportGeneratorService", reportGeneratorServ);
    }));

    beforeEach(inject(function ($injector) {
        scope = $injector.get("$rootScope");
        var controller = $injector.get("$controller");
        reportsController = controller("reportsController", { "$scope": scope });

        getTourOperatorDeferred = $injector.get("$q").defer();
        generateSalesReportDeferred = $injector.get("$q").defer();
        generateActiveReservationsReportDeferred = $injector.get("$q").defer();
        generateSalesByOperatorReportDeferred = $injector.get("$q").defer();
        generateSalesLimitedReportDeferred = $injector.get("$q").defer();
        getRetailersDeferred = $injector.get("$q").defer();
        getCarCategoriesDeferred = $injector.get("$q").defer();
    }));

    describe("init method", function(){
        function arrangeInit(){
            lookUpDataServ.getTourOperators.and.returnValue(getTourOperatorDeferred.promise);
            lookUpDataServ.getReservationStatuses.and.returnValue([]);
            lookUpDataServ.getRetailers.and.returnValue(getRetailersDeferred.promise);
            lookUpDataServ.getCarCategories.and.returnValue(getCarCategoriesDeferred.promise);
        };

        it("should assign todaysDate", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();

            //assert
            expect(scope.todaysDate).toBeDefined();
        });

        it("should assign user role to scope.userRole variable", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();

            //assert
            expect(scope.userRole).toBe("role");
        });

        it("should assign todaysDate a value", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();

            //assert
            expect(scope.todaysDate).not.toBeNull();
        });

        it("should assign tourOperators", function () {
            //arrange
            arrangeInit();

            //arrange
            var tourOperators = [
                {TourOperatorId: 1, Name: "Name", ParentTourOperatorId: 1},
                {TourOperatorId: 2, Name: "Name", ParentTourOperatorId: 1}
            ];

            //act
            scope.init();
            getTourOperatorDeferred.resolve(tourOperators);
            scope.$apply();

            //assert
            expect(scope.tourOperators).toBeDefined();
        });

        it("should assign tourOperators a value", function () {
            //arrange
            var tourOperators = [
                {TourOperatorId: 1, Name: "Name", ParentTourOperatorId: 1},
                {TourOperatorId: 2, Name: "Name", ParentTourOperatorId: 1}
            ];
            arrangeInit();

            //act
            scope.init();
            getTourOperatorDeferred.resolve(tourOperators);
            scope.$apply();

            //assert
            expect(scope.tourOperators).not.toBeNull();
            expect(scope.tourOperators.length).toBeGreaterThan(0);
        });

        it("should call join method session service", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();

            //assert
            expect(sessionServ.join).toHaveBeenCalled();
        });

        it("should assign statuses a value", function () {
            //arrange
            var statuses = [
                {StatusId: 1, Name: "Name"},
                {StatusId: 2, Name: "Name"}
            ];
            arrangeInit();

            //act
            scope.init();

            //assert
            expect(scope.statuses).not.toBeNull();
        });

        it("should initialize report instance", function () {
            //arrange
            arrangeInit();
            //act
            scope.init();

            //assert
            expect(scope.tourOperatorReport).toBeDefined();
        });

        it("should initialize tour operator report instance with default values", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();
            getTourOperatorDeferred.resolve([]);
            scope.$apply();

            //assert
            expect(scope.tourOperatorReport.TourOperatorId).toBe(0);
            expect(scope.tourOperatorReport.StatusId).toBe(0);
        });

        it("should load car categories", function () {
            //arrange
            arrangeInit();
            var categories = [{
                CategoryId: 1
            },
                {
                CategoryId: 10
            }];

            //act
            scope.init();
            getCarCategoriesDeferred.resolve(categories);
            scope.$apply();

            //assert
            expect(scope.carCategories).toBe(categories);
        })

        it("should add All to tourOperator list ", function(){
            //arrange
            arrangeInit();

            var tourOperators = [
                {TourOperatorId: 1, Name: "Name", ParentTourOperatorId: 1},
                {TourOperatorId: 2, Name: "Name", ParentTourOperatorId: 1}
            ];

            //act
            scope.init();
            getTourOperatorDeferred.resolve(tourOperators);
            scope.$apply();

            //assert
            expect(scope.tourOperators).toContain(jasmine.objectContaining({Name: "-Todos-" }));
            expect(scope.tourOperators).toContain(jasmine.objectContaining({TourOperatorId: 0 }));
        });

        it("should add Include All to status list ", function(){
            //arrange
            var statuses = [
                {StatusId: 1, Name: "Name"},
                {StatusId: 2, Name: "Name"}
            ];
            arrangeInit();

            //act
            scope.init();

            //assert
            expect(scope.statuses).toContain(jasmine.objectContaining({Name: "-Todos-" }));
            expect(scope.statuses).toContain(jasmine.objectContaining({StatusId: 0 }));
        });

        it("should initialize scope.salesByOperatorReport", function(){
            //arrange
            arrangeInit();

            //act
            scope.init();

            //assert
            expect(scope.salesByOperatorsReport).toBeDefined();
            //expect(scope.salesByOperatorsReport.StartDate).toBe("");
            //expect(scope.salesByOperatorsReport.EndDate).toBe("");
        });
    });
    
    describe("generateTourOperatorReport method", function(){
        it("should be call generateTourOperatorReport method with right parameters value", function () {
            //arrange
            scope.tourOperatorReport = {
                StartDate: "2015/10/10",
                EndDate: "2015/10/25",
                TourOperatorId: 1,
                StatusId: 4,
                RetailerIdentifier: "retailer"
            };

            //act
            scope.generateTourOperatorReport(true);

            //assert
            expect(reportGeneratorServ.generateSalesReport).toHaveBeenCalledWith("2015/10/10", "2015/10/25", 1, 4, "retailer");
        });

        it("should not called generateTourOperatorReport method if form is not valid", function(){
            //act
            scope.generateTourOperatorReport(false);

            //assert
            expect(reportGeneratorServ.generateSalesReport).not.toHaveBeenCalled();
        });

        it("should call generateTourOperatorReport method on ReportGeneratorService when calling generateTourOperatorReport", function () {
            //arrange
            scope.tourOperatorReport = {};

            //act
            scope.generateTourOperatorReport(true);

            //assert
            expect(reportGeneratorServ.generateSalesReport).toHaveBeenCalled();
        });
    });

    describe("activateTab method", function(){
        it("should set activeTab value when calling activateTab method", function () {
            //act
            scope.activateTab("aTabId");

            //assert
            expect(scope.activeTab).toBe("aTabId");
        });
    });

    describe("selectMenu method", function(){
        it("should set selectMenu value when calling selectMenu method", function () {
            //act
            scope.selectMenu("aMenuId");

            //assert
            expect(scope.selectedMenu).toBe("aMenuId");
        });
    });

    describe("generateReservation method", function() {
        it("should be call generateReservationsReport method with right parameters value", function () {
            //arrange
            scope.reservationsReport = {
                StartDate: "2015/10/10",
                EndDate: "2015/10/25"
            };

            //act
            scope.generateReservationsReport(true);

            //assert
            expect(reportGeneratorServ.generateActiveReservationsReport).toHaveBeenCalledWith("2015/10/10", "2015/10/25");
        });

        it("should not called generateReservationsReport method if form is not valid", function () {
            //act
            scope.generateReservationsReport(false);

            //assert
            expect(reportGeneratorServ.generateActiveReservationsReport).not.toHaveBeenCalled();
        });
    });

    it("should have an instance of lookUpDataService", function () {
        //assert
        expect(lookUpDataServ).toBeDefined();
    });

    it("should be called generateSalesByOperatorReport method with right parameters value", function () {
        //arrange
        scope.salesByOperatorsReport = {
            StartDate: "2015/10/10",
            EndDate: "2015/10/25"
        };

        //act
        scope.generateSalesByOperatorsReport(true);

        //assert
        expect(reportGeneratorServ.generateSalesByOperatorReport).toHaveBeenCalledWith("2015/10/10", "2015/10/25");
    });

    it("should not call generateSalesByOperatorReport method if form is not valid", function () {
        //act
        scope.generateSalesByOperatorsReport(false);

        //assert
        expect(reportGeneratorServ.generateSalesByOperatorReport).not.toHaveBeenCalled();
    });

    it("should be call generateTourOperatorLimitedReport method with right parameters value", function () {
        //arrange
        scope.tourOperatorReport = {
            StartDate: "2015/10/10",
            EndDate: "2015/10/25",
            TourOperatorId: 1,
            StatusId: 4
        };

        //act
        scope.generateTourOperatorLimitedReport(true);

        //assert
        expect(reportGeneratorServ.generateSalesLimitedReport).toHaveBeenCalledWith("2015/10/10", "2015/10/25", 1, 4);
    });

    it("should not called generateTourOperatorLimitedReport method if form is not valid", function () {
        //act
        scope.generateTourOperatorLimitedReport(false);

        //assert
        expect(reportGeneratorServ.generateSalesLimitedReport).not.toHaveBeenCalled();
    });

    it("should call generateTourOperatorLimitedReport method on ReportGeneratorService when calling generateTourOperatorLimitedReport", function () {
        //arrange
        scope.tourOperatorReport = {};

        //act
        scope.generateTourOperatorLimitedReport(true);

        //assert
        expect(reportGeneratorServ.generateSalesLimitedReport).toHaveBeenCalled();
    });

    it("should be call generateSalesByCarCategoryReport method with right parameters value", function () {
        //arrange
        scope.salesByCarCategoryReport = {
            StartDate: "2015/10/10",
            EndDate: "2015/10/25",
            CarCategoryId: 10
        };

        //act
        scope.generateSalesByCarCategoryReport(true);

        //assert
        expect(reportGeneratorServ.generateSalesByCarCategoryReport).toHaveBeenCalledWith("2015/10/10", "2015/10/25", 10);
    });

    it("should not called generateSalesByCarCategoryReport method if form is not valid", function () {
        //arrange
        scope.salesByCarCategoryReport = {
            StartDate: "2015/10/10",
            EndDate: "2015/10/25"
        };

        //act
        scope.generateSalesByCarCategoryReport(false);

        //assert
        expect(reportGeneratorServ.generateSalesByCarCategoryReport).not.toHaveBeenCalled();
    });

    it("should call generateSalesByCarCategoryReport method on ReportGeneratorService when calling generateSalesByCarCategoryReport", function () {
        //arrange
        scope.salesByCarCategoryReport = {
            StartDate: "2015/10/10",
            EndDate: "2015/10/25"
        };

        //act
        scope.generateSalesByCarCategoryReport(true);

        //assert
        expect(reportGeneratorServ.generateSalesByCarCategoryReport).toHaveBeenCalled();
    });
});

var toastr;
var event;