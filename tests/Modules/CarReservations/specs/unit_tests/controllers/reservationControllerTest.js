﻿"use strict";

describe("Reservation Controller", function () {
    var reservationController;
    var scope;
    var resStatusEnumServ;
    var paymentEnumServ;
    var reservationHandler;
    var customersModalMock;
    var modalMock;
    var lookUpDataServ;
    var deferred;
    var loadActiveReservationsDeferred;
    var getCustomerMatchingPhoneNumberPatternDeferred;
    var modalMockPromiseDeferred;
    var getProvincesDeferred;
    var sessionServ;
    var customerServ;
    var reservationServ;
    var paymentServ;
    var emailServ;
    var reportGeneratorServ;
    var params;
    var filter;
    var loadReservationByIdDeferred;
    var pickUpLocationsByProvinceDeferred;
    var carProviderSelectionChangesDeferred;
    var carCategoriesByCarProviderDeferred;
    var tourOperatorByCarProviderDeferred;
    var saveReservationDeferred;
    var addReservationDeferred;
    var paymentServiceAddPaymentDeferred;
    var customerModalDeferred;
    var getPriceDeferred;
    var emailVoucherDeferred;
    var paymentServiceRemovePaymentDeferred;
    var confirmationModalMock;
    var emailVoucherConfirmationModalMock;
    var cancelConfirmationModalMock;
    var location;
    var activeTourOperatorUsedToMakeReservationByCarProviderDeferred;
    var allowPrintVoucherDeferred;
    var emailDirectlyDeferred;
    var compile;
    var form;
    var getRetailersDeferred;

    beforeEach(module("carReservationsApp", function ($provide) {
        reservationHandler = jasmine.createSpyObj("reservationHandler", ["getReservationFinalPrice", "addPaymentToReservation",
            "removePaymentFromReservation", "getReadyToCreate", "assignTotalDaysToReservation", "isPaid", "isNotPaid",
            "isPartiallyPaid", "isConfirmed", "isNotConfirmed", "isCancelled", "cancelReservation", "confirm", "cancel"]);

        sessionServ = jasmine.createSpyObj("sessionServ", ["join", "getHeaders"]);
        sessionServ.sessionData = {agencyId: "agencyId"};

        customerServ = jasmine.createSpyObj("customerServ", ["getCustomerMatchingPhoneNumberPattern"]);
        lookUpDataServ = jasmine.createSpyObj("lookUpDataServ", ["getProvinces",
            "pickUpLocationsByProvince", "getPrice", "getCarProviders", "carCategoriesByCarProvider",
            "tourOperatorByCarProvider", "activeTourOperatorUsedToMakeReservationByCarProvider", "getRetailers"
        ]);

        customersModalMock = jasmine.createSpyObj("customersModelMock", ["show", "hide"]);
        confirmationModalMock = jasmine.createSpyObj("confirmationModalMock", ["show", "hide"]);
        emailVoucherConfirmationModalMock = jasmine.createSpyObj("emailVoucherConfirmationModalMock", ["show", "hide"]);
        cancelConfirmationModalMock = jasmine.createSpyObj("cancelConfirmationModalMock", ["show", "hide"]);

        modalMock = function (modalData) {
            if (modalData.id === "confirmationModal") {
                return confirmationModalMock;
            };

            if (modalData.id === "emailVoucherConfirmationModal") {
                return emailVoucherConfirmationModalMock;
            };

            if (modalData.id === "CancelReservationModal") {
                return cancelConfirmationModalMock;
            };

            return customersModalMock;
        };

        reservationServ = jasmine.createSpyObj("reservationServ", ["loadReservations", "loadInactiveReservations", "loadReservationById",
                                               "saveReservation", "addReservation", "allowPrintVoucher"]);
        paymentServ = jasmine.createSpyObj("paymentServ", ["add", "remove"]);
        emailServ = jasmine.createSpyObj("emailServ", ["emailVoucher", "emailVoucherDirectly"]);


        params = {
            reservation_id: 0
        };

        filter = function () {
            return function () {
                return [{ReservationId: 10}];
            }
        };

        location = jasmine.createSpyObj("location", ["search"]);
        reportGeneratorServ = jasmine.createSpyObj("reportGeneratorServ", ["generateVoucher", "generateReservationHistory"]);

        $provide.value("reservationHandler", reservationHandler);
        $provide.value("lookUpDataService", lookUpDataServ);
        $provide.value("session", sessionServ);
        $provide.value("customerService", customerServ);
        $provide.value("$modal", modalMock);
        $provide.value("reservationService", reservationServ);
        $provide.value("paymentService", paymentServ);
        $provide.value("$routeParams", params);
        $provide.value("$filter", filter);
        $provide.value("emailService", emailServ);
        $provide.value("reportGeneratorService", reportGeneratorServ);
        $provide.value("$location", location);
    }));

    beforeEach(inject(function ($injector) {
        deferred = $injector.get("$q").defer();
        loadActiveReservationsDeferred = $injector.get("$q").defer();
        getProvincesDeferred = $injector.get("$q").defer();
        getCustomerMatchingPhoneNumberPatternDeferred = $injector.get("$q").defer();
        modalMockPromiseDeferred = $injector.get("$q").defer();
        loadReservationByIdDeferred = $injector.get("$q").defer();
        pickUpLocationsByProvinceDeferred = $injector.get("$q").defer();
        carProviderSelectionChangesDeferred = $injector.get("$q").defer();
        carCategoriesByCarProviderDeferred = $injector.get("$q").defer();
        tourOperatorByCarProviderDeferred = $injector.get("$q").defer();
        saveReservationDeferred = $injector.get("$q").defer();
        addReservationDeferred = $injector.get("$q").defer();
        paymentServiceAddPaymentDeferred = $injector.get("$q").defer();
        customerModalDeferred = $injector.get("$q").defer();
        getPriceDeferred = $injector.get("$q").defer();
        emailVoucherDeferred = $injector.get("$q").defer();
        paymentServiceRemovePaymentDeferred = $injector.get("$q").defer();
        allowPrintVoucherDeferred = $injector.get("$q").defer();
        activeTourOperatorUsedToMakeReservationByCarProviderDeferred = $injector.get("$q").defer();
        resStatusEnumServ = $injector.get("reservationStatusEnumService");
        paymentEnumServ = $injector.get("paymentEnumService");
        emailDirectlyDeferred = $injector.get("$q").defer();
        getRetailersDeferred = $injector.get("$q").defer();
        compile = $injector.get("$compile");

        scope = $injector.get("$rootScope");
        scope.$watch = spyOn(scope, "$watch");

        var controller = $injector.get("$controller");
        reservationController = controller("reservationController", {$scope: scope});

    }));

    describe("reservationCancellationModalShow method", function () {

        it("should activate reservation if it is cancelled", function () {
            //arrange
            spyOn(scope, "isCancelled").and.returnValue(true);
            spyOn(scope, "cancelOrActivateReservation");

            //act
            scope.reservationCancellationModalShow();

            //assert
            expect(scope.cancelOrActivateReservation).toHaveBeenCalled();
        });

        it("should NOT show modal when activating reservation", function () {
            //arrange
            spyOn(scope, "isCancelled").and.returnValue(false);

            //act
            scope.reservationCancellationModalShow();

            //assert
            expect(cancelConfirmationModalMock.show).toHaveBeenCalled();
        });
    });

    describe("toggleMayPrintVoucher method", function(){

        it("should call reservationService.allowPrintVoucher with right values", function(){
            //arrange
            scope.selectedReservation = {
                ReservationId: 10,
                AllowRetailerToPrintVoucher: true
            };
            reservationServ.allowPrintVoucher.and.returnValue(allowPrintVoucherDeferred.promise);

            //act
            scope.toggleMayPrintVoucher();

            //assert
            expect(reservationServ.allowPrintVoucher).toHaveBeenCalledWith(10, false);
        });

        it("should toggle selectedReservation.MayPrintVoucher", function(){
            //arrange
            scope.selectedReservation = {
                ReservationId: 10,
                AllowRetailerToPrintVoucher: true
            };
            reservationServ.allowPrintVoucher.and.returnValue(allowPrintVoucherDeferred.promise);

            //act
            scope.toggleMayPrintVoucher();

            //assert
            expect(scope.selectedReservation.AllowRetailerToPrintVoucher).toBeFalsy();
        });

        it("should notify user when successfully executed the action", function(){
            //arrange
            scope.selectedReservation = {
                ReservationId: 10,
                AllowRetailerToPrintVoucher: true
            };
            reservationServ.allowPrintVoucher.and.returnValue(allowPrintVoucherDeferred.promise);
            toastr.info = jasmine.createSpy();

            //act
            scope.toggleMayPrintVoucher();
            allowPrintVoucherDeferred.resolve();
            scope.$apply();

            //assert
            expect(toastr.info).toHaveBeenCalled();
        });

        it("should notify user when unsuccessfully executed the action", function(){
            //arrange
            scope.selectedReservation = {
                ReservationId: 10,
                AllowRetailerToPrintVoucher: true
            };
            reservationServ.allowPrintVoucher.and.returnValue(allowPrintVoucherDeferred.promise);
            toastr.info = jasmine.createSpy();

            //act
            scope.toggleMayPrintVoucher();
            allowPrintVoucherDeferred.resolve();
            scope.$apply();

            //assert
            expect(toastr.info).toHaveBeenCalled();
        });
    });

    describe("canToggleMayPrintVoucher method", function(){
        it("should return false if creating new reservation", function(){
            //arrange
            scope.selectedReservation = {
                HandledByWholesaler: false
            };
            scope.creatingNewReservation = true;

            //act
            var result = scope.canToggleMayPrintVoucher();

            //assert
            expect(result).toBeFalsy();
        });

        it("should return false if reservation is null", function(){
            //arrange
            scope.selectedReservation = null;
            scope.creatingNewReservation = false;

            //act
            var result = scope.canToggleMayPrintVoucher();

            //assert
            expect(result).toBeFalsy();
        });

        it("should return false if reservation is undefined", function(){
            //arrange
            scope.creatingNewReservation = false;

            //act
            var result = scope.canToggleMayPrintVoucher();

            //assert
            expect(result).toBeFalsy();
        });

        it("should return true", function(){
            //arrange
            scope.selectedReservation = {MayPrintVoucher: true};
            scope.creatingNewReservation = false;

            //act
            var result = scope.canToggleMayPrintVoucher();

            //assert
            expect(result).toBeTruthy();
        });

        it("should return false if not allowed to print voucher", function(){
            //arrange
            scope.selectedReservation = { MayPrintVoucher: false };
            scope.creatingNewReservation = false;

            //act
            var result = scope.canToggleMayPrintVoucher();

            //assert
            expect(result).toBeFalsy();
        })
    });

    describe("createPayment method", function(){

        it("should assign scope.selectedPayment.PaymentType to be regular", function(){
            //arrange
            scope.selectedPayment = {
                PaymentType: null
            };
            var reservation = new Reservation();

            //act
            scope.createPayment(reservation);

            //assert
            expect(scope.selectedPayment.PaymentType).toBe(1);
        });

        it("should set creatingNewPayment property to true when calling method createPayment", function () {
            //arrange
            var reservation = new Reservation();

            //act
            scope.createPayment(reservation);

            //assert
            expect(scope.creatingNewPayment).toBe(true);
        });

        it("should assign new payment to selectedPayment when calling createPayment method", function () {
            //arrange
            scope.selectedPayment = null;
            var reservation = new Reservation();

            //act
            scope.createPayment(reservation);

            //assert
            expect(scope.selectedPayment).not.toBeNull();
        });

    });

    it("should have an instance of Session Service", function () {
        expect(!!sessionServ).toBe(true);
    });

    it("should have an instance of LookUpDataService", function () {
        expect(!!lookUpDataServ).toBe(true);
    });

    it("should have an instance of paymentEnumService", function () {
        expect(!!paymentEnumServ).toBe(true);
    });

    it("should have an instance of ReservationStatusEnumService", function () {
        expect(!!resStatusEnumServ).toBe(true);
    });

    it("should have an instance of paymentEnumService", function () {
        expect(!!paymentEnumServ).toBe(true);
    });

    it("should have an instance of ReservationHandler", function () {
        expect(!!reservationHandler).toBe(true);
    });

    it("should have an instance of CustomerService", function () {
        expect(!!customerServ).toBe(true);
    });

    it("should have an instance of PaymentService", function () {
        expect(!!paymentServ).toBe(true);
    });

    it("should have creatingNewReservation property set to false by default", function () {
        //assert
        expect(scope.creatingNewReservation).toBeDefined();
        expect(scope.creatingNewReservation).toBe(false);
    });

    it("should have reservationSubmitted property set to false by default", function () {
        //assert
        expect(scope.reservationSubmitted).toBeDefined();
        expect(scope.reservationSubmitted).toBe(false);
    });

    it("should have submitReservationButtonClicked property set to false by default", function () {
        //assert
        expect(scope.submitReservationButtonClicked).toBeDefined();
        expect(scope.submitReservationButtonClicked).toBe(false);
    });

    it("should have customerSelected property set to false by default", function () {
        expect(scope.customerSelected).toBeDefined();
        expect(scope.customerSelected).toBe(false);
    });

    it("should call reservation handler to find out if reservation is cancelled when calling isCancelled method with valid reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.cancelled};

        //act
        scope.isCancelled(reservation);

        //assert
        expect(reservationHandler.isCancelled).toHaveBeenCalled();
    });

    it("should not call reservation handler to find out if reservation is cancelled when calling isCancelled method and reservation is null", function () {
        //arrange
        var reservation = null;

        //act
        scope.isCancelled(reservation);

        //assert
        expect(reservationHandler.isCancelled).not.toHaveBeenCalled();
    });

    it("should call reservation handler to find out if reservation is paid", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.paid};

        //act
        scope.isPaid(reservation);

        //assert
        expect(reservationHandler.isPaid).toHaveBeenCalled();
    });

    it("should call reservation handler to find out if reservation is partially paid", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.partiallyPaid};

        //act
        scope.isPartiallyPaid(reservation);

        //assert
        expect(reservationHandler.isPartiallyPaid).toHaveBeenCalled();
    });

    it("should call reservation handler to find out if reservation is not paid", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.noPaid};

        //act
        scope.isNotPaid(reservation);

        //assert
        expect(reservationHandler.isNotPaid).toHaveBeenCalled();
    });

    it("should call reservation handler to find out if reservation is confirmed", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.confirmed};

        //act
        scope.isConfirmed(reservation);

        //assert
        expect(reservationHandler.isConfirmed).toHaveBeenCalled();
    });

    it("should call reservation handler to find out if reservation is not confirmed", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.noConfirmed};

        //act
        scope.isNotConfirmed(reservation);

        //assert
        expect(reservationHandler.isNotConfirmed).toHaveBeenCalled();
    });

    it("should return true when calling method wasPaymentMadeByCard sending a card payment", function () {
        var payment = {PaymentMethod: paymentEnumServ.paymentMethod.card};
        var result = scope.wasPaymentMadeByCard(payment);
        expect(result).toBe(true);
    });

    it("should return false when calling method wasPaymentMadeByCard sending a check payment", function () {
        var payment = {PaymentMethod: paymentEnumServ.paymentMethod.check};
        var result = scope.wasPaymentMadeByCard(payment);
        expect(result).toBe(false);
    });

    it("should return false when calling method wasPaymentMadeByCard sending a cash payment", function () {
        var payment = {PaymentMethod: paymentEnumServ.paymentMethod.cash};
        var result = scope.wasPaymentMadeByCard(payment);
        expect(result).toBe(false);
    });

    it("should return false when calling method wasPaymentMadeByCard sending a deposit payment", function () {
        var payment = {PaymentMethod: paymentEnumServ.paymentMethod.deposit};
        var result = scope.wasPaymentMadeByCard(payment);
        expect(result).toBe(false);
    });

    it("should return false when calling method was_payment_made_by_check sending a card payment", function () {
        var payment = {PaymentMethod: paymentEnumServ.paymentMethod.card};
        var result = scope.wasPaymentMadeByCheck(payment);
        expect(result).toBe(false);
    });

    it("should return true when calling method was_payment_made_by_check sending a check payment", function () {
        var payment = {PaymentMethod: paymentEnumServ.paymentMethod.check};
        var result = scope.wasPaymentMadeByCheck(payment);
        expect(result).toBe(true);
    });

    it("should return false when calling method was_payment_made_by_check sending a cash payment", function () {
        var payment = {PaymentMethod: paymentEnumServ.paymentMethod.cash};
        var result = scope.wasPaymentMadeByCheck(payment);
        expect(result).toBe(false);
    });

    it("should return false when calling method was_payment_made_by_check sending a deposit payment", function () {
        var payment = {PaymentMethod: paymentEnumServ.paymentMethod.directDeposit};
        var result = scope.wasPaymentMadeByCheck(payment);
        expect(result).toBe(false);
    });

    it("should return false when calling method wasPaymentMadeByCash sending a card payment", function () {
        var payment = {PaymentMethod: paymentEnumServ.paymentMethod.card};
        var result = scope.wasPaymentMadeByCash(payment);
        expect(result).toBe(false);
    });

    it("should return false when calling method wasPaymentMadeByCash sending a check payment", function () {
        var payment = {PaymentMethod: paymentEnumServ.paymentMethod.check};
        var result = scope.wasPaymentMadeByCash(payment);
        expect(result).toBe(false);
    });

    it("should return true when calling method wasPaymentMadeByCash sending a cash payment", function () {
        var payment = {PaymentMethod: paymentEnumServ.paymentMethod.cash};
        var result = scope.wasPaymentMadeByCash(payment);
        expect(result).toBe(true);
    });

    it("should return false when calling method wasPaymentMadeByCash sending a deposit payment", function () {
        var payment = {PaymentMethod: paymentEnumServ.paymentMethod.directDeposit};
        var result = scope.wasPaymentMadeByCash(payment);
        expect(result).toBe(false);
    });

    it("should return false when calling method was_payment_made_by_deposit sending a card payment", function () {
        var payment = {PaymentMethod: paymentEnumServ.paymentMethod.card};
        var result = scope.wasPaymentMadeByDeposit(payment);
        expect(result).toBe(false);
    });

    it("should return false when calling method was_payment_made_by_deposit sending a check payment", function () {
        var payment = {PaymentMethod: paymentEnumServ.paymentMethod.check};
        var result = scope.wasPaymentMadeByDeposit(payment);
        expect(result).toBe(false);
    });

    it("should return false when calling method was_payment_made_by_deposit sending a cash payment", function () {
        var payment = {PaymentMethod: paymentEnumServ.paymentMethod.cash};
        var result = scope.wasPaymentMadeByDeposit(payment);
        expect(result).toBe(false);
    });

    it("should return true when calling method was_payment_made_by_deposit sending a deposit payment", function () {
        var payment = {PaymentMethod: paymentEnumServ.paymentMethod.directDeposit};
        var result = scope.wasPaymentMadeByDeposit(payment);
        expect(result).toBe(true);
    });

    it("should return true when calling method was_payment_regular sending a regular payment", function () {
        var payment = {PaymentType: paymentEnumServ.paymentType.regular};
        var result = scope.wasPaymentRegular(payment);
        expect(result).toBe(true);
    });

    it("should return false when calling method was_payment_regular sending a tip payment", function () {
        var payment = {PaymentType: paymentEnumServ.paymentType.tip};
        var result = scope.wasPaymentRegular(payment);
        expect(result).toBe(false);
    });

    it("should return true when calling method was_payment_tip sending a tip payment", function () {
        var payment = {PaymentType: paymentEnumServ.paymentType.tip};
        var result = scope.wasPaymentTip(payment);
        expect(result).toBe(true);
    });

    it("should return alse when calling method was_payment_tip sending a regular payment", function () {
        var payment = {PaymentType: paymentEnumServ.paymentType.regular};
        var result = scope.wasPaymentTip(payment);
        expect(result).toBe(false);
    });

    it("should set is_new_reservation to true when calling method bookNewReservation", function () {
        //arrange
        lookUpDataServ.pickUpLocationsByProvince.and.returnValue(pickUpLocationsByProvinceDeferred.promise);

        //act
        scope.bookNewReservation();

        //assert
        expect(scope.creatingNewReservation).toBe(true);
    });

    it("should assign an empty reservation object to selectedReservation", function () {
        //arrange
        lookUpDataServ.pickUpLocationsByProvince.and.returnValue(pickUpLocationsByProvinceDeferred.promise);

        //act
        scope.bookNewReservation();

        //assert
        expect(scope.selectedReservation.ReservationId).toEqual(0);
    });

    it("should set customerSelected to false when calling method bookNewReservation", function () {
        //arrange
        lookUpDataServ.pickUpLocationsByProvince.and.returnValue(pickUpLocationsByProvinceDeferred.promise);

        //act
        scope.bookNewReservation();

        //assert
        expect(scope.customerSelected).toBe(false);
    });

    it("should load scope.pickUpLocations when calling method bookNewReservation", function () {
        //arrange
        scope.pickUpLocations = null;
        lookUpDataServ.pickUpLocationsByProvince.and.returnValue(pickUpLocationsByProvinceDeferred.promise);

        //act
        scope.bookNewReservation();
        pickUpLocationsByProvinceDeferred.resolve({});
        scope.$apply();

        //assert
        expect(scope.pickUpLocations).not.toBeNull();
    });

    it("should set creatingNewReservation to false when calling method cancelReservationChanges", function () {
        //arrange
        scope.creatingNewReservation = true;

        scope.reservationForm = {
            $dirty: true,
            $setPristine: function () {
                this.$dirty = false;
            }
        };

        //act
        scope.cancelReservationChanges();

        //assert
        expect(scope.creatingNewReservation).toBe(false);
    });

    it("should set submitReservationButtonClicked to false when calling method cancelReservationChanges", function () {
        //arrange
        scope.submitReservationButtonClicked = true;

        scope.reservationForm = {
            $dirty: true,
            $setPristine: function () {
                this.$dirty = false;
            }
        };

        //act
        scope.cancelReservationChanges();

        //assert
        expect(scope.submitReservationButtonClicked).toBe(false);
    });

    it("should clean reservationForm when calling method cancelReservationChanges", function () {
        //arrange
        scope.reservationForm = {
            $dirty: true,
            $setPristine: function () {
                this.$dirty = false;
            }
        };

        //act
        scope.cancelReservationChanges();

        //assert
        expect(scope.reservationForm.$dirty).toBe(false);
    });

    it("should assign null to selectedReservation when calling method cancelReservationChanges if creating new reservation", function () {
        //arrange
        scope.selectedReservation = new Reservation();

        scope.reservationForm = {
            $dirty: true,
            $setPristine: function () {
                this.$dirty = false;
            }
        };

        //act
        scope.cancelReservationChanges();

        //assert
        expect(scope.selectedReservation).toBe(null);
    });

    it("should assign a payment to selectedPayment when calling method selectPayment", function () {
        //act
        scope.selectPayment();

        //assert
        expect(scope.selectedPayment).not.toBeNull();
    });

    it("should have creatingNewPayment property set to false by default", function () {
        //assert
        expect(scope.creatingNewPayment).toBeDefined();
        expect(scope.creatingNewPayment).toBe(false);
    });

    it("should set creatingNewPayment to false when calling method cancelPayment", function () {
        //arrange
        scope.paymentForm = {
            $dirty: true,
            $setPristine: function () {
                this.$dirty = false;
            }
        };

        //act
        scope.cancelPayment();

        //assert
        expect(scope.creatingNewPayment).toBe(false);
    });

    it("should clean payment form when calling method cancelPayment", function () {
        //arrange
        scope.paymentForm = {
            $dirty: true,
            $setPristine: function () {
                this.$dirty = false;
            }
        };

        //act
        scope.cancelPayment();

        //assert
        expect(scope.paymentForm.$dirty).toBe(false);
    });

    it("should set selectedPayment to null when calling method cancelPayment", function () {
        //arrange
        scope.paymentForm = {
            $dirty: true,
            $setPristine: function () {
                this.$dirty = false;
            }
        };

        //act
        scope.cancelPayment();

        //assert
        expect(scope.selectedPayment).toBeNull();
    });

    it("should set addPaymentClicked to false when calling method cancelPayment", function () {
        //arrange
        scope.paymentForm = {
            $dirty: true,
            $setPristine: function () {
            }
        };
        scope.addPaymentClicked = true;

        //act
        scope.cancelPayment();

        //assert
        expect(scope.addPaymentClicked).toBe(false);
    });

    describe("addNewPayment method", function(){
        it("should add new payment to list when successfully calling addNewPayment", function () {
            //arrange
            scope.selectedReservation = new Reservation();
            var paymentTotal = scope.selectedReservation.Payments.length;
            scope.paymentForm = {$valid: true};
            toastr = {
                success: function (msg, title) {
                }
            };
            scope.selectedPayment = {
                PaymentId: 0,
                PaymentMethod: 1,
                PaymentType: 1
            };
            paymentServ.add.and.returnValue(paymentServiceAddPaymentDeferred.promise);

            //act
            scope.addNewPayment();
            paymentServiceAddPaymentDeferred.resolve(1);
            scope.$apply();

            //assert
            expect(scope.selectedReservation.Payments.length).toBe(paymentTotal + 1);
        });

        it("should set creatingNewPayment to false when calling method addNewPayment if payment form is valid", function () {
            //arrange
            scope.creatingNewPayment = true;
            scope.selectedReservation = new Reservation();
            scope.paymentForm = {$valid: true};
            toastr = {
                success: function (msg, title) {
                }
            };

            scope.selectedPayment = {
                PaymentId: 0,
                PaymentMethod: 1,
                PaymentType: 1
            };
            paymentServ.add.and.returnValue(paymentServiceAddPaymentDeferred.promise);

            //act
            scope.addNewPayment();
            paymentServiceAddPaymentDeferred.resolve(1);
            scope.$apply();

            //assert
            expect(scope.creatingNewPayment).toBe(false);
        });

        it("should remain all values when calling method addNewPayment and payment form is invalid", function () {
            //arrange
            scope.creatingNewPayment = true;
            scope.selectedReservation = {
                RemainingPaymentsAmount: 200
            };

            scope.selectedPayment = {
                PaymentAmount: 50,
                PaymentType: paymentEnumServ.paymentType.regular,
                PaymentMethod: 1
            };

            scope.paymentForm = {$valid: false};

            //act
            scope.addNewPayment();

            //assert
            expect(scope.creatingNewPayment).toBe(true);
        });

        it("should have addPaymentClicked property set to false by default", function () {
            //assert
            expect(scope.addPaymentClicked).toBeDefined();
            expect(scope.addPaymentClicked).toBe(false);
        });

        it("should set addPaymentClicked to true when calling method addNewPayment", function () {
            //arrange
            scope.addPaymentClicked = false;
            scope.paymentForm = {$valid: false};
            scope.selectedReservation = {
                RemainingPaymentsAmount: 200
            };

            scope.selectedPayment = {
                PaymentAmount: 50,
                PaymentType: paymentEnumServ.paymentType.regular,
                PaymentMethod: 1
            };

            toastr = {
                success: function (msg, title) {
                }
            };

            //act
            scope.addNewPayment();

            //assert
            expect(scope.addPaymentClicked).toBe(true);
        });

        it("should set addPaymentClicked to false when calling method addNewPayment and payment form is valid", function () {
            //arrange
            scope.addPaymentClicked = true;
            scope.paymentForm = {$valid: true};
            scope.selectedReservation = new Reservation();

            toastr = {
                success: function (msg, title) {
                }
            };

            scope.selectedPayment = {
                PaymentId: 0,
                PaymentMethod: 1,
                PaymentType: 1
            };
            paymentServ.add.and.returnValue(paymentServiceAddPaymentDeferred.promise);

            //act
            scope.addNewPayment();
            paymentServiceAddPaymentDeferred.resolve(1);
            scope.$apply();

            //assert
            expect(scope.addPaymentClicked).toBe(false);
        });

        it("should call method ReservationHandler.addPaymentToReservation when calling addNewPayment and payment form is valid", function () {
            //arrange
            scope.paymentForm = {$valid: true};
            scope.selectedReservation = {
                Payments: []
            };
            toastr = {
                success: function (msg, title) {
                }
            };

            scope.selectedPayment = {
                PaymentId: 0,
                PaymentMethod: 1,
                PaymentType: 1
            };

            reservationHandler.addPaymentToReservation = jasmine.createSpy("");
            paymentServ.add.and.returnValue(paymentServiceAddPaymentDeferred.promise);

            //act
            scope.addNewPayment();
            paymentServiceAddPaymentDeferred.resolve(1);
            scope.$apply();

            //assert
            expect(reservationHandler.addPaymentToReservation).toHaveBeenCalled();
        });

        it("should notify client method paymentService.add when calling addNewPayment and payment form is valid", function () {
            //arrange
            scope.paymentForm = {$valid: true};
            toastr = {
                success: jasmine.createSpy()
            };
            scope.selectedReservation = {
                Payments: []
            };
            scope.selectedPayment = {
                PaymentId: 0,
                PaymentMethod: 1,
                PaymentType: 1
            };
            paymentServ.add.and.returnValue(paymentServiceAddPaymentDeferred.promise);

            //act
            scope.addNewPayment();
            paymentServiceAddPaymentDeferred.resolve(1);
            scope.$apply();

            //assert
            expect(toastr.success).toHaveBeenCalled();
        });

        it("should assign paymentId to selectedPayment when calling addNewPayment", function () {
            //arrange
            scope.selectedReservation = new Reservation();
            scope.paymentForm = {$valid: true};
            toastr = {
                success: function (msg, title) {
                }
            };
            scope.selectedPayment = {
                PaymentId: 0,
                PaymentMethod: 1,
                PaymentType: 1
            };
            paymentServ.add.and.returnValue(paymentServiceAddPaymentDeferred.promise);

            //act
            scope.addNewPayment();
            paymentServiceAddPaymentDeferred.resolve({
                PaymentId: 1,
                PaymentDate: '2016-09-31 12:50'
            });
            scope.$apply();

            //assert
            expect(scope.selectedPayment.PaymentId).toBe(1);
        });

        it("should assign scope.selectedReservation.ModifiedOn to right value", function () {
            //arrange
            scope.selectedReservation = new Reservation();
            scope.paymentForm = {$valid: true};
            toastr = {
                success: function (msg, title) {
                }
            };
            scope.selectedPayment = {
                PaymentId: 0,
                PaymentMethod: 1,
                PaymentType: 1
            };
            paymentServ.add.and.returnValue(paymentServiceAddPaymentDeferred.promise);

            //act
            scope.addNewPayment();
            paymentServiceAddPaymentDeferred.resolve({
                PaymentId: 1,
                PaymentDate: '2016-09-31 12:50'
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.ModifiedOn).toBe('2016-09-31 12:50');
        });

        it("should notify client method paymentService.add when unsuccessfully calling addNewPayment", function () {
            //arrange
            scope.paymentForm = {$valid: true};
            toastr = {
                error: jasmine.createSpy()
            };
            scope.selectedReservation = {
                Payments: []
            };
            scope.selectedPayment = {
                PaymentId: 0
            };

            //act
            scope.addNewPayment();
            paymentServiceAddPaymentDeferred.reject("error");
            scope.$apply();

            //assert
            expect(toastr.error).toHaveBeenCalled();
        });

        it("should alert user an error when process tip payment and own money", function () {
            //arrange
            scope.selectedReservation = {
                RemainingPaymentsAmount: 100
            };

            scope.selectedPayment = {
                PaymentAmount: 50,
                PaymentType: paymentEnumServ.paymentType.tip
            };

            toastr.error = jasmine.createSpy();

            //act
            scope.addNewPayment();

            //assert
            expect(toastr.error).toHaveBeenCalled();
        });

        it("should not process payment when process tip payment and own money", function () {
            //arrange
            scope.selectedReservation = {
                RemainingPaymentsAmount: 100
            };

            scope.selectedPayment = {
                PaymentAmount: 50,
                PaymentType: paymentEnumServ.paymentType.tip
            };

            toastr.error = jasmine.createSpy();

            //act
            scope.addNewPayment();

            //assert
            expect(paymentServ.add).not.toHaveBeenCalled();
        });

        it("should not process payment when paying more than is owned on regular payments", function () {
            //arrange
            scope.selectedReservation = {
                RemainingPaymentsAmount: 100
            };

            scope.selectedPayment = {
                PaymentAmount: 110,
                PaymentType: paymentEnumServ.paymentType.regular
            };

            //act
            scope.addNewPayment();

            //assert
            expect(paymentServ.add).not.toHaveBeenCalled();
        });

        it("should process payment when paying more than is owned on tip payments", function () {
            //arrange
            scope.selectedReservation = {
                RemainingPaymentsAmount: 0
            };

            scope.selectedPayment = {
                PaymentAmount: 100,
                PaymentMethod: 1,
                PaymentType: paymentEnumServ.paymentType.tip
            };
            scope.paymentForm = {$valid: true};
            paymentServ.add.and.returnValue(paymentServiceAddPaymentDeferred.promise);

            //act
            scope.addNewPayment();

            //assert
            expect(paymentServ.add).toHaveBeenCalled();
        });

        it("should show user an error when process payment when paying more than is owned", function () {
            //arrange
            scope.selectedReservation = {
                RemainingPaymentsAmount: 100
            };

            scope.selectedPayment = {
                PaymentAmount: 110,
                PaymentType: paymentEnumServ.paymentType.regular
            };

            toastr.error = jasmine.createSpy();
            scope.paymentForm = {$valid: true};

            //act
            scope.addNewPayment();

            //assert
            expect(toastr.error).toHaveBeenCalled();
        });

        it("should show user an error when process payment when no payment type has been selected", function () {
            //arrange
            scope.selectedReservation = {
                RemainingPaymentsAmount: 100
            };

            scope.selectedPayment = {
                PaymentAmount: 100
            };

            toastr.error = jasmine.createSpy();
            scope.paymentForm = {$valid: true};

            //act
            scope.addNewPayment();

            //assert
            expect(toastr.error).toHaveBeenCalled();
        });
    });

    describe("deletePayment method", function(){
        it("should call method ReservationHandler.removePaymentFromReservation when calling deletePayment", function () {
            //arrange
            var payment = {};
            scope.selectedReservation = {
                Payments: []
            };

            toastr = {
                success: function (msg, title) {
                }
            };
            paymentServ.remove.and.returnValue(paymentServiceRemovePaymentDeferred.promise);

            reservationHandler.removePaymentFromReservation = jasmine.createSpy("");

            //act
            scope.deletePayment(payment);
            paymentServiceRemovePaymentDeferred.resolve();
            scope.$apply();

            //assert
            expect(reservationHandler.removePaymentFromReservation).toHaveBeenCalled();
        });

        it("should actually delete payment in the server", function () {
            //arrange
            var reservation = new Reservation();
            var payment = new Payment(reservation);
            paymentServ.remove.and.returnValue(paymentServiceRemovePaymentDeferred.promise);

            //act
            scope.deletePayment(payment);

            //assert
            expect(paymentServ.remove).toHaveBeenCalled();
        });

        it("should remove selected payment when calling method delete_method", function () {
            //arrange
            var reservation = new Reservation();
            var payment1 = new Payment(reservation);
            var payment2 = new Payment(reservation);
            scope.selectedReservation = new Reservation();
            scope.selectedReservation.Payments = [payment1, payment2];
            var payment_total = scope.selectedReservation.Payments.length;
            paymentServ.remove.and.returnValue(paymentServiceRemovePaymentDeferred.promise);

            //act
            scope.deletePayment(payment1);
            paymentServiceRemovePaymentDeferred.resolve();
            scope.$apply();

            //arrange
            expect(scope.selectedReservation.Payments.length).toBe(payment_total - 1);
        });

        it("should assign scope.selectedReservation.ModifiedOn", function(){
            //arrange
            var reservation = new Reservation();
            var payment1 = new Payment(reservation);
            var payment2 = new Payment(reservation);
            scope.selectedReservation = {};
            scope.selectedReservation.Payments = [payment1, payment2];
            paymentServ.remove.and.returnValue(paymentServiceRemovePaymentDeferred.promise);

            //act
            scope.deletePayment(payment1);
            paymentServiceRemovePaymentDeferred.resolve("2016-9-1");
            scope.$apply();

            //assert
            expect(scope.selectedReservation.ModifiedOn).toBe("2016-9-1");
        });
    });

    describe("submitReservation method", function(){
        it("should set submitReservationButtonClicked to true when calling method submitReservation and reservation form is not valid", function () {
            //arrange
            scope.submitReservationButtonClicked = false;
            scope.paymentForm = {$valid: true};
            scope.reservationForm = {$valid: false};
            scope.selectedReservation = {
                StartDate: new Date(2014, 12, 12),
                EndDate: new Date(2014, 12, 24),
                Customer: {
                    CellPhone: 7876665544
                }
            };

            //act
            scope.submitReservation();

            //assert
            expect(scope.submitReservationButtonClicked).toBe(true);
        });

        it("should set submitReservationButtonClicked to true when calling method submitReservation and payment form is not valid", function () {
            //arrange
            scope.submitReservationButtonClicked = false;
            scope.paymentForm = {
                $valid: false,
                $dirty: true
            };
            scope.reservationForm = {$valid: false};
            scope.selectedReservation = {
                StartDate: new Date(2014, 12, 12),
                EndDate: new Date(2014, 12, 24),
                Customer: {
                    CellPhone: 7876665544
                }
            };

            //act
            scope.submitReservation();

            //assert
            expect(scope.submitReservationButtonClicked).toBe(true);
        });

        it("should not place the new reservation at the beginning of the list when calling method submitReservation if reservation form is not valid", function () {
            //arrange
            scope.paymentForm = {$valid: true};
            scope.reservationForm = {$valid: false};
            scope.customerForm = {$valid: true};
            scope.activeReservations = [];
            scope.selectedReservation = new Reservation();

            //act
            scope.submitReservation();

            //assert
            expect(scope.activeReservations.length).toBe(0);
            expect(scope.activeReservations[0]).not.toEqual(scope.selectedReservation);
        });

        it("should not place the new reservation at the beginning of the list when calling method submitReservation if payment form is dirty and not valid", function () {
            //arrange
            scope.paymentForm = {$valid: false, $dirty: true};
            scope.reservationForm = {$valid: true};
            scope.customerForm = {$valid: true};
            scope.activeReservations = [];
            scope.selectedReservation = new Reservation();

            //act

            scope.submitReservation();
            deferred.resolve();

            //assert
            expect(scope.activeReservations.length).toBe(0);
            expect(scope.activeReservations[0]).not.toEqual(scope.selectedReservation);
        });

        it("should clean reservation form after successfully creating a reservation", function () {
            //arrange
            scope.selectedReservation = {
                StartDate: new Date(),
                EndDate: new Date(),
                TotalDays: 9
            };

            scope.reservationForm = {
                $valid: true, $dirty: true, $setPristine: function () {
                }
            };
            scope.paymentForm = {
                $valid: true, $dirty: true, $setPristine: function () {
                }
            };
            scope.customerForm = {$valid: true, $dirty: true};
            scope.selectedReservation = {Customer: {CellPhone: 458745874}};
            scope.selectedReservation.PickUpLocation = {ProvinceId: 1};
            scope.activeReservations = [];

            scope.reservationForm.customerFirstName = {$dirty: false};
            scope.reservationForm.customerLastName = {$dirty: false};
            scope.reservationForm.customerEmail = {$dirty: false};

            reservationServ.addReservation.and.returnValue(addReservationDeferred.promise);
            reservationServ.saveReservation.and.returnValue(saveReservationDeferred.promise);
            toastr.error = jasmine.createSpy();

            //act
            scope.submitReservation();

            //assert
            expect(scope.reservationForm.$dirty).toBe(false);
        });

        it("should clean payment form after successfully creating a reservation", function () {
            //arrange
            scope.selectedReservation = new Reservation();
            scope.reservationForm = {
                $valid: true, $dirty: true, $setPristine: function () {
                }
            };
            scope.paymentForm = {
                $valid: true, $dirty: true, $setPristine: function () {
                }
            };
            scope.customerForm = {$valid: true, $dirty: true};
            scope.selectedReservation = {Customer: {CellPhone: 458745874}};
            scope.selectedReservation.PickUpLocation = {ProvinceId: 1};

            scope.reservationForm.customerFirstName = {$dirty: false};
            scope.reservationForm.customerLastName = {$dirty: false};
            scope.reservationForm.customerEmail = {$dirty: false};

            reservationServ.addReservation.and.returnValue(addReservationDeferred.promise);
            reservationServ.saveReservation.and.returnValue(saveReservationDeferred.promise);

            //act
            scope.submitReservation();

            //assert
            expect(scope.paymentForm.$dirty).toBe(false);
        });

        it("should set submitReservationButtonClicked to false when calling method submitReservation and the call succeeded", function () {
            //arrange

            scope.reservationForm = {
                $valid: true, $setPristine: function () {
                }
            };
            scope.paymentForm = {
                $valid: true, $setPristine: function () {
                }
            };
            scope.customerForm = {$valid: true};
            scope.selectedReservation = {Customer: {CellPhone: 458745874}};
            scope.selectedReservation.PickUpLocation = {ProvinceId: 1};

            scope.reservationForm.customerFirstName = {$dirty: false};
            scope.reservationForm.customerLastName = {$dirty: false};
            scope.reservationForm.customerEmail = {$dirty: false};

            reservationServ.addReservation.and.returnValue(addReservationDeferred.promise);
            reservationServ.saveReservation.and.returnValue(saveReservationDeferred.promise);

            //act
            scope.submitReservation();

            //assert
            expect(scope.submitReservationButtonClicked).toBe(false);
        });

        it("should set ReservationId when calling method submitReservation and the call successes", function () {
            //arrange
            scope.reservationForm = {
                $valid: true, $setPristine: function () {
                }
            };
            scope.paymentForm = {
                $valid: true,
                $setPristine: function () {
                },
                $dirty: false
            };
            scope.customerForm = {$valid: true};
            scope.selectedReservation = {
                ReservationId: 0,
                Customer: {CellPhone: 458745874},
                StartDate: new Date(),
                EndDate: new Date(),
                FinalPrice: 1000
            };
            scope.selectedReservation.PickUpLocation = {ProvinceId: 1};

            reservationServ.addReservation.and.returnValue(addReservationDeferred.promise);

            scope.reservationForm.customerFirstName = {$dirty: false};
            scope.reservationForm.customerLastName = {$dirty: false};
            scope.reservationForm.customerEmail = {$dirty: false};

            //act
            scope.submitReservation();
            getCustomerMatchingPhoneNumberPatternDeferred.resolve(null);
            addReservationDeferred.resolve({
                ReservationId: 10
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.ReservationId).not.toBe(0);
        });

        it("should set ReservationGlobalId when calling method submitReservation and the call successes", function () {
            //arrange
            scope.reservationForm = {
                $valid: true, $setPristine: function () {
                }
            };
            scope.paymentForm = {
                $valid: true, $setPristine: function () {
                }
            };
            scope.customerForm = {$valid: true};
            scope.selectedReservation = {
                ReservationId: 0,
                Customer: {CellPhone: 458745874},
                StartDate: new Date(),
                EndDate: new Date(),
                FinalPrice: 1000
            };
            scope.selectedReservation.PickUpLocation = {ProvinceId: 1};

            reservationServ.addReservation.and.returnValue(addReservationDeferred.promise);

            scope.reservationForm.customerFirstName = {$dirty: false};
            scope.reservationForm.customerLastName = {$dirty: false};
            scope.reservationForm.customerEmail = {$dirty: false};


            //act
            scope.submitReservation();
            getCustomerMatchingPhoneNumberPatternDeferred.resolve(null);
            addReservationDeferred.resolve({ReservationId: 10, ReservationGlobalId: "GlobalId"});
            scope.$apply();

            //assert
            expect(scope.selectedReservation.ReservationGlobalId).toBe("GlobalId");
        });

        it("should call reservationHandler.assignTotalDaysToReservation", function () {
            //arrange
            scope.reservationForm = {
                $valid: true, $setPristine: function () {
                }
            };
            scope.paymentForm = {
                $valid: true, $setPristine: function () {
                }
            };
            scope.customerForm = {$valid: true};
            scope.selectedReservation = {
                ReservationId: 0,
                Customer: {CellPhone: 458745874},
                StartDate: new Date(),
                EndDate: new Date(),
                FinalPrice: 1000
            };
            scope.selectedReservation.PickUpLocation = {ProvinceId: 1};

            reservationServ.addReservation.and.returnValue(addReservationDeferred.promise);
            scope.reservationForm.customerFirstName = {$dirty: false};
            scope.reservationForm.customerLastName = {$dirty: false};
            scope.reservationForm.customerEmail = {$dirty: false};

            //act
            scope.submitReservation();
            getCustomerMatchingPhoneNumberPatternDeferred.resolve(null);
            addReservationDeferred.resolve(10);
            scope.$apply();

            //assert
            expect(reservationHandler.getReadyToCreate).toHaveBeenCalled();
        });

        it("should set status to noPaid before creating reservations", function () {
            //arrange
            scope.reservationForm = {
                $valid: true,
                customerFirstName: {
                    $dirty: false
                },
                customerLastName: {
                    $dirty: false
                },
                customerEmail: {
                    $dirty: false
                },
                $setPristine: function () {
                }
            };
            scope.paymentForm = {
                $valid: true, $dirty: false, $setPristine: function () {
                }
            };
            scope.customerForm = {$valid: true};
            scope.selectedReservation = {
                ReservationId: 0,
                Customer: {CellPhone: 458745874},
                StartDate: new Date(),
                EndDate: new Date(),
                FinalPrice: 1000,
                ConfirmationCode: "ConfirmationCode"
            };
            scope.selectedReservation.PickUpLocation = {ProvinceId: 1};
            reservationServ.addReservation.and.returnValue(addReservationDeferred.promise);

            //act
            scope.submitReservation();

            //arrange
            var status = resStatusEnumServ.status.noPaid;
            expect(reservationServ.addReservation).toHaveBeenCalledWith(jasmine.objectContaining({ReservationStatus: status}));
        });

        it("should call reservationService.saveReservation when submitting a reservation if reservationId > 0 ", function () {
            //arrange
            scope.reservationForm = {
                $valid: true, $setPristine: function () {
                }
            };
            scope.paymentForm = {
                $valid: true, $setPristine: function () {
                }
            };
            scope.customerForm = {$valid: true};

            scope.selectedReservation = {
                ReservationId: 1,
                Customer: {CellPhone: 458745874}
            };
            scope.reservationForm.customerFirstName = {$dirty: false};
            scope.reservationForm.customerLastName = {$dirty: false};
            scope.reservationForm.customerEmail = {$dirty: false};

            reservationServ.saveReservation.and.returnValue(saveReservationDeferred.promise);
            reservationServ.addReservation.and.returnValue(addReservationDeferred.promise);

            //act
            scope.submitReservation();

            //assert
            expect(reservationServ.saveReservation).toHaveBeenCalled();
        });

        it("should not call reservationService.addReservation when submitting a reservation if reservationId > 0 ", function () {
            //arrange
            scope.reservationForm = {
                $valid: true, $setPristine: function () {
                }
            };
            scope.paymentForm = {
                $valid: true, $setPristine: function () {
                }
            };
            scope.customerForm = {$valid: true};

            scope.selectedReservation = {
                ReservationId: 1,
                Customer: {CellPhone: 458745874}
            };

            scope.reservationForm.customerFirstName = {$dirty: false};
            scope.reservationForm.customerLastName = {$dirty: false};
            scope.reservationForm.customerEmail = {$dirty: false};
            reservationServ.saveReservation.and.returnValue(saveReservationDeferred.promise);
            reservationServ.addReservation.and.returnValue(addReservationDeferred.promise);

            //act
            scope.submitReservation();

            //assert
            expect(reservationServ.addReservation).not.toHaveBeenCalled();
        });

        it("should not call reservationService.saveReservation when submitting a reservation if reservationId = 0 ", function () {
            //arrange
            scope.reservationForm = {
                $valid: true, $setPristine: function () {
                }
            };
            scope.paymentForm = {
                $valid: true, $setPristine: function () {
                }
            };
            //scope.customerForm = { $valid: true };
            scope.selectedReservation = {
                ReservationId: 0,
                Customer: {CellPhone: 458745874}
            };
            scope.selectedReservation.PickUpLocation = {ProvinceId: 1};

            scope.reservationForm.customerFirstName = {$dirty: false};
            scope.reservationForm.customerLastName = {$dirty: false};
            scope.reservationForm.customerEmail = {$dirty: false};
            reservationServ.saveReservation.and.returnValue(saveReservationDeferred.promise);
            reservationServ.addReservation.and.returnValue(addReservationDeferred.promise);

            //act
            scope.submitReservation();

            //assert
            expect(reservationServ.saveReservation).not.toHaveBeenCalled();
        });

        it("should place selectedReservation to the top of the activeReservations after successfully creating reservation", function () {
            //arrange
            scope.reservationForm = {
                $valid: true,
                customerFirstName: {
                    $dirty: true
                },
                $setPristine: function () {
                }
            };

            scope.paymentForm = {
                $valid: true, $setPristine: function () {
                }
            };
            scope.selectedReservation = {
                ReservationId: 0,
                Customer: {CellPhone: 458745874},
                PickUpLocation: {ProvinceId: 1},
                StartDate: new Date(),
                EndDate: new Date(),
                FinalPrice: 100
            };
            scope.activeReservations = [];
            scope.activeReservations.unshift({
                ReservationId: 1,
                Customer: {CellPhone: 7823723737}

            });
            reservationServ.addReservation.and.returnValue(addReservationDeferred.promise);
            customerServ.getCustomerMatchingPhoneNumberPattern.and.returnValue(getCustomerMatchingPhoneNumberPatternDeferred.promise);

            //act
            scope.submitReservation();
            getCustomerMatchingPhoneNumberPatternDeferred.resolve(null);
            addReservationDeferred.resolve({ReservationId: 10, ReservationGlobalId: "GlobalId"});
            scope.$apply();

            //assert
            expect(scope.activeReservations[0].ReservationId).toBe(10);
        });

        it("should insert right object to the top of the activeReservations after successfully creating reservation", function () {
            //arrange
            scope.reservationForm = {
                $valid: true,
                customerFirstName: {
                    $dirty: true
                },
                $setPristine: function () {
                }
            };
            scope.paymentForm = {
                $valid: true, $setPristine: function () {
                }
            };
            scope.selectedReservation = {
                ReservationId: 0,
                Customer: {FirstName: "FirstName", LastName: "LastName", CellPhone: 458745874},
                StartDate: new Date(2015, 10, 10),
                EndDate: new Date(2015, 10, 30),
                TotalDays: 10,
                FlightNumber: "9665",
                PickupTime: "10:00",
                PickUpLocation: {ProvinceId: 1},
                FinalPrice: 100
            };

            scope.activeReservations = [];
            scope.activeReservations.unshift({
                ReservationId: 1,
                Customer: {CellPhone: 7823723737}

            });
            reservationServ.addReservation.and.returnValue(addReservationDeferred.promise);
            customerServ.getCustomerMatchingPhoneNumberPattern.and.returnValue(getCustomerMatchingPhoneNumberPatternDeferred.promise);

            //act
            scope.submitReservation();
            getCustomerMatchingPhoneNumberPatternDeferred.resolve(null);
            addReservationDeferred.resolve({
                ReservationId: 10,
                ReservationGlobalId: "GlobalId",
                Customer: {FirstName: "FirstName", LastName: "LastName"},
                StartDate: new Date(2015, 10, 10),
                TotalDays: 10,
                FlightNumber: "9665",
                PickupTime: "10:00"
            });
            scope.$apply();

            //assert
            expect(scope.activeReservations[0].ReservationId).toBe(10);
            expect(scope.activeReservations[0].Customer.FirstName).toBe("FirstName");
            expect(scope.activeReservations[0].Customer.LastName).toBe("LastName");
            expect(scope.activeReservations[0].StartDate).toEqual(new Date(2015, 10, 10));
            expect(scope.activeReservations[0].TotalDays).toBe(10);
            expect(scope.activeReservations[0].FlightNumber).toBe("9665");
            expect(scope.activeReservations[0].PickupTime).toBe("10:00");
        });

        it("should update ReturnProvinceId after successfully creating reservation", function () {
            //arrange
            scope.reservationForm = {
                $valid: true,
                customerFirstName: {
                    $dirty: true
                },
                $setPristine: function () {
                }
            };
            scope.paymentForm = {
                $valid: true, $setPristine: function () {
                }
            };
            scope.selectedReservation = {
                ReservationId: 0,
                Customer: {FirstName: "FirstName", LastName: "LastName", CellPhone: 458745874},
                StartDate: new Date(2015, 10, 10),
                TotalDays: 10,
                FlightNumber: "9665",
                PickupTime: "10:00",
                PickUpProvinceId: 1,
                ReturnProvinceId: 1
            };

            scope.activeReservations = [];
            scope.activeReservations.unshift({
                ReservationId: 1,
                Customer: {CellPhone: 7823723737}

            });
            reservationServ.addReservation.and.returnValue(addReservationDeferred.promise);
            customerServ.getCustomerMatchingPhoneNumberPattern.and.returnValue(getCustomerMatchingPhoneNumberPatternDeferred.promise);

            //act
            scope.submitReservation();
            getCustomerMatchingPhoneNumberPatternDeferred.resolve(null);
            addReservationDeferred.resolve({
                ReservationId: 10,
                ReservationGlobalId: "GlobalId",
                Customer: {FirstName: "FirstName", LastName: "LastName"},
                StartDate: new Date(2015, 10, 10),
                TotalDays: 10,
                FlightNumber: "9665",
                PickupTime: "10:00"
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.ReturnProvinceId).toBe(1);
        });

        it("should update PickupProvinceId after successfully creating reservation", function () {
            //arrange
            scope.reservationForm = {
                $valid: true,
                customerFirstName: {
                    $dirty: true
                },
                $setPristine: function () {
                }
            };
            scope.paymentForm = {
                $valid: true, $setPristine: function () {
                }
            };
            scope.selectedReservation = {
                ReservationId: 0,
                Customer: {FirstName: "FirstName", LastName: "LastName", CellPhone: 458745874},
                StartDate: new Date(2015, 10, 10),
                TotalDays: 10,
                FlightNumber: "9665",
                PickupTime: "10:00",
                PickUpProvinceId: 1,
                ReturnProvinceId: 1
            };

            scope.activeReservations = [];
            scope.activeReservations.unshift({
                ReservationId: 1,
                Customer: {CellPhone: 7823723737}

            });
            reservationServ.addReservation.and.returnValue(addReservationDeferred.promise);
            customerServ.getCustomerMatchingPhoneNumberPattern.and.returnValue(getCustomerMatchingPhoneNumberPatternDeferred.promise);

            //act
            scope.submitReservation();
            getCustomerMatchingPhoneNumberPatternDeferred.resolve(null);
            addReservationDeferred.resolve({
                ReservationId: 10,
                ReservationGlobalId: "GlobalId",
                Customer: {FirstName: "FirstName", LastName: "LastName"},
                StartDate: new Date(2015, 10, 10),
                TotalDays: 10,
                FlightNumber: "9665",
                PickupTime: "10:00"
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.PickUpProvinceId).toBe(1);
        });
    });

    describe("selectReservation method", function(){

        it("should set selectedReservation equals to reservation being selected when calling method selectReservation", function () {
            //arrange
            scope.selectedReservation = new Reservation();
            scope.selectedReservation.ReservationId = 10;
            scope.selectedReservation.CarCategory = {CarProviderId: 1};
            scope.selectedReservation.PickUpLocation = {PickupLocationId: 1};
            scope.selectedReservation.TourOperator = { TourOperatorId: 1};
            scope.reservationForm = {$dirty: false};
            scope.retailers = [];

            var reservationBeingSelected = new Reservation();
            reservationBeingSelected.ReservationId = 10;
            reservationBeingSelected.CarCategory = {CarProviderId: 1};
            reservationBeingSelected.PickUpLocation = {PickupLocationId: 1};
            reservationBeingSelected.TourOperator = {TourOperatorId: 1};

            lookUpDataServ.carCategoriesByCarProvider.and.returnValue(carCategoriesByCarProviderDeferred.promise);
            lookUpDataServ.tourOperatorByCarProvider.and.returnValue(tourOperatorByCarProviderDeferred.promise);
            reservationServ.loadReservationById.and.returnValue(loadReservationByIdDeferred.promise);
            lookUpDataServ.activeTourOperatorUsedToMakeReservationByCarProvider.and.returnValue(activeTourOperatorUsedToMakeReservationByCarProviderDeferred.promise);
            lookUpDataServ.pickUpLocationsByProvince.and.returnValue(pickUpLocationsByProvinceDeferred.promise);

            //act
            scope.selectReservation(reservationBeingSelected);
            loadReservationByIdDeferred.resolve(reservationBeingSelected);
            scope.$apply();

            //assert
            expect(scope.selectedReservation).toEqual(reservationBeingSelected);
        });

        it("should set creatingNewReservation to false when calling method selectReservation", function () {
            //arrange
            scope.creatingNewReservation = true;
            var reservation = new Reservation();
            reservation.ReservationId = 10;
            reservation.CarCategory = {CarProviderId: 1};
            reservation.PickUpLocation = {PickupLocationId: 1, ProvinceId: 1};
            reservation.TourOperator = { TourOperatorId: 1};
            reservation.CarProvider = "Cubacar";
            scope.reservationForm = {$dirty: false};

            scope.selectedReservation = new Reservation();
            scope.selectedReservation.TourOperator = { TourOperatorId: 1};

            spyOn(scope, "carProviderSelectionChanges");
            spyOn(scope, "getPickUpPlaces");

            reservationServ.loadReservationById.and.returnValue(loadReservationByIdDeferred.promise);
            scope.retailers = [];

            //act
            scope.selectReservation(reservation);
            loadReservationByIdDeferred.resolve({
                PickUpLocation: {
                    PickUpLocationId: 182,
                    Name: "Ube Varadero - Hotel Tropical",
                    ProvinceId: 5,
                    Province: null
                },
                TourOperator: {
                    TourOperatorId: 10
                }
            });
            pickUpLocationsByProvinceDeferred.resolve({});
            scope.$apply();

            //assert
            expect(scope.creatingNewReservation).toBe(false);
        });

        it("should explicitly assign selectedReservation.PickUpProvinceId when calling selectReservation method", function () {
            //arrange
            var reservation = {
                ReservationId: 1
            };
            scope.reservationForm = {$dirty: false};
            reservationServ.loadReservationById.and.returnValue(loadReservationByIdDeferred.promise);
            lookUpDataServ.carCategoriesByCarProvider.and.returnValue(carCategoriesByCarProviderDeferred.promise);
            lookUpDataServ.tourOperatorByCarProvider.and.returnValue(tourOperatorByCarProviderDeferred.promise);
            lookUpDataServ.activeTourOperatorUsedToMakeReservationByCarProvider.and.returnValue(activeTourOperatorUsedToMakeReservationByCarProviderDeferred.promise);
            lookUpDataServ.pickUpLocationsByProvince.and.returnValue(pickUpLocationsByProvinceDeferred.promise);
            scope.retailers = [];

            //act
            scope.selectReservation(reservation);
            loadReservationByIdDeferred.resolve({
                PickUpLocation: {
                    ProvinceId: 8,
                    CarProvider: 2
                },
                ReturnLocation: {
                    ProvinceId: 8,
                    CarProvider: 2
                },
                TourOperator:{
                    TourOperatorId: 10
                }
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.PickUpProvinceId).toBe(8);
        });

        it("should call carProviderSelectionChanges when  calling selectReservation", function () {
            var reservation = new Reservation();
            reservation.ReservationId = 10;
            reservation.CarProviderId = 1;
            reservation.PickUpLocation = {PickupLocationId: 1, ProvinceId: 1};
            scope.reservationForm = {$dirty: false};

            spyOn(scope, "carProviderSelectionChanges");
            spyOn(scope, "getPickUpPlaces");
            reservationServ.loadReservationById.and.returnValue(loadReservationByIdDeferred.promise);
            lookUpDataServ.pickUpLocationsByProvince.and.returnValue(pickUpLocationsByProvinceDeferred.promise);
            scope.retailers = [];

            //act
            scope.selectReservation(reservation);
            loadReservationByIdDeferred.resolve({
                PickUpLocation: {
                    PickUpLocationId: 182,
                    Name: "Ube Varadero - Hotel Tropical",
                    ProvinceId: 5,
                    Province: null
                },
                ReturnLocation: {
                    PickUpLocationId: 182,
                    Name: "Ube Varadero - Hotel Tropical",
                    ProvinceId: 5,
                    Province: null
                },
                TourOperator:{
                    TourOperatorId: 1
                }
            });
            scope.$apply();

            //assert
            expect(scope.carProviderSelectionChanges).toHaveBeenCalled();
        });

        it("should call getPickUpPlaces when  calling selectReservation", function () {
            var reservation = new Reservation();
            reservation.ReservationId = 10;
            reservation.CarCategory = {CarProviderId: 1};
            scope.reservationForm = {$dirty: false};

            spyOn(scope, "carProviderSelectionChanges");
            spyOn(scope, "getPickUpPlaces");
            reservationServ.loadReservationById.and.returnValue(loadReservationByIdDeferred.promise);
            lookUpDataServ.pickUpLocationsByProvince.and.returnValue(pickUpLocationsByProvinceDeferred.promise);
            scope.retailers = [];

            //act
            scope.selectReservation(reservation);
            loadReservationByIdDeferred.resolve({
                PickUpLocation: {
                    ProvinceId: 8
                },
                ReturnLocation: {
                    ProvinceId: 8
                },
                TourOperator:{
                    TourOperatorId: 1
                }
            });
            scope.$apply();

            //assert
            expect(scope.getPickUpPlaces).toHaveBeenCalled();

        });
    });

    describe("retrievePrice method", function(){
        it("should assign zero to selectedReservation.AgencyTotalPrice when calculating price and TourOperatorId is not assigned", function () {
            //arrange
            scope.selectedReservation = {
                CarCategoryId: 1,
                StartDate: new Date(),
                EndDate: new Date(),
                AgencyTotalPrice: 2
            };

            //act
            scope.retrievePrice();

            //assert
            expect(scope.selectedReservation.AgencyTotalPrice).toEqual(0);
        });

        it("should assign zero to selectedReservation.AgencyTotalPrice when calculating price and CarCategoryId is not assigned", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                StartDate: new Date(),
                EndDate: new Date()
            };

            //act
            scope.retrievePrice();

            //assert
            expect(scope.selectedReservation.AgencyTotalPrice).toEqual(0);
        });

        it("should assign zero to selectedReservation.AgencyTotalPrice when calculating price and StartDate is not assigned", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                EndDate: new Date()
            };

            //act
            scope.retrievePrice();

            //assert
            expect(scope.selectedReservation.AgencyTotalPrice).toEqual(0);
        });

        it("should assign zero to selectedReservation.AgencyTotalPrice when calculating price and EndDate is not assigned", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date()
            };

            //act
            scope.retrievePrice();

            //assert
            expect(scope.selectedReservation.AgencyTotalPrice).toEqual(0);
        });

        it("should assign a value to selectedReservation.AgencyTotalPrice when calculating price", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2015, 1, 1),
                EndDate: new Date(2015, 1, 10)
            };
            lookUpDataServ.getPrice.and.returnValue(getPriceDeferred.promise);

            //act
            scope.retrievePrice();

            //assert
            expect(scope.selectedReservation.AgencyTotalPrice).not.toBe(0);
        });

        it("should assign zero to selectedReservation.AgencyTotalPrice when calculating price and payments amount is higher than amount to paid", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2016, 1, 20),
                EndDate: new Date(2016, 1, 25),
                TotalPaid: 400
            };
            lookUpDataServ.getPrice.and.returnValue(getPriceDeferred.promise);

            //act
            scope.retrievePrice();

            getPriceDeferred.resolve({
                "PriceId": 1,
                "TourOperatorId": 1,
                "CarCategoryId": 1,
                "IntervalId": 1,
                "SeasonType": 1,
                "PriceValue": 100,
                "ModifiedDate": "0001-01-01T00:00:00",
                "TourOperator": null,
                "CarCategory": null,
                "Interval": null,
                "AgencyTotalPrice": 100
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.RemainingPaymentsAmount).toEqual(0);
        });

        it("should not assign a value to selectedReservation.Fee when retrieving price", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2015, 1, 1),
                EndDate: new Date(2015, 1, 3),
                Fee: 100
            };
            lookUpDataServ.getPrice.and.returnValue(getPriceDeferred.promise);

            //act
            scope.retrievePrice();
            getPriceDeferred.resolve({
                PriceId: 1,
                PriceValue: 100,
                TourOperatorPriceValue: 90,
                Fee: 155
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.Fee).toBe(100);
        });

        it("should display an error when creating a reservation with more than 29 days", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2015, 1, 1),
                EndDate: new Date(2015, 1, 31)
            };

            toastr.error = jasmine.createSpy();

            //act
            scope.retrievePrice();

            //assert
            expect(toastr.error).toHaveBeenCalled();
        });

        it("should display an error when reservation start date is bigger than end date", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2015, 1, 5),
                EndDate: new Date(2015, 1, 4)
            };

            toastr.error = jasmine.createSpy();

            //act
            scope.retrievePrice();

            //assert
            expect(toastr.error).toHaveBeenCalled();
        });

        it("should set TourOperatorBasePrice when successfully retrieving prices ", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2015, 1, 1),
                EndDate: new Date(2015, 1, 5)
            };

            toastr = {
                success: function (msg, title) {
                },
                error: function (msg, title) {
                }
            };
            spyOn(toastr, "error");
            lookUpDataServ.getPrice.and.returnValue(getPriceDeferred.promise);

            //act
            scope.retrievePrice();
            getPriceDeferred.resolve({
                "PriceId": 1,
                "TourOperatorId": 0,
                "CarCategoryId": 0,
                "IntervalId": 0,
                "SeasonType": 0,
                "Value": -1,
                "ModifiedDate": "0001-01-01T00:00:00",
                "TourOperator": null,
                "CarCategory": null,
                "Interval": null,
                TourOperatorBasePrice: 100
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.TourOperatorBasePrice).toBeDefined();
            expect(scope.selectedReservation.TourOperatorBasePrice).not.toBeNull();
        });

        it("should set right value to TourOperatorBasePrice when successfully retrieving prices ", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2015, 1, 1),
                EndDate: new Date(2015, 1, 5)
            };

            toastr = {
                success: function (msg, title) {
                },
                error: function (msg, title) {
                }
            };
            spyOn(toastr, "error");
            lookUpDataServ.getPrice.and.returnValue(getPriceDeferred.promise);

            //act
            scope.retrievePrice();
            getPriceDeferred.resolve({
                "PriceId": 1,
                "TourOperatorId": 0,
                "CarCategoryId": 0,
                "IntervalId": 0,
                "SeasonType": 0,
                "Value": -1,
                "ModifiedDate": "0001-01-01T00:00:00",
                "TourOperator": null,
                "CarCategory": null,
                "Interval": null,
                TourOperatorBasePrice: 100
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.TourOperatorBasePrice).toBe(100);
        });

        it("should set TourOperatorTotalPrice when successfully retrieving prices ", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2015, 1, 1),
                EndDate: new Date(2015, 1, 5)
            };

            toastr = {
                success: function (msg, title) {
                },
                error: function (msg, title) {
                }
            };
            spyOn(toastr, "error");
            lookUpDataServ.getPrice.and.returnValue(getPriceDeferred.promise);

            //act
            scope.retrievePrice();
            getPriceDeferred.resolve({
                "PriceId": 1,
                "TourOperatorId": 0,
                "CarCategoryId": 0,
                "IntervalId": 0,
                "SeasonType": 0,
                "Value": -1,
                "ModifiedDate": "0001-01-01T00:00:00",
                "TourOperator": null,
                "CarCategory": null,
                "Interval": null,
                TourOperatorTotalPrice: 100
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.TourOperatorTotalPrice).toBeDefined();
            expect(scope.selectedReservation.TourOperatorTotalPrice).not.toBeNull();
        });

        it("should set right value to TourOperatorTotalPrice when successfully retrieving prices ", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2015, 1, 1),
                EndDate: new Date(2015, 1, 5)
            };

            toastr = {
                success: function (msg, title) {
                },
                error: function (msg, title) {
                }
            };
            spyOn(toastr, "error");
            lookUpDataServ.getPrice.and.returnValue(getPriceDeferred.promise);

            //act
            scope.retrievePrice();
            getPriceDeferred.resolve({
                "PriceId": 1,
                "TourOperatorId": 0,
                "CarCategoryId": 0,
                "IntervalId": 0,
                "SeasonType": 0,
                "Value": -1,
                "ModifiedDate": "0001-01-01T00:00:00",
                "TourOperator": null,
                "CarCategory": null,
                "Interval": null,
                TourOperatorTotalPrice: 100
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.TourOperatorTotalPrice).toBe(100);
        });

        it("should set AgencyBasePrice when successfully retrieving prices ", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2015, 1, 1),
                EndDate: new Date(2015, 1, 5)
            };

            toastr = {
                success: function (msg, title) {
                },
                error: function (msg, title) {
                }
            };
            spyOn(toastr, "error");
            lookUpDataServ.getPrice.and.returnValue(getPriceDeferred.promise);

            //act
            scope.retrievePrice();
            getPriceDeferred.resolve({
                "PriceId": 1,
                "TourOperatorId": 0,
                "CarCategoryId": 0,
                "IntervalId": 0,
                "SeasonType": 0,
                "Value": -1,
                "ModifiedDate": "0001-01-01T00:00:00",
                "TourOperator": null,
                "CarCategory": null,
                "Interval": null,
                AgencyBasePrice: 100
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.AgencyBasePrice).toBeDefined();
            expect(scope.selectedReservation.AgencyBasePrice).not.toBeNull();
        });

        it("should set right value to AgencyBasePrice when successfully retrieving prices ", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2015, 1, 1),
                EndDate: new Date(2015, 1, 5)
            };

            toastr = {
                success: function (msg, title) {
                },
                error: function (msg, title) {
                }
            };
            spyOn(toastr, "error");
            lookUpDataServ.getPrice.and.returnValue(getPriceDeferred.promise);

            //act
            scope.retrievePrice();
            getPriceDeferred.resolve({
                "PriceId": 1,
                "TourOperatorId": 0,
                "CarCategoryId": 0,
                "IntervalId": 0,
                "SeasonType": 0,
                "Value": -1,
                "ModifiedDate": "0001-01-01T00:00:00",
                "TourOperator": null,
                "CarCategory": null,
                "Interval": null,
                AgencyBasePrice: 100
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.AgencyBasePrice).toBe(100);
        });

        it("should set AgencyBasePrice when successfully retrieving prices ", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2015, 1, 1),
                EndDate: new Date(2015, 1, 5)
            };

            toastr = {
                success: function (msg, title) {
                },
                error: function (msg, title) {
                }
            };
            spyOn(toastr, "error");
            lookUpDataServ.getPrice.and.returnValue(getPriceDeferred.promise);

            //act
            scope.retrievePrice();
            getPriceDeferred.resolve({
                "PriceId": 1,
                "TourOperatorId": 0,
                "CarCategoryId": 0,
                "IntervalId": 0,
                "SeasonType": 0,
                "Value": -1,
                "ModifiedDate": "0001-01-01T00:00:00",
                "TourOperator": null,
                "CarCategory": null,
                "Interval": null,
                AgencyTotalPrice: 100
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.AgencyTotalPrice).toBeDefined();
            expect(scope.selectedReservation.AgencyTotalPrice).not.toBeNull();
        });

        it("should set right value to AgencyTotalPrice when successfully retrieving prices ", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2015, 1, 1),
                EndDate: new Date(2015, 1, 5)
            };

            toastr = {
                success: function (msg, title) {
                },
                error: function (msg, title) {
                }
            };
            spyOn(toastr, "error");
            lookUpDataServ.getPrice.and.returnValue(getPriceDeferred.promise);

            //act
            scope.retrievePrice();
            getPriceDeferred.resolve({
                "PriceId": 1,
                "TourOperatorId": 0,
                "CarCategoryId": 0,
                "IntervalId": 0,
                "SeasonType": 0,
                "Value": -1,
                "ModifiedDate": "0001-01-01T00:00:00",
                "TourOperator": null,
                "CarCategory": null,
                "Interval": null,
                AgencyTotalPrice: 100
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.AgencyTotalPrice).toBe(100);
        });

        it("should display an error when reservation start date equals end date", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2015, 1, 5),
                EndDate: new Date(2015, 1, 5)
            };

            toastr.error = jasmine.createSpy();

            //act
            scope.retrievePrice();

            //assert
            expect(toastr.error).toHaveBeenCalled();
        });

        it("should assign zero to selectedReservation.AgencyTotalPrice if there is no price created for the TO, CC and SD combination", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2015, 1, 1),
                EndDate: new Date(2015, 1, 3)
            };

            toastr = {
                success: function (msg, title) {
                },
                error: function (msg, title) {
                }
            };
            spyOn(toastr, "error");
            lookUpDataServ.getPrice.and.returnValue(getPriceDeferred.promise);

            //act
            scope.retrievePrice();

            getPriceDeferred.resolve({
                "PriceId": 0,
                "TourOperatorId": 0,
                "CarCategoryId": 0,
                "IntervalId": 0,
                "SeasonType": 0,
                "Value": -1,
                "ModifiedDate": "0001-01-01T00:00:00",
                "TourOperator": null,
                "CarCategory": null,
                "Interval": null
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.AgencyTotalPrice).toEqual(0);
        });

        it("should assign the value to selectedReservation.AgencyTotalPrice if there is a price created for the TO, CC and SD combination", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2015, 1, 1),
                EndDate: new Date(2015, 1, 3)
            };
            lookUpDataServ.getPrice.and.returnValue(getPriceDeferred.promise);

            //act
            scope.retrievePrice();

            getPriceDeferred.resolve({
                "PriceId": 1,
                "TourOperatorId": 1,
                "CarCategoryId": 1,
                "IntervalId": 1,
                "SeasonType": 1,
                "AgencyTotalPrice": 100,
                "ModifiedDate": "0001-01-01T00:00:00",
                "TourOperator": null,
                "CarCategory": null,
                "Interval": null
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.AgencyTotalPrice).toEqual(100);
        });

        it("should assign the value  of Remaining Payments to selectedReservation.RemainingPaymentsAmount if there is a price created for the TO, CC and SD combination", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2016, 1, 20),
                EndDate: new Date(2016, 1, 25),
                TotalPaid: 50
            };
            lookUpDataServ.getPrice.and.returnValue(getPriceDeferred.promise);

            //act
            scope.retrievePrice();

            getPriceDeferred.resolve({
                "PriceId": 1,
                "TourOperatorId": 1,
                "CarCategoryId": 1,
                "IntervalId": 1,
                "SeasonType": 1,
                "PriceValue": 100,
                "ModifiedDate": "0001-01-01T00:00:00",
                "TourOperator": null,
                "CarCategory": null,
                "Interval": null,
                "AgencyTotalPrice": 100
            });
            scope.$apply();

            //assert
            expect(scope.selectedReservation.RemainingPaymentsAmount).toEqual(50);
        });

        it("should show toastr message if no prices are created for a given combination ", function () {
            //arrange
            scope.selectedReservation = {
                TourOperatorId: 1,
                CarCategoryId: 1,
                StartDate: new Date(2016, 1, 20),
                EndDate: new Date(2016, 1, 25)
            };

            toastr = {
                success: function (msg, title) {
                },
                error: function (msg, title) {
                }
            };
            spyOn(toastr, "error");
            lookUpDataServ.getPrice.and.returnValue(getPriceDeferred.promise);

            //act
            scope.retrievePrice();
            getPriceDeferred.resolve({
                "PriceId": 0,
                "TourOperatorId": 0,
                "CarCategoryId": 0,
                "IntervalId": 0,
                "SeasonType": 0,
                "Value": -1,
                "ModifiedDate": "0001-01-01T00:00:00",
                "TourOperator": null,
                "CarCategory": null,
                "Interval": null
            });
            scope.$apply();

            //assert
            expect(toastr.error).toHaveBeenCalled();
        });
    });

    it("should set customerSelected to false when calling method selectReservation", function () {
        expect(scope.customerSelected).toBe(false);
    });

    it("should call method ReservationHandler.getReservationFinalPrice when calling method getReservationFinalPrice", function () {
        //arrange
        var reservation = {};

        reservationHandler.getReservationFinalPrice = jasmine.createSpy("");

        //act
        scope.getReservationFinalPrice(reservation);

        //assert
        expect(reservationHandler.getReservationFinalPrice).toHaveBeenCalled();
    });

    it("should return 0 when calling method getReservationFinalPrice and reservation is null", function () {
        //act
        var result = scope.getReservationFinalPrice(null);

        //assert
        expect(result).toEqual(0);
    });

    it("should have minDate property set to today date", function () {
        //assert
        expect(scope.minDate).toBeDefined();
        var today = new Date();
        expect(scope.minDate.getDate()).toBe(today.getDate());
        expect(scope.minDate.getDay()).toBe(today.getDay());
        expect(scope.minDate.getFullYear()).toBe(today.getFullYear());
        expect(scope.minDate.getMonth()).toBe(today.getMonth());
    });

    it("should call lookUpService.carCategoriesByCarProvider when calling carProviderSelectionChanges method", function () {
        //arrange
        lookUpDataServ.carCategoriesByCarProvider.and.returnValue(carCategoriesByCarProviderDeferred.promise);
        lookUpDataServ.tourOperatorByCarProvider.and.returnValue(tourOperatorByCarProviderDeferred.promise);
        lookUpDataServ.activeTourOperatorUsedToMakeReservationByCarProvider.and.returnValue(activeTourOperatorUsedToMakeReservationByCarProviderDeferred.promise);

        //act
        scope.carProviderSelectionChanges(1);

        //assert
        expect(lookUpDataServ.carCategoriesByCarProvider).toHaveBeenCalled();
    });

    it("should call lookUpService.tourOperatorsByCarProvider when calling carProviderSelectionChanges method", function () {
        //arrange
        lookUpDataServ.carCategoriesByCarProvider.and.returnValue(carCategoriesByCarProviderDeferred.promise);
        lookUpDataServ.activeTourOperatorUsedToMakeReservationByCarProvider.and.returnValue(activeTourOperatorUsedToMakeReservationByCarProviderDeferred.promise);

        //act
        scope.carProviderSelectionChanges(1);

        //assert
        expect(lookUpDataServ.activeTourOperatorUsedToMakeReservationByCarProvider).toHaveBeenCalled();
    });

    it("should call lookUpService.pickUpLocationsByProvince when calling getPickUpPlaces method", function () {
        //arrange
        lookUpDataServ.pickUpLocationsByProvince.and.returnValue(pickUpLocationsByProvinceDeferred.promise);

        //act
        scope.getPickUpPlaces(1);
        deferred.resolve();

        //assert
        expect(lookUpDataServ.pickUpLocationsByProvince).toHaveBeenCalled();
    });

    it("should set customerSelected to true when calling findCustomer", function () {
        scope.customerSelected = false;
        scope.selectedReservation = {
            Customer: {
                CellPhone: ""
            }
        };
        customerServ.getCustomerMatchingPhoneNumberPattern.and.returnValue(getCustomerMatchingPhoneNumberPatternDeferred.promise);

        //act
        scope.findCustomer();
        getCustomerMatchingPhoneNumberPatternDeferred.resolve();
        scope.$apply();

        expect(scope.customerSelected).toBeTruthy();
    });

    it("should open the customer modal for existing customer when calling findCustomer", function () {

        scope.selectedReservation = {
            Customer: {
                CellPhone: ""
            }
        };
        var customers = [{}, {}, {}];
        customersModalMock.$promise = modalMockPromiseDeferred.promise;
        customerServ.getCustomerMatchingPhoneNumberPattern.and.returnValue(getCustomerMatchingPhoneNumberPatternDeferred.promise);

        //act
        scope.findCustomer();
        getCustomerMatchingPhoneNumberPatternDeferred.resolve(customers);
        modalMockPromiseDeferred.resolve(null);
        scope.$apply();

        expect(scope.customers).toEqual(customers);
        expect(customersModalMock.show).toHaveBeenCalled();
    });



    it("should set activeTab to right result when calling activateTab method", function () {
        //act
        scope.activateTab("justActivatedTab");

        //assert
        expect(scope.activeReservationTab).toBe("justActivatedTab");
    });

    it("should set activeTab to activeReservationTab when loading", function () {
        //assert
        expect(scope.activeReservationTab).toBe("activeReservationTab");
    });

    describe("init method", function(){

        function arrangeInit(){
            location.search.and.returnValue({reservationId: 0});
            reservationServ.loadReservations.and.returnValue(loadActiveReservationsDeferred.promise);
            lookUpDataServ.getProvinces.and.returnValue(getProvincesDeferred.promise);
            lookUpDataServ.getCarProviders.and.returnValue([]);
            reservationServ.loadReservationById.and.returnValue(loadReservationByIdDeferred.promise);
            lookUpDataServ.pickUpLocationsByProvince.and.returnValue(pickUpLocationsByProvinceDeferred.promise);
            lookUpDataServ.getRetailers.and.returnValue(getRetailersDeferred.promise);
        };

        it("should set carProviders array", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();

            //assert
            expect(scope.carProviders).not.toBe(undefined);
        });

        it("carProviders should have 3 providers", function () {
            arrangeInit();
            lookUpDataServ.getCarProviders.and.returnValue([{}, {}, {}]);


            //act
            scope.init();

            //assert
            expect(scope.carProviders.length).toBe(3);
        });

        it("should call sessionService.join when loading", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();

            //assert
            expect(sessionServ.join).toHaveBeenCalled();
        });

        it("should load active reservation when loading if not coming from dashboard", function () {
            //arrange
            arrangeInit();
            scope.reservationForm = {$dirty: false};
            scope.activeReservations = null;
            location.search.and.returnValue({reservationId: null});

            //act
            scope.init();
            loadActiveReservationsDeferred.resolve([{ReservationId: 1}]);
            scope.$apply();

            //assert
            expect(scope.activeReservations).not.toBeNull();
        });

        it("should call reservationService.loadReservationById when coming from dashboard", function () {
            //arrange
            arrangeInit();
            location.search.and.returnValue({reservationId: 2});

            //act
            scope.init();

            //assert
            expect(reservationServ.loadReservationById).toHaveBeenCalled();
        });

        it("should set cancelButtonPressed to false", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();
            getProvincesDeferred.resolve({});
            scope.$apply();

            //assert
            expect(scope.cancelOrActivateButtonPressed).toBe(false);
        });

        it("should set carProviders array", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();

            //assert
            expect(scope.carProviders).not.toBe(undefined);
        });

        it("should set matchingCustomers when loading", function () {
            //arrange
            arrangeInit();

            //act
            scope.init();

            //assert
            expect(scope.matchingCustomers).not.toBeUndefined();
            expect(scope.matchingCustomers).not.toBeNull();
        });

        it("should load retailers", function(){
            //arrange
            arrangeInit();
            var retailers = [{}, {}];

            //act
            scope.init();
            getRetailersDeferred.resolve(retailers);
            scope.$apply();

            //assert
            expect(scope.retailers).toBe(retailers);
        });
    });

    it("should set to null selectedReservation when changing from active reservation tab to inactive one and viceversa", function () {
        //arrange
        scope.selectedReservation = {};

        //act
        scope.activateTab("");

        //assert
        expect(scope.selectedReservation).toBeNull();
    });

    it("should make a call for active reservations if active reservations tab is selected", function () {
        //arrange
        scope.reservationForm = {
            $dirty: false
        };
        reservationServ.loadReservations.and.returnValue(loadActiveReservationsDeferred.promise);
        reservationServ.loadReservationById.and.returnValue(loadReservationByIdDeferred.promise);
        lookUpDataServ.pickUpLocationsByProvince.and.returnValue(pickUpLocationsByProvinceDeferred.promise);

        //act
        scope.activateTab("activeReservationTab");
        loadActiveReservationsDeferred.resolve([{}]);
        scope.$apply();

        //assert
        expect(reservationServ.loadReservations).toHaveBeenCalledWith(false);
    });

    it("should make a call for inactive reservations if inactive reservations tab is selected", function () {
        //assert
        reservationServ.loadReservations.and.returnValue(loadActiveReservationsDeferred.promise);

        //act
        scope.activateTab("inactiveReservationTab");

        //assert
        expect(reservationServ.loadReservations).toHaveBeenCalledWith(true);
    });

    it("should not make a call for active reservations if active reservations tab is not selected", function () {
        //assert
        reservationServ.loadReservations.and.returnValue(loadActiveReservationsDeferred.promise);

        //act
        scope.activateTab("activeReservationTab");

        //assert
        expect(reservationServ.loadReservations).not.toHaveBeenCalledWith(true);
    });

    it("should not make a call for inactive reservations if inactive reservations tab is not selected", function () {
        //arrange
        reservationServ.loadReservations.and.returnValue(loadActiveReservationsDeferred.promise);

        //act
        scope.activateTab("inactiveReservationTab");

        //assert
        expect(reservationServ.loadReservations).not.toHaveBeenCalledWith(false);
    });

    it("should assign activeReservation when active reservations tab is selected", function () {
        //arrange
        scope.activeReservations = null;
        reservationServ.loadReservations.and.returnValue(loadActiveReservationsDeferred.promise);
        reservationServ.loadReservationById.and.returnValue(loadReservationByIdDeferred.promise);
        lookUpDataServ.pickUpLocationsByProvince.and.returnValue(pickUpLocationsByProvinceDeferred.promise);

        scope.reservationForm = {
            $dirty: false
        };

        //act
        scope.activateTab("activeReservationTab");
        loadActiveReservationsDeferred.resolve([{ReservationId: 1}]);
        scope.$apply();

        //assert
        expect(scope.activeReservations).not.toBeNull();
    });

    it("should assign inactiveReservation when active reservations tab is selected", function () {
        //arrange
        scope.inactiveReservations = null;

        scope.reservationForm = {
            $dirty: false
        };
        reservationServ.loadReservations.and.returnValue(loadActiveReservationsDeferred.promise);

        //act
        scope.activateTab("inactiveReservationTab");
        loadActiveReservationsDeferred.resolve({});
        scope.$apply();

        //assert
        expect(scope.inactiveReservations).not.toBeNull();
    });

    it("should assign selectedReservation to first reservation in list if activeReservation tab is selected", function () {
        //arrange
        scope.selectedReservation = null;

        scope.reservationForm = {
            $dirty: false
        };
        reservationServ.loadReservations.and.returnValue(loadActiveReservationsDeferred.promise);
        reservationServ.loadReservationById.and.returnValue(loadReservationByIdDeferred.promise);
        lookUpDataServ.carCategoriesByCarProvider.and.returnValue(carCategoriesByCarProviderDeferred.promise);
        lookUpDataServ.activeTourOperatorUsedToMakeReservationByCarProvider.and.returnValue(activeTourOperatorUsedToMakeReservationByCarProviderDeferred.promise);
        lookUpDataServ.pickUpLocationsByProvince.and.returnValue(pickUpLocationsByProvinceDeferred.promise);

        //act
        scope.activateTab("activeReservationTab");
        loadActiveReservationsDeferred.resolve([{ReservationId: 1}]);
        loadReservationByIdDeferred.resolve({
            PickUpLocation: {
                PickUpLocationId: 182,
                Name: "Ube Varadero - Hotel Tropical",
                ProvinceId: 5,
                Province: null
            },
            ReturnLocation: {
                PickUpLocationId: 182,
                Name: "Ube Varadero - Hotel Tropical",
                ProvinceId: 5,
                Province: null
            }
        });
        scope.$apply();

        //assert
        expect(scope.selectedReservation).not.toBeNull();
    });

    it("should behave like creating a new reservation when activating tab activeReservation", function () {
        scope.selectedReservation = null;

        scope.reservationForm = {
            $dirty: false
        };
        reservationServ.loadReservations.and.returnValue(loadActiveReservationsDeferred.promise);
        reservationServ.loadReservationById.and.returnValue(loadReservationByIdDeferred.promise);
        lookUpDataServ.carCategoriesByCarProvider.and.returnValue(carCategoriesByCarProviderDeferred.promise);
        lookUpDataServ.activeTourOperatorUsedToMakeReservationByCarProvider.and.returnValue(activeTourOperatorUsedToMakeReservationByCarProviderDeferred.promise);
        lookUpDataServ.pickUpLocationsByProvince.and.returnValue(pickUpLocationsByProvinceDeferred.promise);

        //act
        scope.activateTab("activeReservationTab");
        loadActiveReservationsDeferred.resolve([]);
        scope.$apply();

        //assert
        expect(scope.creatingNewReservation).toBe(true);
    });

    it("should bring matching customer when calling fetchCustomers methond", function () {
        //arrange
        customerServ.getCustomerMatchingPhoneNumberPattern.and.returnValue(getCustomerMatchingPhoneNumberPatternDeferred.promise);

        //act
        scope.FetchCustomers("");
        getCustomerMatchingPhoneNumberPatternDeferred.resolve([]);

        //assert
        expect(customerServ.getCustomerMatchingPhoneNumberPattern).toHaveBeenCalled();
    });

    it("should mark reservation as confirmed when calling confirmReservation method", function () {
        //act
        scope.confirmReservation();

        //assert
        expect(reservationHandler.confirm).toHaveBeenCalled();
    });

    it("should set scope.reservationConfirmed to true when calling confirmReservation", function () {
        //arrange
        scope.reservationConfirmed = false;

        //act
        scope.confirmReservation();

        //assert
        expect(scope.reservationConfirmed).toBe(true);
    });

    it("should refresh reservations list when calling RefreshReservationsList method", function () {
        //arrange
        reservationServ.loadReservations.and.returnValue(loadActiveReservationsDeferred.promise);

        //act
        scope.RefreshReservationsList();

        //assert
        expect(reservationServ.loadReservations).toHaveBeenCalled();
    });

    it("should activate activeReservationTab when calling RefreshReservationsList method", function () {

        spyOn(scope, "activateTab");
        //act
        scope.RefreshReservationsList();
        //assert
        expect(scope.activateTab).toHaveBeenCalledWith("activeReservationTab");
    });

    it("should email voucher when calling emailVoucher method", function () {
        //arrange
        scope.selectedReservation = {
            ReservationId: 1
        };
        emailServ.emailVoucher.and.returnValue(emailVoucherDeferred.promise);

        //act
        scope.emailVoucher();

        //assert
        expect(emailServ.emailVoucher).toHaveBeenCalled();
    });

    it("should email voucher to right customer when calling emailVoucher method", function () {
        //arrange
        scope.selectedReservation = {
            ReservationId: 1
        };
        emailServ.emailVoucher.and.returnValue(emailVoucherDeferred.promise);

        //act
        scope.emailVoucher();

        //assert
        expect(emailServ.emailVoucher).toHaveBeenCalledWith(1);
    });

    it("should notify client after emailing voucher", function () {
        //arrange
        scope.selectedReservation = {
            ReservationId: 1
        };

        toastr.info = jasmine.createSpy();
        emailServ.emailVoucher.and.returnValue(emailVoucherDeferred.promise);

        //act
        scope.emailVoucher();
        emailVoucherDeferred.resolve(null);
        scope.$apply();

        //assert
        expect(toastr.info).toHaveBeenCalled();
    });

    it("should prompt user to save changes when selecting another reservation if changes were pending", function () {

        //arrange
        var reservation = new Reservation();
        reservation.ReservationId = 10;
        reservation.CarCategory = {CarProviderId: 1};
        reservation.PickUpLocation = {PickupLocationId: 1, ProvinceId: 1};
        reservation.CarProvider = "Cubacar";
        scope.reservationForm = {$dirty: true, ReservationId: 11};

        //act
        scope.selectReservation(reservation);

        //assert
        expect(confirmationModalMock.show).toHaveBeenCalled();
    });

    it("should not prompt user to save changes made when selecting another reservation if no pending changes ", function () {

        //arrange
        var reservation = new Reservation();
        reservation.ReservationId = 10;
        reservation.CarCategory = {CarProviderId: 1};
        reservation.PickUpLocation = {PickupLocationId: 1, ProvinceId: 1};
        reservation.CarProvider = "Cubacar";
        scope.reservationForm = {$dirty: false};
        reservationServ.loadReservationById.and.returnValue(loadReservationByIdDeferred.promise);

        //act
        scope.selectReservation(reservation);

        //assert
        expect(confirmationModalMock.show).not.toHaveBeenCalled();
    });

    it("should not show modal when  calling selectReservation and creating new Reservation (ReservationId undefined)", function () {
        //arrange
        var reservation = new Reservation();
        scope.reservationForm = {$dirty: true};
        reservationServ.loadReservationById.and.returnValue(loadReservationByIdDeferred.promise);

        //act
        scope.selectReservation(reservation);

        //assert
        expect(confirmationModalMock.show).not.toHaveBeenCalled();

    });

    it("should not show modal when  calling selectReservation and creating new Reservation (ReservationId 0)", function () {
        //arrange
        var reservation = new Reservation();
        scope.reservationForm = {$dirty: true, ReservationId: 0};
        reservationServ.loadReservationById.and.returnValue(loadReservationByIdDeferred.promise);

        //act
        scope.selectReservation(reservation);

        //assert
        expect(confirmationModalMock.show).not.toHaveBeenCalled();
    });


    it("should call reportGenerator generateVoucher method when calling printVoucher method", function () {
        //arrange
        scope.selectedReservation = {
            ReservationId: 1
        };

        //act
        scope.printVoucher();

        //assert
        expect(reportGeneratorServ.generateVoucher).toHaveBeenCalled();
    });

    it("should call reportGeneratorService.generateReservationHistory when calling showReservationHistory method", function () {
        //arrange
        scope.selectedReservation = {
            ReservationId: 1
        };

        //act
        scope.showReservationHistory();

        //assert
        expect(reportGeneratorServ.generateReservationHistory).toHaveBeenCalled();
    });

    it("should alert user when calling submitVoucher and there is no email(blank)", function () {
        //arrange
        scope.selectedReservation = {
            Customer: {
                Email: ""
            }
        };

        scope.reservationForm = {
            customerEmail: {
                $dirty: false
            },
            customerFirstName: {
                $dirty: false
            },
            customerLastName: {
                $dirty: false
            }
        };

        toastr.error = jasmine.createSpy();

        //act
        scope.submitVoucher();

        //assert
        expect(toastr.error).toHaveBeenCalled();
    });

    it("should alert user when calling submitVoucher and there is no email(null)", function () {
        //arrange
        scope.selectedReservation = {
            Customer: {
                Email: null
            }
        };

        toastr.error = jasmine.createSpy();

        scope.reservationForm = {
            customerEmail: {
                $dirty: false
            },
            customerFirstName: {
                $dirty: false
            },
            customerLastName: {
                $dirty: false
            }
        };

        //act
        scope.submitVoucher();

        //assert
        expect(toastr.error).toHaveBeenCalled();
    });

    it("should alert user when calling submitVoucher and there is no email(undefined)", function () {
        //arrange
        scope.selectedReservation = {
            Customer: {}
        };

        toastr.error = jasmine.createSpy();

        scope.reservationForm = {
            customerEmail: {
                $dirty: false
            },
            customerFirstName: {
                $dirty: false
            },
            customerLastName: {
                $dirty: false
            }
        };

        //act
        scope.submitVoucher();

        //assert
        expect(toastr.error).toHaveBeenCalled();
    });

    it("should not email voucher when calling submitVoucher and there is no email(blank)", function () {
        //arrange
        scope.selectedReservation = {
            Customer: {
                Email: ""
            }
        };

        toastr.error = jasmine.createSpy();

        scope.reservationForm = {
            customerEmail: {
                $dirty: false
            },
            customerFirstName: {
                $dirty: false
            },
            customerLastName: {
                $dirty: false
            }
        };

        //act
        scope.submitVoucher();

        //assert
        expect(emailServ.emailVoucher).not.toHaveBeenCalled();
    });

    it("should not email voucher when calling submitVoucher and there is no email(null)", function () {
        //arrange
        scope.selectedReservation = {
            Customer: {
                Email: null
            }
        };

        toastr.error = jasmine.createSpy();

        scope.reservationForm = {
            customerEmail: {
                $dirty: false
            },
            customerFirstName: {
                $dirty: false
            },
            customerLastName: {
                $dirty: false
            }
        };

        //act
        scope.submitVoucher();

        //assert
        expect(emailServ.emailVoucher).not.toHaveBeenCalled();
    });

    it("should not email voucher when calling submitVoucher and there is no email(undefined)", function () {
        //arrange
        scope.selectedReservation = {
            Customer: {}
        };

        toastr.error = jasmine.createSpy();

        scope.reservationForm = {
            customerEmail: {
                $dirty: false
            },
            customerFirstName: {
                $dirty: false
            },
            customerLastName: {
                $dirty: false
            }
        };

        //act
        scope.submitVoucher();

        //assert
        expect(emailServ.emailVoucher).not.toHaveBeenCalled();
    });

    it("should prompt user when submitting a voucher and reservation is not paid", function () {
        //arrange
        scope.reservationForm = {
            customerEmail: {
                $dirty: false
            },
            customerFirstName: {
                $dirty: false
            },
            customerLastName: {
                $dirty: false
            }
        };

        scope.selectedReservation = {
            Customer: {
                Email: "something"
            }
        };
        reservationHandler.isNotPaid.and.returnValue(true);

        //act
        scope.submitVoucher();

        //assert
        expect(emailVoucherConfirmationModalMock.show).toHaveBeenCalled();
    });

    it("should prompt user when submitting a voucher and reservation is not confirmed", function () {
        //arrange
        scope.reservationForm = {
            customerEmail: {
                $dirty: false
            },
            customerFirstName: {
                $dirty: false
            },
            customerLastName: {
                $dirty: false
            }
        };

        scope.selectedReservation = {
            Customer: {
                Email: "something"
            }
        };
        reservationHandler.isNotConfirmed.and.returnValue(true);

        //act
        scope.submitVoucher();

        //assert
        expect(emailVoucherConfirmationModalMock.show).toHaveBeenCalled();
    });

    it("should not email voucher when submitting a voucher and reservation is not paid", function () {
        //arrange
        scope.reservationForm = {
            customerEmail: {
                $dirty: false
            },
            customerFirstName: {
                $dirty: false
            },
            customerLastName: {
                $dirty: false
            }
        };

        scope.selectedReservation = {
            Customer: {
                Email: "something"
            }
        };
        reservationHandler.isNotPaid.and.returnValue(true);

        //act
        scope.submitVoucher();

        //assert
        expect(emailServ.emailVoucher).not.toHaveBeenCalled();
    });

    it("should not email voucher when submitting a voucher and reservation is not confirmed", function () {
        //arrange
        scope.reservationForm = {
            customerEmail: {
                $dirty: false
            },
            customerFirstName: {
                $dirty: false
            },
            customerLastName: {
                $dirty: false
            }
        };

        scope.selectedReservation = {
            Customer: {
                Email: "something"
            }
        };
        reservationHandler.isNotConfirmed.and.returnValue(true);

        //act
        scope.submitVoucher();

        //assert
        expect(emailServ.emailVoucher).not.toHaveBeenCalled();
    });

    it("should prompt user when emailing voucher and customer form was changed(email)", function () {
        //arrange
        scope.reservationForm = {
            customerEmail: {
                $dirty: true
            },
            customerFirstName: {
                $dirty: false
            },
            customerLastName: {
                $dirty: false
            }
        };

        toastr.error = jasmine.createSpy();

        scope.customerForm = {
            $dirty: true
        };

        //act
        scope.submitVoucher();

        //assert
        expect(toastr.error).toHaveBeenCalled();
    });

    it("should prompt user when emailing voucher and customer form was changed(firstName)", function () {
        //arrange
        scope.reservationForm = {
            customerEmail: {
                $dirty: false
            },
            customerFirstName: {
                $dirty: true
            },
            customerLastName: {
                $dirty: false
            }
        };

        toastr.error = jasmine.createSpy();

        scope.customerForm = {
            $dirty: true
        };

        //act
        scope.submitVoucher();

        //assert
        expect(toastr.error).toHaveBeenCalled();
    });

    it("should prompt user when emailing voucher and customer form was changed(lastName)", function () {
        //arrange
        scope.reservationForm = {
            customerEmail: {
                $dirty: false
            },
            customerFirstName: {
                $dirty: false
            },
            customerLastName: {
                $dirty: true
            }
        };

        toastr.error = jasmine.createSpy();

        scope.customerForm = {
            $dirty: true
        };

        //act
        scope.submitVoucher();

        //assert
        expect(toastr.error).toHaveBeenCalled();
    });

    it("should return right value of payment method when calling getPaymentMethod(card-string)", function () {
        //act
        var result = scope.getPaymentMethod({
            PaymentMethod: "1"
        });

        //assert
        expect(result).toBe("Tarjeta");
    });

    it("should return right value of payment method when calling getPaymentMethod(card-int)", function () {
        //act
        var result = scope.getPaymentMethod({
            PaymentMethod: 1
        });

        //assert
        expect(result).toBe("Tarjeta");
    });

    it("should return right value of payment method when calling getPaymentMethod(check-string)", function () {
        //act
        var result = scope.getPaymentMethod({
            PaymentMethod: "2"
        });

        //assert
        expect(result).toBe("Cheque");
    });

    it("should return right value of payment method when calling getPaymentMethod(check-int)", function () {
        //act
        var result = scope.getPaymentMethod({
            PaymentMethod: 2
        });

        //assert
        expect(result).toBe("Cheque");
    });

    it("should return right value of payment method when calling getPaymentMethod(cash-string)", function () {
        //act
        var result = scope.getPaymentMethod({
            PaymentMethod: "3"
        });

        //assert
        expect(result).toBe("Efectivo");
    });

    it("should return right value of payment method when calling getPaymentMethod(cash-int)", function () {
        //act
        var result = scope.getPaymentMethod({
            PaymentMethod: 3
        });

        //assert
        expect(result).toBe("Efectivo");
    });

    it("should return right value of payment method when calling getPaymentMethod(deposit-string)", function () {
        //act
        var result = scope.getPaymentMethod({
            PaymentMethod: "4"
        });

        //assert
        expect(result).toBe("Depósito");
    });

    it("should return right value of payment method when calling getPaymentMethod(deposit-int)", function () {
        //act
        var result = scope.getPaymentMethod({
            PaymentMethod: 4
        });

        //assert
        expect(result).toBe("Depósito");
    });

    it("should return right value of payment method when calling getPaymentType(regular-string)", function () {
        //act
        var result = scope.getPaymentType({
            PaymentType: "1"
        });

        //assert
        expect(result).toBe("Regular");
    });

    it("should return right value of payment method when calling getPaymentType(regular-int)", function () {
        //act
        var result = scope.getPaymentType({
            PaymentType: 1
        });

        //assert
        expect(result).toBe("Regular");
    });

    it("should return right value of payment method when calling getPaymentType(tip-string)", function () {
        //act
        var result = scope.getPaymentType({
            PaymentType: "2"
        });

        //assert
        expect(result).toBe("Propina");
    });

    it("should return right value of payment method when calling getPaymentType(tip-int)", function () {
        //act
        var result = scope.getPaymentType({
            PaymentType: 2
        });

        //assert
        expect(result).toBe("Propina");
    });

    it("should call emailVoucherDirectly when calling sendEmailDirectly", function () {
        //arrange
        scope.selectedReservation = { ReservationId: 10 };
        emailServ.emailVoucherDirectly.and.returnValue(emailDirectlyDeferred.promise);
        //act
        scope.sendEmailDirectly();
        //assert
        expect(emailServ.emailVoucherDirectly).toHaveBeenCalled();
    });

    it(" should call emailVoucherDirectly and pass directEmailToObject when calling sendEmailDirectly", function () {
        //arrange
        scope.selectedReservation = { ReservationId: 10 };
        scope.directEmailTo.reservationId = scope.selectedReservation.ReservationId;
        emailServ.emailVoucherDirectly.and.returnValue(emailDirectlyDeferred.promise);
        //act
        scope.sendEmailDirectly();
        //assert
        expect(emailServ.emailVoucherDirectly).toHaveBeenCalledWith(scope.directEmailTo);
    });

    it(" should call emailVoucherDirectly and pass directEmailToObject and ReservationId should not be null when calling sendEmailDirectly", function () {
        //arrange
        scope.selectedReservation = { ReservationId: 10 };
        //scope.directEmailTo.reservationId = scope.selectedReservation.ReservationId;
        emailServ.emailVoucherDirectly.and.returnValue(emailDirectlyDeferred.promise);
        //act
        scope.sendEmailDirectly();
        //assert
        expect(scope.directEmailTo.reservationId).toEqual(scope.selectedReservation.ReservationId);
    });

    it(" directEmailFrom should no be valid if incorrect email is passed", function () {
       var formElem = ['<form name="directEmailFrom">',
            '<input type="text" name="directEmailAddress" ng-model="directEmailTo.emailAddress" ' +
            'ng-pattern="/^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[_A-Za-z0-9]+[_A-Za-z0-9-]*[_A-Za-z0-9]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$/">',
            '</form>'
        ].join('');
        compile(formElem)(scope);
        form = scope.directEmailFrom;

        expect(form.$valid).toBeTruthy();
        form.directEmailAddress.$setViewValue('invalid email address');
        expect(form.directEmailAddress.$valid).toBeFalsy();
    });

    it(" directEmailFrom should be valid if correct email is passed", function () {
        var formElem = ['<form name="directEmailFrom">',
            '<input type="text" name="directEmailAddress" ng-model="directEmailTo.emailAddress" ' +
            'ng-pattern="/^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[_A-Za-z0-9]+[_A-Za-z0-9-]*[_A-Za-z0-9]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$/">',
            '</form>'
        ].join('');
        compile(formElem)(scope);
        form = scope.directEmailFrom;

        expect(form.$valid).toBeTruthy();
        form.directEmailAddress.$setViewValue('some@email.com');
        expect(form.directEmailAddress.$valid).toBeTruthy();
    });
    it(" directEmailFrom should be valid if multiple emails seperated by semicolon are passed", function () {
        var formElem = ['<form name="directEmailFrom">',
            '<input type="text" name="directEmailAddress" ng-model="directEmailTo.emailAddress" ' +
            'ng-pattern="/^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[_A-Za-z0-9]+[_A-Za-z0-9-]*[_A-Za-z0-9]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$/">',
            '</form>'
        ].join('');
        compile(formElem)(scope);
        form = scope.directEmailFrom;

        expect(form.$valid).toBeTruthy();
        form.directEmailAddress.$setViewValue('some@email.com;other@email.com');
        expect(form.directEmailAddress.$valid).toBeTruthy();
    });

});

var toastr;