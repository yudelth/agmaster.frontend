﻿'use strict';

describe('Model : Reservation', function () {
    it('should set CarProviderId to null when creating new reservation', function() {
        //act
        var reservation = new Reservation();

        //assert
        expect(reservation.CarProviderId).toBeNull();

    });

    it('should set TourOperatorId to null when creating new reservation', function() {
        //act
        var reservation = new Reservation();

        //assert
        expect(reservation.TourOperatorId).toBeNull();
    });

    it('should set CarCategoryId to null when creating new reservation', function () {
        //act
        var reservation = new Reservation();

        //assert 
        expect(reservation.CarCategoryId).toBeNull();
    });

    it('should set PickupTime to null when creating new reservation', function () {
        //act
        var reservation = new Reservation();

        //assert
        expect(reservation.PickupTime).toBeNull();
    });

    it('should set PickUpProvinceId to Habana when creating new reservation', function () {
        //act
        var reservation = new Reservation();

        //assert
        expect(reservation.PickUpProvinceId).toBe(3);
    });

    it('should set PickUpLocationId to null when creating new reservation', function () {
        //act
        var reservation = new Reservation();

        //assert
        expect(reservation.PickUpLocationId).toBeNull();
    });

    it('should set TourOperatorBasePrice to null when creating new reservation', function () {
        //act
        var reservation = new Reservation();

        //assert
        expect(reservation.TourOperatorBasePrice).toBe(0);
    });

    it('should set TourOperatorTotalPrice to null when creating new reservation', function () {
        //act
        var reservation = new Reservation();

        //assert
        expect(reservation.TourOperatorTotalPrice).toBe(0);
    });

    it('should set AgencyBasePrice to null when creating new reservation', function () {
        //act
        var reservation = new Reservation();

        //assert
        expect(reservation.AgencyBasePrice).toBe(0);
    });

    it('should set AgencyTotalPrice to null when creating new reservation', function () {
        //act
        var reservation = new Reservation();

        //assert
        expect(reservation.AgencyTotalPrice).toBe(0);
    });

    it('should set Discount to null when creating new reservation', function () {
        //act
        var reservation = new Reservation();

        //assert
        expect(reservation.Discount).toBe(0);
    });

    it('should set TotalPaid to null when creating new reservation', function () {
        //act
        var reservation = new Reservation();

        //assert
        expect(reservation.TotalPaid).toBe(0);
    });

    it('should set RemainingPaymentsAmount to null when creating new reservation', function () {
        //act
        var reservation = new Reservation();

        //assert
        expect(reservation.RemainingPaymentsAmount).toBe(0);
    });

    it('should initialize Payments to be an array when creating new reservation', function () {
        //act
        var reservation = new Reservation();

        //assert
        expect(reservation.Payments).toBeDefined();
        expect(reservation.Payments.length).toBe(0);
    });
});