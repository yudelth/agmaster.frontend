/**
 * Created by ytchicamboud on 11/16/2015.
 */

"use strict";

describe("Alert Service", function () {
    var alertService;
    var httpBackend;
    var sessionMockService;
    var carReservationInterceptorMock;

    // load the controller's module
    beforeEach(module("carReservationsApp", function ($provide) {
        sessionMockService = {
            getHeaders: function () {
                return {'Authorization': "Bearer " + "Some Bearer"};
            }
        };

        $provide.value("Session", sessionMockService);
    }));

    beforeEach(inject(function ($injector) {
        alertService = $injector.get("alertService");
        httpBackend = $injector.get("$httpBackend");
        carReservationInterceptorMock = $injector.get("carReservationHttpInterceptor");

        httpBackend.when("GET", "http://localhost:49269/api/Alerts/").respond({});
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it("should create Session", function () {
        expect(sessionMockService).toBeDefined();
        expect(sessionMockService).not.toBeNull();
    });

    it("should make a GET request to right url when calling getAlerts method", function () {
        //arrange
        httpBackend.expect("GET", "http://localhost:49269/api/Alerts/");
        //spyOn(carReservationInterceptorMock, "request");

        //act
        alertService.getAlerts();
        httpBackend.flush();
    });
});