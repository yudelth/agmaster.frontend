﻿"use strict";

describe("Service : Customer Service", function () {
    var customerService, deferred, httpBackend, sessionMockSrvc;

    beforeEach(function () {
        module("carReservationsApp", function ($provide) {
            sessionMockSrvc = {
                getHeaders: function () {
                    return { 'Authorization': "Bearer " + "12344" };
                }
            };

            $provide.value("Session", sessionMockSrvc);
        });

        inject(function (_customerService_, _$q_, _$httpBackend_) {
            deferred = _$q_.defer();
            customerService = _customerService_;
            httpBackend = _$httpBackend_;

            var customers = [
                { FirstName: "Yudel", LastName: "Tchicamboud", PhoneNumber: "1234567891", EmailAddress: "a@a.com" },
                { FirstName: "Yorcelys", LastName: "Tchicamboud", PhoneNumber: "1234567892", EmailAddress: "b@b.com" },
                { FirstName: "Barbara Nadia", LastName: "Gonzalez", PhoneNumber: "1234567893", EmailAddress: "c@c.com" },
                { FirstName: "Jose Adrian", LastName: "Gutierrez", PhoneNumber: "1234567894", EmailAddress: "d@d.com" },
                { FirstName: "Maria", LastName: "Perez", PhoneNumber: "1234567895", EmailAddress: "e@e.com" },
                { FirstName: "Angel", LastName: "Valdez", PhoneNumber: "1234567896", EmailAddress: "f@f.com" }
            ];

            var matchingCustomers = [
                { FirstName: "Maria", LastName: "Perez", PhoneNumber: "1234567895", EmailAddress: "e@e.com" }
            ];

            httpBackend.when("GET", "http://localhost:49269/api/Customers").respond(customers);
            httpBackend.when("GET", "http://localhost:49269/api/Customers/1234567895").respond(matchingCustomers);
            httpBackend.when("GET", "http://localhost:49269/api/Customers/1234567897").respond([]);
        });
    });

    it("should exist", function () {
        expect(!!customerService).toBe(true);
    });

    it("should create Session", function() {
        expect(sessionMockSrvc).toBeDefined();
    });

    it("should make a GET request to right url when calling getCustomers method", function () {
        //assert
        httpBackend.expect("GET", "http://localhost:49269/api/Customers").respond({});

        //act
        customerService.get();
        httpBackend.flush();
    });

    it("should make a GET request to right url when calling getCustomerMatchingPhoneNumberPattern method", function () {
        //assert
        var pattern = "pattern";
        httpBackend.expect("GET", "http://localhost:49269/api/Customers?pattern=" + pattern).respond({});

        //act
        customerService.getCustomerMatchingPhoneNumberPattern(pattern);
        httpBackend.flush();
    });

    it("should make a PUT request to right url when calling method post", function () {
        //assert
        httpBackend.when("POST", "http://localhost:49269/api/Customers").respond(true);
        httpBackend.expect("POST", "http://localhost:49269/api/Customers");

        //act
        customerService.post({ CustomerId: 1 });
        httpBackend.flush();
    });

    it("should return true when calling method post is done successfully", function () {
        //arrange
        httpBackend.when("POST", "http://localhost:49269/api/Customers").respond(true);

        //act
        var data = customerService.post({ CustomerId: 1 });
        httpBackend.flush();

        //assert
        expect(data.$$state.value).toBe(true);
    });

    it("should return false when calling method post is done unsuccessfully", function () {
        //arrange
        httpBackend.when("POST", "http://localhost:49269/api/Customers").respond(false);

        //act
        var data = customerService.post({ CustomerId: 1 });
        httpBackend.flush();

        //assert
        expect(data.$$state.value).toBe(false);
    });

    it("should make a PUT request to right url when calling method put", function () {
        //assert
        httpBackend.when("PUT", "http://localhost:49269/api/Customers").respond(true);
        httpBackend.expect("PUT", "http://localhost:49269/api/Customers");

        //act
        customerService.put({ CustomerId: 1 });
        httpBackend.flush();
    });

    it("should return true when calling method put is done successfully", function () {
        //arrange
        httpBackend.when("PUT", "http://localhost:49269/api/Customers").respond(true);

        //act
        var data = customerService.put({ CustomerId: 1 });
        httpBackend.flush();

        //assert
        expect(data.$$state.value).toBe(true);
    });

    it("should return false when calling method put is done unsuccessfully", function () {
        //arrange
        httpBackend.when("PUT", "http://localhost:49269/api/Customers").respond(false);

        //act
        var data = customerService.put({ CustomerId: 1 });
        httpBackend.flush();

        //assert
        expect(data.$$state.value).toBe(false);
    });

    //it("should return a customers if one with the same phoneNumber exists when calling findCustomer method", function () {
    //    var customers = customerService.findCustomer("1234567895");
    //    httpBackend.flush();

    //    expect(customers).not.toBeNull();
    //    expect(customers.$$state.value.length).toEqual(1);
    //});

    //it("should return nothing if no customer exists with the same phoneNumber exists when calling findCustomer method", function () {
    //    var customers = customerService.findCustomer("123-456-7897");
    //    httpBackend.flush();

    //    expect(customers).not.toBeNull();
    //    expect(customers.$$state.value.length).toEqual(0);
    //});

});