﻿"use strict";

describe("Service : Dashboard Service", function () {
    var dashboardService, deferred, httpBackend, sessionMockSrvc;

    beforeEach(function () {
        module("carReservationsApp", function ($provide) {
            sessionMockSrvc = {
                getHeaders: function () {
                    return { 'Authorization': "Bearer " + "12344" };
                }
            };

            $provide.value("Session", sessionMockSrvc);
        });

        inject(function (_dashboardService_, _$q_, _$httpBackend_) {
            deferred = _$q_.defer();
            dashboardService = _dashboardService_;
            httpBackend = _$httpBackend_;

            var data = [
                { Count: 1, Value: 1 },
                { Count: 2, Value: 2 },
                { Count: 1, Value: 3 },
                { Count: 4, Value: 4 },
                { Count: 5, Value: 5 },
                { Count: 2, Value: 6 },
            ];

            httpBackend.when("GET", "http://localhost:49269/api/Dashboard/Reservations/Month").respond(data);
            httpBackend.when("GET", "http://localhost:49269/api/Dashboard/Reservations/PartiallyPaid").respond(data);
        });
    });

    it("should exist", function () {
        expect(!!dashboardService).toBe(true);
    });

    it("should create Session", function() {
        expect(sessionMockSrvc).toBeDefined();
    });

    it("should return the data when calling reservations by month method", function () {
        var data = dashboardService.getReservationsByMonth();
        httpBackend.flush();

        expect(data).not.toBeNull();
        expect(data.$$state.value.length).toEqual(6);
    });

    it("should return data when calling partially paid reservations method", function () {
        var data = dashboardService.getPartiallyPaidReservations();
        httpBackend.flush();

        expect(data).not.toBeNull();
        expect(data.$$state.value.length).toEqual(6);
    });

    it("should make a GET request to right url when calling getMonthlySalesByCarCategory", function(){
        //arrange
        var tourOperatorId = 3;
        httpBackend.when("GET", "http://localhost:49269/api/Dashboard/Reservations/CarCategory").respond({});
        httpBackend.expect("GET", "http://localhost:49269/api/Dashboard/Reservations/CarCategory");

        //act
        dashboardService.getMonthlySalesByCarCategory();
        httpBackend.flush();
    });
});