/**
 * Created by ytchicamboud on 9/4/2015.
 */
"use strict";

describe("Email Service", function () {
    var emailService;
    var httpBackend;
    var sessionMockService;

    // load the controller's module
    beforeEach(module("carReservationsApp", function ($provide) {
        sessionMockService = {
            getHeaders: function () {
                return {'Authorization': "Bearer " + "Some Bearer"};
            }
        };
        $provide.value("Session", sessionMockService);
    }));

    beforeEach(inject(function ($injector) {
        emailService = $injector.get("emailService");
        httpBackend = $injector.get("$httpBackend");
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it("should create Session", function () {
        expect(sessionMockService).toBeDefined();
        expect(sessionMockService).not.toBeNull();
    });

    it("should make a POST request to right url when calling emailVoucher method", function () {
        //arrange
        var reservationId = 1;
        httpBackend.expect("POST", "http://localhost:49269/api/Emails/" + reservationId);
        httpBackend.when("POST", "http://localhost:49269/api/Emails/" + reservationId).respond({});

        //act
        emailService.emailVoucher(reservationId);
        httpBackend.flush();
    });
});