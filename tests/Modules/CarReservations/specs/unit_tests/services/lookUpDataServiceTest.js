﻿describe("Service :  LookUp Data Service", function () {
    it('has a dummy spec to test 2 + 2', function() {
        expect(2 + 2).toEqual(4);
    });
});

// "use strict";
//
// describe("Service : LookUp Data Service", function () {
//     //var lookUpDataService, deferred, rootScope, httpBackend, sessionMockSrvc;
//
//
//     // load the controller's module
//     // beforeEach(function () {
//     //     module("carReservationsApp", function ($provide) {
//     //         sessionMockSrvc = {
//     //             getHeaders: function () {
//     //                 return { 'Authorization': "Bearer " + "12344" };
//     //             }
//     //         };
//     //
//     //         $provide.value("Session", sessionMockSrvc);
//     //     });
//     //
//     //     inject(function (_lookUpDataService_, _$q_, _$rootScope_, _$httpBackend_) {
//     //         // deferred = _$q_.defer();
//     //         lookUpDataService = _lookUpDataService_;
//     //         // rootScope = _$rootScope_;
//     //         // httpBackend = _$httpBackend_;
//     //         //
//     //         // var carProviders = [
//     //         //     { CarProviderId: 1, Name: "CubaCar" },
//     //         //     { CarProviderId: 2, Name: "Havanautos" },
//     //         //     { CarProviderId: 3, Name: "Rex" }
//     //         // ];
//     //
//     //         // var provinces = [
//     //         //     { "ProvinceId": 1, "Name": "Pinar del Rio" },
//     //         //     { "ProvinceId": 2, "Name": "Artemisa" },
//     //         //     { "ProvinceId": 3, "Name": "La Habana" },
//     //         //     { "ProvinceId": 4, "Name": "Mayabeque" },
//     //         //     { "ProvinceId": 5, "Name": "Matanzas" },
//     //         //     { "ProvinceId": 6, "Name": "Cienfuegos" },
//     //         //     { "ProvinceId": 7, "Name": "Villa Clara" },
//     //         //     { "ProvinceId": 8, "Name": "Santi Spiritus" },
//     //         //     { "ProvinceId": 9, "Name": "Ciego de Avila" },
//     //         //     { "ProvinceId": 10, "Name": "Camaguey" },
//     //         //     { "ProvinceId": 11, "Name": "Las Tunas" },
//     //         //     { "ProvinceId": 12, "Name": "Granma" },
//     //         //     { "ProvinceId": 13, "Name": "Holguín" },
//     //         //     { "ProvinceId": 14, "Name": "Santiago de Cuba" },
//     //         //     { "ProvinceId": 15, "Name": "Guantanamo" },
//     //         //     { "ProvinceId": 16, "Name": "Isla de la Juventud" }
//     //         // ];
//     //
//     //         // var reservations = [
//     //         // {
//     //         //     ReservationId: "1",
//     //         //     Customer: { FirstName: "Yorcelys", LastName: "Tchicamboud", CellPhone: "3054787854", EmailAddress: "b@b.com" },
//     //         //     StartDate: "04/01/2014",
//     //         //     PickupTime: "10:00",
//     //         //     EndDate: "04/03/2014",
//     //         //     TotalDays: 2,
//     //         //     FlightNumber: "AA8899",
//     //         //     ModifiedBy: "Vivian",
//     //         //     CarProviderId: 3,
//     //         //     TourOperatorId: 2,
//     //         //     CarCategoryId: 1,
//     //         //     Status: "Confirmed",
//     //         //     Memo: "dkijopinf k,iondwe",
//     //         //     PickUpProvinceId: 2,
//     //         //     PickUpLocationId: 2,
//     //         //     SalesPrice: 1520,
//     //         //     Discount: 150,
//     //         //     TotalPaid: 550,
//     //         //     RemainingPaymentsAmount: 970,
//     //         //     Payments: [
//     //         //         { Amount: 100, PaymentDate: "10 / 11 / 2014", PaymentMethod: 1, PaymentType: 1 },
//     //         //         { Amount: 250, PaymentDate: "10 / 11 / 2014", PaymentMethod: 2, PaymentType: 2 },
//     //         //         { Amount: 200, PaymentDate: "10 / 11 / 2014", PaymentMethod: 3, PaymentType: 2 }
//     //         //     ]
//     //         // },
//     //         //     {
//     //         //         ReservationId: "2",
//     //         //         Customer: { FirstName: "Barbara Nadia", LastName: "Gonzalez", CellPhone: "7869952204", EmailAddress: "c@c.com" },
//     //         //         StartDate: "20/05/2014",
//     //         //         PickupTime: "11:00",
//     //         //         EndDate: "28/05/2014",
//     //         //         TotalDays: 8,
//     //         //         FlightNumber: "AA2098",
//     //         //         ModifiedBy: "Vivian",
//     //         //         CarProviderId: 1,
//     //         //         TourOperatorId: 3,
//     //         //         CarCategoryId: 2,
//     //         //         Status: "Canceled",
//     //         //         Memo: "djdop kddp siod",
//     //         //         PickUpProvinceId: 3,
//     //         //         PickUpLocationId: 3,
//     //         //         SalesPrice: 1800,
//     //         //         Discount: 200,
//     //         //         TotalPaid: 550,
//     //         //         RemainingPaymentsAmount: 1250,
//     //         //         Payments: [
//     //         //             { Amount: 100, PaymentDate: "10 / 11 / 2014", PaymentMethod: 1, PaymentType: 1 },
//     //         //             { Amount: 250, PaymentDate: "10 / 11 / 2014", PaymentMethod: 2, PaymentType: 1 },
//     //         //             { Amount: 200, PaymentDate: "10 / 11 / 2014", PaymentMethod: 3, PaymentType: 2 }]
//     //         //     }
//     //         // ];
//     //         //
//     //         // httpBackend.when("GET", "http://localhost:49269/api/CarProviders/").respond(carProviders);
//     //         // httpBackend.when("GET", "http://localhost:49269/api/Provinces/").respond(provinces);
//     //         // httpBackend.when("GET", "http://localhost:49269/api/PickUpLocations/Province/1").respond({});
//     //         // httpBackend.when("GET", "http://localhost:49269/api/CarCategories/CarProvider/1").respond({});
//     //         // httpBackend.when("GET", "http://localhost:49269/api/TourOperators/CarProvider/1").respond({});
//     //         //
//     //         // var url = "http://localhost:49269/api/Prices";
//     //         // url += "/TourOperator/" + 1;
//     //         // url += "/CarCategory/" + 2;
//     //         // url += "/StartDate/2015-2-2";
//     //         // url += "/EndDate/2015-2-4";
//     //         //
//     //         // var price = [{}];
//     //         // httpBackend.when("GET", url).respond(price);
//     //         // httpBackend.when("GET", "http://localhost:49269/api/Reservations/").respond(reservations);
//     //         // httpBackend.when("GET", "http://localhost:49269/api/TourOperators/").respond({});
//     //         // httpBackend.when("GET", "http://localhost:49269/api/Seasons/").respond({});
//     //         // httpBackend.when("GET", "http://localhost:49269/api/Intervals/").respond({});
//     //         // httpBackend.when("GET", "http://localhost:49269/api/Prices/").respond({});
//     //         // httpBackend.when("GET", "http://localhost:49269/api/CarCategories/").respond({});
//     //         // httpBackend.when("GET", "http://localhost:49269/api/CarCategories/TourOperator/1").respond({});
//     //     });
//     // });
//     //
//     // afterEach(function () {
//     //     // httpBackend.verifyNoOutstandingExpectation();
//     //     // httpBackend.verifyNoOutstandingRequest();
//     // });
//
//     // it("should exist", function () {
//     //     expect(lookUpDataService).toBeDefined(true);
//     // });
//
//     //it("should create Session", function () {
//     //    expect(sessionMockSrvc).toBeDefined();
//     //});
//
//     //it("should return a list of car providers when calling carProviders method", function () {
//     //    //act
//     //    var carProviders = lookUpDataService.getCarProviders();
//
//     //    //assert
//     //    expect(carProviders).not.toBeNull();
//     //    expect(carProviders.length).toEqual(3);
//     //});
//
//     //it("should make a GET request to right url when calling Provinces method", function () {
//     //    //assert
//     //    httpBackend.expect("GET", "http://localhost:49269/api/Provinces/");
//
//     //    //act
//     //    lookUpDataService.getProvinces();
//     //    httpBackend.flush();
//     //});
//
//     //it("should return a list of provinces when calling Provinces method", function () {
//     //    //act
//     //    var provinces = lookUpDataService.getProvinces();
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(provinces).not.toBeNull();
//     //    expect(provinces.$$state.value.length).toEqual(16);
//     //});
//
//     //it("should make a GET request to right url when calling PickUpLocationsByProvince method", function () {
//     //    //arrange
//     //    var provinceId = 1;
//
//     //    //assert
//     //    httpBackend.expect("GET", "http://localhost:49269/api/PickUpLocations/Province/" + provinceId);
//
//     //    //act
//     //    lookUpDataService.pickUpLocationsByProvince(provinceId);
//     //    httpBackend.flush();
//     //});
//
//     //it("should return a list of pickUpLocations when calling PickUpLocationsByProvince method", function () {
//     //    //arrange
//     //    var provinceId = 1;
//
//     //    //act
//     //    var places = lookUpDataService.pickUpLocationsByProvince(provinceId);
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(places).not.toBeUndefined();
//     //    expect(places).not.toBeNull();
//     //});
//
//     //it("should make a GET request to right url when calling carCategoriesByCarProvider method", function () {
//     //    //arrange
//     //    var carProviderId = 1;
//
//     //    //assert
//     //    httpBackend.expect("GET", "http://localhost:49269/api/CarCategories/CarProvider/" + carProviderId);
//
//     //    //act
//     //    lookUpDataService.carCategoriesByCarProvider(carProviderId);
//     //    httpBackend.flush();
//     //});
//
//     //it("should return a list of CarCategory when calling carCategoriesByCarProvider method", function () {
//     //    //arrange
//     //    var carProviderId = 1;
//
//     //    //act
//     //    var carCategories = lookUpDataService.carCategoriesByCarProvider(carProviderId);
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(carCategories).not.toBeUndefined();
//     //    expect(carCategories).not.toBeNull();
//     //});
//
//     //it("should make a GET request to right url when calling tourOperatorByCarProvider method", function () {
//     //    //arrange
//     //    var carProviderId = 1;
//
//     //    //assert
//     //    httpBackend.expect("GET", "http://localhost:49269/api/TourOperators/CarProvider/" + carProviderId);
//
//     //    //act
//     //    lookUpDataService.tourOperatorByCarProvider(carProviderId);
//     //    httpBackend.flush();
//     //});
//
//     //it("should return a list of TourOperator when calling tourOperatorByCarProvider method", function () {
//     //    //arrange
//     //    var providerId = 1;
//
//     //    //act
//     //    var tourOperators = lookUpDataService.tourOperatorByCarProvider(providerId);
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(tourOperators).not.toBeUndefined();
//     //    expect(tourOperators).not.toBeNull();
//     //});
//
//     //it("should make a GET request to right url when calling getPrice method", function () {
//     //    //arrange
//     //    var tourOperatorId = 1;
//     //    var carCategoryId = 2;
//     //    var startDate = "02/02/2015";
//     //    var endDate = "02/04/2015";
//
//     //    //assert
//     //    httpBackend.expect("GET", "http://localhost:49269/api/Prices/TourOperator/" + tourOperatorId + "/CarCategory/" + carCategoryId + "/StartDate/2015-2-2/EndDate/2015-2-4");
//
//     //    //act
//     //    lookUpDataService.getPrice(tourOperatorId, carCategoryId, startDate, endDate);
//     //    httpBackend.flush();
//     //});
//
//     //it("should return a price when calling getPrice method", function () {
//     //    //arrange
//     //    var tourOperatorId = 1;
//     //    var carCategoryId = 2;
//     //    var startDate = "02/02/2015";
//     //    var endDate = "02/04/2015";
//
//     //    //act
//     //    var price = lookUpDataService.getPrice(tourOperatorId, carCategoryId, startDate, endDate);
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(price).not.toBeUndefined();
//     //    expect(price).not.toBeNull();
//     //});
//
//     //it("should make a GET request to right url when calling Reservations method", function () {
//
//     //    httpBackend.expect("GET", "http://localhost:49269/api/Reservations/");
//     //    lookUpDataService.loadReservations();
//     //    httpBackend.flush();
//     //});
//
//     //it("should make a GET request to right url when calling getTourOperators method", function () {
//     //    //assert
//     //    httpBackend.expect("GET", "http://localhost:49269/api/TourOperators/");
//
//     //    //act
//     //    lookUpDataService.getTourOperators();
//     //    httpBackend.flush();
//     //});
//
//     //it("should return a list of tourOperators when calling getTourOperators method", function () {
//     //    //act
//     //    var tourOperators = lookUpDataService.getTourOperators();
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(tourOperators).not.toBeUndefined();
//     //    expect(tourOperators).not.toBeNull();
//     //});
//
//     //it("should make a GET request to right url when calling getSeasons method", function () {
//     //    //assert
//     //    httpBackend.expect("GET", "http://localhost:49269/api/Seasons/");
//
//     //    //act
//     //    lookUpDataService.getSeasons();
//     //    httpBackend.flush();
//     //});
//
//     //it("should return a list of seasons when calling getSeasons method", function () {
//     //    //act
//     //    var seasons = lookUpDataService.getSeasons();
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(seasons).not.toBeUndefined();
//     //    expect(seasons).not.toBeNull();
//     //});
//
//     //it("should make a GET request to right url when calling getIntervals method", function () {
//     //    //assert
//     //    httpBackend.expect("GET", "http://localhost:49269/api/Intervals/");
//
//     //    //act
//     //    lookUpDataService.getIntervals();
//     //    httpBackend.flush();
//     //});
//
//     //it("should return a list of seasons when calling getIntervals method", function () {
//     //    //act
//     //    var intervals = lookUpDataService.getIntervals();
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(intervals).not.toBeUndefined();
//     //    expect(intervals).not.toBeNull();
//     //});
//
//     //it("should make a GET request to right url when calling getPrices method", function () {
//     //    //assert
//     //    httpBackend.expect("GET", "http://localhost:49269/api/Prices/");
//
//     //    //act
//     //    lookUpDataService.getPrices();
//     //    httpBackend.flush();
//     //});
//
//     //it("should return a list of prices when calling getPrices method", function () {
//     //    //act
//     //    var prices = lookUpDataService.getPrices();
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(prices).not.toBeUndefined();
//     //    expect(prices).not.toBeNull();
//     //});
//
//     //it("should make a GET request to right url when calling getCarCategories method", function () {
//     //    //assert
//     //    httpBackend.expect("GET", "http://localhost:49269/api/CarCategories/");
//
//     //    //act
//     //    lookUpDataService.getCarCategories();
//     //    httpBackend.flush();
//     //});
//
//     //it("should return a list of prices when calling getCarCategories method", function () {
//     //    //act
//     //    var carCategories = lookUpDataService.getCarCategories();
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(carCategories).not.toBeUndefined();
//     //    expect(carCategories).not.toBeNull();
//     //});
//
//     //it("should make a GET request to right url when calling getCarCategoriesByTourOperator method", function () {
//     //    //assert
//     //    httpBackend.expect("GET", "http://localhost:49269/api/CarCategories/TourOperator/1");
//
//     //    //act
//     //    lookUpDataService.getCarCategoriesByTourOperator(1);
//     //    httpBackend.flush();
//     //});
//
//     //it("should return a list of prices when calling getCarCategoriesByTourOperator method", function () {
//     //    //act
//     //    var carCategories = lookUpDataService.getCarCategoriesByTourOperator(1);
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(carCategories).not.toBeUndefined();
//     //    expect(carCategories).not.toBeNull();
//     //});
//
//     //it("should make a POST request to right url when calling method postPrice", function () {
//     //    //assert
//     //    httpBackend.when("POST", "http://localhost:49269/api/Prices").respond(true);
//     //    httpBackend.expect("POST", "http://localhost:49269/api/Prices");
//
//     //    //act
//     //    lookUpDataService.postPrice({});
//     //    httpBackend.flush();
//     //});
//
//     //it("should return true when calling method postPrice is done successfully", function () {
//     //    //arrange
//     //    httpBackend.when("POST", "http://localhost:49269/api/Prices").respond(true);
//
//     //    //act
//     //    var data = lookUpDataService.postPrice({});
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(data.$$state.value).toBe(true);
//     //});
//
//     //it("should return false when calling method postPrice is done unsuccessfully", function () {
//     //    //arrange
//     //    httpBackend.when("POST", "http://localhost:49269/api/Prices").respond(false);
//
//     //    //act
//     //    var data = lookUpDataService.postPrice({});
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(data.$$state.value).toBe(false);
//     //});
//
//     //it("should make a POST request to right url when calling method putPrice", function () {
//     //    //assert
//     //    httpBackend.when("PUT", "http://localhost:49269/api/Prices").respond(true);
//     //    httpBackend.expect("PUT", "http://localhost:49269/api/Prices");
//
//     //    //act
//     //    lookUpDataService.putPrice({});
//     //    httpBackend.flush();
//     //});
//
//     //it("should return true when calling method putPrice is done successfully", function () {
//     //    //arrange
//     //    httpBackend.when("PUT", "http://localhost:49269/api/Prices").respond(true);
//
//     //    //act
//     //    var data = lookUpDataService.putPrice({});
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(data.$$state.value).toBe(true);
//     //});
//
//     //it("should return false when calling method putPrice is done unsuccessfully", function () {
//     //    //arrange
//     //    httpBackend.when("PUT", "http://localhost:49269/api/Prices").respond(false);
//
//     //    //act
//     //    var data = lookUpDataService.putPrice({});
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(data.$$state.value).toBe(false);
//     //});
//
//     //it("should make a DELETE request to right url when calling method deletePrice", function () {
//     //    //assert
//     //    httpBackend.when("DELETE", "http://localhost:49269/api/Prices/1").respond(true);
//     //    httpBackend.expect("DELETE", "http://localhost:49269/api/Prices/1");
//
//     //    //act
//     //    lookUpDataService.deletePrice({ PriceId: 1 });
//     //    httpBackend.flush();
//     //});
//
//     //it("should return true when calling method deletePrice is done successfully", function () {
//     //    //arrange
//     //    httpBackend.when("DELETE", "http://localhost:49269/api/Prices/1").respond(true);
//
//     //    //act
//     //    var data = lookUpDataService.deletePrice({ PriceId: 1 });
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(data.$$state.value).toBe(true);
//     //});
//
//     //it("should return false when calling method deletePrice is done unsuccessfully", function () {
//     //    //arrange
//     //    // var somefunc = function() {};
//     //    // var $ = jasmine.createSpyObj("$", ["somefunc"]);
//
//     //    httpBackend.when("DELETE", "http://localhost:49269/api/Prices/1").respond(false);
//
//     //    //act
//     //    var data = lookUpDataService.deletePrice({ PriceId: 1 });
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(data.$$state.value).toBe(false);
//     //});
//
//     //it("should make a POST request to right url when calling method postTourOperator", function () {
//     //    //assert
//     //    httpBackend.when("POST", "http://localhost:49269/api/TourOperators").respond(null);
//     //    httpBackend.expect("POST", "http://localhost:49269/api/TourOperators");
//
//     //    //act
//     //    lookUpDataService.postTourOperator({ PriceId: 1 });
//     //    httpBackend.flush();
//     //});
//
//     //it("should return true when calling method postTourOperator is done successfully", function () {
//     //    //arrange
//     //    httpBackend.when("POST", "http://localhost:49269/api/TourOperators").respond(true);
//
//     //    //act
//     //    var data = lookUpDataService.postTourOperator({ PriceId: 1 });
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(data.$$state.value).toBe(true);
//     //});
//
//     //it("should return false when calling method postTourOperator is done unsuccessfully", function () {
//     //    //arrange
//     //    httpBackend.when("POST", "http://localhost:49269/api/TourOperators").respond(false);
//
//     //    //act
//     //    var data = lookUpDataService.postTourOperator({ PriceId: 1 });
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(data.$$state.value).toBe(false);
//     //});
//
//     //it("should make a PUT request to right url when calling method postTourOperator", function () {
//     //    //assert
//     //    httpBackend.when("PUT", "http://localhost:49269/api/TourOperators").respond(true);
//     //    httpBackend.expect("PUT", "http://localhost:49269/api/TourOperators");
//
//     //    //act
//     //    lookUpDataService.putTourOperator({ PriceId: 1 });
//     //    httpBackend.flush();
//     //});
//
//     //it("should return true when calling method putTourOperator is done successfully", function () {
//     //    //arrange
//     //    httpBackend.when("PUT", "http://localhost:49269/api/TourOperators").respond(true);
//
//     //    //act
//     //    var data = lookUpDataService.putTourOperator({ PriceId: 1 });
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(data.$$state.value).toBe(true);
//     //});
//
//     //it("should return false when calling method putTourOperator is done unsuccessfully", function () {
//     //    //arrange
//     //    httpBackend.when("PUT", "http://localhost:49269/api/TourOperators").respond(false);
//
//     //    //act
//     //    var data = lookUpDataService.putTourOperator({ PriceId: 1 });
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(data.$$state.value).toBe(false);
//     //});
//
//     //it("should make a GET request to right url when calling method getTourOperatorByName", function () {
//     //    //arrange
//     //    var tourOperatorName = "San Cristobal Viaje";
//     //    httpBackend.when("GET", "http://localhost:49269/api/TourOperators?name=" + tourOperatorName).respond({});
//
//     //    //assert
//     //    httpBackend.expect("GET", "http://localhost:49269/api/TourOperators?name=" + tourOperatorName);
//
//     //    //act
//     //    lookUpDataService.getTourOperatorByName(tourOperatorName);
//     //    httpBackend.flush();
//     //});
//
//     //it("should make a GET request to right url when calling getActiveTourOperators method", function () {
//     //    //assert
//     //    httpBackend.expect("GET", "http://localhost:49269/api/TourOperators?active=true").respond({});
//
//     //    //act
//     //    lookUpDataService.getActiveTourOperators();
//     //    httpBackend.flush();
//     //});
//
//     //it("should return a list of tourOperators when calling getActiveTourOperators method", function () {
//     //    //arrange
//     //    httpBackend.when("GET", "http://localhost:49269/api/TourOperators?active=true").respond({});
//
//     //    //act
//     //    var tourOperators = lookUpDataService.getActiveTourOperators();
//     //    httpBackend.flush();
//
//     //    //assert
//     //    expect(tourOperators).not.toBeUndefined();
//     //    expect(tourOperators).not.toBeNull();
//     //});
//
//     //it("should make a GET request to right url when calling getPriceByConfiguration method", function () {
//     //    //arrange
//     //    var price = {
//     //        TourOperatorId: 1,
//     //        CarCategoryId: 2,
//     //        IntervalId: 1,
//     //        SeasonId: 2
//     //    };
//
//     //    var queryString = "tourOperatorId=" + price.TourOperatorId;
//     //    queryString += "&carCategoryId=" + price.CarCategoryId;
//     //    queryString += "&seasonId=" + price.SeasonId;
//     //    queryString += "&intervalId=" + price.IntervalId;
//
//     //    //assert
//     //    httpBackend.expect("GET", "http://localhost:49269/api/Prices?" + queryString).respond({});
//
//     //    //act
//     //    lookUpDataService.getPriceByConfiguration(price);
//     //    httpBackend.flush();
//     //});
//
//     //it("should return a list of reservation status when calling getReservationStatuses method", function () {
//     //    //act
//     //    var statuses = lookUpDataService.getReservationStatuses("paymentStatus");
//
//     //    //assert
//     //    expect(statuses).not.toBeNull();
//     //    expect(statuses.length).toEqual(3);
//     //});
//
//     //it("should make a GET request to right url when calling getTourOperatorById", function(){
//     //    //arrange
//     //    var tourOperatorId = 3;
//     //    httpBackend.when("GET", "http://localhost:49269/api/TourOperators/" + tourOperatorId).respond({});
//     //    httpBackend.expect("GET", "http://localhost:49269/api/TourOperators/" + tourOperatorId);
//
//     //    //act
//     //    lookUpDataService.getTourOperatorById(tourOperatorId);
//     //    httpBackend.flush();
//     //});
//
//     //it("should make a GET request to right url when calling activeTourOperatorUsedToMakeReservationByCarProvider method", function () {
//     //    //arrange
//     //    var carProviderId = 1;
//     //    var url = "http://localhost:49269/api/TourOperators?carProviderId=" + carProviderId + "&active=true&tobook=true";
//     //    httpBackend.when("GET", url).respond({});
//
//     //    //assert1
//     //    httpBackend.expect("GET", url);
//
//     //    //act
//     //    lookUpDataService.activeTourOperatorUsedToMakeReservationByCarProvider(carProviderId);
//     //    httpBackend.flush();
//     //});
//
//     //it("should make a GET request to right url when calling getRetailers", function(){
//     //    //arrange
//     //    var url = "http://localhost:49269/api/Retailers";
//     //    httpBackend.when("GET", url).respond([]);
//
//     //    //assert
//     //    httpBackend.expect("GET", url);
//
//     //    //act
//     //    lookUpDataService.getRetailers();
//     //    httpBackend.flush();
//     //});
//
//     //it("should make a GET request to right url when calling getPriceGroups", function(){
//     //    //arrange
//     //    var url = "http://localhost:49269/api/PriceGroups";
//     //    httpBackend.when("GET", url).respond([]);
//
//     //    //assert
//     //    httpBackend.expect("GET", url);
//
//     //    //act
//     //    lookUpDataService.getPriceGroups();
//     //    httpBackend.flush();
//     //});
// //});
//
// // var $ = function () {
// //     return {
// //         removeClass: function () {
// //             return {
// //                 fadeIn: function () { }
// //             };
// //         },
// //         addClass: function () {
// //             return {
// //                 fadeOut: function () { }
// //             };
// //         }
// //     };
// // };
//
