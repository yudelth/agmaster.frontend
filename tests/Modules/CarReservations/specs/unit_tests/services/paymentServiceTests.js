/**
 * Created by ytchicamboud on 9/4/2015.
 */
"use strict";

describe("Payment Service", function () {
    var paymentService;
    var httpBackend;
    var sessionMockService;

    // load the controller's module
    beforeEach(module("carReservationsApp", function ($provide) {
        sessionMockService = {
            getHeaders: function () {
                return {'Authorization': "Bearer " + "Some Bearer"};
            }
        };
        $provide.value("Session", sessionMockService);
    }));

    beforeEach(inject(function ($injector) {
        paymentService = $injector.get("paymentService");
        httpBackend = $injector.get("$httpBackend");

        httpBackend.when("POST", "http://localhost:49269/api/Payments/").respond({});
        httpBackend.when("DELETE", "http://localhost:49269/api/Payments/1").respond(null);
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it("should create Session", function () {
        expect(sessionMockService).toBeDefined();
        expect(sessionMockService).not.toBeNull();
    });

    it("should make a POST request to right url when calling add method", function () {
        //arrange
        httpBackend.expect("POST", "http://localhost:49269/api/Payments/");

        //act
        paymentService.add();
        httpBackend.flush();
    });

    it("should make a DELETE request to right url when calling remove method", function () {
        //arrange
        httpBackend.expect("DELETE", "http://localhost:49269/api/Payments/1");

        //act
        paymentService.remove({PaymentId: 1});
        httpBackend.flush();
    });
});