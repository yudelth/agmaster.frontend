/**
 * Created by ytchicamboud on 7/26/2015.
 */
"use strict";

describe("Service : Report Generator Service", function () {
    var reportGeneratorService, deferred, rootScope, httpBackend, sessionMockSrvc;

    // load the controller's module
    beforeEach(function () {
        module("carReservationsApp", function ($provide) {
            sessionMockSrvc = {
                getHeaders: function () {
                    return { 'Authorization': "Bearer " + "12344" };
                }
            };

            $provide.value("Session", sessionMockSrvc);
        });

        inject(function ($injector) {
            deferred = $injector.get("$q").defer();
            reportGeneratorService = $injector.get("reportGeneratorService");
            rootScope = $injector.get("$rootScope");
            httpBackend = $injector.get('$httpBackend');
        });
    });

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it("should create Session", function() {
        expect(sessionMockSrvc).toBeDefined();
    });

    it("should make a GET request to right url when calling generateSalesReport", function () {
        //arrange
        var from = new Date(2015, 10, 10);
        var to = new Date(2015, 10, 30);
        var tourOperatorId = 1;
        var statusId = 2;
        var retailer = "retailer";

        //assert
        httpBackend.expect("GET", "http://localhost:49269/api/Reports/Accounting?from=2015-11-10&to=2015-11-30&tourOperatorId=1&statusId=2&retailerIdentifier=retailer").respond({});

        //act
        reportGeneratorService.generateSalesReport(from, to, tourOperatorId, statusId, retailer);
        httpBackend.flush();
    });

    it("should make a GET request to right url when calling generateActiveReservationsReport", function () {
        //arrange
        var from = new Date(2015, 10, 10);
        var to = new Date(2015, 10, 30);

        //assert
        httpBackend.expect("GET", "http://localhost:49269/api/Reports/Reservations?from=2015-11-10&to=2015-11-30").respond({});

        //act
        reportGeneratorService.generateActiveReservationsReport(from, to);
        httpBackend.flush();
    });

    it("should make a GET request to right url when calling generateVoucher", function () {
        //arrange
        var reservationId = 1;

        //assert
        httpBackend.expect("GET", "http://localhost:49269/api/Reports/Voucher/" + reservationId).respond({});

        //act
        reportGeneratorService.generateVoucher(1);
        httpBackend.flush();
    });

    it("should make a GET request to right url when calling generateSalesByOperatorReport", function () {
        //arrange
        var from = new Date(2015, 10, 10);
        var to = new Date(2015, 10, 30);

        //assert
        httpBackend.expect("GET", "http://localhost:49269/api/Reports/Agents?from=2015-11-10&to=2015-11-30").respond({});

        //act
        reportGeneratorService.generateSalesByOperatorReport(from, to);
        httpBackend.flush();
    });

    it("should make a GET request to right url when calling generateReservationHistory", function () {
        //arrange
        var reservationId = 1;

        //assert
        httpBackend.expect("GET", "http://localhost:49269/api/Reports/Reservations/History/" + reservationId).respond({});

        //act
        reportGeneratorService.generateReservationHistory(reservationId);
        httpBackend.flush();
    });

    it("should make a GET request to right url when calling generateSalesLimitedReport", function () {
        //arrange
        var from = new Date(2015, 10, 10);
        var to = new Date(2015, 10, 30);
        var tourOperatorId = 1;
        var statusId = 2;

        //assert
        httpBackend.expect("GET", "http://localhost:49269/api/Reports/LimitedAccounting?from=2015-11-10&to=2015-11-30&tourOperatorId=1&statusId=2").respond({});

        //act
        reportGeneratorService.generateSalesLimitedReport(from, to, tourOperatorId, statusId);
        httpBackend.flush();
    });

    it("should make a GET request to right url when calling generateSalesByCarCategoryReport", function () {
        //arrange
        var from = new Date(2015, 10, 10);
        var to = new Date(2015, 10, 30);
        var carCategoryId = 10

        //assert
        httpBackend.expect("GET", "http://localhost:49269/api/Reports/Reservations/CarCategory?from=2015-11-10&to=2015-11-30&carCategoryId=10").respond({});

        //act
        reportGeneratorService.generateSalesByCarCategoryReport(from, to, carCategoryId);
        httpBackend.flush();
    });
});


