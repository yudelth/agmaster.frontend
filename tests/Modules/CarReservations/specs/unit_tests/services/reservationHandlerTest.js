﻿"use strict";

describe("Service : Reservation Handler", function () {
    var handler;
    var resStatusEnumServ;
    var paymentEnumServ;

    beforeEach(module("carReservationsApp"));

    beforeEach(inject(function (reservationHandler, reservationStatusEnumService, paymentEnumService) {
        resStatusEnumServ = reservationStatusEnumService;
        handler = reservationHandler;
        paymentEnumServ = paymentEnumService;
    }));

    it("should adjust reservation TotalPaid when adding a new payment", function () {
        //arrange
        var totalPaid = 200;
        var reservation = {TotalPaid: totalPaid};
        var payment = {
            PaymentAmount: 150,
            PaymentType: paymentEnumServ.paymentType.regular
        };

        //act
        handler.addPaymentToReservation(reservation, payment);

        //assert
        expect(reservation.TotalPaid).toEqual(totalPaid + payment.PaymentAmount);
    });

    it("should adjust reservation RemainingPaymentsAmount when adding a new payment", function () {
        //arrange
        var reservation = {
            AgencyTotalPrice: 700,
            TotalPaid: 600,
            TotalDays: 2,
            Discount: 0,
            RemainingPaymentsAmount: 100,
            Fee: 0
        };
        var payment = {
            PaymentAmount: 100,
            PaymentType: paymentEnumServ.paymentType.regular
        };
        var remaining = reservation.RemainingPaymentsAmount;

        //act
        handler.addPaymentToReservation(reservation, payment);

        //assert
        expect(reservation.RemainingPaymentsAmount).toEqual(remaining - payment.PaymentAmount);
    });

    it("should adjust reservation status when adding a new payment (partially paid)", function () {
        //arrange
        var reservation = {
            AgencyTotalPrice: 700,
            TotalPaid: 0,
            TotalDays: 2,
            Discount: 0,
            RemainingPaymentsAmount: 700,
            Fee: 0,
            ReservationStatus: resStatusEnumServ.status.confirmed | resStatusEnumServ.status.noPaid
        };
        var payment = {
            PaymentAmount: 100,
            PaymentType: paymentEnumServ.paymentType.regular
        };

        //act
        handler.addPaymentToReservation(reservation, payment);

        //assert
        expect(reservation.ReservationStatus & resStatusEnumServ.status.partiallyPaid).toBe(resStatusEnumServ.status.partiallyPaid);
    });

    it("should adjust reservation status when adding a new payment (paid)", function () {
        //arrange
        var reservation = {
            AgencyTotalPrice: 700,
            TotalPaid: 600,
            TotalDays: 2,
            Discount: 0,
            RemainingPaymentsAmount: 100,
            Fee: 0,
            ReservationStatus: resStatusEnumServ.status.confirmed | resStatusEnumServ.status.partiallyPaid
        };
        var payment = {
            PaymentAmount: 100,
            PaymentType: paymentEnumServ.paymentType.regular
        };

        //act
        handler.addPaymentToReservation(reservation, payment);

        //assert
        expect(reservation.ReservationStatus & resStatusEnumServ.status.paid).toBe(resStatusEnumServ.status.paid);
    });

    it("should not do anything when paying tips", function(){
        //arrange
        var reservation = {
            AgencyTotalPrice: 700,
            TotalPaid: 600,
            TotalDays: 2,
            Discount: 0,
            RemainingPaymentsAmount: 100,
            Fee: 0
        };
        var payment = {
            PaymentAmount: 100,
            PaymentType: paymentEnumServ.paymentType.tip
        };

        //act
        handler.addPaymentToReservation(reservation, payment);

        //assert
        expect(reservation.RemainingPaymentsAmount).toEqual(100);
        expect(reservation.TotalPaid).toEqual(600);
    });

    it("should not adjust reservation TotalPaid when removing a tip payment", function () {
        //arrange
        var totalPaid = 200;
        var reservation = { TotalPaid: totalPaid };
        var payment = { PaymentAmount: 150, PaymentType: paymentEnumServ.paymentType.tip };

        //act
        handler.removePaymentFromReservation(reservation, payment);

        //assert
        expect(reservation.TotalPaid).toEqual(200);
    });

    it("should not adjust reservation RemainingPaymentsAmount when removing a tip payment", function () {
        //arrange
        var reservation = {
            AgencyTotalPrice: 700,
            TotalPaid: 700,
            TotalDays: 2,
            Discount: 0,
            RemainingPaymentsAmount: 0,
            Fee: 0
        };
        var payment = { PaymentAmount: 100, PaymentType: paymentEnumServ.paymentType.tip };

        //act
        handler.removePaymentFromReservation(reservation, payment);

        //assert
        expect(reservation.RemainingPaymentsAmount).toEqual(0);
    });

    it("should not adjust reservation status when removing a tip payment (partially paid)", function () {
        //arrange
        var reservation = {
            AgencyTotalPrice: 700,
            TotalPaid: 700,
            TotalDays: 2,
            Discount: 0,
            RemainingPaymentsAmount: 0,
            Fee: 0,
            ReservationStatus: resStatusEnumServ.status.confirmed | resStatusEnumServ.status.paid
        };
        var payment = { PaymentAmount: 100, PaymentType: paymentEnumServ.paymentType.tip };

        //act
        handler.removePaymentFromReservation(reservation, payment);

        //assert
        expect(reservation.ReservationStatus).toBe(resStatusEnumServ.status.confirmed | resStatusEnumServ.status.paid);
    });

    it("should not adjust reservation status when removing a tip payment (noPaid)", function () {
        //arrange
        var reservation = {
            AgencyTotalPrice: 700,
            TotalPaid: 100,
            TotalDays: 2,
            Discount: 0,
            RemainingPaymentsAmount: 600,
            Fee: 0,
            ReservationStatus: resStatusEnumServ.status.confirmed | resStatusEnumServ.status.partiallyPaid
        };
        var payment = { PaymentAmount: 100, PaymentType: paymentEnumServ.paymentType.tip };

        //act
        handler.removePaymentFromReservation(reservation, payment);

        //assert
        expect(reservation.ReservationStatus).toBe(resStatusEnumServ.status.confirmed | resStatusEnumServ.status.partiallyPaid);
    });

    it("should adjust reservation TotalPaid when removing a payment", function () {
        //arrange
        var totalPaid = 200;
        var reservation = {TotalPaid: totalPaid};
        var payment = { PaymentAmount: 150, PaymentType: paymentEnumServ.paymentType.regular };

        //act
        handler.removePaymentFromReservation(reservation, payment);

        //assert
        expect(reservation.TotalPaid).toEqual(totalPaid - payment.PaymentAmount);
    });

    it("should adjust reservation RemainingPaymentsAmount when removing a payment", function () {
        //arrange
        var reservation = {
            AgencyTotalPrice: 700,
            TotalPaid: 700,
            TotalDays: 2,
            Discount: 0,
            RemainingPaymentsAmount: 0,
            Fee: 0
        };
        var payment = { PaymentAmount: 100, PaymentType: paymentEnumServ.paymentType.regular };

        //act
        handler.removePaymentFromReservation(reservation, payment);

        //assert
        expect(reservation.RemainingPaymentsAmount).toEqual(payment.PaymentAmount);
    });

    it("should adjust reservation status when removing a payment (partially paid)", function () {
        //arrange
        var reservation = {
            AgencyTotalPrice: 700,
            TotalPaid: 700,
            TotalDays: 2,
            Discount: 0,
            RemainingPaymentsAmount: 0,
            Fee: 0,
            ReservationStatus: resStatusEnumServ.status.confirmed | resStatusEnumServ.status.paid
        };
        var payment = { PaymentAmount: 100, PaymentType: paymentEnumServ.paymentType.regular };

        //act
        handler.removePaymentFromReservation(reservation, payment);

        //assert
        expect(reservation.ReservationStatus & resStatusEnumServ.status.partiallyPaid).toBe(resStatusEnumServ.status.partiallyPaid);
    });

    it("should adjust reservation status when removing a payment (noPaid)", function () {
        //arrange
        var reservation = {
            AgencyTotalPrice: 700,
            TotalPaid: 100,
            TotalDays: 2,
            Discount: 0,
            RemainingPaymentsAmount: 600,
            Fee: 0,
            ReservationStatus: resStatusEnumServ.status.confirmed | resStatusEnumServ.status.partiallyPaid
        };
        var payment = { PaymentAmount: 100, PaymentType: paymentEnumServ.paymentType.regular };

        //act
        handler.removePaymentFromReservation(reservation, payment);

        //assert
        expect(reservation.ReservationStatus & resStatusEnumServ.status.noPaid).toBe(resStatusEnumServ.status.noPaid);
    });

    describe("getReservationFinalPrice method", function(){
        it("should calculate reservation final price", function () {
            //arrange
            var reservation = {
                AgencyTotalPrice: 300,
                TotalDays: 3,
                Discount: 2,
                Fee: 0
            };

            var finalPrice = 306;

            //act
            var result = handler.getReservationFinalPrice(reservation);

            //assert
            expect(result).toEqual(finalPrice);
        });

        it("should assign reservation final price", function () {
            //arrange
            var reservation = {
                AgencyTotalPrice: 1500,
                TotalDays: 10,
                Discount: 1,
                Fee: 0
            };

            //act
            handler.getReservationFinalPrice(reservation);

            //assert
            expect(reservation.FinalPrice).toEqual(1510);
        });

        it("should assign reservation final price including Fee", function () {
            //arrange
            var reservation = {
                AgencyTotalPrice: 1500,
                TotalDays: 10,
                Discount: -1,
                Fee: 100
            };

            //act
            handler.getReservationFinalPrice(reservation);

            //assert
            expect(reservation.FinalPrice).toEqual(1590);
        });

        it("should recalculate what's left to pay", function(){
            //arrange
            var reservation = {
                AgencyTotalPrice: 1500,
                TotalDays: 10,
                Discount: -2,
                Fee: 150,
                TotalPaid: 500,
                RemainingPaymentsAmount: 0
            };

            //act
            handler.getReservationFinalPrice(reservation);

            //assert
            expect(reservation.RemainingPaymentsAmount).toBe(1130);
        });
    });

    it("should calculate reservation total days 1", function () {
        //arrange
        var reservation = {StartDate: new Date(2014, 4, 1), EndDate: new Date(2014, 4, 10)};

        //act
        handler.assignTotalDaysToReservation(reservation);

        //assert
        expect(reservation.TotalDays).toEqual(9);
    });

    it("should calculate reservation total days 2", function () {
        //arrange
        var reservation = {StartDate: new Date(2016, 0, 28), EndDate: new Date(2016, 1, 1)};

        //act
        handler.assignTotalDaysToReservation(reservation);

        //assert
        expect(reservation.TotalDays).toEqual(4);
    });

    it("should assign final price when calling getReadyToCreate methods", function () {
        //arrange
        var reservation = {
            AgencyTotalPrice: 1500,
            TotalDays: 10,
            Discount: -1,
            FinalPrice: 0,
            Fee: 0,
            StartDate: new Date(),
            EndDate: new Date()
        };

        //act
        handler.getReadyToCreate(reservation);

        //assert
        expect(reservation.FinalPrice).toEqual(1490);
    });

    it("should assign total day when calling getReadyToCreate methods", function () {
        //arrange
        var reservation = {
            SalesPrice: 1500,
            TotalDays: 10,
            Discount: 1,
            FinalPrice: 0,
            StartDate: new Date(2014, 4, 1),
            EndDate: new Date(2014, 4, 10)
        };

        //act
        var result = handler.getReadyToCreate(reservation);

        //assert
        expect(reservation.TotalDays).toEqual(9);
    });

    it("should assign totalPaid when calling getReadyToCreate methods", function () {
        //arrange
        var reservation = {
            SalesPrice: 1500,
            TotalDays: 10,
            Discount: 1,
            FinalPrice: 0,
            StartDate: new Date(2014, 4, 1),
            EndDate: new Date(2014, 4, 10)
        };

        //act
        var result = handler.getReadyToCreate(reservation);

        //assert
        expect(reservation.TotalPaid).toEqual(0);
    });

    it("should return true when calling method isCancelled sending a cancelled reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.cancelled};

        //act
        var result = handler.isCancelled(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should return true when calling method isPaid sending a paid reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.paid};

        //act
        var result = handler.isPaid(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should return true when calling method isPaid sending a paid and confirmed reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.paid | resStatusEnumServ.confirmed};

        //act
        var result = handler.isPaid(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should return true when calling method isPaid sending a paid and no confirmed reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.paid | resStatusEnumServ.noConfirmed};

        //act
        var result = handler.isPaid(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should return true when calling method isPartiallyPaid sending a partially paid reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.partiallyPaid};

        //act
        var result = handler.isPartiallyPaid(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should return true when calling method isPartiallyPaid sending a partially paid and confirmed reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.partiallyPaid | resStatusEnumServ.confirmed};

        //act
        var result = handler.isPartiallyPaid(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should return true when calling method isPartiallyPaid sending a partially paid and no confirmed reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.partiallyPaid | resStatusEnumServ.noConfirmed};

        //act
        var result = handler.isPartiallyPaid(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should return true when calling method isNotPaid sending a no paid reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.noPaid};

        //act
        var result = handler.isNotPaid(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should return true when calling method isNotPaid sending a no paid and confirmed reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.noPaid | resStatusEnumServ.confirmed};

        //act
        var result = handler.isNotPaid(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should return true when calling method isNotPaid sending a no paid and no confirmed reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.noPaid | resStatusEnumServ.noConfirmed};

        //act
        var result = handler.isNotPaid(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should return true when calling method isConfirmed sending a confirmed reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.confirmed};

        //act
        var result = handler.isConfirmed(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should return true when calling method isConfirmed sending a confirmed and paid reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.confirmed | resStatusEnumServ.status.paid};

        //act
        var result = handler.isConfirmed(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should return true when calling method isConfirmed sending a confirmed and partially paid reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.partiallyPaid | resStatusEnumServ.status.confirmed};

        //act
        var result = handler.isConfirmed(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should return true when calling method isConfirmed sending a confirmed and no paid reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.noPaid | resStatusEnumServ.status.confirmed};

        //act
        var result = handler.isConfirmed(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should return true when calling method isNotConfirmed sending a not confirmed reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.noConfirmed};

        //act
        var result = handler.isNotConfirmed(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should return true when calling method isNotConfirmed sending a not confirmed and paid reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.noConfirmed | resStatusEnumServ.status.paid};

        //act
        var result = handler.isNotConfirmed(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should return true when calling method isNotConfirmed sending a not confirmed and partially paid reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.partiallyPaid | resStatusEnumServ.status.noConfirmed};

        //act
        var result = handler.isNotConfirmed(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should return true when calling method isNotConfirmed sending a not confirmed and no paid reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.noPaid | resStatusEnumServ.status.noConfirmed};

        //act
        var result = handler.isNotConfirmed(reservation);

        //assert
        expect(result).toBe(true);
    });

    it("should mark reservation as confirmed when calling confirm method", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.noPaid | resStatusEnumServ.status.noConfirmed};

        //act
        handler.confirm(reservation);

        //assert
        expect(reservation.ReservationStatus & resStatusEnumServ.status.confirmed).toBe(resStatusEnumServ.status.confirmed);
        expect(reservation.ReservationStatus & resStatusEnumServ.status.noConfirmed).not.toBe(resStatusEnumServ.status.noConfirmed);
    });

    it("should mark reservation as unconfirmed when calling unconfirm method", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.noPaid | resStatusEnumServ.status.confirmed};

        //act
        handler.unconfirm(reservation);

        //assert
        expect(reservation.ReservationStatus & resStatusEnumServ.status.noConfirmed).toBe(resStatusEnumServ.status.noConfirmed);
        expect(reservation.ReservationStatus & resStatusEnumServ.status.confirmed).not.toBe(resStatusEnumServ.status.confirmed);
    });

    //it("should keep reservation payment status when calling confirm method", function(){
    //    //arrange
    //    var reservation = { ReservationStatus: resStatusEnumServ.status.noPaid | resStatusEnumServ.status.noConfirmed };
    //
    //    //act
    //    handler.confirm(reservation, true);
    //
    //    //assert
    //    expect((reservation.ReservationStatus & resStatusEnumServ.status.noPaid)).toBe(resStatusEnumServ.status.noPaid);
    //});

    it("should mark reservation as cancelled when calling toggleActiveCancelledStatus method on active reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.noPaid | resStatusEnumServ.status.noConfirmed};

        //act
        handler.toggleActiveCancelledStatus(reservation);

        //assert
        expect((reservation.ReservationStatus & resStatusEnumServ.status.cancelled)).toBe(resStatusEnumServ.status.cancelled);
    });

    it("should keep reservation status when calling toggleActiveCancelledStatus method on active reservation", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.noPaid | resStatusEnumServ.status.noConfirmed};

        //act
        handler.toggleActiveCancelledStatus(reservation);

        //assert
        expect((reservation.ReservationStatus & resStatusEnumServ.status.noPaid)).toBe(resStatusEnumServ.status.noPaid);
        expect((reservation.ReservationStatus & resStatusEnumServ.status.noConfirmed)).toBe(resStatusEnumServ.status.noConfirmed);
    });

    it("should mark reservation as active when calling toggleActiveCancelledStatus method on cancelled", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.noPaid | resStatusEnumServ.status.noConfirmed | resStatusEnumServ.status.cancelled};

        //act
        handler.toggleActiveCancelledStatus(reservation);

        //assert
        expect((reservation.ReservationStatus & resStatusEnumServ.status.cancelled)).not.toBe(resStatusEnumServ.status.cancelled);
    });

    it("should keep other reservation statuses when calling activate method", function () {
        //arrange
        var reservation = {ReservationStatus: resStatusEnumServ.status.noPaid | resStatusEnumServ.status.noConfirmed};

        //act
        handler.toggleActiveCancelledStatus(reservation);

        //assert
        expect((reservation.ReservationStatus & resStatusEnumServ.status.noPaid)).toBe(resStatusEnumServ.status.noPaid);
        expect((reservation.ReservationStatus & resStatusEnumServ.status.noConfirmed)).toBe(resStatusEnumServ.status.noConfirmed);
    });
});