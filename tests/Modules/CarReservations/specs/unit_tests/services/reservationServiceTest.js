﻿"use strict";

describe("Reservation Service", function () {
    var reservationService, deferred, rootScope, httpBackend, sessionMockSrvc;

    // load the controller's module
    beforeEach(function () {
        module("carReservationsApp", function ($provide) {
            sessionMockSrvc = {
                getHeaders: function () {
                    return { 'Authorization': "Bearer " + "Some Bearer" };
                }
            };

            $provide.value("Session", sessionMockSrvc);
        });
        inject(function (_reservationService_, _$q_, _$rootScope_, _$httpBackend_) {
            deferred = _$q_.defer();
            reservationService = _reservationService_;
            rootScope = _$rootScope_;
            httpBackend = _$httpBackend_;

            var reservations = [
            {
                ReservationId: "1",
                Customer: { FirstName: "Yorcelys", LastName: "Tchicamboud", CellPhone: "3054787854", EmailAddress: "b@b.com" },
                StartDate: "04/01/2014", PickupTime: "10:00", EndDate: "04/03/2014", TotalDays: 2,
                FlightNumber: "AA8899", ModifiedBy: "Vivian", CarProviderId: 3, TourOperatorId: 2,
                CarCategoryId: 1, Status: "Confirmed", Memo: "dkijopinf k,iondwe", PickUpProvinceId: 2,
                PickUpLocationId: 2, SalesPrice: 1520, Discount: 150, TotalPaid: 550, RemainingPaymentsAmount: 970,
                Payments: [
                    { Amount: 100, PaymentDate: "10 / 11 / 2014", PaymentMethod: 1, PaymentType: 1 },
                    { Amount: 250, PaymentDate: "10 / 11 / 2014", PaymentMethod: 2, PaymentType: 2 },
                    { Amount: 200, PaymentDate: "10 / 11 / 2014", PaymentMethod: 3, PaymentType: 2 }
                ]
            },
            {
                ReservationId: "2",
                Customer: { FirstName: "Barbara Nadia", LastName: "Gonzalez", CellPhone: "7869952204", EmailAddress: "c@c.com" },
                StartDate: "20/05/2014", PickupTime: "11:00", EndDate: "28/05/2014", TotalDays: 8,
                FlightNumber: "AA2098", ModifiedBy: "Vivian", CarProviderId: 1, TourOperatorId: 3,
                CarCategoryId: 2, Status: "Canceled", Memo: "djdop kddp siod", PickUpProvinceId: 3, PickUpLocationId: 3,
                SalesPrice: 1800, Discount: 200, TotalPaid: 550, RemainingPaymentsAmount: 1250,
                Payments: [
                    { Amount: 100, PaymentDate: "10 / 11 / 2014", PaymentMethod: 1, PaymentType: 1 },
                    { Amount: 250, PaymentDate: "10 / 11 / 2014", PaymentMethod: 2, PaymentType: 1 },
                    { Amount: 200, PaymentDate: "10 / 11 / 2014", PaymentMethod: 3, PaymentType: 2 }]
            }
            ];

            httpBackend.when("GET", "http://localhost:49269/api/Reservations/").respond(reservations);
        });
    });

    it("should exist", function () {
        expect(!!reservationService).toBe(true);
    });

    it("should create Session", function () {
        expect(sessionMockSrvc).toBeDefined();
    });

    it("should have a defined loadReservations Method", function () {
        expect(reservationService.loadReservations).toBeDefined();
    });

    it("should have a defined saveReservation Method", function () {
        expect(reservationService.saveReservation).toBeDefined();
    });

    it("should make a GET request to right url when calling loadReservations", function () {
        //act
        reservationService.loadReservations();
        //assert
        httpBackend.expect("GET", "http://localhost:49269/api/Reservations/");

    });

    it("should return a non-empty list of Reservations when calling loadReservations method", function () {
        //act
        var reservations = reservationService.loadReservations();
        //assert
        expect(reservations).not.toBeNull();

    });

    it("Method saveReservation should expect a reservation", function () {
        //arrange
        var reservationToSave = new Reservation();
        spyOn(reservationService, "saveReservation");

        //act
        reservationService.saveReservation(reservationToSave);

        //assert
        expect(reservationService.saveReservation).toHaveBeenCalled();
        expect(reservationService.saveReservation).toHaveBeenCalledWith(reservationToSave);

    });

    it("should have a defined addReservation Method", function () {
        expect(reservationService.addReservation).toBeDefined();
    });

    it("should make a POST request to right url when calling loadReservations", function () {
        //arrange
        var reservationToAdd = new Reservation();
        reservationToAdd.ReservationId = 1;

        //act
        reservationService.addReservation(reservationToAdd);

        //assert
        httpBackend.expect("POST", "http://localhost:49269/api/Reservations/");
    });

    it("should have a defined loadReservationById Method", function () {
        expect(reservationService.loadReservationById).toBeDefined();
    });

    it("should make a GET request to right url when calling loadReservationById passing reservationId as parameter", function () {
        //arrange
        var reservationId = 1;
        //act
        reservationService.loadReservationById(reservationId);
        //assert
        httpBackend.expect("GET", "http://localhost:49269/api/Reservations/" + reservationId);

    });

    it("should return a non-empty reservations when calling loadReservationById method", function () {
        //arrange
        var reservationId = 1;
        //act
        var reservation = reservationService.loadReservationById(reservationId);
       //assert
        expect(reservation).not.toBeNull();

    });

    it("should have define updateUrgentNotification method", function(){
       expect(reservationService.updateUrgentNotification).toBeDefined();
    });

    it("should make a GET request to right url when calling updateUrgentNotification", function () {
        //arrange
        var reservationId = 1;
        var value = false;
        httpBackend.expect("PUT", "http://localhost:49269/api/Reservations/" + reservationId + "/UrgentNotification/" + value);
        httpBackend.when("PUT", "http://localhost:49269/api/Reservations/" + reservationId + "/UrgentNotification/" + value).respond(null);

        //act
        reservationService.updateUrgentNotification(reservationId, value);
        httpBackend.flush();
    });

    it("should make a PUT request to right url when calling allowPrintVoucher", function(){
       // arrange
        var reservationId = 10;
        var value = false;
        httpBackend.expect("PUT", "http://localhost:49269/api/Reservations/" + reservationId + "/PrintVoucher/" + value);
        httpBackend.when("PUT", "http://localhost:49269/api/Reservations/" + reservationId + "/PrintVoucher/" + value).respond(null);

        //act
        reservationService.allowPrintVoucher(reservationId, value);
        httpBackend.flush();
    });
});